package bp.wf.template;
/** 
 方向与工作岗位对应属性	  
*/
public class DirectionStationAttr
{
	/** 
	 节点
	*/
	public static final String FK_Direction = "FK_Direction";
	/** 
	 工作岗位
	*/
	public static final String FK_Station = "FK_Station";
}