package bp.en;

import bp.da.*;
import bp.*;

/** 
 属性列表
*/
public class EntityOIDMyFileAttr extends EntityOIDAttr
{
	/** 
	 MyFileName
	*/
	public static final String MyFileName = "MyFileName";
	/** 
	 MyFilePath
	*/
	public static final String MyFilePath = "MyFilePath";
	/** 
	 MyFileExt
	*/
	public static final String MyFileExt = "MyFileExt";
}