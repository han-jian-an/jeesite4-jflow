package bp.app.sugon;

import bp.app.sugon.util.BaseDataUtil;
import bp.web.WebUser;
import bp.wf.Dev2Interface;
import bp.wf.FlowEventBase;

import java.util.Hashtable;

/**
 * @AUTHOR: YZY
 * @DATE: 20/8/18
 * @NOTE: 挂牌督办——苏州市级，上级流程
 **/
public class UPFlowGPDB extends FlowEventBase {
    @Override
    public String getFlowMark() {
        return "uPFlowGPDB";
    }

    public UPFlowGPDB(){}

    /**
     * 判断是否到截止时间
     * 当流程走到指定节点，判断是否到截止时间
     * 未到截止时间，计算出下一次发送给上报人的时间并保存到数据库
     * 到截止时间，则走向下一个节点
     * @return
     * @throws Exception
     */
    @Override
    public String WorkArrive() throws Exception {
        Object JDQF = this.getSysPara().GetValByKey("JDQF");
        if (null == JDQF) {
            return super.SendWhen();
        }
        if ("1".equals(JDQF.toString())) {
            long workID = this.getWorkID();
            int nodeID = this.HisNode.getNodeID();
            String loginUser = WebUser.getNo();
            Hashtable<String, Comparable> ht = BaseDataUtil.setFormData(this);
            String flowNo = this.HisNode.getFK_Flow();
            if (!BaseDataUtil.checkAddTask(this, flowNo, workID, nodeID-1, loginUser)) {
                Dev2Interface.Port_Login("admin");
                bp.wf.SendReturnObjs objs = Dev2Interface.Node_SendWork(flowNo, workID, ht, nodeID+1, "");
                Dev2Interface.Port_SigOut();
                Dev2Interface.Port_Login(loginUser);
                return objs.ToMsgOfHtml();
            }
        }
        return super.WorkArrive();
    }
}
