package bp.app.sugon;

import bp.app.sugon.util.BaseDataUtil;
import bp.da.DBAccess;
import bp.da.DataRow;
import bp.da.DataTable;
import bp.web.WebUser;
import bp.wf.Dev2Interface;
import bp.wf.FlowEventBase;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * @Author: HANSZ
 * @Date: 2020-07-31 11:16
 * @Description: 区镇委办局下发给网格
 */
public class ToGridLogic extends FlowEventBase {
    @Override
    public String getFlowMark() {
        return "toGridLogic";
    }

    /**
     * 发送前
     *
     * @throws Exception
     */
    @Override
    public String SendWhen() throws Exception {
        Object JDQF = this.getSysPara().GetValByKey("JDQF");
        if (null == JDQF) {
            return super.SendWhen();
        }
        if ("1".equals(JDQF.toString())) {
            //区镇选择网格下发
            Long workid = this.getWorkID();
            Long mainWorkId = BaseDataUtil.getMainworkId(workid);
            //获得选择的网格组织.
            String sql = "SELECT Tag1 as DeptNo, Tag2 as DeptName FROM Sys_Frmeledb WHERE EleID='QuZhenXiaWangGeJiGou' AND RefPKVal='" + workid + "'";
            DataTable dt = DBAccess.RunSQLReturnTable(sql);
            if (dt.Rows.size() == 0) {
                throw new Exception("请选择网格组织.");
            }
            sql = "select org_id from  dispatch_record where work_id=" + mainWorkId;
            DataTable dataTable = DBAccess.RunSQLReturnTable(sql);
            List<String> list = new ArrayList<>();
            if (dataTable.Rows.size() > 0) {
                dataTable.Rows.forEach(item -> {
                    list.add(item.getValue(0).toString());
                });
            }
            //组织数据，放入到子流程的开始节点表单上.
            Hashtable<String, Comparable> ht = BaseDataUtil.setFormData(this);
            String subprocessNum = this.GetValStr("ZLCBH");
            int subprocessNodeId = BaseDataUtil.getSubprocessNodeId(subprocessNum);
            //遍历选择的区镇.
            //遍历选择的网格组织.
            int ii = 0;
            for (DataRow dr : dt.Rows) {
                String deptNo = dr.getValue(0).toString(); //网格的部门编号.
                if (!list.contains(deptNo)) {
                    //求出来502节点的处理人员(分支结构的人员), 根据当前操作员的部门部门编号.WebUser.FK_Dept
                    String sqlqu = "select pe.no as No, pe.Name from port_emp pe where pe.FK_Dept=" + deptNo;
                    DataTable dtemp = DBAccess.RunSQLReturnTable(sqlqu);
                    StringBuilder toEmps = new StringBuilder();
                    if (dtemp.Rows.size() == 0) {
                        if (ii == dt.Rows.size() - 1) {
                            throw new Exception("该网格组织下没有对应人员.");
                        } else {
                            ii++;
                            continue;
                        }
                    }
                    for (int i = 0; i < dtemp.Rows.size(); i++) {
                        DataRow dremo = dtemp.Rows.get(i);
                        toEmps.append(",").append(dremo.getValue("No"));
                    }
                    //创建一个空白的sub工作.
                    long workidSubFlow = Dev2Interface.Node_CreateBlankWork(subprocessNum, WebUser.getNo());

                    //计算出来区的接受人.
                    Dev2Interface.Node_SendWork(subprocessNum, workidSubFlow, ht, subprocessNodeId, toEmps.substring(1));

                    //创建设置父子关系.
                    Dev2Interface.SetParentInfo(subprocessNum, workidSubFlow, workid);

                    sql = "insert into dispatch_record (org_id, work_id, create_time) values (" + deptNo + "," + mainWorkId + ", now())";
                    DBAccess.RunSQL(sql);
                }
            }
        }
        // end for.
        this.setJumpToNodeID(0);
        this.setJumpToEmps("");
        return super.SendWhen();
    }

    /**
     * 发送成功时
     */
    @Override
    public String SendSuccess() throws Exception {
        return super.SendSuccess();

    }

    /**
     * 工作到达的时候
     *
     * @return
     */
    @Override
    public String WorkArrive() throws Exception {
        return super.WorkArrive();
    }

    
}
