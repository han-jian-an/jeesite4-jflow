package bp.app.sugon;

import java.util.Hashtable;
import bp.da.*;
import bp.difference.SystemConfig;
import bp.difference.handler.WebContralBase;
import bp.sys.*;
import bp.web.*;
import bp.port.*;
import bp.en.*;
import bp.wf.*;
import bp.wf.template.*;

//C# TO JAVA CONVERTER TODO TASK: C# to Java Converter could not confirm whether this is a namespace alias or a type alias:
//using Flow = BP.DOC.WD.Flow;

/**
 * 页面功能实体
 */
public class App_Page extends WebContralBase {
	
	
	
	/**
	 * 构造函数
	 */
	public App_Page() {
		
	}

	/**
	 * 单点登录.
	 * 
	 * @return
	 * @throws Exception 
	 */
	public final String Port_Init() throws Exception {
		
		
		String token = null;

		if (token == null) {
			token = this.GetRequestVal("myToken");
		}

		String host = SystemConfig.GetValByKey("TokenHost", "zzz");
		String url = host + token;
		String data = bp.da.DataType.ReadURLContext(url, 5000);

		if (DataType.IsNullOrEmpty(data) == true) {
			return "err@token失效，请重新登录。" + url;
		}

		Emp emp = new Emp();
		emp.setNo( data);
		if (emp.RetrieveFromDBSources() == 0) {
			return "err@根据token获取用户名错误:" + token + ",获取数据为:" + data;
		}

		// 执行登录.
		bp.wf.Dev2Interface.Port_Login(data);
		WebUser.setToken( token);
		WebUser.setSID(token);
		

		DBAccess.RunSQL("UPDATE WF_Emp SET Token='" + token + "'  WHERE No='" + emp.getNo() + "'");

		return "info@登录成功.";
	}
	/**
	 * 按周启动子流程
	 * 
	 * @return
	 * @throws Exception 
	 */
	public final String DTSPage_Init() throws Exception {
		
		//让admin登录.
		Dev2Interface.Port_Login("admin");
		 
		//获得停留区镇上的，分支机构用户.
        String sql = "SELECT WorkID,FK_Dept FROM WF_GenerWorkFlow WHERE FK_Node=202";
        DataTable dt = DBAccess.RunSQLReturnTable(sql);

        //当前的第几周.
        int currWeekNum = DataType.getCurrentWeek();
        String nianDu = DataType.getCurrentYear(); //当前年度 2020.
        for (DataRow dr : dt.Rows)
        {
            //获得 workid.
        	long workid =Long.parseLong(dr.getValue(0).toString());

            //判断本周是否发起过？ 
            sql = "SELECT * FROM WF_GenerWorkFlow WHERE PWorkID=" + workid + "  ";
            sql += " AND WeekNum=" + currWeekNum; //第几周.
            sql += " AND RDT like '" + nianDu + "%'"; //本年度的.
            DataTable mydt = DBAccess.RunSQLReturnTable(sql);
            if (mydt.Rows.size() != 0)
                continue; //说明已经启动过了.

            //1 没有启动过，就执行启动子流程.

            //1.1 准备数据 - 要传递的父流程数据到子流程上.
            sql = "SELECT * FROM ND2Rpt WHERE OID="+ workid;
            DataTable dtRpt = DBAccess.RunSQLReturnTable(sql);

            Hashtable ht = new Hashtable(); //把父流程的数据传递的子流程上去.
            ht.put("BiaoTi", dtRpt.Rows.get(0).getValue("Biaoti").toString());

            //1.2 计算要发给的片管员.
            String toEmps = "zhangsan,lisi,wangwu,";
            
            //1.3 计算出来，区镇 工作人员
            sql="SELECT FK_Emp FROM WF_GenerWorkerList WHERE WorkID="+workid+" AND FK_Node=202";
            String quZhenEmp = DBAccess.RunSQLReturnStringIsNull(sql, "");
            
            //1。4 让当事人登录.
            Dev2Interface.Port_Login(quZhenEmp);
            

            //1.5 创建workid子流程的.
            long workIDSubFlow = Dev2Interface.Node_CreateBlankWork("003", WebUser.getNo());

            //1.6 执行发送.
            Dev2Interface.Node_SendWork("003", workIDSubFlow, ht, 302, toEmps);
            

            //1.7 设置父子流程关系.
            Dev2Interface.SetParentInfo("003", workIDSubFlow, workid ); 
        }

     
		 
		return "执行成功.";
	} 

}
