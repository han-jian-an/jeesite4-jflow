package bp.app.sugon;

import bp.app.sugon.util.BaseDataUtil;
import bp.da.DBAccess;
import bp.da.DataRow;
import bp.da.DataTable;
import bp.web.WebUser;
import bp.wf.Dev2Interface;
import bp.wf.FlowEventBase;

import java.util.Hashtable;

/**
 * @Author: HANSZ
 * @Date: 2020-07-30 14:40
 * @Description: 给区镇安委办或者区镇委办局下发任务(302 ， 702)
 */
public class ToTownLogic extends FlowEventBase {
    @Override
    public String getFlowMark() {
        return "toTownLogic";
    }

    /**
     * 发送前
     *
     * @throws Exception
     */
    @Override
    public String SendWhen() throws Exception {
        Object JDQF = this.getSysPara().GetValByKey("JDQF");
        if (null == JDQF) {
            return super.SendWhen();
        }
        if ("1".equals(JDQF.toString())) {
            long workid = this.getWorkID();
            //获得选择的乡镇.007与003表单中选择项目名字：QuZhenWeiBanJu
            String sql = "SELECT Tag1 as DeptNo, Tag2 as DeptName FROM Sys_Frmeledb WHERE EleID='QuZhenWeiBanJu' AND RefPKVal='" + workid + "'";
            DataTable dt = DBAccess.RunSQLReturnTable(sql);
            if (dt.Rows.size() == 0) {
                throw new Exception("请选择区镇.");
            }

            // GenerWorkFlow gwf = new GenerWorkFlow(this.getWorkID() );
            //组织数据，放入到子流程的开始节点表单上.
            Hashtable<String, Comparable> ht = BaseDataUtil.setFormData(this);
            String subprocessNum = this.GetValStr("ZLCBH");
            int subprocessNodeId = BaseDataUtil.getSubprocessNodeId(subprocessNum);
            //遍历选择的区镇.
            int ii = 0;
            for (DataRow dr : dt.Rows) {
                String deptNo = dr.getValue(0).toString(); //乡镇的部门编号.
                String deptName = dr.getValue(1).toString(); //乡镇的部门名称.
                ht.put("XingYeID", deptNo);//赋值给子流程的父ID
                // 获取接收人
                String sqlqu = "select pe.no as No, pe.Name from port_emp pe where pe.FK_Dept=" + deptNo;
                DataTable dtemp = DBAccess.RunSQLReturnTable(sqlqu);
                StringBuilder toEmps = new StringBuilder();
                if (dtemp.Rows.size() == 0) {
                    if (ii == dt.Rows.size() - 1) {
                        throw new Exception("该行业或区镇下没有对应人员.");
                    } else {
                        ii++;
                        continue;
                    }
                }
                for (int i = 0; i < dtemp.Rows.size(); i++) {
                    DataRow dremo = dtemp.Rows.get(i);
                    toEmps.append(",").append(dremo.getValue("No"));
                }
                //创建一个空白的sub工作.
                long workidSubFlow = Dev2Interface.Node_CreateBlankWork(subprocessNum, WebUser.getNo());
                //创建设置父子关系.
                Dev2Interface.SetParentInfo(subprocessNum, workidSubFlow, workid);

                //计算出来区的接受人.
                Dev2Interface.Node_SendWork(subprocessNum, workidSubFlow, ht, subprocessNodeId, toEmps.substring(1));

            }// end for.
            this.setJumpToNodeID(0);
            this.setJumpToEmps("");
        }
        return super.SendWhen();
    }

    /**
     * 发送成功时
     */
    @Override
    public String SendSuccess() throws Exception {
        return super.SendSuccess();
    }

    /**
     * 工作到达的时候
     *
     * @return
     */
    @Override
    public String WorkArrive() throws Exception {
        return super.WorkArrive();
    }

   

}
