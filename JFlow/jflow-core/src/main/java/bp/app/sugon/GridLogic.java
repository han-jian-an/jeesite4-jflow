package bp.app.sugon;

import bp.app.sugon.entity.FlowTask;
import bp.app.sugon.util.BaseDataUtil;
import bp.da.DBAccess;
import bp.da.DataRow;
import bp.da.DataTable;
import bp.tools.DateUtils;
import bp.web.WebUser;
import bp.wf.Dev2Interface;
import bp.wf.FlowEventBase;
import bp.wf.template.Task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.chrono.ChronoLocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.Date;
import java.util.Hashtable;

/**
 * @Author: HANSZ
 * @Date: 2020-07-31 15:06
 * @Description: 网格自己的处理逻辑
 */
public class GridLogic extends FlowEventBase {
    @Override
    public String getFlowMark() {
        return "gridLogic";
    }

    /**
     * 发送前
     *
     * @throws Exception
     */
    @Override
    public String SendWhen() throws Exception {
//        Object JDQF = this.getSysPara().GetValByKey("JDQF");
//        if (null == JDQF) {
//            return super.SendWhen();
//        }
//        if ("1".equals(JDQF.toString())) {
//            int nodeID = this.HisNode.getNodeID();
////            Hashtable<String, Comparable> ht = BaseDataUtil.setFormData(this);
//            this.setJumpToNodeID(++nodeID);
//            this.setJumpToEmps("");
//        }
        return super.SendWhen();

    }

    /**
     * 发送成功时
     */
    @Override
    public String SendSuccess() throws Exception {
        return super.SendSuccess();
    }

    /**
     * 工作到达的时候
     *
     * @return
     */
    @Override
    public String WorkArrive() throws Exception {
        Object JDQF = this.getSysPara().GetValByKey("JDQF");
        if (null == JDQF) {
            return super.SendWhen();
        }
        if ("1".equals(JDQF.toString())) {
            long workID = this.getWorkID();
            int nodeID = this.HisNode.getNodeID();
            String loginUser = WebUser.getNo();
            Hashtable<String, Comparable> ht = BaseDataUtil.setFormData(this);
            String flowNo = this.HisNode.getFK_Flow();
            if (!BaseDataUtil.checkAddTask(this, flowNo, workID, nodeID - 1, loginUser)) {
                Dev2Interface.Port_Login("admin");
                bp.wf.SendReturnObjs objs = Dev2Interface.Node_SendWork(flowNo, workID, ht, nodeID + 1, "");
                Dev2Interface.Port_SigOut();
                Dev2Interface.Port_Login(loginUser);
                return objs.ToMsgOfHtml();
            }
        }
        return super.WorkArrive();
    }

    

}
