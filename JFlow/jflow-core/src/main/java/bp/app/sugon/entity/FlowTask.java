package bp.app.sugon.entity;

import java.util.Date;

/**
 * @Author: HANSZ
 * @Date: 2020-08-13 21:24
 * @Description:
 */
public class FlowTask {
    private Long id;
    private Long workId;
    private Long mainWorkId;
    private String userId;
    private String flowNo;
    private Integer toNodeId;
    private Integer status;
    private Date createTime;
    private Date issuedTime;
    private Date finishTime;
    private String expan1;
    private String expan2;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWorkId() {
        return workId;
    }

    public void setWorkId(Long workId) {
        this.workId = workId;
    }

    public Long getMainWorkId() {
        return mainWorkId;
    }

    public void setMainWorkId(Long mainWorkId) {
        this.mainWorkId = mainWorkId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFlowNo() {
        return flowNo;
    }

    public void setFlowNo(String flowNo) {
        this.flowNo = flowNo;
    }

    public Integer getToNodeId() {
        return toNodeId;
    }

    public void setToNodeId(Integer toNodeId) {
        this.toNodeId = toNodeId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getIssuedTime() {
        return issuedTime;
    }

    public void setIssuedTime(Date issuedTime) {
        this.issuedTime = issuedTime;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getExpan1() {
        return expan1;
    }

    public void setExpan1(String expan1) {
        this.expan1 = expan1;
    }

    public String getExpan2() {
        return expan2;
    }

    public void setExpan2(String expan2) {
        this.expan2 = expan2;
    }
}
