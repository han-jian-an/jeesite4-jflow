package bp.app.sugon;

import bp.app.sugon.util.BaseDataUtil;
import bp.da.DBAccess;
import bp.da.DataRow;
import bp.da.DataTable;
import bp.web.WebUser;
import bp.wf.Dev2Interface;
import bp.wf.FlowEventBase;

import java.util.Hashtable;

/**
 * 此类库必须放入到 BP.*.dll 才能被解析发射出来。
 */
// 市安委办分配任务
public class F001 extends FlowEventBase {
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region 属性.

    /**
     * 重写流程标记，和流程属性中的流程标记对应
     */
    @Override
    public String getFlowMark() {
        return "001";
    }

    public F001() {
    }

    /**
     * 重写发送前事件
     *
     * @return
     * @throws Exception
     */
    @Override
    public String SendWhen() throws Exception {
        int nodeID = this.HisNode.getNodeID();
        
       /// bp.web.WebUser.getFK_DeptNameOfFull()
       
//        String flowNo = this.HisNode.getFK_Flow(); // 流程编号.
        //102节点中定义的区分节点的字段
        //市安委办下发
        //if (nodeID == 102) {

        //节点区分
        Object JDQF = this.getSysPara().GetValByKey("JDQF");
        if (null == JDQF) {
            return super.SendWhen();
        }
        if ("1".equals(JDQF.toString())) {
            Long workid = this.getWorkID();
            // 事件类型
            int sjlx = this.GetValInt("SJLX");
            String sql;
            String subprocessNum;
            if (sjlx == 0) {
                //行业
                sql = "select distinct(org_id),org_name,industry_id from port_industry where parent_id = 1 and industry_id in (select tag1 from sys_frmeledb where FK_MapData='ND" + nodeID + "' and refpkval='" + this.getWorkID() + "')";
                subprocessNum = this.GetValStr("XYZLCBH");
            } else {
                //区镇
				/*sql = "select distinct(id), name,parent_id FROM port_town where type = 1 and "+
						" parent_id  in (select tag1 from sys_frmeledb where FK_MapData='ND"+nodeID+"' and refpkval='"+this.getWorkID()+"')";*/
                sql = "select  No,	NAME, No FROM port_dept where no in (select tag1 from sys_frmeledb where FK_MapData='ND" + nodeID + "' and refpkval='" + this.getWorkID() + "')";
                subprocessNum = this.GetValStr("QZZLCBH");
            }

            DataTable dt = DBAccess.RunSQLReturnTable(sql);
            if (dt.Rows.size() == 0) {
                throw new Exception("请选择行业或区镇.");
            }
            int subprocessNodeId = BaseDataUtil.getSubprocessNodeId(subprocessNum);
            //组织数据，放入到子流程的开始节点表单上.
            Hashtable<String, Comparable> ht = BaseDataUtil.setFormData(this);

            //遍历取得的组织.
            int ii = 0;
            for (DataRow dr : dt.Rows) {
                String deptNo = dr.getValue(0).toString(); //部门编号.
                String deptName = dr.getValue(1).toString(); //部门名称.
                String industryId = dr.getValue(2).toString(); //行业ID或者是区镇组织的ID
                ht.put("XingYeID", industryId);//赋值给子流程的父ID

                //根据选择的行业或区镇的部门部门编号.WebUser.FK_Dept
                String sqlqu = "select pe.no as No, pe.Name from port_emp pe where pe.FK_Dept=" + deptNo;
                DataTable dtemp = DBAccess.RunSQLReturnTable(sqlqu);
                String toEmps = "";
                if (dtemp.Rows.size() == 0) {

                    if (ii == dt.Rows.size() - 1) {
                        throw new Exception("该行业或区镇下没有对应人员.");
                    } else {
                        ii++;
                        continue;
                    }
                }
                for (int i = 0; i < dtemp.Rows.size(); i++) {
                    DataRow dremo = dtemp.Rows.get(i);
                    toEmps += "," + dremo.getValue("No");
                }
                long workidSubFlow = Dev2Interface.Node_CreateBlankWork(subprocessNum, WebUser.getNo());
                //计算出来区的接受人.
                Dev2Interface.Node_SendWork(subprocessNum, workidSubFlow, ht, subprocessNodeId, toEmps.substring(1));
                //创建设置父子关系.
                Dev2Interface.SetParentInfo(subprocessNum, workidSubFlow, workid);

            }// end for.
            //主流跳转节点、接收人
            this.setJumpToNodeID(0);
            this.setJumpToEmps("");
        }
        return super.SendSuccess();
    }

    /**
     * 发送成功事件，发送成功时，把流程的待办写入其他系统里.
     *
     * @return 返回执行结果，如果返回null就不提示。
     * @throws Exception
     */
    @Override
    public String SendSuccess() throws Exception {
        return super.SendSuccess();
    }

    
    /**
     * 工作到达的时候
     *
     * @return
     */
    @Override
    public String WorkArrive() throws Exception {
        return super.WorkArrive();
    }

}
