package bp.app.sugon;
import bp.en.*;
import java.util.*;

/** 
 时效考核s
*/
public class QuZhens extends EntitiesNoName
{
	private static final long serialVersionUID = 1L;
		///构造方法属性
	/** 
	 时效考核s
	*/
	public QuZhens()
	{
		
	}
	 
		///属性
	/** 
	 时效考核
	*/
	@Override
	public Entity getGetNewEntity()
	{
		return new QuZhen();
	}

		///


		///为了适应自动翻译成java的需要,把实体转换成List.
	/** 
	 转化成 java list,C#不能调用.
	 
	 @return List
	*/
	public final List<QuZhen> ToJavaList()
	{
		return (List<QuZhen>)(Object)this;
	}
	/** 
	 转化成list
	 
	 @return List
	*/
	public final ArrayList<QuZhen> Tolist()
	{
		ArrayList<QuZhen> list = new ArrayList<QuZhen>();
		for (int i = 0; i < this.size(); i++)
		{
			list.add((QuZhen)this.get(i));
		}
		return list;
	}

		/// 为了适应自动翻译成java的需要,把实体转换成List.
}