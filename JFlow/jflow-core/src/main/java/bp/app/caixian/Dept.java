package bp.app.caixian;

import bp.app.port.DeptAttr;
import bp.app.port.Depts;
import bp.da.DBAccess;
import bp.da.DataTable;
import bp.en.EntityNoName;
import bp.en.Map;
import bp.en.RefMethod;
import bp.en.RefMethodType;
import bp.en.UAC;

/** 
 部门
*/
public class Dept extends EntityNoName
{
	private static final long serialVersionUID = 1L;
	///属性
	/** 
	 父节点的ID
	*/
	public final String getParentNo()throws Exception
	{
		return this.GetValStrByKey(DeptAttr.ParentNo);
	}
	public final void setParentNo(String value) throws Exception
	{
		this.SetValByKey(DeptAttr.ParentNo, value);
	}

	public final void setLeader(String value)throws Exception{
		this.SetValByKey(DeptAttr.Leader,value);
	}
	public final void setNameOfPath(String value)throws Exception{
		this.SetValByKey(DeptAttr.NameOfPath,value);
	}
	public final void setDeptType(int value)throws Exception{
		this.SetValByKey(DeptAttr.DeptType,value);
	}

	

 	public final int getGrade()
	{
		return 1;
	}
	private Depts _HisSubDepts = null;
	/** 
	 它的子节点
	 * @throws Exception 
	*/
	public final Depts getHisSubDepts() throws Exception
	{
		if (_HisSubDepts == null)
		{
			_HisSubDepts = new Depts(this.getNo());
		}
		return _HisSubDepts;
	}

		///


		///构造函数
	/** 
	 部门
	*/
	public Dept()
	{
	}
	/** 
	 部门
	 
	 @param no 编号
	 * @throws Exception 
	*/
	public Dept(String no) throws Exception
	{
		super(no);
	}

		///


		///重写方法
	@Override
	public UAC getHisUAC() throws Exception
	{
		UAC uac = new UAC();
		uac.OpenForSysAdmin();
		return uac;
	}
	/** 
	 Map
	*/
	@Override
	public Map getEnMap() throws Exception
	{
		if (this.get_enMap() != null)
		{
			return this.get_enMap();
		}

		Map map = new Map("Port_Dept", "部门");
		map.IsEnableVer = true;

		map.AddTBStringPK(DeptAttr.No, null, "编号", true, false, 1, 50, 20);
		map.AddTBString(DeptAttr.Name, null, "名称", true, false, 0, 100, 30);
		map.AddTBString(DeptAttr.ParentNo, null, "父节点编号", true, true, 0, 100, 30);
		map.AddTBString(DeptAttr.OrgNo, null, "OrgNo", true, true, 0, 50, 30);
		map.AddTBString(DeptAttr.Leader, null, "部门领导", true, true, 0, 50, 30);
		
		map.AddTBString(bp.gpm.DeptAttr.NameOfPath, null, "部门路径", true, true, 0, 300, 30, true);
		map.AddTBInt(DeptAttr.DeptType,0,"部门类型",false,true);
		map.AddTBInt(DeptAttr.Idx, 0, "序号", false, true);


		RefMethod rm = new RefMethod();
		rm.Title = "历史变更";
		rm.ClassMethodName = this.toString() + ".History";
		rm.refMethodType = RefMethodType.RightFrameOpen;
		map.AddRefMethod(rm);


			///增加点对多属性
			//他的部门权限
		   // map.getAttrsOfOneVSM().Add(new DeptStations(), new Stations(), DeptStationAttr.FK_Dept, DeptStationAttr.FK_Station, StationAttr.Name, StationAttr.No, "岗位权限");

			///

		this.set_enMap(map);
		return this.get_enMap();
	}

		///

	public final String History()throws Exception
	{
		return "EnVerDtl.htm?EnName=" + this.toString() + "&PK=" + this.getNo();
	}


		///重写查询. 2015.09.31 为适应ws的查询.
	/** 
	 查询
	 
	 @return 
	 * @throws Exception 
	*/
	@Override
	public int Retrieve() throws Exception
	{

			return super.Retrieve();

	}
	/** 
	 查询.
	 
	 @return 
	 * @throws Exception 
	*/
	@Override
	public int RetrieveFromDBSources() throws Exception
	{

			return super.RetrieveFromDBSources();

	}
	/** 
	 检查名称的问题.
	 
	 @return 
	 * @throws Exception 
	*/
	@Override
	protected boolean beforeInsert() throws Exception
	{

        String sql = "SELECT * FROM  cd_com_all WHERE comCode='"+this.getParentNo()+"'";
        DataTable dt = DBAccess.RunSQLReturnTable(sql);
        int ComLevle = Integer.valueOf(dt.Rows.get(0).get("COMLEVEL").toString())+1;

        String level1ComCode = (String)dt.Rows.get(0).get("LEVEL1COMCODE");
        String level2ComCode = (String)dt.Rows.get(0).get("LEVEL2COMCODE");
        String level3ComCode = (String)dt.Rows.get(0).get("LEVEL3COMCODE");
        String level4ComCode = (String)dt.Rows.get(0).get("LEVEL4COMCODE");
        String level5ComCode = (String)dt.Rows.get(0).get("LEVEL5COMCODE");
        String level1ComDesc = (String) dt.Rows.get(0).get("LEVEL1COMDESC");
        String level2ComDesc = (String) dt.Rows.get(0).get("LEVEL2COMDESC");
        String level3ComDesc = (String) dt.Rows.get(0).get("LEVEL3COMDESC");
        String level4ComDesc = (String) dt.Rows.get(0).get("LEVEL4COMDESC");
        String level5ComDesc = (String) dt.Rows.get(0).get("LEVEL5COMDESC");
        
        sql = " INSERT INTO cd_com_all (COMCODE,COMNAME,"
        		+ "Level1ComCode,Level2ComCode,Level3ComCode,Level4ComCode,Level5ComCode,"
        		+ "LEVEL1COMDESC,LEVEL2COMDESC,LEVEL3COMDESC,LEVEL4COMDESC,LEVEL5COMDESC,";
        sql += " COMLEVEL,"; //级别
        sql += " IMPORTSOURCE)"; //来源
        sql += " VALUES ('"+this.getNo()+"','"+this.getName()+"','"+ level1ComCode + "','"
              + level2ComCode + "','"+ level3ComCode + "','"+ level4ComCode + "','"
        	  + level5ComCode + "','"+ level1ComDesc + "','"+ level2ComDesc + "','"
              + level3ComDesc + "','"+ level4ComDesc + "','"+ level5ComDesc + "',"
              + ComLevle + "," //级别.
              + "'JFlow') "; //来源
        DBAccess.RunSQL(sql);
		return false;
	}
	@Override
	protected boolean beforeUpdate() throws Exception
	{
		String sql ="update cd_com_all set name ='"+this.getName()+"' where COMCODE = "+this.getNo();
		DBAccess.RunSQL(sql);
		return false;
	}

		///

}