package bp.app.port;
import bp.en.EntityNoNameAttr;
/** 
 部门属性
*/
public class DeptAttr extends EntityNoNameAttr
{
	/** 
	 父节点的编号
	*/
	public static final String ParentNo = "ParentNo";
	public static final String Idx = "Idx";
	public static final String OrgNo = "OrgNo";
	public static final String Leader = "Leader";
	public static final String NameOfPath="NameOfPath";
	public static final String DeptType="DeptType";
	public static final String OrgCode="OrgCode";
}