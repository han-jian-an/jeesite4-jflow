/*
Navicat MySQL Data Transfer

Source Server         : 本地mysql
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : jeesite_jflow

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-05-06 16:20:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cn_city
-- ----------------------------
DROP TABLE IF EXISTS `cn_city`;
CREATE TABLE `cn_city` (
  `No` varchar(50) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Names` varchar(50) DEFAULT NULL COMMENT '小名',
  `Grade` int(11) DEFAULT '0' COMMENT 'Grade',
  `FK_SF` varchar(100) DEFAULT NULL COMMENT '省份',
  `FK_PQ` varchar(100) DEFAULT NULL COMMENT '片区',
  `PinYin` varchar(200) DEFAULT NULL COMMENT '搜索拼音',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cn_city
-- ----------------------------
INSERT INTO `cn_city` VALUES ('1101', '北京市东城区', '东城', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1102', '北京市西城区', '西城', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1103', '北京市崇文区', '崇文', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1104', '北京市宣武区', '宣武', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1105', '北京市朝阳区', '朝阳', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1106', '北京市丰台区', '丰台', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1107', '北京市石景山区', '石景山', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1108', '北京市海淀区', '海淀', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1109', '北京市门头沟区', '门头沟', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1111', '北京市房山区', '房山', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1112', '北京市通州区', '通州', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1113', '北京市顺义区', '顺义', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1114', '北京市昌平区', '昌平', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1115', '北京市大兴区', '大兴', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1116', '北京市怀柔区', '怀柔', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1117', '北京市平谷区', '平谷', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1128', '北京市密云县', '密云', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1129', '北京市延庆县', '延庆', '2', '11', 'AA', null);
INSERT INTO `cn_city` VALUES ('1201', '天津市和平区', '和平区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1202', '天津市河东区', '河东区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1203', '天津市河西区', '河西区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1204', '天津市南开区', '南开区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1205', '天津市河北区', '河北区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1206', '天津市红桥区', '红桥区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1207', '天津市塘沽区', '塘沽区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1208', '天津市汉沽区', '汉沽区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1209', '天津市大港区', '大港区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1210', '天津市东丽区', '东丽区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1211', '天津市西青区', '西青区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1212', '天津市津南区', '津南区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1213', '天津市北辰区', '北辰区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1214', '天津市武清区', '武清区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1215', '天津市宝坻区', '宝坻区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1221', '天津市宁河县', '宁河区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1223', '天津市静海县', '静海区', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1225', '天津市蓟县', '蓟县', '2', '12', 'AA', null);
INSERT INTO `cn_city` VALUES ('1301', '河北省石家庄市', '石家庄', '2', '13', 'HB', null);
INSERT INTO `cn_city` VALUES ('1302', '河北省唐山市', '唐山', '2', '13', 'HB', null);
INSERT INTO `cn_city` VALUES ('1303', '河北省秦皇岛市', '秦皇岛', '2', '13', 'HB', null);
INSERT INTO `cn_city` VALUES ('1304', '河北省邯郸市', '邯郸', '2', '13', 'HB', null);
INSERT INTO `cn_city` VALUES ('1305', '河北省邢台市', '邢台', '2', '13', 'HB', null);
INSERT INTO `cn_city` VALUES ('1306', '河北省保定市', '保定', '2', '13', 'HB', null);
INSERT INTO `cn_city` VALUES ('1307', '河北省张家口市', '张家口', '2', '13', 'HB', null);
INSERT INTO `cn_city` VALUES ('1308', '河北省承德市', '承德', '2', '13', 'HB', null);
INSERT INTO `cn_city` VALUES ('1309', '河北省沧州市', '沧州', '2', '13', 'HB', null);
INSERT INTO `cn_city` VALUES ('1310', '河北省廊坊市', '廊坊', '2', '13', 'HB', null);
INSERT INTO `cn_city` VALUES ('1311', '河北省衡水市', '衡水', '2', '13', 'HB', null);
INSERT INTO `cn_city` VALUES ('1401', '山西省太原市', '太原', '2', '14', 'HB', null);
INSERT INTO `cn_city` VALUES ('1402', '山西省大同市', '大同', '2', '14', 'HB', null);
INSERT INTO `cn_city` VALUES ('1403', '山西省阳泉市', '阳泉', '2', '14', 'HB', null);
INSERT INTO `cn_city` VALUES ('1404', '山西省长治市', '长治', '2', '14', 'HB', null);
INSERT INTO `cn_city` VALUES ('1405', '山西省晋城市', '晋城', '2', '14', 'HB', null);
INSERT INTO `cn_city` VALUES ('1406', '山西省朔州市', '朔州', '2', '14', 'HB', null);
INSERT INTO `cn_city` VALUES ('1407', '山西省晋中市', '晋中', '2', '14', 'HB', null);
INSERT INTO `cn_city` VALUES ('1408', '山西省运城市', '运城', '2', '14', 'HB', null);
INSERT INTO `cn_city` VALUES ('1409', '山西省忻州市', '忻州', '2', '14', 'HB', null);
INSERT INTO `cn_city` VALUES ('1410', '山西省临汾市', '临汾', '2', '14', 'HB', null);
INSERT INTO `cn_city` VALUES ('1411', '山西省吕梁市', '吕梁', '2', '14', 'HB', null);
INSERT INTO `cn_city` VALUES ('1412', '山西省临汾市尧都区', '临汾尧都区', '2', '14', 'HB', null);
INSERT INTO `cn_city` VALUES ('1501', '内蒙古呼和浩特市', '呼和浩特', '2', '15', 'HB', null);
INSERT INTO `cn_city` VALUES ('1502', '内蒙古包头市', '包头', '2', '15', 'HB', null);
INSERT INTO `cn_city` VALUES ('1503', '内蒙古乌海市', '乌海', '2', '15', 'HB', null);
INSERT INTO `cn_city` VALUES ('1504', '内蒙古赤峰市', '赤峰', '2', '15', 'HB', null);
INSERT INTO `cn_city` VALUES ('1505', '内蒙古通辽市', '通辽', '2', '15', 'HB', null);
INSERT INTO `cn_city` VALUES ('1506', '内蒙古鄂尔多斯市', '鄂尔多斯', '2', '15', 'HB', null);
INSERT INTO `cn_city` VALUES ('1507', '内蒙古呼伦贝尔市', '呼伦贝尔', '2', '15', 'HB', null);
INSERT INTO `cn_city` VALUES ('1508', '内蒙古巴彦淖尔市', '巴彦淖尔', '2', '15', 'HB', null);
INSERT INTO `cn_city` VALUES ('1509', '内蒙古乌兰察布市', '乌兰察布', '2', '15', 'HB', null);
INSERT INTO `cn_city` VALUES ('1522', '内蒙古兴安盟', '兴安盟', '2', '15', 'HB', null);
INSERT INTO `cn_city` VALUES ('1525', '内蒙古锡林郭勒盟', '锡林郭勒盟', '2', '15', 'HB', null);
INSERT INTO `cn_city` VALUES ('1526', '内蒙古乌兰察布盟', '乌兰察布盟', '2', '15', 'HB', null);
INSERT INTO `cn_city` VALUES ('1529', '内蒙古阿拉善盟', '阿拉善盟', '2', '15', 'HB', null);
INSERT INTO `cn_city` VALUES ('2101', '辽宁省沈阳市', '沈阳', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2102', '辽宁省大连市', '大连', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2103', '辽宁省鞍山市', '鞍山', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2104', '辽宁省抚顺市', '抚顺', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2105', '辽宁省本溪市', '本溪', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2106', '辽宁省丹东市', '丹东', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2107', '辽宁省锦州市', '锦州', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2108', '辽宁省营口市', '营口', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2109', '辽宁省阜新市', '阜新', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2110', '辽宁省辽阳市', '辽阳', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2111', '辽宁省盘锦市', '盘锦市', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2112', '辽宁省铁岭市', '铁岭', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2113', '辽宁省朝阳市', '朝阳', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2114', '辽宁省葫芦岛市', '葫芦岛市', '2', '21', 'DB', null);
INSERT INTO `cn_city` VALUES ('2201', '吉林省长春市', '长春', '2', '22', 'DB', null);
INSERT INTO `cn_city` VALUES ('2202', '吉林省吉林市', '吉林', '2', '22', 'DB', null);
INSERT INTO `cn_city` VALUES ('2203', '吉林省四平市', '四平', '2', '22', 'DB', null);
INSERT INTO `cn_city` VALUES ('2204', '吉林省辽源市', '辽源', '2', '22', 'DB', null);
INSERT INTO `cn_city` VALUES ('2205', '吉林省通化市', '通化', '2', '22', 'DB', null);
INSERT INTO `cn_city` VALUES ('2206', '吉林省白山市', '白山', '2', '22', 'DB', null);
INSERT INTO `cn_city` VALUES ('2207', '吉林省松原市', '松原', '2', '22', 'DB', null);
INSERT INTO `cn_city` VALUES ('2208', '吉林省白城市', '白城', '2', '22', 'DB', null);
INSERT INTO `cn_city` VALUES ('2224', '吉林省延边朝鲜族自治州', '延边', '2', '22', 'DB', null);
INSERT INTO `cn_city` VALUES ('2301', '黑龙江省哈尔滨市', '哈尔滨', '2', '23', 'DB', null);
INSERT INTO `cn_city` VALUES ('2302', '黑龙江省齐齐哈尔市', '齐齐哈尔', '2', '23', 'DB', null);
INSERT INTO `cn_city` VALUES ('2303', '黑龙江省鸡西市', '鸡西', '2', '23', 'DB', null);
INSERT INTO `cn_city` VALUES ('2304', '黑龙江省鹤岗市', '鹤岗', '2', '23', 'DB', null);
INSERT INTO `cn_city` VALUES ('2305', '黑龙江省双鸭山市', '双鸭山', '2', '23', 'DB', null);
INSERT INTO `cn_city` VALUES ('2306', '黑龙江省大庆市', '大庆', '2', '23', 'DB', null);
INSERT INTO `cn_city` VALUES ('2307', '黑龙江省伊春市', '伊春', '2', '23', 'DB', null);
INSERT INTO `cn_city` VALUES ('2308', '黑龙江省佳木斯市', '佳木斯', '2', '23', 'DB', null);
INSERT INTO `cn_city` VALUES ('2309', '黑龙江省七台河市', '七台河', '2', '23', 'DB', null);
INSERT INTO `cn_city` VALUES ('2310', '黑龙江省牡丹江市', '牡丹江', '2', '23', 'DB', null);
INSERT INTO `cn_city` VALUES ('2311', '黑龙江省黑河市', '黑河', '2', '23', 'DB', null);
INSERT INTO `cn_city` VALUES ('2312', '黑龙江省绥化市', '绥化', '2', '23', 'DB', null);
INSERT INTO `cn_city` VALUES ('2327', '黑龙江省大兴安岭地区', '大兴安岭地区', '2', '23', 'DB', null);
INSERT INTO `cn_city` VALUES ('3101', '上海市黄浦区', '黄浦区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3103', '上海市卢湾区', '卢湾区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3104', '上海市徐汇区', '徐汇区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3105', '上海市长宁区', '长宁区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3106', '上海市静安区', '静安区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3107', '上海市普陀区', '普陀区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3108', '上海市闸北区', '闸北区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3109', '上海市虹口区', '虹口区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3110', '上海市杨浦区', '杨浦区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3112', '上海市闵行区', '闵行区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3113', '上海市宝山区', '宝山区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3114', '上海市嘉定区', '嘉定区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3115', '上海市浦东新区', '浦东新区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3116', '上海市金山区', '金山区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3117', '上海市松江区', '松江区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3118', '上海市青浦区', '青浦区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3119', '上海市南汇区', '南汇区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3120', '上海市奉贤区', '奉贤区', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3130', '上海市崇明县', '崇明县', '2', '31', 'AA', null);
INSERT INTO `cn_city` VALUES ('3201', '江苏省南京市', '南京', '2', '32', 'HD', null);
INSERT INTO `cn_city` VALUES ('3202', '江苏省无锡市', '无锡', '2', '32', 'HD', null);
INSERT INTO `cn_city` VALUES ('3203', '江苏省徐州市', '徐州', '2', '32', 'HD', null);
INSERT INTO `cn_city` VALUES ('3204', '江苏省常州市', '常州', '2', '32', 'HD', null);
INSERT INTO `cn_city` VALUES ('3205', '江苏省苏州市', '苏州', '2', '32', 'HD', null);
INSERT INTO `cn_city` VALUES ('3206', '江苏省南通市', '南通', '2', '32', 'HD', null);
INSERT INTO `cn_city` VALUES ('3207', '江苏省连云港市', '连云港', '2', '32', 'HD', null);
INSERT INTO `cn_city` VALUES ('3208', '江苏省淮安市', '淮安', '2', '32', 'HD', null);
INSERT INTO `cn_city` VALUES ('3209', '江苏省盐城市', '盐城', '2', '32', 'HD', null);
INSERT INTO `cn_city` VALUES ('3210', '江苏省扬州市', '扬州', '2', '32', 'HD', null);
INSERT INTO `cn_city` VALUES ('3211', '江苏省镇江市', '镇江', '2', '32', 'HD', null);
INSERT INTO `cn_city` VALUES ('3212', '江苏省泰州市', '泰州', '2', '32', 'HD', null);
INSERT INTO `cn_city` VALUES ('3213', '江苏省宿迁市', '宿迁', '2', '32', 'HD', null);
INSERT INTO `cn_city` VALUES ('3301', '浙江省杭州市', '杭州', '2', '33', 'HD', null);
INSERT INTO `cn_city` VALUES ('3302', '浙江省宁波市', '宁波', '2', '33', 'HD', null);
INSERT INTO `cn_city` VALUES ('3303', '浙江省温州市', '温州', '2', '33', 'HD', null);
INSERT INTO `cn_city` VALUES ('3304', '浙江省嘉兴市', '嘉兴', '2', '33', 'HD', null);
INSERT INTO `cn_city` VALUES ('3305', '浙江省湖州市', '湖州', '2', '33', 'HD', null);
INSERT INTO `cn_city` VALUES ('3306', '浙江省绍兴市', '绍兴', '2', '33', 'HD', null);
INSERT INTO `cn_city` VALUES ('3307', '浙江省金华市', '金华', '2', '33', 'HD', null);
INSERT INTO `cn_city` VALUES ('3308', '浙江省衢州市', '衢州', '2', '33', 'HD', null);
INSERT INTO `cn_city` VALUES ('3309', '浙江省舟山市', '舟山', '2', '33', 'HD', null);
INSERT INTO `cn_city` VALUES ('3310', '浙江省台州市', '台州', '2', '33', 'HD', null);
INSERT INTO `cn_city` VALUES ('3311', '浙江省丽水市', '丽水', '2', '33', 'HD', null);
INSERT INTO `cn_city` VALUES ('3401', '安徽省合肥市', '合肥', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3402', '安徽省芜湖市', '芜湖', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3403', '安徽省蚌埠市', '蚌埠', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3404', '安徽省淮南市', '淮南', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3405', '安徽省马鞍山市', '马鞍山', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3406', '安徽省淮北市', '淮北', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3407', '安徽省铜陵市', '铜陵', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3408', '安徽省安庆市', '安庆', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3410', '安徽省黄山市', '黄山', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3411', '安徽省滁州市', '滁州', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3412', '安徽省阜阳市', '阜阳', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3413', '安徽省宿州市', '宿州', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3414', '安徽省巢湖市', '巢湖', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3415', '安徽省六安市', '六安', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3416', '安徽省亳州市', '亳州', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3417', '安徽省池州市', '池州', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3418', '安徽省宣城市', '宣城', '2', '34', 'HD', null);
INSERT INTO `cn_city` VALUES ('3501', '福建省福州市', '福州', '2', '35', 'HD', null);
INSERT INTO `cn_city` VALUES ('3502', '福建省厦门市', '厦门', '2', '35', 'HD', null);
INSERT INTO `cn_city` VALUES ('3503', '福建省莆田市', '莆田', '2', '35', 'HD', null);
INSERT INTO `cn_city` VALUES ('3504', '福建省三明市', '三明', '2', '35', 'HD', null);
INSERT INTO `cn_city` VALUES ('3505', '福建省泉州市', '泉州', '2', '35', 'HD', null);
INSERT INTO `cn_city` VALUES ('3506', '福建省漳州市', '漳州', '2', '35', 'HD', null);
INSERT INTO `cn_city` VALUES ('3507', '福建省南平市', '南平', '2', '35', 'HD', null);
INSERT INTO `cn_city` VALUES ('3508', '福建省龙岩市', '龙岩', '2', '35', 'HD', null);
INSERT INTO `cn_city` VALUES ('3509', '福建省宁德市', '宁德', '2', '35', 'HD', null);
INSERT INTO `cn_city` VALUES ('3601', '江西省南昌市', '南昌', '2', '36', 'HD', null);
INSERT INTO `cn_city` VALUES ('3602', '江西省景德镇市', '景德镇', '2', '36', 'HD', null);
INSERT INTO `cn_city` VALUES ('3603', '江西省萍乡市', '萍乡', '2', '36', 'HD', null);
INSERT INTO `cn_city` VALUES ('3604', '江西省九江市', '九江', '2', '36', 'HD', null);
INSERT INTO `cn_city` VALUES ('3605', '江西省新余市', '新余', '2', '36', 'HD', null);
INSERT INTO `cn_city` VALUES ('3606', '江西省鹰潭市', '鹰潭', '2', '36', 'HD', null);
INSERT INTO `cn_city` VALUES ('3607', '江西省赣州市', '赣州', '2', '36', 'HD', null);
INSERT INTO `cn_city` VALUES ('3608', '江西省吉安市', '吉安', '2', '36', 'HD', null);
INSERT INTO `cn_city` VALUES ('3609', '江西省宜春市', '宜春', '2', '36', 'HD', null);
INSERT INTO `cn_city` VALUES ('3610', '江西省抚州市', '抚州', '2', '36', 'HD', null);
INSERT INTO `cn_city` VALUES ('3611', '江西省上饶市', '上饶', '2', '36', 'HD', null);
INSERT INTO `cn_city` VALUES ('3701', '山东省济南市', '济南', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3702', '山东省青岛市', '青岛', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3703', '山东省淄博市', '淄博', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3704', '山东省枣庄市', '枣庄', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3705', '山东省东营市', '东营', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3706', '山东省烟台市', '烟台', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3707', '山东省潍坊市', '潍坊', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3708', '山东省济宁市', '济宁', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3709', '山东省泰安市', '泰安', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3710', '山东省威海市', '威海', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3711', '山东省日照市', '日照', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3712', '山东省莱芜市', '莱芜', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3713', '山东省临沂市', '临沂', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3714', '山东省德州市', '德州', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3715', '山东省聊城市', '聊城', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3716', '山东省滨州市', '滨州', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('3717', '山东省荷泽市', '荷泽', '2', '37', 'HD', null);
INSERT INTO `cn_city` VALUES ('4101', '河南省郑州市', '郑州', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4102', '河南省开封市', '开封', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4103', '河南省洛阳市', '洛阳', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4104', '河南省平顶山市', '平顶山', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4105', '河南省安阳市', '安阳', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4106', '河南省鹤壁市', '鹤壁', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4107', '河南省新乡市', '新乡', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4108', '河南省焦作市', '焦作', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4109', '河南省濮阳市', '濮阳', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4110', '河南省许昌市', '许昌', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4111', '河南省漯河市', '漯河', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4112', '河南省三门峡市', '三门峡', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4113', '河南省南阳市', '南阳', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4114', '河南省商丘市', '商丘', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4115', '河南省信阳市', '信阳', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4116', '河南省周口市', '周口', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4117', '河南省驻马店市', '驻马店', '2', '41', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4201', '湖北省武汉市', '武汉', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4202', '湖北省黄石市', '黄石', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4203', '湖北省十堰市', '十堰', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4205', '湖北省宜昌市', '宜昌', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4206', '湖北省襄樊市', '襄樊', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4207', '湖北省鄂州市', '鄂州', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4208', '湖北省荆门市', '荆门', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4209', '湖北省孝感市', '孝感', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4210', '湖北省荆州市', '荆州', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4211', '湖北省荆州市市辖区', '荆州辖区', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4212', '湖北省咸宁市', '咸宁', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4213', '湖北省随州市', '随州', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4228', '湖北省恩施土家族苗族自治州', '恩施', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4290', '湖北省省直辖行政单位', '直辖行政单位', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4294', '湖北省仙桃市', '仙桃', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4295', '湖北省潜江市', '潜江', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4296', '湖北省天门市', '天门', '2', '42', '', null);
INSERT INTO `cn_city` VALUES ('4301', '湖南省长沙市', '长沙', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4302', '湖南省株洲市', '株洲', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4303', '湖南省湘潭市', '湘潭', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4304', '湖南省衡阳市', '衡阳', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4305', '湖南省邵阳市', '邵阳', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4306', '湖南省岳阳市', '岳阳', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4307', '湖南省常德市', '常德', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4308', '湖南省张家界市', '张家界', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4309', '湖南省益阳市', '益阳', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4310', '湖南省郴州市', '郴州', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4311', '湖南省永州市', '永州', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4312', '湖南省郴州市北湖区', '郴州北湖区', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4313', '湖南省郴州市苏仙区', '郴州苏仙区', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4331', '湖南省湘西土家族苗族自治州', '湘西', '2', '43', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4401', '广东省广州市', '广州', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4402', '广东省韶关市', '韶关', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4403', '广东省深圳市', '深圳', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4404', '广东省珠海市', '珠海', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4405', '广东省汕头市', '汕头', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4406', '广东省佛山市', '佛山', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4407', '广东省江门市', '江门', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4408', '广东省湛江市', '湛江', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4409', '广东省茂名市', '茂名', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4412', '广东省肇庆市', '肇庆', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4413', '广东省惠州市', '惠州', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4414', '广东省梅州市', '梅州', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4415', '广东省汕尾市', '汕尾', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4416', '广东省河源市', '河源', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4417', '广东省阳江市', '阳江', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4418', '广东省清远市', '清远', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4419', '广东省东莞市', '东莞', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4420', '广东省中山市', '中山', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4451', '广东省潮州市', '潮州', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4452', '广东省揭阳市', '揭阳', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4453', '广东省云浮市', '云浮', '2', '44', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4501', '广西南宁市', '南宁市', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4502', '广西柳州市', '柳州市', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4503', '广西桂林市', '桂林市', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4504', '广西梧州市', '梧州市', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4505', '广西北海市', '北海市', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4506', '广西防城港市', '城港', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4507', '广西钦州市', '钦州市', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4508', '广西贵港市', '贵港市', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4509', '广西玉林市', '玉林市', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4510', '广西百色市', '百色市', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4511', '广西贺州市', '贺州市', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4512', '广西河池市', '河池市', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4513', '广西来宾市', '来宾市', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4514', '广西崇左市', '崇左市', '2', '45', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4601', '海南省海口市', '海口', '2', '46', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4602', '海南省三亚市', '三亚', '2', '46', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4691', '海南省五指山市', '五指山', '2', '46', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4692', '海南省琼海市', '琼海', '2', '46', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4693', '海南省儋州市', '儋州', '2', '46', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4695', '海南省文昌市', '文昌', '2', '46', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4696', '海南省万宁市', '万宁', '2', '46', 'ZN', null);
INSERT INTO `cn_city` VALUES ('4697', '海南省东方市', '东方', '2', '46', 'ZN', null);
INSERT INTO `cn_city` VALUES ('5001', '重庆市万州区', '万州区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5002', '重庆市涪陵区', '涪陵区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5003', '重庆市渝中区', '渝中区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5004', '重庆市大渡口区', '大渡口区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5005', '重庆市江北区', '江北区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5006', '重庆市沙坪坝区', '沙坪坝区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5007', '重庆市九龙坡区', '九龙坡区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5008', '重庆市南岸区', '南岸区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5009', '重庆市北碚区', '北碚区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5010', '重庆市万盛区', '万盛区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5011', '重庆市双桥区', '双桥区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5012', '重庆市渝北区', '渝北区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5013', '重庆市巴南区', '巴南区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5014', '重庆市黔江区', '黔江区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5015', '重庆市长寿区', '长寿区', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5022', '重庆市綦江县', '綦江县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5023', '重庆市潼南县', '潼南县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5024', '重庆市铜梁县', '铜梁县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5025', '重庆市大足县', '大足县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5026', '重庆市荣昌县', '荣昌县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5027', '重庆市璧山县', '璧山县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5028', '重庆市梁平县', '梁平县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5029', '重庆市城口县', '城口县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5030', '重庆市丰都县', '丰都县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5031', '重庆市垫江县', '垫江县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5032', '重庆市武隆县', '武隆县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5033', '重庆市忠县', '市忠县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5034', '重庆市开县', '市开县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5035', '重庆市云阳县', '云阳县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5036', '重庆市奉节县', '奉节县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5037', '重庆市巫山县', '巫山县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5038', '重庆市巫溪县', '巫溪县', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5040', '重庆市石柱土家族自治县', '石柱', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5041', '重庆市秀山土家族苗族自治县', '秀山', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5042', '重庆市酉阳土家族苗族自治县', '酉阳', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5043', '重庆市彭水苗族土家族自治县', '彭水', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5081', '重庆市江津市', '江津市', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5082', '重庆市合川市', '合川市', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5083', '重庆市永川市', '永川市', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5084', '重庆市南川市', '南川市', '2', '50', 'AA', null);
INSERT INTO `cn_city` VALUES ('5101', '四川省成都市', '成都', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5103', '四川省自贡市', '自贡', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5104', '四川省攀枝花市', '攀枝花', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5105', '四川省泸州市', '泸州', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5106', '四川省德阳市', '德阳', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5107', '四川省绵阳市', '绵阳', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5108', '四川省广元市', '广元', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5109', '四川省遂宁市', '遂宁', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5110', '四川省内江市', '内江', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5111', '四川省内江市市辖区', '内江辖区', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5112', '四川省内江市市中区', '内江中区', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5113', '四川省南充市', '南充', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5114', '四川省眉山市', '眉山', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5115', '四川省宜宾市', '宜宾', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5116', '四川省广安市', '广安', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5117', '四川省达州市', '达州', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5118', '四川省雅安市', '雅安', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5119', '四川省巴中市', '巴中', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5120', '四川省资阳市', '资阳', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5121', '四川省资阳市市辖区', '资阳辖区', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5122', '四川省资阳市雁江区', '资阳雁江区', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5132', '四川省阿坝藏族羌族自治州', '阿坝', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5133', '四川省甘孜藏族自治州', '甘孜', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5134', '四川省凉山彝族自治州', '凉山', '2', '51', 'XN', null);
INSERT INTO `cn_city` VALUES ('5201', '贵州省贵阳市', '贵阳', '2', '52', 'XN', null);
INSERT INTO `cn_city` VALUES ('5202', '贵州省六盘水市', '六盘水', '2', '52', 'XN', null);
INSERT INTO `cn_city` VALUES ('5203', '贵州省遵义市', '遵义', '2', '52', 'XN', null);
INSERT INTO `cn_city` VALUES ('5204', '贵州省安顺市', '安顺', '2', '52', 'XN', null);
INSERT INTO `cn_city` VALUES ('5222', '贵州省铜仁地区', '铜仁地区', '2', '52', 'XN', null);
INSERT INTO `cn_city` VALUES ('5223', '贵州省黔西南布依族苗族自治州', '黔西南', '2', '52', 'XN', null);
INSERT INTO `cn_city` VALUES ('5224', '贵州省毕节地区', '毕节地区', '2', '52', 'XN', null);
INSERT INTO `cn_city` VALUES ('5226', '贵州省黔东南苗族侗族自治州', '黔东南', '2', '52', 'XN', null);
INSERT INTO `cn_city` VALUES ('5227', '贵州省黔南布依族苗族自治州', '黔南', '2', '52', 'XN', null);
INSERT INTO `cn_city` VALUES ('5301', '云南省昆明市', '昆明', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5303', '云南省曲靖市', '曲靖', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5304', '云南省玉溪市', '玉溪', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5305', '云南省保山市', '保山', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5306', '云南省昭通市', '昭通', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5307', '云南省丽江市', '丽江', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5308', '云南省思茅市', '思茅', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5309', '云南省临沧市', '临沧', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5323', '云南省楚雄彝族自治州', '楚雄', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5325', '云南省红河哈尼族彝族自治州', '红河', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5326', '云南省文山壮族苗族自治州', '文山', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5328', '云南省西双版纳傣族自治州', '西双版纳', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5329', '云南省大理白族自治州', '大理', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5331', '云南省德宏傣族景颇族自治州', '德宏', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5333', '云南省怒江僳僳族自治州', '怒江僳', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5334', '云南省迪庆藏族自治州', '迪庆', '2', '53', 'XN', null);
INSERT INTO `cn_city` VALUES ('5401', '西藏拉萨市', '拉萨', '2', '54', 'XN', null);
INSERT INTO `cn_city` VALUES ('5421', '西藏昌都地区', '昌都', '2', '54', 'XN', null);
INSERT INTO `cn_city` VALUES ('5422', '西藏山南地区', '山南', '2', '54', 'XN', null);
INSERT INTO `cn_city` VALUES ('5423', '西藏日喀则地区', '日喀则', '2', '54', 'XN', null);
INSERT INTO `cn_city` VALUES ('5424', '西藏那曲地区', '那曲', '2', '54', 'XN', null);
INSERT INTO `cn_city` VALUES ('5425', '西藏阿里地区', '阿里', '2', '54', 'XN', null);
INSERT INTO `cn_city` VALUES ('5426', '西藏林芝地区', '林芝', '2', '54', 'XN', null);
INSERT INTO `cn_city` VALUES ('6101', '陕西省西安市', '西安', '2', '61', 'XB', null);
INSERT INTO `cn_city` VALUES ('6102', '陕西省铜川市', '铜川', '2', '61', 'XB', null);
INSERT INTO `cn_city` VALUES ('6103', '陕西省宝鸡市', '宝鸡', '2', '61', 'XB', null);
INSERT INTO `cn_city` VALUES ('6104', '陕西省咸阳市', '咸阳', '2', '61', 'XB', null);
INSERT INTO `cn_city` VALUES ('6105', '陕西省渭南市', '渭南', '2', '61', 'XB', null);
INSERT INTO `cn_city` VALUES ('6106', '陕西省延安市', '延安', '2', '61', 'XB', null);
INSERT INTO `cn_city` VALUES ('6107', '陕西省汉中市', '汉中', '2', '61', 'XB', null);
INSERT INTO `cn_city` VALUES ('6108', '陕西省榆林市', '榆林', '2', '61', 'XB', null);
INSERT INTO `cn_city` VALUES ('6109', '陕西省安康市', '安康', '2', '61', 'XB', null);
INSERT INTO `cn_city` VALUES ('6110', '陕西省商洛市', '商洛', '2', '61', 'XB', null);
INSERT INTO `cn_city` VALUES ('6201', '甘肃省兰州市', '兰州', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6202', '甘肃省嘉峪关市', '嘉峪关', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6203', '甘肃省金昌市', '金昌', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6204', '甘肃省白银市', '白银', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6205', '甘肃省天水市', '天水', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6206', '甘肃省武威市', '武威', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6207', '甘肃省张掖市', '张掖', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6208', '甘肃省平凉市', '平凉', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6209', '甘肃省酒泉市', '酒泉', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6210', '甘肃省庆阳市', '庆阳', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6211', '甘肃省庆阳市市辖区', '庆阳辖区', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6212', '甘肃省陇南市', '陇南', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6229', '甘肃省临夏回族自治州', '临夏', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6230', '甘肃省甘南藏族自治州', '甘南', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6231', '甘肃省合作市', '合作', '2', '62', 'XB', null);
INSERT INTO `cn_city` VALUES ('6301', '青海省西宁市', '西宁', '2', '63', 'XB', null);
INSERT INTO `cn_city` VALUES ('6321', '青海省海东地区', '海东', '2', '63', 'XB', null);
INSERT INTO `cn_city` VALUES ('6322', '青海省海北藏族自治州', '海北州', '2', '63', 'XB', null);
INSERT INTO `cn_city` VALUES ('6323', '青海省黄南藏族自治州', '黄南州', '2', '63', 'XB', null);
INSERT INTO `cn_city` VALUES ('6325', '青海省海南藏族自治州', '海南州', '2', '63', 'XB', null);
INSERT INTO `cn_city` VALUES ('6326', '青海省果洛藏族自治州', '果洛州', '2', '63', 'XB', null);
INSERT INTO `cn_city` VALUES ('6327', '青海省玉树藏族自治州', '玉树州', '2', '63', 'XB', null);
INSERT INTO `cn_city` VALUES ('6328', '青海省海西蒙古族藏族自治州', '海西州', '2', '63', 'XB', null);
INSERT INTO `cn_city` VALUES ('6401', '宁夏银川市', '银川市', '2', '64', 'XB', null);
INSERT INTO `cn_city` VALUES ('6402', '宁夏石嘴山市', '嘴山', '2', '64', 'XB', null);
INSERT INTO `cn_city` VALUES ('6403', '宁夏吴忠市', '吴忠市', '2', '64', 'XB', null);
INSERT INTO `cn_city` VALUES ('6404', '宁夏固原市', '固原市', '2', '64', 'XB', null);
INSERT INTO `cn_city` VALUES ('6405', '宁夏中卫市', '中卫市', '2', '64', 'XB', null);
INSERT INTO `cn_city` VALUES ('6501', '新疆乌鲁木齐市', '乌鲁木齐', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6502', '新疆克拉玛依市', '克拉玛依', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6521', '新疆克拉玛依市吐鲁番地区', '吐鲁番', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6522', '新疆哈密地区', '哈密', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6523', '新疆昌吉回族自治州', '昌吉', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6527', '新疆博尔塔拉蒙古自治州', '博尔塔拉州', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6528', '新疆巴音郭楞蒙古自治州', '巴音郭楞州', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6529', '新疆阿克苏地区', '阿克苏', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6530', '新疆克孜勒苏柯尔克孜自治州', '克孜勒苏柯尔克孜州', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6531', '新疆喀什地区', '喀什', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6532', '新疆和田地区', '和田', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6540', '新疆伊宁市', '伊宁', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6542', '新疆塔城地区', '塔城', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6543', '新疆奎屯市', '奎屯市', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6591', '新疆石河子市', '石河子', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6592', '新疆阿拉尔市', '阿拉尔', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6593', '新疆图木舒克市', '图木舒克', '2', '65', 'XB', null);
INSERT INTO `cn_city` VALUES ('6594', '新疆五家渠市', '五家渠', '2', '65', 'XB', null);

-- ----------------------------
-- Table structure for cn_pq
-- ----------------------------
DROP TABLE IF EXISTS `cn_pq`;
CREATE TABLE `cn_pq` (
  `No` varchar(50) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cn_pq
-- ----------------------------
INSERT INTO `cn_pq` VALUES ('AA', '城市');
INSERT INTO `cn_pq` VALUES ('DB', '东北');
INSERT INTO `cn_pq` VALUES ('HB', '华北');
INSERT INTO `cn_pq` VALUES ('HD', '华东');
INSERT INTO `cn_pq` VALUES ('XB', '西北');
INSERT INTO `cn_pq` VALUES ('XN', '西南');
INSERT INTO `cn_pq` VALUES ('ZN', '中南');
INSERT INTO `cn_pq` VALUES ('ZZ', '香澳台');

-- ----------------------------
-- Table structure for cn_sf
-- ----------------------------
DROP TABLE IF EXISTS `cn_sf`;
CREATE TABLE `cn_sf` (
  `No` varchar(2) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Names` varchar(50) DEFAULT NULL COMMENT '小名称',
  `JC` varchar(50) DEFAULT NULL COMMENT '简称',
  `FK_PQ` varchar(100) DEFAULT NULL COMMENT '片区',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cn_sf
-- ----------------------------
INSERT INTO `cn_sf` VALUES ('11', '北京', '北京市', '京', 'AA');
INSERT INTO `cn_sf` VALUES ('12', '天津', '天津市', '津', 'AA');
INSERT INTO `cn_sf` VALUES ('13', '河北', '河北省', '冀', 'HB');
INSERT INTO `cn_sf` VALUES ('14', '山西', '山西省', '晋', 'HB');
INSERT INTO `cn_sf` VALUES ('15', '内蒙', '内蒙古自治区', '蒙', 'HB');
INSERT INTO `cn_sf` VALUES ('21', '辽宁', '辽宁省', '辽', 'DB');
INSERT INTO `cn_sf` VALUES ('22', '吉林', '吉林省', '吉', 'DB');
INSERT INTO `cn_sf` VALUES ('23', '黑龙江', '黑龙江省', '黑', 'DB');
INSERT INTO `cn_sf` VALUES ('31', '上海', '上海市', '沪', 'AA');
INSERT INTO `cn_sf` VALUES ('32', '江苏', '江苏省', '苏', 'HD');
INSERT INTO `cn_sf` VALUES ('33', '浙江', '浙江省', '浙', 'HD');
INSERT INTO `cn_sf` VALUES ('34', '安徽', '安徽省', '皖', 'HD');
INSERT INTO `cn_sf` VALUES ('35', '福建', '福建省', '闽', 'HD');
INSERT INTO `cn_sf` VALUES ('36', '江西', '江西省', '赣', 'HD');
INSERT INTO `cn_sf` VALUES ('37', '山东', '山东省', '鲁', 'HD');
INSERT INTO `cn_sf` VALUES ('41', '河南', '河南省', '豫', 'ZN');
INSERT INTO `cn_sf` VALUES ('42', '湖北', '湖北省', '鄂', 'ZN');
INSERT INTO `cn_sf` VALUES ('43', '湖南', '湖南省', '湘', 'ZN');
INSERT INTO `cn_sf` VALUES ('44', '广东', '广东省', '粤', 'ZN');
INSERT INTO `cn_sf` VALUES ('45', '广西', '广西壮族自治区', '桂', 'ZN');
INSERT INTO `cn_sf` VALUES ('46', '海南', '海南省', '琼', 'ZN');
INSERT INTO `cn_sf` VALUES ('50', '重庆', '重庆市', '渝', 'AA');
INSERT INTO `cn_sf` VALUES ('51', '四川', '四川省', '川', 'XN');
INSERT INTO `cn_sf` VALUES ('52', '贵州', '贵州省', '贵', 'XN');
INSERT INTO `cn_sf` VALUES ('53', '云南', '云南省', '云', 'XN');
INSERT INTO `cn_sf` VALUES ('54', '西藏', '西藏自治区', '藏', 'XN');
INSERT INTO `cn_sf` VALUES ('61', '陕西', '陕西省', '陕', 'XB');
INSERT INTO `cn_sf` VALUES ('62', '甘肃', '甘肃省', '甘', 'XB');
INSERT INTO `cn_sf` VALUES ('63', '青海', '青海省', '青', 'XB');
INSERT INTO `cn_sf` VALUES ('64', '宁夏', '宁夏回族自治区', '宁', 'XB');
INSERT INTO `cn_sf` VALUES ('65', '新疆', '新疆维吾尔自治区', '新', 'XB');
INSERT INTO `cn_sf` VALUES ('71', '台湾', '台湾省', '台', 'ZZ');
INSERT INTO `cn_sf` VALUES ('81', '香港', '香港特别行政区', '港', 'ZZ');
INSERT INTO `cn_sf` VALUES ('82', '澳门', '澳门特别行政区', '澳', 'ZZ');

-- ----------------------------
-- Table structure for frm_diyigebiaodan
-- ----------------------------
DROP TABLE IF EXISTS `frm_diyigebiaodan`;
CREATE TABLE `frm_diyigebiaodan` (
  `OID` int(11) NOT NULL,
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of frm_diyigebiaodan
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_app
-- ----------------------------
DROP TABLE IF EXISTS `gpm_app`;
CREATE TABLE `gpm_app` (
  `No` varchar(30) NOT NULL COMMENT '编号',
  `AppModel` int(11) DEFAULT '0' COMMENT '应用类型',
  `Name` text COMMENT '名称',
  `FK_AppSort` varchar(100) DEFAULT NULL COMMENT '类别',
  `IsEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `Url` text COMMENT '默认连接',
  `SubUrl` text COMMENT '第二连接',
  `UidControl` varchar(100) DEFAULT NULL COMMENT '用户名控件',
  `PwdControl` varchar(100) DEFAULT NULL COMMENT '密码控件',
  `ActionType` int(11) DEFAULT '0' COMMENT '提交类型',
  `SSOType` int(11) DEFAULT '0' COMMENT '登录方式',
  `OpenWay` int(11) DEFAULT '0' COMMENT '打开方式',
  `RefMenuNo` text COMMENT '关联菜单编号',
  `AppRemark` varchar(500) DEFAULT NULL COMMENT '备注',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT 'ICON',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpm_app
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_appsort
-- ----------------------------
DROP TABLE IF EXISTS `gpm_appsort`;
CREATE TABLE `gpm_appsort` (
  `No` varchar(2) NOT NULL COMMENT '编号',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  `RefMenuNo` varchar(300) DEFAULT NULL COMMENT '关联的菜单编号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpm_appsort
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_bar
-- ----------------------------
DROP TABLE IF EXISTS `gpm_bar`;
CREATE TABLE `gpm_bar` (
  `No` varchar(200) NOT NULL COMMENT '编号',
  `Name` text COMMENT '名称',
  `Title` text COMMENT '标题',
  `OpenWay` int(11) DEFAULT '0' COMMENT '打开方式',
  `IsLine` int(11) DEFAULT '0' COMMENT '是否独占一行',
  `MoreUrl` text COMMENT '更多标签Url',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpm_bar
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_baremp
-- ----------------------------
DROP TABLE IF EXISTS `gpm_baremp`;
CREATE TABLE `gpm_baremp` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Bar` varchar(90) DEFAULT NULL COMMENT '信息块编号',
  `FK_Emp` varchar(90) DEFAULT NULL COMMENT '人员编号',
  `Title` text COMMENT '标题',
  `IsShow` int(11) DEFAULT '1' COMMENT '是否显示',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpm_baremp
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_empapp
-- ----------------------------
DROP TABLE IF EXISTS `gpm_empapp`;
CREATE TABLE `gpm_empapp` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Emp` varchar(50) DEFAULT NULL COMMENT '操作员',
  `FK_App` varchar(50) DEFAULT NULL COMMENT '系统',
  `Name` text COMMENT '系统-名称',
  `Url` text COMMENT '连接',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpm_empapp
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_empmenu
-- ----------------------------
DROP TABLE IF EXISTS `gpm_empmenu`;
CREATE TABLE `gpm_empmenu` (
  `FK_Menu` varchar(50) NOT NULL COMMENT '菜单',
  `FK_Emp` varchar(100) NOT NULL COMMENT '菜单功能',
  `IsChecked` int(11) DEFAULT '1' COMMENT '是否选中',
  PRIMARY KEY (`FK_Menu`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpm_empmenu
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_group
-- ----------------------------
DROP TABLE IF EXISTS `gpm_group`;
CREATE TABLE `gpm_group` (
  `No` varchar(3) NOT NULL COMMENT '编号',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpm_group
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_groupemp
-- ----------------------------
DROP TABLE IF EXISTS `gpm_groupemp`;
CREATE TABLE `gpm_groupemp` (
  `FK_Group` varchar(50) NOT NULL COMMENT '权限组',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员',
  PRIMARY KEY (`FK_Group`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpm_groupemp
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_groupmenu
-- ----------------------------
DROP TABLE IF EXISTS `gpm_groupmenu`;
CREATE TABLE `gpm_groupmenu` (
  `FK_Group` varchar(50) NOT NULL COMMENT '权限组',
  `FK_Menu` varchar(50) NOT NULL COMMENT '菜单',
  `IsChecked` int(11) DEFAULT '1' COMMENT '是否选中',
  PRIMARY KEY (`FK_Group`,`FK_Menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpm_groupmenu
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_groupstation
-- ----------------------------
DROP TABLE IF EXISTS `gpm_groupstation`;
CREATE TABLE `gpm_groupstation` (
  `FK_Group` varchar(50) NOT NULL COMMENT '权限组',
  `FK_Station` varchar(100) NOT NULL COMMENT '岗位',
  PRIMARY KEY (`FK_Group`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpm_groupstation
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_menu
-- ----------------------------
DROP TABLE IF EXISTS `gpm_menu`;
CREATE TABLE `gpm_menu` (
  `No` varchar(4) NOT NULL COMMENT '功能编号',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序号',
  `MenuType` int(11) DEFAULT '0' COMMENT '菜单类型',
  `FK_App` varchar(100) DEFAULT NULL COMMENT '系统',
  `OpenWay` int(11) DEFAULT '1' COMMENT '打开方式',
  `Url` text COMMENT '连接',
  `IsEnable` int(11) DEFAULT '1' COMMENT '是否启用?',
  `Icon` varchar(500) DEFAULT NULL COMMENT 'Icon',
  `MenuCtrlWay` int(11) DEFAULT '0' COMMENT '控制方式',
  `Flag` varchar(500) DEFAULT NULL COMMENT '标记',
  `Tag1` varchar(500) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(500) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(500) DEFAULT NULL COMMENT 'Tag3',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpm_menu
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_persetting
-- ----------------------------
DROP TABLE IF EXISTS `gpm_persetting`;
CREATE TABLE `gpm_persetting` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Emp` varchar(200) DEFAULT NULL COMMENT '人员',
  `FK_App` varchar(200) DEFAULT NULL COMMENT '系统',
  `UserNo` varchar(200) DEFAULT NULL COMMENT 'UserNo',
  `UserPass` varchar(200) DEFAULT NULL COMMENT 'UserPass',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpm_persetting
-- ----------------------------

-- ----------------------------
-- Table structure for gpm_stationmenu
-- ----------------------------
DROP TABLE IF EXISTS `gpm_stationmenu`;
CREATE TABLE `gpm_stationmenu` (
  `FK_Station` varchar(50) NOT NULL COMMENT '岗位',
  `FK_Menu` varchar(50) NOT NULL COMMENT '菜单',
  `IsChecked` int(11) DEFAULT '1' COMMENT '是否选中',
  PRIMARY KEY (`FK_Station`,`FK_Menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpm_stationmenu
-- ----------------------------

-- ----------------------------
-- Table structure for hy_dataincomeway
-- ----------------------------
DROP TABLE IF EXISTS `hy_dataincomeway`;
CREATE TABLE `hy_dataincomeway` (
  `No` varchar(20) NOT NULL COMMENT '编号',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  `BeiZhui` text COMMENT '适应对象&优缺点',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hy_dataincomeway
-- ----------------------------

-- ----------------------------
-- Table structure for hy_datasort
-- ----------------------------
DROP TABLE IF EXISTS `hy_datasort`;
CREATE TABLE `hy_datasort` (
  `No` varchar(20) NOT NULL COMMENT '编号',
  `Name` varchar(300) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hy_datasort
-- ----------------------------

-- ----------------------------
-- Table structure for hy_deptsort
-- ----------------------------
DROP TABLE IF EXISTS `hy_deptsort`;
CREATE TABLE `hy_deptsort` (
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门',
  `FK_Sort` varchar(100) NOT NULL COMMENT '类别',
  PRIMARY KEY (`FK_Dept`,`FK_Sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hy_deptsort
-- ----------------------------

-- ----------------------------
-- Table structure for js_filemanager_folder
-- ----------------------------
DROP TABLE IF EXISTS `js_filemanager_folder`;
CREATE TABLE `js_filemanager_folder` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `folder_name` varchar(100) NOT NULL COMMENT '文件夹名',
  `group_type` varchar(64) NOT NULL COMMENT '文件分组类型',
  `office_code` varchar(64) DEFAULT NULL COMMENT '部门编码',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`id`),
  KEY `idx_sys_file_tree_pc` (`parent_code`),
  KEY `idx_sys_file_tree_ts` (`tree_sort`),
  KEY `idx_sys_file_tree_tss` (`tree_sorts`),
  KEY `idx_sys_file_tree_gt` (`group_type`),
  KEY `idx_sys_file_tree_oc` (`office_code`),
  KEY `idx_sys_file_tree_cb` (`create_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件管理文件夹';

-- ----------------------------
-- Records of js_filemanager_folder
-- ----------------------------
INSERT INTO `js_filemanager_folder` VALUES ('1119900392012357632', '0', '0,', '40', '0000000040,', '0', '0', '公司文件', '公司文件', 'global', null, '0', 'system', '2020-05-06 14:20:22', 'system', '2020-05-06 14:20:22', null, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1119900392196907008', '1119900392012357632', '0,1119900392012357632,', '30', '0000000040,0000000030,', '1', '1', '公司文件/规章制度', '规章制度', 'global', null, '0', 'system', '2020-05-06 14:20:22', 'system', '2020-05-06 14:20:22', null, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1119900392284987392', '1119900392012357632', '0,1119900392012357632,', '40', '0000000040,0000000040,', '1', '1', '公司文件/产品资料', '产品资料', 'global', null, '0', 'system', '2020-05-06 14:20:22', 'system', '2020-05-06 14:20:22', null, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1119900392331124736', '0', '0,', '50', '0000000050,', '0', '0', '员工之家', '员工之家', 'global', null, '0', 'system', '2020-05-06 14:20:22', 'system', '2020-05-06 14:20:22', null, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1119900392360484864', '1119900392331124736', '0,1119900392331124736,', '30', '0000000050,0000000030,', '1', '1', '员工之家/企业文化', '企业文化', 'global', null, '0', 'system', '2020-05-06 14:20:22', 'system', '2020-05-06 14:20:22', null, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1119900392406622208', '1119900392331124736', '0,1119900392331124736,', '40', '0000000050,0000000040,', '1', '1', '员工之家/公司活动', '公司活动', 'global', null, '0', 'system', '2020-05-06 14:20:22', 'system', '2020-05-06 14:20:22', null, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1119900392461148160', '0', '0,', '30', '0000000030,', '1', '0', '我的文档', '我的文档', 'self', null, '0', 'system', '2020-05-06 14:20:22', 'system', '2020-05-06 14:20:22', null, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1119900392490508288', '0', '0,', '40', '0000000040,', '1', '0', '我的图片', '我的图片', 'self', null, '0', 'system', '2020-05-06 14:20:22', 'system', '2020-05-06 14:20:22', null, '0', 'JeeSite');

-- ----------------------------
-- Table structure for js_filemanager_shared
-- ----------------------------
DROP TABLE IF EXISTS `js_filemanager_shared`;
CREATE TABLE `js_filemanager_shared` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `folder_id` varchar(64) DEFAULT NULL COMMENT '文件夹编码',
  `file_upload_id` varchar(64) DEFAULT NULL COMMENT '文件上传编码',
  `file_name` varchar(500) NOT NULL COMMENT '文件或文件夹名',
  `receive_user_code` varchar(100) NOT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(200) NOT NULL COMMENT '接收者用户名称',
  `click_num` decimal(10,0) DEFAULT NULL COMMENT '点击次数',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_file_share_ruc` (`receive_user_code`),
  KEY `idx_sys_file_share_cb` (`create_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件管理共享表';

-- ----------------------------
-- Records of js_filemanager_shared
-- ----------------------------

-- ----------------------------
-- Table structure for js_gen_table
-- ----------------------------
DROP TABLE IF EXISTS `js_gen_table`;
CREATE TABLE `js_gen_table` (
  `table_name` varchar(64) NOT NULL COMMENT '表名',
  `class_name` varchar(100) NOT NULL COMMENT '实体类名称',
  `comments` varchar(500) NOT NULL COMMENT '表说明',
  `parent_table_name` varchar(64) DEFAULT NULL COMMENT '关联父表的表名',
  `parent_table_fk_name` varchar(64) DEFAULT NULL COMMENT '本表关联父表的外键名',
  `data_source_name` varchar(64) DEFAULT NULL COMMENT '数据源名称',
  `tpl_category` varchar(200) DEFAULT NULL COMMENT '使用的模板',
  `package_name` varchar(500) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `sub_module_name` varchar(30) DEFAULT NULL COMMENT '生成子模块名',
  `function_name` varchar(200) DEFAULT NULL COMMENT '生成功能名',
  `function_name_simple` varchar(50) DEFAULT NULL COMMENT '生成功能名（简写）',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_base_dir` varchar(1000) DEFAULT NULL COMMENT '生成基础路径',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`table_name`),
  KEY `idx_gen_table_ptn` (`parent_table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代码生成表';

-- ----------------------------
-- Records of js_gen_table
-- ----------------------------
INSERT INTO `js_gen_table` VALUES ('test_data', 'TestData', '测试数据', null, null, null, 'crud', 'com.jeesite.modules', 'test', '', '测试数据', '数据', 'ThinkGem', null, '{\"isHaveDelete\":\"1\",\"isFileUpload\":\"1\",\"isHaveDisableEnable\":\"1\",\"isImageUpload\":\"1\"}', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null);
INSERT INTO `js_gen_table` VALUES ('test_data_child', 'TestDataChild', '测试数据子表', 'test_data', 'test_data_id', null, 'crud', 'com.jeesite.modules', 'test', '', '测试子表', '数据', 'ThinkGem', null, null, 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null);
INSERT INTO `js_gen_table` VALUES ('test_tree', 'TestTree', '测试树表', null, null, null, 'treeGrid', 'com.jeesite.modules', 'test', '', '测试树表', '数据', 'ThinkGem', null, '{\"treeViewName\":\"tree_name\",\"isHaveDelete\":\"1\",\"treeViewCode\":\"tree_code\",\"isFileUpload\":\"1\",\"isHaveDisableEnable\":\"1\",\"isImageUpload\":\"1\"}', 'system', '2018-12-03 11:21:30', 'system', '2018-12-03 11:21:30', null);

-- ----------------------------
-- Table structure for js_gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `js_gen_table_column`;
CREATE TABLE `js_gen_table_column` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `table_name` varchar(64) NOT NULL COMMENT '表名',
  `column_name` varchar(64) NOT NULL COMMENT '列名',
  `column_sort` decimal(10,0) DEFAULT NULL COMMENT '列排序（升序）',
  `column_type` varchar(100) NOT NULL COMMENT '类型',
  `column_label` varchar(50) DEFAULT NULL COMMENT '列标签名',
  `comments` varchar(500) NOT NULL COMMENT '列备注说明',
  `attr_name` varchar(200) NOT NULL COMMENT '类的属性名',
  `attr_type` varchar(200) NOT NULL COMMENT '类的属性类型',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键',
  `is_null` char(1) DEFAULT NULL COMMENT '是否可为空',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否插入字段',
  `is_update` char(1) DEFAULT NULL COMMENT '是否更新字段',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段',
  `query_type` varchar(200) DEFAULT NULL COMMENT '查询方式',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段',
  `show_type` varchar(200) DEFAULT NULL COMMENT '表单类型',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  PRIMARY KEY (`id`),
  KEY `idx_gen_table_column_tn` (`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代码生成表列';

-- ----------------------------
-- Records of js_gen_table_column
-- ----------------------------
INSERT INTO `js_gen_table_column` VALUES ('1069431398897889280', 'test_data', 'id', '10', 'varchar(64)', '编号', '编号', 'id', 'String', '1', '0', '1', null, null, null, null, '1', 'hidden', '{\"fieldValid\":\"abc\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431398923055104', 'test_data', 'test_input', '20', 'varchar(200)', '单行文本', '单行文本', 'testInput', 'String', null, '1', '1', '1', '1', '1', 'LIKE', '1', 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431398935638016', 'test_data', 'test_textarea', '30', 'varchar(200)', '多行文本', '多行文本', 'testTextarea', 'String', null, '1', '1', '1', '1', '1', 'LIKE', '1', 'textarea', '{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431398948220928', 'test_data', 'test_select', '40', 'varchar(10)', '下拉框', '下拉框', 'testSelect', 'String', null, '1', '1', '1', '1', '1', null, '1', 'select', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431398960803840', 'test_data', 'test_select_multiple', '50', 'varchar(200)', '下拉多选', '下拉多选', 'testSelectMultiple', 'String', null, '1', '1', '1', '1', '1', null, '1', 'select_multiple', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431398973386752', 'test_data', 'test_radio', '60', 'varchar(10)', '单选框', '单选框', 'testRadio', 'String', null, '1', '1', '1', '1', '1', null, '1', 'radio', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431398985969664', 'test_data', 'test_checkbox', '70', 'varchar(200)', '复选框', '复选框', 'testCheckbox', 'String', null, '1', '1', '1', '1', '1', null, '1', 'checkbox', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431398998552576', 'test_data', 'test_date', '80', 'datetime', '日期选择', '日期选择', 'testDate', 'java.util.Date', null, '1', '1', '1', '1', '1', 'BETWEEN', '1', 'date', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431399019524096', 'test_data', 'test_datetime', '90', 'datetime', '日期时间', '日期时间', 'testDatetime', 'java.util.Date', null, '1', '1', '1', '1', '1', 'BETWEEN', '1', 'datetime', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431399036301312', 'test_data', 'test_user_code', '100', 'varchar(64)', '用户选择', '用户选择', 'testUser', 'com.jeesite.modules.sys.entity.User', null, '1', '1', '1', '1', '1', null, '1', 'userselect', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431399053078528', 'test_data', 'test_office_code', '110', 'varchar(64)', '机构选择', '机构选择', 'testOffice', 'com.jeesite.modules.sys.entity.Office', null, '1', '1', '1', '1', '1', null, '1', 'officeselect', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431399069855744', 'test_data', 'test_area_code', '120', 'varchar(64)', '区域选择', '区域选择', 'testAreaCode|testAreaName', 'String', null, '1', '1', '1', '1', '1', null, '1', 'areaselect', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431399082438656', 'test_data', 'test_area_name', '130', 'varchar(100)', '区域名称', '区域名称', 'testAreaName', 'String', null, '1', '1', '1', '1', '0', 'LIKE', '0', 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431399099215872', 'test_data', 'status', '140', 'char(1)', '状态', '状态（0正常 1删除 2停用）', 'status', 'String', null, '0', '1', null, '1', '1', null, null, 'select', '{\"dictName\":\"sys_search_status\",\"dictType\":\"sys_search_status\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431399115993088', 'test_data', 'create_by', '150', 'varchar(64)', '创建者', '创建者', 'createBy', 'String', null, '0', '1', null, null, null, null, null, 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431399124381696', 'test_data', 'create_date', '160', 'datetime', '创建时间', '创建时间', 'createDate', 'java.util.Date', null, '0', '1', null, '1', null, null, null, 'dateselect', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431399136964608', 'test_data', 'update_by', '170', 'varchar(64)', '更新者', '更新者', 'updateBy', 'String', null, '0', '1', '1', null, null, null, null, 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431399149547520', 'test_data', 'update_date', '180', 'datetime', '更新时间', '更新时间', 'updateDate', 'java.util.Date', null, '0', '1', '1', null, null, null, null, 'dateselect', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431399162130432', 'test_data', 'remarks', '190', 'varchar(500)', '备注信息', '备注信息', 'remarks', 'String', null, '1', '1', '1', '1', '1', null, '1', 'textarea', '{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431399237627904', 'test_data_child', 'id', '10', 'varchar(64)', '编号', '编号', 'id', 'String', '1', '0', '1', null, null, null, null, '1', 'hidden', '{\"fieldValid\":\"abc\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431399355068416', 'test_data_child', 'test_sort', '20', 'int(11)', '排序号', '排序号', 'testSort', 'Long', null, '1', '1', '1', '1', '1', null, '1', 'input', '{\"fieldValid\":\"digits\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431399522840576', 'test_data_child', 'test_data_id', '30', 'varchar(64)', '父表主键', '父表主键', 'testData', 'String', null, '1', '1', '1', '1', '1', null, '1', 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401032790016', 'test_data_child', 'test_input', '40', 'varchar(200)', '单行文本', '单行文本', 'testInput', 'String', null, '1', '1', '1', '1', '1', 'LIKE', '1', 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401049567232', 'test_data_child', 'test_textarea', '50', 'varchar(200)', '多行文本', '多行文本', 'testTextarea', 'String', null, '1', '1', '1', '1', '1', 'LIKE', '1', 'textarea', '{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431401125064704', 'test_data_child', 'test_select', '60', 'varchar(10)', '下拉框', '下拉框', 'testSelect', 'String', null, '1', '1', '1', '1', '1', null, '1', 'select', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431401141841920', 'test_data_child', 'test_select_multiple', '70', 'varchar(200)', '下拉多选', '下拉多选', 'testSelectMultiple', 'String', null, '1', '1', '1', '1', '1', null, '1', 'select_multiple', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431401158619136', 'test_data_child', 'test_radio', '80', 'varchar(10)', '单选框', '单选框', 'testRadio', 'String', null, '1', '1', '1', '1', '1', null, '1', 'radio', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431401175396352', 'test_data_child', 'test_checkbox', '90', 'varchar(200)', '复选框', '复选框', 'testCheckbox', 'String', null, '1', '1', '1', '1', '1', null, '1', 'checkbox', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431401192173568', 'test_data_child', 'test_date', '100', 'datetime', '日期选择', '日期选择', 'testDate', 'java.util.Date', null, '1', '1', '1', '1', '1', 'BETWEEN', '1', 'date', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401208950784', 'test_data_child', 'test_datetime', '110', 'datetime', '日期时间', '日期时间', 'testDatetime', 'java.util.Date', null, '1', '1', '1', '1', '1', 'BETWEEN', '1', 'datetime', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401225728000', 'test_data_child', 'test_user_code', '120', 'varchar(64)', '用户选择', '用户选择', 'testUser', 'com.jeesite.modules.sys.entity.User', null, '1', '1', '1', '1', '1', null, '1', 'userselect', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401242505216', 'test_data_child', 'test_office_code', '130', 'varchar(64)', '机构选择', '机构选择', 'testOffice', 'com.jeesite.modules.sys.entity.Office', null, '1', '1', '1', '1', '1', null, '1', 'officeselect', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401255088128', 'test_data_child', 'test_area_code', '140', 'varchar(64)', '区域选择', '区域选择', 'testAreaCode|testAreaName', 'String', null, '1', '1', '1', '1', '1', null, '1', 'areaselect', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401267671040', 'test_data_child', 'test_area_name', '150', 'varchar(100)', '区域名称', '区域名称', 'testAreaName', 'String', null, '1', '1', '1', '1', '0', 'LIKE', '0', 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401347362816', 'test_tree', 'tree_code', '10', 'varchar(64)', '节点编码', '节点编码', 'treeCode', 'String', '1', '0', '1', null, null, null, null, '1', 'input', '{\"fieldValid\":\"abc\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431401359945728', 'test_tree', 'parent_code', '20', 'varchar(64)', '父级编号', '父级编号', 'parentCode|parentName', 'This', null, '0', '1', '1', '1', '1', null, '1', 'treeselect', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401376722944', 'test_tree', 'parent_codes', '30', 'varchar(1000)', '所有父级编号', '所有父级编号', 'parentCodes', 'String', null, '0', '1', '1', '1', '1', 'LIKE', '0', 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401393500160', 'test_tree', 'tree_sort', '40', 'decimal(10,0)', '本级排序号', '本级排序号（升序）', 'treeSort', 'Integer', null, '0', '1', '1', '1', '1', null, '1', 'input', '{\"fieldValid\":\"digits\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431401410277376', 'test_tree', 'tree_sorts', '50', 'varchar(1000)', '所有级别排序号', '所有级别排序号', 'treeSorts', 'String', null, '0', '1', '1', '0', '1', null, '0', 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401427054592', 'test_tree', 'tree_leaf', '60', 'char(1)', '是否最末级', '是否最末级', 'treeLeaf', 'String', null, '0', '1', '1', null, null, null, null, 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401439637504', 'test_tree', 'tree_level', '70', 'decimal(4,0)', '层次级别', '层次级别', 'treeLevel', 'Integer', null, '0', '1', '1', null, null, null, null, 'input', '{\"fieldValid\":\"digits\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431401456414720', 'test_tree', 'tree_names', '80', 'varchar(1000)', '全节点名', '全节点名', 'treeNames', 'String', null, '0', '1', '1', '1', '1', 'LIKE', '1', 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401468997632', 'test_tree', 'tree_name', '90', 'varchar(200)', '节点名称', '节点名称', 'treeName', 'String', null, '0', '1', '1', '1', '1', 'LIKE', '1', 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401481580544', 'test_tree', 'status', '100', 'char(1)', '状态', '状态（0正常 1删除 2停用）', 'status', 'String', null, '0', '1', null, '1', '1', null, null, 'select', '{\"dictName\":\"sys_search_status\",\"dictType\":\"sys_search_status\"}');
INSERT INTO `js_gen_table_column` VALUES ('1069431401498357760', 'test_tree', 'create_by', '110', 'varchar(64)', '创建者', '创建者', 'createBy', 'String', null, '0', '1', null, null, null, null, null, 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401510940672', 'test_tree', 'create_date', '120', 'datetime', '创建时间', '创建时间', 'createDate', 'java.util.Date', null, '0', '1', null, '1', null, null, null, 'dateselect', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401527717888', 'test_tree', 'update_by', '130', 'varchar(64)', '更新者', '更新者', 'updateBy', 'String', null, '0', '1', '1', null, null, null, null, 'input', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401540300800', 'test_tree', 'update_date', '140', 'datetime', '更新时间', '更新时间', 'updateDate', 'java.util.Date', null, '0', '1', '1', null, null, null, null, 'dateselect', null);
INSERT INTO `js_gen_table_column` VALUES ('1069431401552883712', 'test_tree', 'remarks', '150', 'varchar(500)', '备注信息', '备注信息', 'remarks', 'String', null, '1', '1', '1', '1', '1', null, '1', 'textarea', '{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}');

-- ----------------------------
-- Table structure for js_job_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `js_job_blob_triggers`;
CREATE TABLE `js_job_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `js_job_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of js_job_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for js_job_calendars
-- ----------------------------
DROP TABLE IF EXISTS `js_job_calendars`;
CREATE TABLE `js_job_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of js_job_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for js_job_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `js_job_cron_triggers`;
CREATE TABLE `js_job_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `js_job_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of js_job_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for js_job_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `js_job_fired_triggers`;
CREATE TABLE `js_job_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of js_job_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for js_job_job_details
-- ----------------------------
DROP TABLE IF EXISTS `js_job_job_details`;
CREATE TABLE `js_job_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of js_job_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for js_job_locks
-- ----------------------------
DROP TABLE IF EXISTS `js_job_locks`;
CREATE TABLE `js_job_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of js_job_locks
-- ----------------------------

-- ----------------------------
-- Table structure for js_job_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `js_job_paused_trigger_grps`;
CREATE TABLE `js_job_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of js_job_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for js_job_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `js_job_scheduler_state`;
CREATE TABLE `js_job_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of js_job_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for js_job_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `js_job_simple_triggers`;
CREATE TABLE `js_job_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `js_job_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of js_job_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for js_job_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `js_job_simprop_triggers`;
CREATE TABLE `js_job_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `js_job_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of js_job_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for js_job_triggers
-- ----------------------------
DROP TABLE IF EXISTS `js_job_triggers`;
CREATE TABLE `js_job_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `js_job_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `js_job_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of js_job_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_area
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_area`;
CREATE TABLE `js_sys_area` (
  `area_code` varchar(100) NOT NULL COMMENT '区域编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `area_name` varchar(100) NOT NULL COMMENT '区域名称',
  `area_type` char(1) DEFAULT NULL COMMENT '区域类型',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`area_code`),
  KEY `idx_sys_area_pc` (`parent_code`),
  KEY `idx_sys_area_ts` (`tree_sort`),
  KEY `idx_sys_area_status` (`status`),
  KEY `idx_sys_area_pcs` (`parent_codes`(255)),
  KEY `idx_sys_area_tss` (`tree_sorts`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='行政区划';

-- ----------------------------
-- Records of js_sys_area
-- ----------------------------
INSERT INTO `js_sys_area` VALUES ('370000', '0', '0,', '370000', '0000370000,', '0', '0', '山东省', '山东省', '1', '0', 'system', '2018-12-03 11:20:28', 'system', '2018-12-03 11:20:28', null);
INSERT INTO `js_sys_area` VALUES ('370100', '370000', '0,370000,', '370100', '0000370000,0000370100,', '0', '1', '山东省/济南市', '济南市', '2', '0', 'system', '2018-12-03 11:20:28', 'system', '2018-12-03 11:20:28', null);
INSERT INTO `js_sys_area` VALUES ('370102', '370100', '0,370000,370100,', '370102', '0000370000,0000370100,0000370102,', '1', '2', '山东省/济南市/历下区', '历下区', '3', '0', 'system', '2018-12-03 11:20:28', 'system', '2018-12-03 11:20:29', null);
INSERT INTO `js_sys_area` VALUES ('370103', '370100', '0,370000,370100,', '370103', '0000370000,0000370100,0000370103,', '1', '2', '山东省/济南市/市中区', '市中区', '3', '0', 'system', '2018-12-03 11:20:29', 'system', '2018-12-03 11:20:29', null);
INSERT INTO `js_sys_area` VALUES ('370104', '370100', '0,370000,370100,', '370104', '0000370000,0000370100,0000370104,', '1', '2', '山东省/济南市/槐荫区', '槐荫区', '3', '0', 'system', '2018-12-03 11:20:29', 'system', '2018-12-03 11:20:29', null);
INSERT INTO `js_sys_area` VALUES ('370105', '370100', '0,370000,370100,', '370105', '0000370000,0000370100,0000370105,', '1', '2', '山东省/济南市/天桥区', '天桥区', '3', '0', 'system', '2018-12-03 11:20:29', 'system', '2018-12-03 11:20:29', null);
INSERT INTO `js_sys_area` VALUES ('370112', '370100', '0,370000,370100,', '370112', '0000370000,0000370100,0000370112,', '1', '2', '山东省/济南市/历城区', '历城区', '3', '0', 'system', '2018-12-03 11:20:29', 'system', '2018-12-03 11:20:29', null);
INSERT INTO `js_sys_area` VALUES ('370113', '370100', '0,370000,370100,', '370113', '0000370000,0000370100,0000370113,', '1', '2', '山东省/济南市/长清区', '长清区', '3', '0', 'system', '2018-12-03 11:20:30', 'system', '2018-12-03 11:20:30', null);
INSERT INTO `js_sys_area` VALUES ('370114', '370100', '0,370000,370100,', '370114', '0000370000,0000370100,0000370114,', '1', '2', '山东省/济南市/章丘区', '章丘区', '3', '0', 'system', '2018-12-03 11:20:30', 'system', '2018-12-03 11:20:30', null);
INSERT INTO `js_sys_area` VALUES ('370124', '370100', '0,370000,370100,', '370124', '0000370000,0000370100,0000370124,', '1', '2', '山东省/济南市/平阴县', '平阴县', '3', '0', 'system', '2018-12-03 11:20:30', 'system', '2018-12-03 11:20:30', null);
INSERT INTO `js_sys_area` VALUES ('370125', '370100', '0,370000,370100,', '370125', '0000370000,0000370100,0000370125,', '1', '2', '山东省/济南市/济阳县', '济阳县', '3', '0', 'system', '2018-12-03 11:20:30', 'system', '2018-12-03 11:20:30', null);
INSERT INTO `js_sys_area` VALUES ('370126', '370100', '0,370000,370100,', '370126', '0000370000,0000370100,0000370126,', '1', '2', '山东省/济南市/商河县', '商河县', '3', '0', 'system', '2018-12-03 11:20:30', 'system', '2018-12-03 11:20:30', null);
INSERT INTO `js_sys_area` VALUES ('370200', '370000', '0,370000,', '370200', '0000370000,0000370200,', '0', '1', '山东省/青岛市', '青岛市', '2', '0', 'system', '2018-12-03 11:20:28', 'system', '2018-12-03 11:20:28', null);
INSERT INTO `js_sys_area` VALUES ('370202', '370200', '0,370000,370200,', '370202', '0000370000,0000370200,0000370202,', '1', '2', '山东省/青岛市/市南区', '市南区', '3', '0', 'system', '2018-12-03 11:20:30', 'system', '2018-12-03 11:20:30', null);
INSERT INTO `js_sys_area` VALUES ('370203', '370200', '0,370000,370200,', '370203', '0000370000,0000370200,0000370203,', '1', '2', '山东省/青岛市/市北区', '市北区', '3', '0', 'system', '2018-12-03 11:20:30', 'system', '2018-12-03 11:20:30', null);
INSERT INTO `js_sys_area` VALUES ('370211', '370200', '0,370000,370200,', '370211', '0000370000,0000370200,0000370211,', '1', '2', '山东省/青岛市/黄岛区', '黄岛区', '3', '0', 'system', '2018-12-03 11:20:30', 'system', '2018-12-03 11:20:30', null);
INSERT INTO `js_sys_area` VALUES ('370212', '370200', '0,370000,370200,', '370212', '0000370000,0000370200,0000370212,', '1', '2', '山东省/青岛市/崂山区', '崂山区', '3', '0', 'system', '2018-12-03 11:20:30', 'system', '2018-12-03 11:20:30', null);
INSERT INTO `js_sys_area` VALUES ('370213', '370200', '0,370000,370200,', '370213', '0000370000,0000370200,0000370213,', '1', '2', '山东省/青岛市/李沧区', '李沧区', '3', '0', 'system', '2018-12-03 11:20:30', 'system', '2018-12-03 11:20:30', null);
INSERT INTO `js_sys_area` VALUES ('370214', '370200', '0,370000,370200,', '370214', '0000370000,0000370200,0000370214,', '1', '2', '山东省/青岛市/城阳区', '城阳区', '3', '0', 'system', '2018-12-03 11:20:30', 'system', '2018-12-03 11:20:31', null);
INSERT INTO `js_sys_area` VALUES ('370281', '370200', '0,370000,370200,', '370281', '0000370000,0000370200,0000370281,', '1', '2', '山东省/青岛市/胶州市', '胶州市', '3', '0', 'system', '2018-12-03 11:20:31', 'system', '2018-12-03 11:20:31', null);
INSERT INTO `js_sys_area` VALUES ('370282', '370200', '0,370000,370200,', '370282', '0000370000,0000370200,0000370282,', '1', '2', '山东省/青岛市/即墨区', '即墨区', '3', '0', 'system', '2018-12-03 11:20:31', 'system', '2018-12-03 11:20:31', null);
INSERT INTO `js_sys_area` VALUES ('370283', '370200', '0,370000,370200,', '370283', '0000370000,0000370200,0000370283,', '1', '2', '山东省/青岛市/平度市', '平度市', '3', '0', 'system', '2018-12-03 11:20:31', 'system', '2018-12-03 11:20:31', null);
INSERT INTO `js_sys_area` VALUES ('370285', '370200', '0,370000,370200,', '370285', '0000370000,0000370200,0000370285,', '1', '2', '山东省/青岛市/莱西市', '莱西市', '3', '0', 'system', '2018-12-03 11:20:31', 'system', '2018-12-03 11:20:31', null);

-- ----------------------------
-- Table structure for js_sys_company
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_company`;
CREATE TABLE `js_sys_company` (
  `company_code` varchar(64) NOT NULL COMMENT '公司编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `view_code` varchar(100) NOT NULL COMMENT '公司代码',
  `company_name` varchar(200) NOT NULL COMMENT '公司名称',
  `full_name` varchar(200) NOT NULL COMMENT '公司全称',
  `area_code` varchar(100) DEFAULT NULL COMMENT '区域编码',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`company_code`),
  KEY `idx_sys_company_cc` (`corp_code`),
  KEY `idx_sys_company_pc` (`parent_code`),
  KEY `idx_sys_company_ts` (`tree_sort`),
  KEY `idx_sys_company_status` (`status`),
  KEY `idx_sys_company_vc` (`view_code`),
  KEY `idx_sys_company_pcs` (`parent_codes`(255)),
  KEY `idx_sys_company_tss` (`tree_sorts`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公司表';

-- ----------------------------
-- Records of js_sys_company
-- ----------------------------
INSERT INTO `js_sys_company` VALUES ('SD', '0', '0,', '40', '0000000040,', '0', '0', '山东公司', 'SD', '山东公司', '山东公司', null, '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_company` VALUES ('SDJN', 'SD', '0,SD,', '30', '0000000040,0000000030,', '1', '1', '山东公司/济南公司', 'SDJN', '济南公司', '山东济南公司', null, '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_company` VALUES ('SDQD', 'SD', '0,SD,', '40', '0000000040,0000000040,', '1', '1', '山东公司/青岛公司', 'SDQD', '青岛公司', '山东青岛公司', null, '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for js_sys_company_office
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_company_office`;
CREATE TABLE `js_sys_company_office` (
  `company_code` varchar(64) NOT NULL COMMENT '公司编码',
  `office_code` varchar(64) NOT NULL COMMENT '机构编码',
  PRIMARY KEY (`company_code`,`office_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公司部门关联表';

-- ----------------------------
-- Records of js_sys_company_office
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_config
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_config`;
CREATE TABLE `js_sys_config` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `config_name` varchar(100) NOT NULL COMMENT '名称',
  `config_key` varchar(100) NOT NULL COMMENT '参数键',
  `config_value` varchar(1000) DEFAULT NULL COMMENT '参数值',
  `is_sys` char(1) NOT NULL COMMENT '系统内置（1是 0否）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_sys_config_key` (`config_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='参数配置表';

-- ----------------------------
-- Records of js_sys_config
-- ----------------------------
INSERT INTO `js_sys_config` VALUES ('1069431155850555392', '研发工具-代码生成默认包名', 'gen.defaultPackageName', 'com.jeesite.modules', '0', 'system', '2018-12-03 11:20:31', 'system', '2018-12-03 11:20:31', '新建项目后，修改该键值，在生成代码的时候就不要再修改了');
INSERT INTO `js_sys_config` VALUES ('1069431156207071232', '主框架页-桌面仪表盘首页地址', 'sys.index.desktopUrl', '/desktop', '0', 'system', '2018-12-03 11:20:31', 'system', '2018-12-03 11:20:31', '主页面的第一个页签首页桌面地址');
INSERT INTO `js_sys_config` VALUES ('1069431156848799744', '主框架页-侧边栏的默认显示样式', 'sys.index.sidebarStyle', '1', '0', 'system', '2018-12-03 11:20:32', 'system', '2018-12-03 11:20:32', '1：默认显示侧边栏；2：默认折叠侧边栏');
INSERT INTO `js_sys_config` VALUES ('1069431157607968768', '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue-light', '0', 'system', '2018-12-03 11:20:32', 'system', '2018-12-03 11:20:32', 'skin-black-light、skin-black、skin-blue-light、skin-blue、skin-green-light、skin-green、skin-purple-light、skin-purple、skin-red-light、skin-red、skin-yellow-light、skin-yellow');
INSERT INTO `js_sys_config` VALUES ('1069431157884792832', '用户登录-登录失败多少次数后显示验证码', 'sys.login.failedNumAfterValidCode', '100', '0', 'system', '2018-12-03 11:20:32', 'system', '2018-12-03 11:20:32', '设置为0强制使用验证码登录');
INSERT INTO `js_sys_config` VALUES ('1069431158207754240', '用户登录-登录失败多少次数后锁定账号', 'sys.login.failedNumAfterLockAccount', '200', '0', 'system', '2018-12-03 11:20:32', 'system', '2018-12-03 11:20:32', '登录失败多少次数后锁定账号');
INSERT INTO `js_sys_config` VALUES ('1069431158480384000', '用户登录-登录失败多少次数后锁定账号的时间', 'sys.login.failedNumAfterLockMinute', '20', '0', 'system', '2018-12-03 11:20:32', 'system', '2018-12-03 11:20:32', '锁定账号的时间（单位：分钟）');
INSERT INTO `js_sys_config` VALUES ('1069431158643961856', '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'true', '0', 'system', '2018-12-03 11:20:32', 'system', '2018-12-03 11:20:32', '是否开启注册用户功能');
INSERT INTO `js_sys_config` VALUES ('1069431160183271424', '账号自助-允许自助注册的用户类型', 'sys.account.registerUser.userTypes', '-1', '0', 'system', '2018-12-03 11:20:32', 'system', '2018-12-03 11:20:32', '允许注册的用户类型（多个用逗号隔开，如果注册时不选择用户类型，则第一个为默认类型）');
INSERT INTO `js_sys_config` VALUES ('1069431161038909440', '账号自助-验证码有效时间（分钟）', 'sys.account.validCodeTimeout', '10', '0', 'system', '2018-12-03 11:20:33', 'system', '2018-12-03 11:20:33', '找回密码时，短信/邮件验证码有效时间（单位：分钟，0表示不限制）');
INSERT INTO `js_sys_config` VALUES ('1069431161210875904', '用户管理-账号默认角色-员工类型', 'sys.user.defaultRoleCodes.employee', 'default', '0', 'system', '2018-12-03 11:20:33', 'system', '2018-12-03 11:20:33', '所有员工用户都拥有的角色权限（适用于菜单授权查询）');
INSERT INTO `js_sys_config` VALUES ('1069431161395425280', '用户管理-账号初始密码', 'sys.user.initPassword', '123456', '0', 'system', '2018-12-03 11:20:33', 'system', '2018-12-03 11:20:33', '创建用户和重置密码的时候用户的密码');
INSERT INTO `js_sys_config` VALUES ('1069431161571586048', '用户管理-初始密码修改策略', 'sys.user.initPasswordModify', '1', '0', 'system', '2018-12-03 11:20:33', 'system', '2018-12-03 11:20:33', '0：初始密码修改策略关闭，没有任何提示；1：提醒用户，如果未修改初始密码，则在登录时和点击菜单就会提醒修改密码对话框；2：强制实行初始密码修改，登录后若不修改密码则不能进行系统操作');
INSERT INTO `js_sys_config` VALUES ('1069431161739358208', '用户管理-账号密码修改策略', 'sys.user.passwordModify', '1', '0', 'system', '2018-12-03 11:20:33', 'system', '2018-12-03 11:20:33', '0：密码修改策略关闭，没有任何提示；1：提醒用户，如果未修改初始密码，则在登录时和点击菜单就会提醒修改密码对话框；2：强制实行初始密码修改，登录后若不修改密码则不能进行系统操作。');
INSERT INTO `js_sys_config` VALUES ('1069431162204925952', '用户管理-账号密码修改策略验证周期', 'sys.user.passwordModifyCycle', '30', '0', 'system', '2018-12-03 11:20:33', 'system', '2018-12-03 11:20:33', '密码安全修改周期是指定时间内提醒或必须修改密码（例如设置30天）的验证时间（天），超过这个时间登录系统时需，提醒用户修改密码或强制修改密码才能继续使用系统。单位：天，如果设置30天，则第31天开始强制修改密码');
INSERT INTO `js_sys_config` VALUES ('1069431162376892416', '用户管理-密码修改多少次内不允许重复', 'sys.user.passwordModifyNotRepeatNum', '1', '0', 'system', '2018-12-03 11:20:33', 'system', '2018-12-03 11:20:33', '默认1次，表示不能与上次密码重复；若设置为3，表示并与前3次密码重复');
INSERT INTO `js_sys_config` VALUES ('1069431162641133568', '用户管理-账号密码修改最低安全等级', 'sys.user.passwordModifySecurityLevel', '0', '0', 'system', '2018-12-03 11:20:33', 'system', '2018-12-03 11:20:33', '0：不限制等级（用户在修改密码的时候不进行等级验证）\r；1：限制最低等级为很弱\r；2：限制最低等级为弱\r；3：限制最低等级为安全\r；4：限制最低等级为很安全');
INSERT INTO `js_sys_config` VALUES ('1092344803460943872', '主框架页-主导航菜单显示风格', 'sys.index.menuStyle', '1', '0', 'system', '2019-08-30 11:44:41', 'system', '2019-08-30 11:44:41', '1：菜单全部在左侧；2：根菜单显示在顶部');

-- ----------------------------
-- Table structure for js_sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_dict_data`;
CREATE TABLE `js_sys_dict_data` (
  `dict_code` varchar(64) NOT NULL COMMENT '字典编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `dict_label` varchar(100) NOT NULL COMMENT '字典标签',
  `dict_value` varchar(100) NOT NULL COMMENT '字典键值',
  `dict_type` varchar(100) NOT NULL COMMENT '字典类型',
  `is_sys` char(1) NOT NULL COMMENT '系统内置（1是 0否）',
  `description` varchar(500) DEFAULT NULL COMMENT '字典描述',
  `css_style` varchar(500) DEFAULT NULL COMMENT 'css样式（如：color:red)',
  `css_class` varchar(500) DEFAULT NULL COMMENT 'css类名（如：red）',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`dict_code`),
  KEY `idx_sys_dict_data_cc` (`corp_code`),
  KEY `idx_sys_dict_data_dt` (`dict_type`),
  KEY `idx_sys_dict_data_pc` (`parent_code`),
  KEY `idx_sys_dict_data_status` (`status`),
  KEY `idx_sys_dict_data_pcs` (`parent_codes`(255)),
  KEY `idx_sys_dict_data_ts` (`tree_sort`),
  KEY `idx_sys_dict_data_tss` (`tree_sorts`(255)),
  KEY `idx_sys_dict_data_dv` (`dict_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典数据表';

-- ----------------------------
-- Records of js_sys_dict_data
-- ----------------------------
INSERT INTO `js_sys_dict_data` VALUES ('1069431175337291776', '0', '0,', '30', '0000000030,', '1', '0', '是', '是', '1', 'sys_yes_no', '1', '', '', '', '0', 'system', '2018-12-03 11:20:36', 'system', '2018-12-03 11:20:36', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431176478142464', '0', '0,', '40', '0000000040,', '1', '0', '否', '否', '0', 'sys_yes_no', '1', '', 'color:#aaa;', '', '0', 'system', '2018-12-03 11:20:36', 'system', '2018-12-03 11:20:36', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431177656741888', '0', '0,', '20', '0000000020,', '1', '0', '正常', '正常', '0', 'sys_status', '1', '', '', '', '0', 'system', '2018-12-03 11:20:36', 'system', '2018-12-03 11:20:37', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431178235555840', '0', '0,', '30', '0000000030,', '1', '0', '删除', '删除', '1', 'sys_status', '1', '', 'color:#f00;', '', '0', 'system', '2018-12-03 11:20:37', 'system', '2018-12-03 11:20:37', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431181108654080', '0', '0,', '40', '0000000040,', '1', '0', '停用', '停用', '2', 'sys_status', '1', '', 'color:#f00;', '', '0', 'system', '2018-12-03 11:20:37', 'system', '2018-12-03 11:20:37', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431181255454720', '0', '0,', '50', '0000000050,', '1', '0', '冻结', '冻结', '3', 'sys_status', '1', '', 'color:#fa0;', '', '0', 'system', '2018-12-03 11:20:37', 'system', '2018-12-03 11:20:38', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431182945759232', '0', '0,', '60', '0000000060,', '1', '0', '待审', '待审', '4', 'sys_status', '1', '', '', '', '0', 'system', '2018-12-03 11:20:38', 'system', '2018-12-03 11:20:38', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431183151280128', '0', '0,', '70', '0000000070,', '1', '0', '驳回', '驳回', '5', 'sys_status', '1', '', '', '', '0', 'system', '2018-12-03 11:20:38', 'system', '2018-12-03 11:20:38', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431185470730240', '0', '0,', '80', '0000000080,', '1', '0', '草稿', '草稿', '9', 'sys_status', '1', '', 'color:#aaa;', '', '0', 'system', '2018-12-03 11:20:38', 'system', '2018-12-03 11:20:38', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431185986629632', '0', '0,', '30', '0000000030,', '1', '0', '显示', '显示', '1', 'sys_show_hide', '1', '', '', '', '0', 'system', '2018-12-03 11:20:38', 'system', '2018-12-03 11:20:39', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431187806957568', '0', '0,', '40', '0000000040,', '1', '0', '隐藏', '隐藏', '0', 'sys_show_hide', '1', '', 'color:#aaa;', '', '0', 'system', '2018-12-03 11:20:39', 'system', '2018-12-03 11:20:39', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431189811834880', '0', '0,', '30', '0000000030,', '1', '0', '简体中文', '简体中文', 'zh_CN', 'sys_lang_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:39', 'system', '2018-12-03 11:20:39', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431189992189952', '0', '0,', '40', '0000000040,', '1', '0', '英语', '英语', 'en', 'sys_lang_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:39', 'system', '2018-12-03 11:20:40', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431191900598272', '0', '0,', '30', '0000000030,', '1', '0', 'PC电脑', 'PC电脑', 'pc', 'sys_device_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:40', 'system', '2018-12-03 11:20:40', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431192131284992', '0', '0,', '40', '0000000040,', '1', '0', '手机APP', '手机APP', 'mobileApp', 'sys_device_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:40', 'system', '2018-12-03 11:20:40', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431194186493952', '0', '0,', '50', '0000000050,', '1', '0', '手机Web', '手机Web', 'mobileWeb', 'sys_device_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:40', 'system', '2018-12-03 11:20:40', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431194324905984', '0', '0,', '60', '0000000060,', '1', '0', '微信设备', '微信设备', 'weixin', 'sys_device_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:40', 'system', '2018-12-03 11:20:40', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431194480095232', '0', '0,', '30', '0000000030,', '1', '0', '主导航菜单', '主导航菜单', 'default', 'sys_menu_sys_code', '1', '', '', '', '0', 'system', '2018-12-03 11:20:40', 'system', '2018-12-03 11:20:41', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431196778573824', '0', '0,', '30', '0000000030,', '1', '0', '菜单', '菜单', '1', 'sys_menu_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:41', 'system', '2018-12-03 11:20:41', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431196916985856', '0', '0,', '40', '0000000040,', '1', '0', '权限', '权限', '2', 'sys_menu_type', '1', '', 'color:#c243d6;', '', '0', 'system', '2018-12-03 11:20:41', 'system', '2018-12-03 11:20:41', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431198393380864', '0', '0,', '30', '0000000030,', '1', '0', '默认权重', '默认权重', '20', 'sys_menu_weight', '1', '', '', '', '0', 'system', '2018-12-03 11:20:41', 'system', '2018-12-03 11:20:41', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431198594707456', '0', '0,', '40', '0000000040,', '1', '0', '二级管理员', '二级管理员', '40', 'sys_menu_weight', '1', '', '', '', '0', 'system', '2018-12-03 11:20:41', 'system', '2018-12-03 11:20:42', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431200297594880', '0', '0,', '50', '0000000050,', '1', '0', '系统管理员', '系统管理员', '60', 'sys_menu_weight', '1', '', '', '', '0', 'system', '2018-12-03 11:20:42', 'system', '2018-12-03 11:20:42', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431200461172736', '0', '0,', '60', '0000000060,', '1', '0', '超级管理员', '超级管理员', '80', 'sys_menu_weight', '1', '', 'color:#c243d6;', '', '0', 'system', '2018-12-03 11:20:42', 'system', '2018-12-03 11:20:42', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431201664937984', '0', '0,', '30', '0000000030,', '1', '0', '国家', '国家', '0', 'sys_area_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:42', 'system', '2018-12-03 11:20:42', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431202042425344', '0', '0,', '40', '0000000040,', '1', '0', '省份直辖市', '省份直辖市', '1', 'sys_area_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:42', 'system', '2018-12-03 11:20:42', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431202235363328', '0', '0,', '50', '0000000050,', '1', '0', '地市', '地市', '2', 'sys_area_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:42', 'system', '2018-12-03 11:20:43', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431203975999488', '0', '0,', '60', '0000000060,', '1', '0', '区县', '区县', '3', 'sys_area_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:43', 'system', '2018-12-03 11:20:43', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431206282866688', '0', '0,', '30', '0000000030,', '1', '0', '省级公司', '省级公司', '1', 'sys_office_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:43', 'system', '2018-12-03 11:20:43', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431206396112896', '0', '0,', '40', '0000000040,', '1', '0', '市级公司', '市级公司', '2', 'sys_office_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:43', 'system', '2018-12-03 11:20:44', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431208061251584', '0', '0,', '50', '0000000050,', '1', '0', '部门', '部门', '3', 'sys_office_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:44', 'system', '2018-12-03 11:20:44', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431208203857920', '0', '0,', '30', '0000000030,', '1', '0', '正常', '正常', '0', 'sys_search_status', '1', '', '', '', '0', 'system', '2018-12-03 11:20:44', 'system', '2018-12-03 11:20:44', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431209806082048', '0', '0,', '40', '0000000040,', '1', '0', '停用', '停用', '2', 'sys_search_status', '1', '', 'color:#f00;', '', '0', 'system', '2018-12-03 11:20:44', 'system', '2018-12-03 11:20:44', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431209936105472', '0', '0,', '30', '0000000030,', '1', '0', '男', '男', '1', 'sys_user_sex', '1', '', '', '', '0', 'system', '2018-12-03 11:20:44', 'system', '2018-12-03 11:20:44', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431210074517504', '0', '0,', '40', '0000000040,', '1', '0', '女', '女', '2', 'sys_user_sex', '1', '', '', '', '0', 'system', '2018-12-03 11:20:44', 'system', '2018-12-03 11:20:44', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431210460393472', '0', '0,', '30', '0000000030,', '1', '0', '正常', '正常', '0', 'sys_user_status', '1', '', '', '', '0', 'system', '2018-12-03 11:20:45', 'system', '2018-12-03 11:20:45', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431212784037888', '0', '0,', '40', '0000000040,', '1', '0', '停用', '停用', '2', 'sys_user_status', '1', '', 'color:#f00;', '', '0', 'system', '2018-12-03 11:20:45', 'system', '2018-12-03 11:20:45', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431214373679104', '0', '0,', '50', '0000000050,', '1', '0', '冻结', '冻结', '3', 'sys_user_status', '1', '', 'color:#fa0;', '', '0', 'system', '2018-12-03 11:20:45', 'system', '2018-12-03 11:20:45', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431214558228480', '0', '0,', '30', '0000000030,', '1', '0', '员工', '员工', 'employee', 'sys_user_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:45', 'system', '2018-12-03 11:20:46', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431216013651968', '0', '0,', '40', '0000000040,', '1', '0', '会员', '会员', 'member', 'sys_user_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:46', 'system', '2018-12-03 11:20:46', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431216139481088', '0', '0,', '50', '0000000050,', '1', '0', '单位', '单位', 'btype', 'sys_user_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:46', 'system', '2018-12-03 11:20:46', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431217397772288', '0', '0,', '60', '0000000060,', '1', '0', '个人', '个人', 'persion', 'sys_user_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:46', 'system', '2018-12-03 11:20:46', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431217557155840', '0', '0,', '70', '0000000070,', '1', '0', '专家', '专家', 'expert', 'sys_user_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:46', 'system', '2018-12-03 11:20:46', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431218530234368', '0', '0,', '30', '0000000030,', '1', '0', '高管', '高管', '1', 'sys_role_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:46', 'system', '2018-12-03 11:20:46', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431218664452096', '0', '0,', '40', '0000000040,', '1', '0', '中层', '中层', '2', 'sys_role_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:47', 'system', '2018-12-03 11:20:47', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431219926937600', '0', '0,', '50', '0000000050,', '1', '0', '基层', '基层', '3', 'sys_role_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:47', 'system', '2018-12-03 11:20:47', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431221810180096', '0', '0,', '60', '0000000060,', '1', '0', '其它', '其它', '4', 'sys_role_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:47', 'system', '2018-12-03 11:20:47', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431222120558592', '0', '0,', '30', '0000000030,', '1', '0', '未设置', '未设置', '0', 'sys_role_data_scope', '1', '', '', '', '0', 'system', '2018-12-03 11:20:47', 'system', '2018-12-03 11:20:47', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431222569349120', '0', '0,', '40', '0000000040,', '1', '0', '全部数据', '全部数据', '1', 'sys_role_data_scope', '1', '', '', '', '0', 'system', '2018-12-03 11:20:47', 'system', '2018-12-03 11:20:48', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431226046427136', '0', '0,', '50', '0000000050,', '1', '0', '自定义数据', '自定义数据', '2', 'sys_role_data_scope', '1', '', '', '', '0', 'system', '2018-12-03 11:20:48', 'system', '2018-12-03 11:20:48', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431226210004992', '0', '0,', '60', '0000000060,', '1', '0', '本部门数据', '本部门数据', '3', 'sys_role_data_scope', '1', '', '', '', '0', 'system', '2018-12-03 11:20:48', 'system', '2018-12-03 11:20:49', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431229066326016', '0', '0,', '70', '0000000070,', '1', '0', '本公司数据', '本公司数据', '4', 'sys_role_data_scope', '1', '', '', '', '0', 'system', '2018-12-03 11:20:49', 'system', '2018-12-03 11:20:49', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431229229903872', '0', '0,', '80', '0000000080,', '1', '0', '本部门和本公司数据', '本部门和本公司数据', '5', 'sys_role_data_scope', '1', '', '', '', '0', 'system', '2018-12-03 11:20:49', 'system', '2018-12-03 11:20:49', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431229355732992', '0', '0,', '30', '0000000030,', '1', '0', '高管', '高管', '1', 'sys_post_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:49', 'system', '2018-12-03 11:20:49', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431231725514752', '0', '0,', '40', '0000000040,', '1', '0', '中层', '中层', '2', 'sys_post_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:49', 'system', '2018-12-03 11:20:49', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431232086224896', '0', '0,', '50', '0000000050,', '1', '0', '基层', '基层', '3', 'sys_post_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:49', 'system', '2018-12-03 11:20:50', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431233248047104', '0', '0,', '60', '0000000060,', '1', '0', '其它', '其它', '4', 'sys_post_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:50', 'system', '2018-12-03 11:20:50', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431233399042048', '0', '0,', '30', '0000000030,', '1', '0', '接入日志', '接入日志', 'access', 'sys_log_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:50', 'system', '2018-12-03 11:20:50', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431234426646528', '0', '0,', '40', '0000000040,', '1', '0', '修改日志', '修改日志', 'update', 'sys_log_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:50', 'system', '2018-12-03 11:20:50', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431235693326336', '0', '0,', '50', '0000000050,', '1', '0', '查询日志', '查询日志', 'select', 'sys_log_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:50', 'system', '2018-12-03 11:20:50', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431235861098496', '0', '0,', '60', '0000000060,', '1', '0', '登录登出', '登录登出', 'loginLogout', 'sys_log_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:50', 'system', '2018-12-03 11:20:51', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431237660454912', '0', '0,', '30', '0000000030,', '1', '0', '默认', '默认', 'DEFAULT', 'sys_job_group', '1', '', '', '', '0', 'system', '2018-12-03 11:20:51', 'system', '2018-12-03 11:20:51', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431237811449856', '0', '0,', '40', '0000000040,', '1', '0', '系统', '系统', 'SYSTEM', 'sys_job_group', '1', '', '', '', '0', 'system', '2018-12-03 11:20:51', 'system', '2018-12-03 11:20:51', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431239774384128', '0', '0,', '30', '0000000030,', '1', '0', '错过计划等待本次计划完成后立即执行一次', '错过计划等待本次计划完成后立即执行一次', '1', 'sys_job_misfire_instruction', '1', '', '', '', '0', 'system', '2018-12-03 11:20:51', 'system', '2018-12-03 11:20:51', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431239916990464', '0', '0,', '40', '0000000040,', '1', '0', '本次执行时间根据上次结束时间重新计算（时间间隔方式）', '本次执行时间根据上次结束时间重新计算（时间间隔方式）', '2', 'sys_job_misfire_instruction', '1', '', '', '', '0', 'system', '2018-12-03 11:20:51', 'system', '2018-12-03 11:20:51', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431240063791104', '0', '0,', '30', '0000000030,', '1', '0', '正常', '正常', '0', 'sys_job_status', '1', '', '', '', '0', 'system', '2018-12-03 11:20:51', 'system', '2018-12-03 11:20:52', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431242525847552', '0', '0,', '40', '0000000040,', '1', '0', '删除', '删除', '1', 'sys_job_status', '1', '', '', '', '0', 'system', '2018-12-03 11:20:52', 'system', '2018-12-03 11:20:52', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431242672648192', '0', '0,', '50', '0000000050,', '1', '0', '暂停', '暂停', '2', 'sys_job_status', '1', '', 'color:#f00;', '', '0', 'system', '2018-12-03 11:20:52', 'system', '2018-12-03 11:20:52', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431242819448832', '0', '0,', '30', '0000000030,', '1', '0', '完成', '完成', '3', 'sys_job_status', '1', '', '', '', '0', 'system', '2018-12-03 11:20:52', 'system', '2018-12-03 11:20:53', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431245273116672', '0', '0,', '40', '0000000040,', '1', '0', '错误', '错误', '4', 'sys_job_status', '1', '', 'color:#f00;', '', '0', 'system', '2018-12-03 11:20:53', 'system', '2018-12-03 11:20:53', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431245424111616', '0', '0,', '50', '0000000050,', '1', '0', '运行', '运行', '5', 'sys_job_status', '1', '', '', '', '0', 'system', '2018-12-03 11:20:53', 'system', '2018-12-03 11:20:53', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431248230100992', '0', '0,', '30', '0000000030,', '1', '0', '计划日志', '计划日志', 'scheduler', 'sys_job_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:53', 'system', '2018-12-03 11:20:53', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431248406261760', '0', '0,', '40', '0000000040,', '1', '0', '任务日志', '任务日志', 'job', 'sys_job_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:53', 'system', '2018-12-03 11:20:54', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431249614221312', '0', '0,', '50', '0000000050,', '1', '0', '触发日志', '触发日志', 'trigger', 'sys_job_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:54', 'system', '2018-12-03 11:20:54', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431249798770688', '0', '0,', '30', '0000000030,', '1', '0', '计划创建', '计划创建', 'jobScheduled', 'sys_job_event', '1', '', '', '', '0', 'system', '2018-12-03 11:20:54', 'system', '2018-12-03 11:20:54', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431251090616320', '0', '0,', '40', '0000000040,', '1', '0', '计划移除', '计划移除', 'jobUnscheduled', 'sys_job_event', '1', '', '', '', '0', 'system', '2018-12-03 11:20:54', 'system', '2018-12-03 11:20:54', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431252843835392', '0', '0,', '50', '0000000050,', '1', '0', '计划暂停', '计划暂停', 'triggerPaused', 'sys_job_event', '1', '', '', '', '0', 'system', '2018-12-03 11:20:54', 'system', '2018-12-03 11:20:54', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431253003218944', '0', '0,', '60', '0000000060,', '1', '0', '计划恢复', '计划恢复', 'triggerResumed', 'sys_job_event', '1', '', '', '', '0', 'system', '2018-12-03 11:20:54', 'system', '2018-12-03 11:20:54', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431253154213888', '0', '0,', '70', '0000000070,', '1', '0', '调度错误', '调度错误', 'schedulerError', 'sys_job_event', '1', '', '', '', '0', 'system', '2018-12-03 11:20:54', 'system', '2018-12-03 11:20:55', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431255511412736', '0', '0,', '80', '0000000080,', '1', '0', '任务执行', '任务执行', 'jobToBeExecuted', 'sys_job_event', '1', '', '', '', '0', 'system', '2018-12-03 11:20:55', 'system', '2018-12-03 11:20:55', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431257872805888', '0', '0,', '90', '0000000090,', '1', '0', '任务结束', '任务结束', 'jobWasExecuted', 'sys_job_event', '1', '', '', '', '0', 'system', '2018-12-03 11:20:56', 'system', '2018-12-03 11:20:56', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431258002829312', '0', '0,', '100', '0000000100,', '1', '0', '任务停止', '任务停止', 'jobExecutionVetoed', 'sys_job_event', '1', '', '', '', '0', 'system', '2018-12-03 11:20:56', 'system', '2018-12-03 11:20:56', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431259902849024', '0', '0,', '110', '0000000110,', '1', '0', '触发计划', '触发计划', 'triggerFired', 'sys_job_event', '1', '', '', '', '0', 'system', '2018-12-03 11:20:56', 'system', '2018-12-03 11:20:56', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431260024483840', '0', '0,', '120', '0000000120,', '1', '0', '触发验证', '触发验证', 'vetoJobExecution', 'sys_job_event', '1', '', '', '', '0', 'system', '2018-12-03 11:20:56', 'system', '2018-12-03 11:20:57', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431262218104832', '0', '0,', '130', '0000000130,', '1', '0', '触发完成', '触发完成', 'triggerComplete', 'sys_job_event', '1', '', '', '', '0', 'system', '2018-12-03 11:20:57', 'system', '2018-12-03 11:20:57', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431262578814976', '0', '0,', '140', '0000000140,', '1', '0', '触发错过', '触发错过', 'triggerMisfired', 'sys_job_event', '1', '', '', '', '0', 'system', '2018-12-03 11:20:57', 'system', '2018-12-03 11:20:57', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431263904215040', '0', '0,', '30', '0000000030,', '1', '0', 'PC', 'PC', 'pc', 'sys_msg_type', '1', '消息类型', '', '', '0', 'system', '2018-12-03 11:20:57', 'system', '2018-12-03 11:20:57', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431264113930240', '0', '0,', '40', '0000000040,', '1', '0', 'APP', 'APP', 'app', 'sys_msg_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:57', 'system', '2018-12-03 11:20:57', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431265162506240', '0', '0,', '50', '0000000050,', '1', '0', '短信', '短信', 'sms', 'sys_msg_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:57', 'system', '2018-12-03 11:20:58', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431266265608192', '0', '0,', '60', '0000000060,', '1', '0', '邮件', '邮件', 'email', 'sys_msg_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:58', 'system', '2018-12-03 11:20:58', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431266412408832', '0', '0,', '70', '0000000070,', '1', '0', '微信', '微信', 'weixin', 'sys_msg_type', '1', '', '', '', '0', 'system', '2018-12-03 11:20:58', 'system', '2018-12-03 11:20:58', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431267993661440', '0', '0,', '30', '0000000030,', '1', '0', '待推送', '待推送', '0', 'sys_msg_push_status', '1', '推送状态', '', '', '0', 'system', '2018-12-03 11:20:58', 'system', '2018-12-03 11:20:58', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431268140462080', '0', '0,', '30', '0000000030,', '1', '0', '成功', '成功', '1', 'sys_msg_push_status', '1', '', '', '', '0', 'system', '2018-12-03 11:20:58', 'system', '2018-12-03 11:20:58', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431269558136832', '0', '0,', '40', '0000000040,', '1', '0', '失败', '失败', '2', 'sys_msg_push_status', '1', '', 'color:#f00;', '', '0', 'system', '2018-12-03 11:20:58', 'system', '2018-12-03 11:20:58', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431269679771648', '0', '0,', '30', '0000000030,', '1', '0', '未送达', '未送达', '0', 'sys_msg_read_status', '1', '读取状态', '', '', '0', 'system', '2018-12-03 11:20:58', 'system', '2018-12-03 11:20:59', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431271005171712', '0', '0,', '40', '0000000040,', '1', '0', '已读', '已读', '1', 'sys_msg_read_status', '1', '', '', '', '0', 'system', '2018-12-03 11:20:59', 'system', '2018-12-03 11:20:59', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431271118417920', '0', '0,', '50', '0000000050,', '1', '0', '未读', '未读', '2', 'sys_msg_read_status', '1', '', '', '', '0', 'system', '2018-12-03 11:20:59', 'system', '2018-12-03 11:20:59', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431272104079360', '0', '0,', '30', '0000000030,', '1', '0', '普通', '普通', '1', 'msg_inner_content_level', '1', '内容级别', '', '', '0', 'system', '2018-12-03 11:20:59', 'system', '2018-12-03 11:21:00', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431274813599744', '0', '0,', '40', '0000000040,', '1', '0', '一般', '一般', '2', 'msg_inner_content_level', '1', '', '', '', '0', 'system', '2018-12-03 11:21:00', 'system', '2018-12-03 11:21:00', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431274972983296', '0', '0,', '50', '0000000050,', '1', '0', '紧急', '紧急', '3', 'msg_inner_content_level', '1', '', 'color:#f00;', '', '0', 'system', '2018-12-03 11:21:00', 'system', '2018-12-03 11:21:00', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431277019803648', '0', '0,', '30', '0000000030,', '1', '0', '公告', '公告', '1', 'msg_inner_content_type', '1', '内容类型', '', '', '0', 'system', '2018-12-03 11:21:00', 'system', '2018-12-03 11:21:00', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431277137244160', '0', '0,', '40', '0000000040,', '1', '0', '新闻', '新闻', '2', 'msg_inner_content_type', '1', '', '', '', '0', 'system', '2018-12-03 11:21:00', 'system', '2018-12-03 11:21:01', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431278680748032', '0', '0,', '50', '0000000050,', '1', '0', '会议', '会议', '3', 'msg_inner_content_type', '1', '', '', '', '0', 'system', '2018-12-03 11:21:01', 'system', '2018-12-03 11:21:01', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431278944989184', '0', '0,', '60', '0000000060,', '1', '0', '其它', '其它', '4', 'msg_inner_content_type', '1', '', '', '', '0', 'system', '2018-12-03 11:21:01', 'system', '2018-12-03 11:21:01', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431280341692416', '0', '0,', '30', '0000000030,', '1', '0', '用户', '用户', '1', 'msg_inner_receiver_type', '1', '接受类型', '', '', '0', 'system', '2018-12-03 11:21:01', 'system', '2018-12-03 11:21:01', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431280522047488', '0', '0,', '40', '0000000040,', '1', '0', '部门', '部门', '2', 'msg_inner_receiver_type', '1', '', '', '', '0', 'system', '2018-12-03 11:21:01', 'system', '2018-12-03 11:21:01', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431281809698816', '0', '0,', '50', '0000000050,', '1', '0', '角色', '角色', '3', 'msg_inner_receiver_type', '1', '', '', '', '0', 'system', '2018-12-03 11:21:01', 'system', '2018-12-03 11:21:01', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431282027802624', '0', '0,', '60', '0000000060,', '1', '0', '岗位', '岗位', '4', 'msg_inner_receiver_type', '1', '', '', '', '0', 'system', '2018-12-03 11:21:02', 'system', '2018-12-03 11:21:02', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431283508391936', '0', '0,', '30', '0000000030,', '1', '0', '正常', '正常', '0', 'msg_inner_msg_status', '1', '消息状态', '', '', '0', 'system', '2018-12-03 11:21:02', 'system', '2018-12-03 11:21:02', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431286138220544', '0', '0,', '40', '0000000040,', '1', '0', '删除', '删除', '1', 'msg_inner_msg_status', '1', '', '', '', '0', 'system', '2018-12-03 11:21:02', 'system', '2018-12-03 11:21:02', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431286268243968', '0', '0,', '50', '0000000050,', '1', '0', '审核', '审核', '4', 'msg_inner_msg_status', '1', '', '', '', '0', 'system', '2018-12-03 11:21:02', 'system', '2018-12-03 11:21:03', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431288113737728', '0', '0,', '60', '0000000060,', '1', '0', '驳回', '驳回', '5', 'msg_inner_msg_status', '1', '', 'color:#f00;', '', '0', 'system', '2018-12-03 11:21:03', 'system', '2018-12-03 11:21:03', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1069431288495419392', '0', '0,', '70', '0000000070,', '1', '0', '草稿', '草稿', '9', 'msg_inner_msg_status', '1', '', '', '', '0', 'system', '2018-12-03 11:21:03', 'system', '2018-12-03 11:21:03', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1106135527342673920', '0', '0,', '20', '0000000020,', '1', '0', '全部', '全部', '0', 'msg_inner_receiver_type', '1', '', '', '', '0', 'system', '2019-08-30 11:44:41', 'system', '2019-08-30 11:44:41', '', '0', 'JeeSite', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1119900365974118400', '0', '0,', '30', '0000000030,', '1', '0', '公共文件柜', '公共文件柜', 'global', 'filemanager_group_type', '1', '文件组类型', '', '', '0', 'system', '2020-05-06 16:13:43', 'system', '2020-05-06 16:13:43', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1119900366011867136', '0', '0,', '40', '0000000040,', '1', '0', '个人文件柜', '个人文件柜', 'self', 'filemanager_group_type', '1', '', '', '', '0', 'system', '2020-05-06 16:13:43', 'system', '2020-05-06 16:13:43', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1119900366058004480', '0', '0,', '50', '0000000050,', '1', '0', '部门文件柜', '部门文件柜', 'office', 'filemanager_group_type', '1', '', '', '', '0', 'system', '2020-05-06 16:13:43', 'system', '2020-05-06 16:13:43', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1126375062364020736', '0', '0,', '60', '0000000060,', '1', '0', '日本語', '日本語', 'ja_JP', 'sys_lang_type', '1', '', '', '', '0', 'system', '2019-08-30 11:44:41', 'system', '2019-08-30 11:44:41', '', '0', 'JeeSite', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_dict_data` VALUES ('1149344606834356224', '0', '0,', '30', '0000000030,', '1', '0', '组织管理', '组织管理', 'office_user', 'sys_role_biz_scope', '1', '', '', '', '0', 'system', '2019-08-30 11:44:41', 'system', '2019-08-30 11:44:41', '', '0', 'JeeSite', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for js_sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_dict_type`;
CREATE TABLE `js_sys_dict_type` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `dict_name` varchar(100) NOT NULL COMMENT '字典名称',
  `dict_type` varchar(100) NOT NULL COMMENT '字典类型',
  `is_sys` char(1) NOT NULL COMMENT '是否系统字典',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_dict_type_is` (`is_sys`),
  KEY `idx_sys_dict_type_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典类型表';

-- ----------------------------
-- Records of js_sys_dict_type
-- ----------------------------
INSERT INTO `js_sys_dict_type` VALUES ('1069431166361481216', '是否', 'sys_yes_no', '1', '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:34', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431166562807808', '状态', 'sys_status', '1', '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:34', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431166692831232', '显示隐藏', 'sys_show_hide', '1', '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:34', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431166814466048', '国际化语言类型', 'sys_lang_type', '1', '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:34', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431166894157824', '客户端设备类型', 'sys_device_type', '1', '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:34', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431166965460992', '菜单归属系统', 'sys_menu_sys_code', '1', '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:34', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431167040958464', '菜单类型', 'sys_menu_type', '1', '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:34', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431167783350272', '菜单权重', 'sys_menu_weight', '1', '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:34', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431167842070528', '区域类型', 'sys_area_type', '1', '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:34', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431167909179392', '机构类型', 'sys_office_type', '1', '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:34', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431168001454080', '查询状态', 'sys_search_status', '1', '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:34', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431168072757248', '用户性别', 'sys_user_sex', '1', '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:34', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431168144060416', '用户状态', 'sys_user_status', '1', '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:35', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431169972776960', '用户类型', 'sys_user_type', '1', '0', 'system', '2018-12-03 11:20:35', 'system', '2018-12-03 11:20:35', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431170245406720', '角色分类', 'sys_role_type', '1', '0', 'system', '2018-12-03 11:20:35', 'system', '2018-12-03 11:20:35', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431170778083328', '角色数据范围', 'sys_role_data_scope', '1', '0', 'system', '2018-12-03 11:20:35', 'system', '2018-12-03 11:20:35', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431171088461824', '岗位分类', 'sys_post_type', '1', '0', 'system', '2018-12-03 11:20:35', 'system', '2018-12-03 11:20:35', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431171218485248', '日志类型', 'sys_log_type', '1', '0', 'system', '2018-12-03 11:20:35', 'system', '2018-12-03 11:20:35', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431171314954240', '作业分组', 'sys_job_group', '1', '0', 'system', '2018-12-03 11:20:35', 'system', '2018-12-03 11:20:35', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431171386257408', '作业错过策略', 'sys_job_misfire_instruction', '1', '0', 'system', '2018-12-03 11:20:35', 'system', '2018-12-03 11:20:35', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431173412106240', '作业状态', 'sys_job_status', '1', '0', 'system', '2018-12-03 11:20:35', 'system', '2018-12-03 11:20:35', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431173487603712', '作业任务类型', 'sys_job_type', '1', '0', 'system', '2018-12-03 11:20:35', 'system', '2018-12-03 11:20:36', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431173634404352', '作业任务事件', 'sys_job_event', '1', '0', 'system', '2018-12-03 11:20:36', 'system', '2018-12-03 11:20:36', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431173718290432', '消息类型', 'sys_msg_type', '1', '0', 'system', '2018-12-03 11:20:36', 'system', '2018-12-03 11:20:36', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431173806370816', '推送状态', 'sys_msg_push_status', '1', '0', 'system', '2018-12-03 11:20:36', 'system', '2018-12-03 11:20:36', null);
INSERT INTO `js_sys_dict_type` VALUES ('1069431173898645504', '读取状态', 'sys_msg_read_status', '1', '0', 'system', '2018-12-03 11:20:36', 'system', '2018-12-03 11:20:36', null);
INSERT INTO `js_sys_dict_type` VALUES ('1105440848414543872', '消息状态', 'msg_inner_msg_status', '0', '0', 'system', '2019-08-30 11:44:41', 'system', '2019-08-30 11:44:41', '');
INSERT INTO `js_sys_dict_type` VALUES ('1119900357518401536', '文件组类型', 'filemanager_group_type', '1', '0', 'system', '2020-05-06 16:13:43', 'system', '2020-05-06 16:13:43', null);
INSERT INTO `js_sys_dict_type` VALUES ('1149344200121085952', '角色业务范围', 'sys_role_biz_scope', '1', '0', 'system', '2019-08-30 11:44:41', 'system', '2019-08-30 11:44:41', '');

-- ----------------------------
-- Table structure for js_sys_employee
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_employee`;
CREATE TABLE `js_sys_employee` (
  `emp_code` varchar(64) NOT NULL COMMENT '员工编码',
  `emp_no` varchar(100) DEFAULT NULL COMMENT '员工工号',
  `emp_name` varchar(100) NOT NULL COMMENT '员工姓名',
  `emp_name_en` varchar(100) DEFAULT NULL COMMENT '英文名',
  `office_code` varchar(64) NOT NULL COMMENT '机构编码',
  `office_name` varchar(100) NOT NULL COMMENT '机构名称',
  `company_code` varchar(64) DEFAULT NULL COMMENT '公司编码',
  `company_name` varchar(200) DEFAULT NULL COMMENT '公司名称',
  `status` char(1) NOT NULL COMMENT '状态（0在职 1删除 2离职）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`emp_code`),
  KEY `idx_sys_employee_cco` (`company_code`),
  KEY `idx_sys_employee_cc` (`corp_code`),
  KEY `idx_sys_employee_ud` (`update_date`),
  KEY `idx_sys_employee_oc` (`office_code`),
  KEY `idx_sys_employee_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工表';

-- ----------------------------
-- Records of js_sys_employee
-- ----------------------------
INSERT INTO `js_sys_employee` VALUES ('user10_nw09', null, '用户10', null, 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user11_ibcm', null, '用户11', null, 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user12_8hyj', null, '用户12', null, 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user13_h3sk', null, '用户13', null, 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user14_deqy', null, '用户14', null, 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user15_c5sr', null, '用户15', null, 'SDQD01', '企管部', 'SDQD', '青岛公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user16_pkfs', null, '用户16', null, 'SDQD01', '企管部', 'SDQD', '青岛公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user17_ogfo', null, '用户17', null, 'SDQD01', '企管部', 'SDQD', '青岛公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user18_ycie', null, '用户18', null, 'SDQD02', '财务部', 'SDQD', '青岛公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user19_urge', null, '用户19', null, 'SDQD02', '财务部', 'SDQD', '青岛公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user1_cwme', null, '用户01', null, 'SDJN01', '企管部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user20_e7n4', null, '用户20', null, 'SDQD02', '财务部', 'SDQD', '青岛公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user21_oxfg', null, '用户21', null, 'SDQD03', '研发部', 'SDQD', '青岛公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user22_o5j5', null, '用户22', null, 'SDQD03', '研发部', 'SDQD', '青岛公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user23_ymbm', null, '用户23', null, 'SDQD03', '研发部', 'SDQD', '青岛公司', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user2_fuda', null, '用户02', null, 'SDJN01', '企管部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user3_ll1n', null, '用户03', null, 'SDJN01', '企管部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user4_m1vk', null, '用户04', null, 'SDJN02', '财务部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user5_pwtv', null, '用户05', null, 'SDJN02', '财务部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user6_hpw5', null, '用户06', null, 'SDJN02', '财务部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user7_lxut', null, '用户07', null, 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user8_wp9j', null, '用户08', null, 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user9_twd4', null, '用户09', null, 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');

-- ----------------------------
-- Table structure for js_sys_employee_office
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_employee_office`;
CREATE TABLE `js_sys_employee_office` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `emp_code` varchar(64) NOT NULL COMMENT '员工编码',
  `office_code` varchar(64) NOT NULL COMMENT '机构编码',
  `post_code` varchar(64) DEFAULT NULL COMMENT '岗位编码',
  PRIMARY KEY (`emp_code`,`office_code`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='员工附属机构关系表';

-- ----------------------------
-- Records of js_sys_employee_office
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_employee_post
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_employee_post`;
CREATE TABLE `js_sys_employee_post` (
  `emp_code` varchar(64) NOT NULL COMMENT '员工编码',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  PRIMARY KEY (`emp_code`,`post_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工与岗位关联表';

-- ----------------------------
-- Records of js_sys_employee_post
-- ----------------------------
INSERT INTO `js_sys_employee_post` VALUES ('user10_nw09', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user11_ibcm', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user12_8hyj', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user13_h3sk', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user14_deqy', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user15_c5sr', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user16_pkfs', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user17_ogfo', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user18_ycie', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user19_urge', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user1_cwme', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user20_e7n4', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user21_oxfg', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user22_o5j5', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user23_ymbm', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user2_fuda', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user3_ll1n', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user4_m1vk', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user5_pwtv', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user6_hpw5', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user7_lxut', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user8_wp9j', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user9_twd4', 'user');

-- ----------------------------
-- Table structure for js_sys_file_entity
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_file_entity`;
CREATE TABLE `js_sys_file_entity` (
  `file_id` varchar(64) NOT NULL COMMENT '文件编号',
  `file_md5` varchar(64) NOT NULL COMMENT '文件MD5',
  `file_path` varchar(1000) NOT NULL COMMENT '文件相对路径',
  `file_content_type` varchar(200) NOT NULL COMMENT '文件内容类型',
  `file_extension` varchar(100) NOT NULL COMMENT '文件后缀扩展名',
  `file_size` decimal(31,0) NOT NULL COMMENT '文件大小(单位B)',
  `file_meta` varchar(255) DEFAULT NULL COMMENT '文件信息(JSON格式)',
  PRIMARY KEY (`file_id`),
  KEY `idx_sys_file_entity_md5` (`file_md5`),
  KEY `idx_sys_file_entity_size` (`file_size`),
  KEY `file_md5` (`file_md5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件实体表';

-- ----------------------------
-- Records of js_sys_file_entity
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_file_upload
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_file_upload`;
CREATE TABLE `js_sys_file_upload` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `file_id` varchar(64) NOT NULL COMMENT '文件编号',
  `file_name` varchar(500) NOT NULL COMMENT '文件名称',
  `file_type` varchar(20) NOT NULL COMMENT '文件分类（image、media、file）',
  `file_sort` decimal(10,0) DEFAULT NULL COMMENT '文件排序',
  `biz_key` varchar(64) DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) DEFAULT NULL COMMENT '业务类型',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_file_biz_ft` (`file_type`),
  KEY `idx_sys_file_biz_fi` (`file_id`),
  KEY `idx_sys_file_biz_status` (`status`),
  KEY `idx_sys_file_biz_cb` (`create_by`),
  KEY `idx_sys_file_biz_ud` (`update_date`),
  KEY `idx_sys_file_biz_bt` (`biz_type`),
  KEY `idx_sys_file_biz_bk` (`biz_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件上传表';

-- ----------------------------
-- Records of js_sys_file_upload
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_job
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_job`;
CREATE TABLE `js_sys_job` (
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `description` varchar(100) NOT NULL COMMENT '任务描述',
  `invoke_target` varchar(1000) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) NOT NULL COMMENT 'Cron执行表达式',
  `misfire_instruction` decimal(1,0) NOT NULL COMMENT '计划执行错误策略',
  `concurrent` char(1) NOT NULL COMMENT '是否并发执行',
  `instance_name` varchar(64) NOT NULL DEFAULT 'JeeSiteScheduler' COMMENT '集群的实例名字',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1删除 2暂停）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`job_name`,`job_group`),
  KEY `idx_sys_job_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作业调度表';

-- ----------------------------
-- Records of js_sys_job
-- ----------------------------
INSERT INTO `js_sys_job` VALUES ('MsgLocalMergePushTask', 'SYSTEM', '消息推送服务 (延迟推送)', 'msgLocalMergePushTask.execute()', '0 0/30 * * * ?', '2', '0', 'JeeSiteScheduler', '2', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null);
INSERT INTO `js_sys_job` VALUES ('MsgLocalPushTask', 'SYSTEM', '消息推送服务 (实时推送)', 'msgLocalPushTask.execute()', '0/3 * * * * ?', '2', '0', 'JeeSiteScheduler', '2', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null);

-- ----------------------------
-- Table structure for js_sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_job_log`;
CREATE TABLE `js_sys_job_log` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `job_type` varchar(50) DEFAULT NULL COMMENT '日志类型',
  `job_event` varchar(200) DEFAULT NULL COMMENT '日志事件',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `is_exception` char(1) DEFAULT NULL COMMENT '是否异常',
  `exception_info` text COMMENT '异常信息',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_sys_job_log_jn` (`job_name`),
  KEY `idx_sys_job_log_jg` (`job_group`),
  KEY `idx_sys_job_log_t` (`job_type`),
  KEY `idx_sys_job_log_e` (`job_event`),
  KEY `idx_sys_job_log_ie` (`is_exception`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作业调度日志表';

-- ----------------------------
-- Records of js_sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_lang
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_lang`;
CREATE TABLE `js_sys_lang` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `module_code` varchar(64) NOT NULL COMMENT '归属模块',
  `lang_code` varchar(500) NOT NULL COMMENT '语言编码',
  `lang_text` varchar(500) NOT NULL COMMENT '语言译文',
  `lang_type` varchar(50) NOT NULL COMMENT '语言类型',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_lang_code` (`lang_code`(255)),
  KEY `idx_sys_lang_type` (`lang_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='国际化语言';

-- ----------------------------
-- Records of js_sys_lang
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_log`;
CREATE TABLE `js_sys_log` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `log_type` varchar(50) NOT NULL COMMENT '日志类型',
  `log_title` varchar(500) NOT NULL COMMENT '日志标题',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_by_name` varchar(100) NOT NULL COMMENT '用户名称',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `request_uri` varchar(500) DEFAULT NULL COMMENT '请求URI',
  `request_method` varchar(10) DEFAULT NULL COMMENT '操作方式',
  `request_params` longtext COMMENT '操作提交的数据',
  `diff_modify_data` text COMMENT '新旧数据比较结果',
  `biz_key` varchar(64) DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) DEFAULT NULL COMMENT '业务类型',
  `remote_addr` varchar(255) NOT NULL COMMENT '操作IP地址',
  `server_addr` varchar(255) NOT NULL COMMENT '请求服务器地址',
  `is_exception` char(1) DEFAULT NULL COMMENT '是否异常',
  `exception_info` text COMMENT '异常信息',
  `user_agent` varchar(500) DEFAULT NULL COMMENT '用户代理',
  `device_name` varchar(100) DEFAULT NULL COMMENT '设备名称/操作系统',
  `browser_name` varchar(100) DEFAULT NULL COMMENT '浏览器名称',
  `execute_time` decimal(19,0) DEFAULT NULL COMMENT '执行时间',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`id`),
  KEY `idx_sys_log_cb` (`create_by`),
  KEY `idx_sys_log_cc` (`corp_code`),
  KEY `idx_sys_log_lt` (`log_type`),
  KEY `idx_sys_log_bk` (`biz_key`),
  KEY `idx_sys_log_bt` (`biz_type`),
  KEY `idx_sys_log_ie` (`is_exception`),
  KEY `idx_sys_log_cd` (`create_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作日志表';

-- ----------------------------
-- Records of js_sys_log
-- ----------------------------
INSERT INTO `js_sys_log` VALUES ('1257946907387580416', 'loginLogout', '系统登录', 'system', '超级管理员', '2020-05-06 16:14:49', '/js/a/login', 'POST', 'username=12D443AAC95D9747DCF554C726719ED2&password=*&validCode=&__url=', null, null, null, '127.0.0.1', 'http://localhost:8980', '0', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', 'Firefox 7', '0', '0', 'JeeSite');
INSERT INTO `js_sys_log` VALUES ('1257947445000884224', 'loginLogout', '系统登录', 'system', '超级管理员', '2020-05-06 16:16:57', '/js/a/login', 'POST', 'username=12D443AAC95D9747DCF554C726719ED2&password=*&validCode=&__url=', null, null, null, '127.0.0.1', 'http://localhost:8980', '0', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'Windows 10', 'Firefox 7', '0', '0', 'JeeSite');

-- ----------------------------
-- Table structure for js_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_menu`;
CREATE TABLE `js_sys_menu` (
  `menu_code` varchar(64) NOT NULL COMMENT '菜单编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `menu_name` varchar(100) NOT NULL COMMENT '菜单名称',
  `menu_type` char(1) NOT NULL COMMENT '菜单类型（1菜单 2权限 3开发）',
  `menu_href` varchar(1000) DEFAULT NULL COMMENT '链接',
  `menu_target` varchar(20) DEFAULT NULL COMMENT '目标',
  `menu_icon` varchar(100) DEFAULT NULL COMMENT '图标',
  `menu_color` varchar(50) DEFAULT NULL COMMENT '颜色',
  `menu_title` varchar(100) DEFAULT NULL COMMENT '菜单标题',
  `permission` varchar(1000) DEFAULT NULL COMMENT '权限标识',
  `weight` decimal(4,0) DEFAULT NULL COMMENT '菜单权重',
  `is_show` char(1) NOT NULL COMMENT '是否显示（1显示 0隐藏）',
  `sys_code` varchar(64) NOT NULL COMMENT '归属系统（default:主导航菜单、mobileApp:APP菜单）',
  `module_codes` varchar(500) NOT NULL COMMENT '归属模块（多个用逗号隔开）',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`menu_code`),
  KEY `idx_sys_menu_pc` (`parent_code`),
  KEY `idx_sys_menu_ts` (`tree_sort`),
  KEY `idx_sys_menu_status` (`status`),
  KEY `idx_sys_menu_mt` (`menu_type`),
  KEY `idx_sys_menu_pss` (`parent_codes`(255)),
  KEY `idx_sys_menu_tss` (`tree_sorts`(255)),
  KEY `idx_sys_menu_sc` (`sys_code`),
  KEY `idx_sys_menu_is` (`is_show`),
  KEY `idx_sys_menu_mcs` (`module_codes`(255)),
  KEY `idx_sys_menu_wt` (`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of js_sys_menu
-- ----------------------------
INSERT INTO `js_sys_menu` VALUES ('1028924699103330304', '0', '0,', '9030', '0000009030,', '0', '0', 'JFlow流程', 'JFlow流程', '1', '', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 16:42:19', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028924891047264256', '1028924699103330304', '0,1028924699103330304,', '30', '0000009030,0000000030,', '1', '1', 'JFlow流程/流程设计器', '流程设计器', '1', '//WF/Admin/Portal/Default.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 16:43:05', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028925472004505600', '1028924699103330304', '0,1028924699103330304,', '60', '0000009030,0000000060,', '0', '1', 'JFlow流程/流程办理', '流程办理', '1', '', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 16:45:24', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028933723626536960', '1028925472004505600', '0,1028924699103330304,1028925472004505600,', '30', '0000009030,0000000060,0000000030,', '1', '2', 'JFlow流程/流程办理/发起', '发起', '1', '//WF/Start.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:18:11', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028933804492718080', '1028925472004505600', '0,1028924699103330304,1028925472004505600,', '60', '0000009030,0000000060,0000000060,', '1', '2', 'JFlow流程/流程办理/待办', '待办', '1', '//WF/Todolist.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:18:30', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028933895135821824', '1028925472004505600', '0,1028924699103330304,1028925472004505600,', '90', '0000009030,0000000060,0000000090,', '1', '2', 'JFlow流程/流程办理/会签', '会签', '1', '//WF/HuiQianList.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:18:52', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028933988475863040', '1028925472004505600', '0,1028924699103330304,1028925472004505600,', '120', '0000009030,0000000060,0000000120,', '1', '2', 'JFlow流程/流程办理/在途', '在途', '1', '//WF/Runing.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:19:14', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028934263999692800', '1028924699103330304', '0,1028924699103330304,', '90', '0000009030,0000000090,', '0', '1', 'JFlow流程/流程查询', '流程查询', '1', '', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:20:20', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028934340843536384', '1028934263999692800', '0,1028924699103330304,1028934263999692800,', '30', '0000009030,0000000090,0000000030,', '1', '2', 'JFlow流程/流程查询/我发起的', '我发起的', '1', '//WF/Comm/Search.htm?EnsName=BP.WF.Data.MyStartFlows', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:20:38', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028934445013270528', '1028934263999692800', '0,1028924699103330304,1028934263999692800,', '60', '0000009030,0000000090,0000000060,', '1', '2', 'JFlow流程/流程查询/我审批的', '我审批的', '1', '//WF/Comm/Search.htm?EnsName=BP.WF.Data.MyJoinFlows', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:21:03', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028934560448905216', '1028934263999692800', '0,1028924699103330304,1028934263999692800,', '90', '0000009030,0000000090,0000000090,', '1', '2', 'JFlow流程/流程查询/流程分布', '流程分布', '1', '//WF/RptSearch/DistributedOfMy.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:21:31', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028934755261743104', '1028934263999692800', '0,1028924699103330304,1028934263999692800,', '120', '0000009030,0000000090,0000000120,', '1', '2', 'JFlow流程/流程查询/我的流程', '我的流程', '1', '//WF/Search.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:22:17', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028934869279703040', '1028934263999692800', '0,1028924699103330304,1028934263999692800,', '150', '0000009030,0000000090,0000000150,', '1', '2', 'JFlow流程/流程查询/单流程查询', '单流程查询', '1', '//WF/RptDfine/Flowlist.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:22:44', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028935178097917952', '1028934263999692800', '0,1028924699103330304,1028934263999692800,', '180', '0000009030,0000000090,0000000180,', '1', '2', 'JFlow流程/流程查询/综合查询', '综合查询', '1', '//WF/RptSearch/Default.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:23:58', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028935301783748608', '1028925472004505600', '0,1028924699103330304,1028925472004505600,', '150', '0000009030,0000000060,0000000150,', '1', '2', 'JFlow流程/流程办理/共享任务', '共享任务', '1', '//WF/TaskPoolSharing.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:24:27', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028935378346573824', '1028925472004505600', '0,1028924699103330304,1028925472004505600,', '180', '0000009030,0000000060,0000000180,', '1', '2', 'JFlow流程/流程办理/申请下来的任务', '申请下来的任务', '1', '//WF/TaskPoolApply.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:24:46', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028935550501781504', '1028924699103330304', '0,1028924699103330304,', '120', '0000009030,0000000120,', '0', '1', 'JFlow流程/高级功能', '高级功能', '1', '', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:25:27', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028935644458385408', '1028935550501781504', '0,1028924699103330304,1028935550501781504,', '30', '0000009030,0000000120,0000000030,', '1', '2', 'JFlow流程/高级功能/我的草稿', '我的草稿', '1', '//WF/Draft.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:25:49', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028935717149868032', '1028935550501781504', '0,1028924699103330304,1028935550501781504,', '60', '0000009030,0000000120,0000000060,', '1', '2', 'JFlow流程/高级功能/抄送', '抄送', '1', '//WF/CC.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:26:06', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028935775526191104', '1028935550501781504', '0,1028924699103330304,1028935550501781504,', '90', '0000009030,0000000120,0000000090,', '1', '2', 'JFlow流程/高级功能/我的关注', '我的关注', '1', '//WF/Focus.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:26:20', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028935931373944832', '1028935550501781504', '0,1028924699103330304,1028935550501781504,', '120', '0000009030,0000000120,0000000120,', '1', '2', 'JFlow流程/高级功能/授权待办', '授权待办', '1', '//WF/TodolistOfAuth.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:26:57', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1028936085451702272', '1028935550501781504', '0,1028924699103330304,1028935550501781504,', '150', '0000009030,0000000120,0000000150,', '1', '2', 'JFlow流程/高级功能/挂起的工作', '挂起的工作', '1', '//WF/HungUpList.htm', '', '', '', null, '', '40', '1', 'default', 'jflow', '0', 'system', '2018-08-13 20:27:34', 'system', '2020-05-06 16:13:44', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431293998346240', '0', '0,', '9000', '0000009000,', '0', '0', '系统管理', '系统管理', '1', '', '', 'icon-settings', '', null, '', '40', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:04', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431294329696256', '1069431293998346240', '0,1069431293998346240,', '300', '0000009000,0000000300,', '0', '1', '系统管理/组织管理', '组织管理', '1', '', '', 'icon-grid', '', null, '', '40', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:04', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431296649146368', '1069431294329696256', '0,1069431293998346240,1069431294329696256,', '40', '0000009000,0000000300,0000000040,', '0', '2', '系统管理/组织管理/用户管理', '用户管理', '1', '/sys/empUser/index', '', 'icon-user', '', null, '', '40', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:05', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431298603692032', '1069431296649146368', '0,1069431293998346240,1069431294329696256,1069431296649146368,', '30', '0000009000,0000000300,0000000040,0000000030,', '1', '3', '系统管理/组织管理/用户管理/查看', '查看', '2', '', '', '', '', null, 'sys:empUser:view', '40', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:05', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431298796630016', '1069431296649146368', '0,1069431293998346240,1069431294329696256,1069431296649146368,', '40', '0000009000,0000000300,0000000040,0000000040,', '1', '3', '系统管理/组织管理/用户管理/编辑', '编辑', '2', '', '', '', '', null, 'sys:empUser:edit', '40', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:05', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431300721815552', '1069431296649146368', '0,1069431293998346240,1069431294329696256,1069431296649146368,', '60', '0000009000,0000000300,0000000040,0000000060,', '1', '3', '系统管理/组织管理/用户管理/分配角色', '分配角色', '2', '', '', '', '', null, 'sys:empUser:authRole', '40', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:06', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431302433091584', '1069431296649146368', '0,1069431293998346240,1069431294329696256,1069431296649146368,', '50', '0000009000,0000000300,0000000040,0000000050,', '1', '3', '系统管理/组织管理/用户管理/分配数据', '分配数据', '2', '', '', '', '', null, 'sys:empUser:authDataScope', '40', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:06', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431302592475136', '1069431296649146368', '0,1069431293998346240,1069431294329696256,1069431296649146368,', '60', '0000009000,0000000300,0000000040,0000000060,', '1', '3', '系统管理/组织管理/用户管理/停用启用', '停用启用', '2', '', '', '', '', null, 'sys:empUser:updateStatus', '40', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:06', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431304442163200', '1069431296649146368', '0,1069431293998346240,1069431294329696256,1069431296649146368,', '70', '0000009000,0000000300,0000000040,0000000070,', '1', '3', '系统管理/组织管理/用户管理/重置密码', '重置密码', '2', '', '', '', '', null, 'sys:empUser:resetpwd', '40', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:07', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431306438651904', '1069431294329696256', '0,1069431293998346240,1069431294329696256,', '50', '0000009000,0000000300,0000000050,', '0', '2', '系统管理/组织管理/机构管理', '机构管理', '1', '/sys/office/list', '', 'icon-grid', '', null, '', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:07', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431306631589888', '1069431306438651904', '0,1069431293998346240,1069431294329696256,1069431306438651904,', '30', '0000009000,0000000300,0000000050,0000000030,', '1', '3', '系统管理/组织管理/机构管理/查看', '查看', '2', '', '', '', '', null, 'sys:office:view', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:07', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431308800045056', '1069431306438651904', '0,1069431293998346240,1069431294329696256,1069431306438651904,', '40', '0000009000,0000000300,0000000050,0000000040,', '1', '3', '系统管理/组织管理/机构管理/编辑', '编辑', '2', '', '', '', '', null, 'sys:office:edit', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:08', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431310695870464', '1069431294329696256', '0,1069431293998346240,1069431294329696256,', '70', '0000009000,0000000300,0000000070,', '0', '2', '系统管理/组织管理/公司管理', '公司管理', '1', '/sys/company/list', '', 'icon-fire', '', null, '', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:08', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431310913974272', '1069431310695870464', '0,1069431293998346240,1069431294329696256,1069431310695870464,', '30', '0000009000,0000000300,0000000070,0000000030,', '1', '3', '系统管理/组织管理/公司管理/查看', '查看', '2', '', '', '', '', null, 'sys:company:view', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:08', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431313849987072', '1069431310695870464', '0,1069431293998346240,1069431294329696256,1069431310695870464,', '40', '0000009000,0000000300,0000000070,0000000040,', '1', '3', '系统管理/组织管理/公司管理/编辑', '编辑', '2', '', '', '', '', null, 'sys:company:edit', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:09', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431315615789056', '1069431294329696256', '0,1069431293998346240,1069431294329696256,', '70', '0000009000,0000000300,0000000070,', '0', '2', '系统管理/组织管理/岗位管理', '岗位管理', '1', '/sys/post/list', '', 'icon-trophy', '', null, '', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:09', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431315817115648', '1069431315615789056', '0,1069431293998346240,1069431294329696256,1069431315615789056,', '30', '0000009000,0000000300,0000000070,0000000030,', '1', '3', '系统管理/组织管理/岗位管理/查看', '查看', '2', '', '', '', '', null, 'sys:post:view', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:09', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431318912512000', '1069431315615789056', '0,1069431293998346240,1069431294329696256,1069431315615789056,', '40', '0000009000,0000000300,0000000070,0000000040,', '1', '3', '系统管理/组织管理/岗位管理/编辑', '编辑', '2', '', '', '', '', null, 'sys:post:edit', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:10', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431320846086144', '1069431293998346240', '0,1069431293998346240,', '400', '0000009000,0000000400,', '0', '1', '系统管理/权限管理', '权限管理', '1', '', '', 'icon-social-dropbox', '', null, '', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:11', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431321043218432', '1069431320846086144', '0,1069431293998346240,1069431320846086144,', '30', '0000009000,0000000400,0000000030,', '1', '2', '系统管理/权限管理/角色管理', '角色管理', '1', '/sys/role/list', '', 'icon-people', '', null, 'sys:role', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:11', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431324021174272', '1069431320846086144', '0,1069431293998346240,1069431320846086144,', '40', '0000009000,0000000400,0000000040,', '1', '2', '系统管理/权限管理/二级管理员', '二级管理员', '1', '/sys/secAdmin/list', '', 'icon-user-female', '', null, 'sys:secAdmin', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:11', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431325766004736', '1069431320846086144', '0,1069431293998346240,1069431320846086144,', '50', '0000009000,0000000400,0000000050,', '1', '2', '系统管理/权限管理/系统管理员', '系统管理员', '1', '/sys/corpAdmin/list', '', 'icon-badge', '', null, 'sys:corpAdmin', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:12', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431325963137024', '1069431293998346240', '0,1069431293998346240,', '500', '0000009000,0000000500,', '0', '1', '系统管理/系统设置', '系统设置', '1', '', '', 'icon-settings', '', null, '', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:12', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431328970452992', '1069431325963137024', '0,1069431293998346240,1069431325963137024,', '30', '0000009000,0000000500,0000000030,', '1', '2', '系统管理/系统设置/菜单管理', '菜单管理', '1', '/sys/menu/list', '', 'icon-book-open', '', null, 'sys:menu', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:13', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431330744643584', '1069431325963137024', '0,1069431293998346240,1069431325963137024,', '40', '0000009000,0000000500,0000000040,', '1', '2', '系统管理/系统设置/模块管理', '模块管理', '1', '/sys/module/list', '', 'icon-grid', '', null, 'sys:module', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:13', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431330924998656', '1069431325963137024', '0,1069431293998346240,1069431325963137024,', '50', '0000009000,0000000500,0000000050,', '1', '2', '系统管理/系统设置/参数设置', '参数设置', '1', '/sys/config/list', '', 'icon-wrench', '', null, 'sys:config', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:13', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431332514639872', '1069431325963137024', '0,1069431293998346240,1069431325963137024,', '60', '0000009000,0000000500,0000000060,', '0', '2', '系统管理/系统设置/字典管理', '字典管理', '1', '/sys/dictType/list', '', 'icon-social-dropbox', '', null, '', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:13', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431334368522240', '1069431332514639872', '0,1069431293998346240,1069431325963137024,1069431332514639872,', '30', '0000009000,0000000500,0000000060,0000000030,', '1', '3', '系统管理/系统设置/字典管理/类型查看', '类型查看', '2', '', '', '', '', null, 'sys:dictType:view', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:14', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431334557265920', '1069431332514639872', '0,1069431293998346240,1069431325963137024,1069431332514639872,', '40', '0000009000,0000000500,0000000060,0000000040,', '1', '3', '系统管理/系统设置/字典管理/类型编辑', '类型编辑', '2', '', '', '', '', null, 'sys:dictType:edit', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:14', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431336167878656', '1069431332514639872', '0,1069431293998346240,1069431325963137024,1069431332514639872,', '50', '0000009000,0000000500,0000000060,0000000050,', '1', '3', '系统管理/系统设置/字典管理/数据查看', '数据查看', '2', '', '', '', '', null, 'sys:dictData:view', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:14', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431338181144576', '1069431332514639872', '0,1069431293998346240,1069431325963137024,1069431332514639872,', '60', '0000009000,0000000500,0000000060,0000000060,', '1', '3', '系统管理/系统设置/字典管理/数据编辑', '数据编辑', '2', '', '', '', '', null, 'sys:dictData:edit', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:15', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431340198604800', '1069431325963137024', '0,1069431293998346240,1069431325963137024,', '70', '0000009000,0000000500,0000000070,', '1', '2', '系统管理/系统设置/行政区划', '行政区划', '1', '/sys/area/list', '', 'icon-map', '', null, 'sys:area', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:15', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431340357988352', '1069431325963137024', '0,1069431293998346240,1069431325963137024,', '80', '0000009000,0000000500,0000000080,', '1', '2', '系统管理/系统设置/国际化管理', '国际化管理', '1', '/sys/lang/list', '', 'icon-globe', '', null, 'sys:lang', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:15', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431342304145408', '1069431325963137024', '0,1069431293998346240,1069431325963137024,', '90', '0000009000,0000000500,0000000090,', '1', '2', '系统管理/系统设置/产品许可信息', '产品许可信息', '1', '//licence', '', 'icon-paper-plane', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:16', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431343885398016', '1069431293998346240', '0,1069431293998346240,', '600', '0000009000,0000000600,', '0', '1', '系统管理/系统监控', '系统监控', '1', '', '', 'icon-ghost', '', null, '', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:16', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431344317411328', '1069431343885398016', '0,1069431293998346240,1069431343885398016,', '40', '0000009000,0000000600,0000000040,', '1', '2', '系统管理/系统监控/访问日志', '访问日志', '1', '/sys/log/list', '', 'fa fa-bug', '', null, 'sys:log', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:16', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431345902858240', '1069431343885398016', '0,1069431293998346240,1069431343885398016,', '50', '0000009000,0000000600,0000000050,', '1', '2', '系统管理/系统监控/数据监控', '数据监控', '1', '//druid', '', 'icon-disc', '', null, 'sys:state:druid', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:17', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431347442167808', '1069431343885398016', '0,1069431293998346240,1069431343885398016,', '60', '0000009000,0000000600,0000000060,', '1', '2', '系统管理/系统监控/缓存监控', '缓存监控', '1', '/state/cache/index', '', 'icon-social-dribbble', '', null, 'sys:stste:cache', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:17', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431347626717184', '1069431343885398016', '0,1069431293998346240,1069431343885398016,', '70', '0000009000,0000000600,0000000070,', '1', '2', '系统管理/系统监控/服务器监控', '服务器监控', '1', '/state/server/index', '', 'icon-speedometer', '', null, 'sys:state:server', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:17', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431349526736896', '1069431343885398016', '0,1069431293998346240,1069431343885398016,', '80', '0000009000,0000000600,0000000080,', '1', '2', '系统管理/系统监控/作业监控', '作业监控', '1', '/job/list', '', 'icon-notebook', '', null, 'sys:job', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:17', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431351468699648', '1069431343885398016', '0,1069431293998346240,1069431343885398016,', '90', '0000009000,0000000600,0000000090,', '1', '2', '系统管理/系统监控/在线用户', '在线用户', '1', '/sys/online/list', '', 'icon-social-twitter', '', null, 'sys:online', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:18', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431351686803456', '1069431343885398016', '0,1069431293998346240,1069431343885398016,', '100', '0000009000,0000000600,0000000100,', '1', '2', '系统管理/系统监控/在线文档', '在线文档', '1', '//swagger-ui.html', '', 'icon-book-open', '', null, 'sys:swagger', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:18', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431353876230144', '1069431293998346240', '0,1069431293998346240,', '700', '0000009000,0000000700,', '0', '1', '系统管理/消息推送', '消息推送', '1', '', '', 'icon-envelope-letter', '', null, '', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:19', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431355646226432', '1069431353876230144', '0,1069431293998346240,1069431353876230144,', '30', '0000009000,0000000700,0000000030,', '1', '2', '系统管理/消息推送/未完成消息', '未完成消息', '1', '/msg/msgPush/list', '', '', '', null, 'msg:msgPush', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:19', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431355839164416', '1069431353876230144', '0,1069431293998346240,1069431353876230144,', '40', '0000009000,0000000700,0000000040,', '1', '2', '系统管理/消息推送/已完成消息', '已完成消息', '1', '/msg/msgPush/list?pushed=true', '', '', '', null, 'msg:msgPush', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:19', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431358032785408', '1069431353876230144', '0,1069431293998346240,1069431353876230144,', '50', '0000009000,0000000700,0000000050,', '1', '2', '系统管理/消息推送/消息模板管理', '消息模板管理', '1', '/msg/msgTemplate/list', '', '', '', null, 'msg:msgTemplate', '60', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:19', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431359710507008', '1069431293998346240', '0,1069431293998346240,', '900', '0000009000,0000000900,', '0', '1', '系统管理/研发工具', '研发工具', '1', '', '', 'fa fa-code', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:20', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431359953776640', '1069431359710507008', '0,1069431293998346240,1069431359710507008,', '30', '0000009000,0000000900,0000000030,', '1', '2', '系统管理/研发工具/代码生成工具', '代码生成工具', '1', '/gen/genTable/list', '', 'fa fa-code', '', null, 'gen:genTable', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:20', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431362164174848', '1069431359710507008', '0,1069431293998346240,1069431359710507008,', '100', '0000009000,0000000900,0000000100,', '0', '2', '系统管理/研发工具/代码生成实例', '代码生成实例', '1', '', '', 'icon-social-dropbox', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:20', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431363963531264', '1069431362164174848', '0,1069431293998346240,1069431359710507008,1069431362164174848,', '30', '0000009000,0000000900,0000000100,0000000030,', '1', '3', '系统管理/研发工具/代码生成实例/单表_主子表', '单表/主子表', '1', '/test/testData/list', '', '', '', null, 'test:testData', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:21', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431364265521152', '1069431362164174848', '0,1069431293998346240,1069431359710507008,1069431362164174848,', '40', '0000009000,0000000900,0000000100,0000000040,', '1', '3', '系统管理/研发工具/代码生成实例/树表_树结构表', '树表/树结构表', '1', '/test/testTree/list', '', '', '', null, 'test:testTree', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:21', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431367176368128', '1069431359710507008', '0,1069431293998346240,1069431359710507008,', '200', '0000009000,0000000900,0000000200,', '0', '2', '系统管理/研发工具/数据表格实例', '数据表格实例', '1', '', '', '', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:22', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431368921198592', '1069431367176368128', '0,1069431293998346240,1069431359710507008,1069431367176368128,', '30', '0000009000,0000000900,0000000200,0000000030,', '1', '3', '系统管理/研发工具/数据表格实例/多表头分组小计合计', '多表头分组小计合计', '1', '/demo/dataGrid/groupGrid', '', '', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:22', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431369105747968', '1069431367176368128', '0,1069431293998346240,1069431359710507008,1069431367176368128,', '40', '0000009000,0000000900,0000000200,0000000040,', '1', '3', '系统管理/研发工具/数据表格实例/编辑表格多行编辑', '编辑表格多行编辑', '1', '/demo/dataGrid/editGrid', '', '', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:22', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431372050149376', '1069431359710507008', '0,1069431293998346240,1069431359710507008,', '300', '0000009000,0000000900,0000000300,', '0', '2', '系统管理/研发工具/表单组件实例', '表单组件实例', '1', '', '', '', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:23', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431373895643136', '1069431372050149376', '0,1069431293998346240,1069431359710507008,1069431372050149376,', '30', '0000009000,0000000900,0000000300,0000000030,', '1', '3', '系统管理/研发工具/表单组件实例/组件应用实例', '组件应用实例', '1', '/demo/form/editForm', '', '', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:23', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431374126329856', '1069431372050149376', '0,1069431293998346240,1069431359710507008,1069431372050149376,', '40', '0000009000,0000000900,0000000300,0000000040,', '1', '3', '系统管理/研发工具/表单组件实例/栅格布局实例', '栅格布局实例', '1', '/demo/form/layoutForm', '', '', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:23', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431376357699584', '1069431372050149376', '0,1069431293998346240,1069431359710507008,1069431372050149376,', '50', '0000009000,0000000900,0000000300,0000000050,', '1', '3', '系统管理/研发工具/表单组件实例/表格表单实例', '表格表单实例', '1', '/demo/form/tableForm', '', '', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:24', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431377938952192', '1069431359710507008', '0,1069431293998346240,1069431359710507008,', '400', '0000009000,0000000900,0000000400,', '0', '2', '系统管理/研发工具/前端界面实例', '前端界面实例', '1', '', '', '', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:24', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431378144473088', '1069431377938952192', '0,1069431293998346240,1069431359710507008,1069431377938952192,', '30', '0000009000,0000000900,0000000400,0000000030,', '1', '3', '系统管理/研发工具/前端界面实例/图标样式查找', '图标样式查找', '1', '//tags/iconselect', '', '', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:24', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431381374087168', '1069431293998346240', '0,1069431293998346240,', '999', '0000009000,0000000999,', '0', '1', '系统管理/JeeSite社区', 'JeeSite社区', '1', '', '', 'fa fa-code', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:25', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431383135694848', '1069431381374087168', '0,1069431293998346240,1069431381374087168,', '30', '0000009000,0000000999,0000000030,', '1', '2', '系统管理/JeeSite社区/官方网站', '官方网站', '1', 'http://jeesite.com', '_blank', '', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:25', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431383324438528', '1069431381374087168', '0,1069431293998346240,1069431381374087168,', '50', '0000009000,0000000999,0000000050,', '1', '2', '系统管理/JeeSite社区/作者博客', '作者博客', '1', 'https://my.oschina.net/thinkgem', '_blank', '', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:26', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431384863748096', '1069431381374087168', '0,1069431293998346240,1069431381374087168,', '40', '0000009000,0000000999,0000000040,', '1', '2', '系统管理/JeeSite社区/问题反馈', '问题反馈', '1', 'https://gitee.com/thinkgem/jeesite4/issues', '_blank', '', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:26', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1069431386726019072', '1069431381374087168', '0,1069431293998346240,1069431381374087168,', '60', '0000009000,0000000999,0000000060,', '1', '2', '系统管理/JeeSite社区/开源社区', '开源社区', '1', 'http://jeesite.net', '_blank', '', '', null, '', '80', '1', 'default', 'core', '0', 'system', '2018-12-03 11:21:26', 'system', '2019-03-11 15:20:06', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1105443204287991808', '0', '0,', '9030', '0000009030,', '1', '0', '站内消息', '站内消息', '1', '/msg/msgInner/list', '', 'icon-speech', '', null, 'msg:msgInner', '40', '1', 'default', 'core', '0', 'system', '2019-08-30 11:44:41', 'system', '2019-08-30 11:44:41', '', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1119900377470705664', '0', '0,', '9090', '0000009090,', '0', '0', '文件管理', '文件管理', '1', '', '', 'icon-folder', '', null, '', '40', '1', 'default', 'core', '0', 'system', '2020-05-06 16:13:43', 'system', '2020-05-06 16:13:43', '', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1119900377554591744', '1119900377470705664', '0,1119900377470705664,', '30', '0000009090,0000000030,', '1', '1', '文件管理/文件管理', '文件管理', '1', '/filemanager/index', '', '', '', null, 'filemanager', '40', '1', 'default', 'core', '0', 'system', '2020-05-06 16:13:43', 'system', '2020-05-06 16:13:43', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_menu` VALUES ('1119900377638477824', '1119900377470705664', '0,1119900377470705664,', '50', '0000009090,0000000050,', '1', '1', '文件管理/文件分享', '文件分享', '1', '/filemanager/filemanagerShared/list', '', '', '', null, 'filemanager', '40', '1', 'default', 'core', '0', 'system', '2020-05-06 16:13:43', 'system', '2020-05-06 16:13:43', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for js_sys_module
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_module`;
CREATE TABLE `js_sys_module` (
  `module_code` varchar(64) NOT NULL COMMENT '模块编码',
  `module_name` varchar(100) NOT NULL COMMENT '模块名称',
  `description` varchar(500) DEFAULT NULL COMMENT '模块描述',
  `main_class_name` varchar(500) DEFAULT NULL COMMENT '主类全名',
  `current_version` varchar(50) DEFAULT NULL COMMENT '当前版本',
  `upgrade_info` varchar(300) DEFAULT NULL COMMENT '升级信息',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`module_code`),
  KEY `idx_sys_module_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模块表';

-- ----------------------------
-- Records of js_sys_module
-- ----------------------------
INSERT INTO `js_sys_module` VALUES ('bpm', '业务流程', '流程设计器、流程监管控制、流程办理、流程追踪', 'com.jeesite.modules.bpm.entity.BpmEntity', '4.1.x', null, '0', 'system', '2020-05-06 16:13:36', 'system', '2020-05-06 16:13:36', null);
INSERT INTO `js_sys_module` VALUES ('cms', '内容管理', '网站、站点、栏目、文章、链接、评论、留言板', 'com.jeesite.modules.cms.web.CmsController', '4.0.0', null, '0', 'system', '2018-12-03 11:20:34', 'system', '2018-12-03 11:20:34', null);
INSERT INTO `js_sys_module` VALUES ('core', '核心模块', '用户、角色、组织、模块、菜单、字典、参数', 'com.jeesite.modules.sys.web.LoginController', '4.1.8', 'upgrade 2020-05-06 16:13:36 (4.1.6 -> 4.1.8)', '0', 'system', '2018-12-03 11:20:33', 'system', '2020-05-06 16:13:37', null);
INSERT INTO `js_sys_module` VALUES ('filemanager', '文件管理', '公共文件柜、个人文件柜、文件分享', 'com.jeesite.modules.filemanager.web.FilemanagerController', '4.1.6', 'upgrade 2020-05-06 16:13:44 (4.1.4 -> 4.1.6)', '0', 'system', '2019-08-30 11:44:41', 'system', '2020-05-06 16:13:44', null);
INSERT INTO `js_sys_module` VALUES ('jflow', 'JFlow工作流', '适合OA、可视化的java工作流引擎、自定义表单引擎。', 'com.jeesite.modules.jflow.config.JflowConfig', '4.0.6', null, '0', 'system', '2018-08-13 21:47:17', 'system', '2018-08-13 21:48:00', null);

-- ----------------------------
-- Table structure for js_sys_msg_inner
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_msg_inner`;
CREATE TABLE `js_sys_msg_inner` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `msg_title` varchar(200) NOT NULL COMMENT '消息标题',
  `content_level` char(1) NOT NULL COMMENT '内容级别（1普通 2一般 3紧急）',
  `content_type` char(1) DEFAULT NULL COMMENT '内容类型（1公告 2新闻 3会议 4其它）',
  `msg_content` text NOT NULL COMMENT '消息内容',
  `receive_type` char(1) NOT NULL COMMENT '接受者类型（1用户 2部门 3角色 4岗位）',
  `receive_codes` text NOT NULL COMMENT '接受者字符串',
  `receive_names` text NOT NULL COMMENT '接受者名称字符串',
  `send_user_code` varchar(64) NOT NULL COMMENT '发送者用户编码',
  `send_user_name` varchar(100) NOT NULL COMMENT '发送者用户姓名',
  `send_date` datetime NOT NULL COMMENT '发送时间',
  `is_attac` char(1) DEFAULT NULL COMMENT '是否有附件',
  `notify_types` varchar(100) NOT NULL COMMENT '通知类型（PC APP 短信 邮件 微信）多选',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1删除 4审核 5驳回 9草稿）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_inner_cb` (`create_by`),
  KEY `idx_sys_msg_inner_status` (`status`),
  KEY `idx_sys_msg_inner_cl` (`content_level`),
  KEY `idx_sys_msg_inner_sc` (`send_user_code`),
  KEY `idx_sys_msg_inner_sd` (`send_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='内部消息';

-- ----------------------------
-- Records of js_sys_msg_inner
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_msg_inner_record
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_msg_inner_record`;
CREATE TABLE `js_sys_msg_inner_record` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `msg_inner_id` varchar(64) NOT NULL COMMENT '所属消息',
  `receive_user_code` varchar(64) DEFAULT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(100) NOT NULL COMMENT '接受者用户姓名',
  `read_status` char(1) NOT NULL COMMENT '读取状态（0未送达 1未读 2已读）',
  `read_date` datetime DEFAULT NULL COMMENT '阅读时间',
  `is_star` char(1) DEFAULT NULL COMMENT '是否标星',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_inner_r_mi` (`msg_inner_id`),
  KEY `idx_sys_msg_inner_r_rc` (`receive_user_code`),
  KEY `idx_sys_msg_inner_r_ruc` (`receive_user_code`),
  KEY `idx_sys_msg_inner_r_status` (`read_status`),
  KEY `idx_sys_msg_inner_r_star` (`is_star`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='内部消息发送记录表';

-- ----------------------------
-- Records of js_sys_msg_inner_record
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_msg_push
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_msg_push`;
CREATE TABLE `js_sys_msg_push` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `msg_type` varchar(16) NOT NULL COMMENT '消息类型（PC APP 短信 邮件 微信）',
  `msg_title` varchar(200) NOT NULL COMMENT '消息标题',
  `msg_content` text NOT NULL COMMENT '消息内容',
  `biz_key` varchar(64) DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) DEFAULT NULL COMMENT '业务类型',
  `receive_code` varchar(64) NOT NULL COMMENT '接受者账号',
  `receive_user_code` varchar(64) NOT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(100) NOT NULL COMMENT '接受者用户姓名',
  `send_user_code` varchar(64) NOT NULL COMMENT '发送者用户编码',
  `send_user_name` varchar(100) NOT NULL COMMENT '发送者用户姓名',
  `send_date` datetime NOT NULL COMMENT '发送时间',
  `is_merge_push` char(1) DEFAULT NULL COMMENT '是否合并推送',
  `plan_push_date` datetime DEFAULT NULL COMMENT '计划推送时间',
  `push_number` int(11) DEFAULT NULL COMMENT '推送尝试次数',
  `push_return_code` varchar(200) DEFAULT NULL COMMENT '推送返回结果码',
  `push_return_msg_id` varchar(200) DEFAULT NULL COMMENT '推送返回消息编号',
  `push_return_content` text COMMENT '推送返回的内容信息',
  `push_status` char(1) DEFAULT NULL COMMENT '推送状态（0未推送 1成功  2失败）',
  `push_date` datetime DEFAULT NULL COMMENT '推送时间',
  `read_status` char(1) DEFAULT NULL COMMENT '读取状态（0未送达 1未读 2已读）',
  `read_date` datetime DEFAULT NULL COMMENT '读取时间',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_push_type` (`msg_type`),
  KEY `idx_sys_msg_push_rc` (`receive_code`),
  KEY `idx_sys_msg_push_uc` (`receive_user_code`),
  KEY `idx_sys_msg_push_suc` (`send_user_code`),
  KEY `idx_sys_msg_push_pd` (`plan_push_date`),
  KEY `idx_sys_msg_push_ps` (`push_status`),
  KEY `idx_sys_msg_push_rs` (`read_status`),
  KEY `idx_sys_msg_push_bk` (`biz_key`),
  KEY `idx_sys_msg_push_bt` (`biz_type`),
  KEY `idx_sys_msg_push_imp` (`is_merge_push`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息推送表';

-- ----------------------------
-- Records of js_sys_msg_push
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_msg_pushed
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_msg_pushed`;
CREATE TABLE `js_sys_msg_pushed` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `msg_type` varchar(16) NOT NULL COMMENT '消息类型（PC APP 短信 邮件 微信）',
  `msg_title` varchar(200) NOT NULL COMMENT '消息标题',
  `msg_content` text NOT NULL COMMENT '消息内容',
  `biz_key` varchar(64) DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) DEFAULT NULL COMMENT '业务类型',
  `receive_code` varchar(64) NOT NULL COMMENT '接受者账号',
  `receive_user_code` varchar(64) NOT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(100) NOT NULL COMMENT '接受者用户姓名',
  `send_user_code` varchar(64) NOT NULL COMMENT '发送者用户编码',
  `send_user_name` varchar(100) NOT NULL COMMENT '发送者用户姓名',
  `send_date` datetime NOT NULL COMMENT '发送时间',
  `is_merge_push` char(1) DEFAULT NULL COMMENT '是否合并推送',
  `plan_push_date` datetime DEFAULT NULL COMMENT '计划推送时间',
  `push_number` int(11) DEFAULT NULL COMMENT '推送尝试次数',
  `push_return_content` text COMMENT '推送返回的内容信息',
  `push_return_code` varchar(200) DEFAULT NULL COMMENT '推送返回结果码',
  `push_return_msg_id` varchar(200) DEFAULT NULL COMMENT '推送返回消息编号',
  `push_status` char(1) DEFAULT NULL COMMENT '推送状态（0未推送 1成功  2失败）',
  `push_date` datetime DEFAULT NULL COMMENT '推送时间',
  `read_status` char(1) DEFAULT NULL COMMENT '读取状态（0未送达 1未读 2已读）',
  `read_date` datetime DEFAULT NULL COMMENT '读取时间',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_pushed_type` (`msg_type`),
  KEY `idx_sys_msg_pushed_rc` (`receive_code`),
  KEY `idx_sys_msg_pushed_uc` (`receive_user_code`),
  KEY `idx_sys_msg_pushed_suc` (`send_user_code`),
  KEY `idx_sys_msg_pushed_pd` (`plan_push_date`),
  KEY `idx_sys_msg_pushed_ps` (`push_status`),
  KEY `idx_sys_msg_pushed_rs` (`read_status`),
  KEY `idx_sys_msg_pushed_bk` (`biz_key`),
  KEY `idx_sys_msg_pushed_bt` (`biz_type`),
  KEY `idx_sys_msg_pushed_imp` (`is_merge_push`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息已推送表';

-- ----------------------------
-- Records of js_sys_msg_pushed
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_msg_template
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_msg_template`;
CREATE TABLE `js_sys_msg_template` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `module_code` varchar(64) DEFAULT NULL COMMENT '归属模块',
  `tpl_key` varchar(100) NOT NULL COMMENT '模板键值',
  `tpl_name` varchar(100) NOT NULL COMMENT '模板名称',
  `tpl_type` varchar(16) NOT NULL COMMENT '模板类型',
  `tpl_content` text NOT NULL COMMENT '模板内容',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `idx_sys_msg_tpl_key` (`tpl_key`),
  KEY `idx_sys_msg_tpl_type` (`tpl_type`),
  KEY `idx_sys_msg_tpl_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息模板';

-- ----------------------------
-- Records of js_sys_msg_template
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_office
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_office`;
CREATE TABLE `js_sys_office` (
  `office_code` varchar(64) NOT NULL COMMENT '机构编码',
  `parent_code` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10,0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4,0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) NOT NULL COMMENT '全节点名',
  `view_code` varchar(100) NOT NULL COMMENT '机构代码',
  `office_name` varchar(100) NOT NULL COMMENT '机构名称',
  `full_name` varchar(200) NOT NULL COMMENT '机构全称',
  `office_type` char(1) NOT NULL COMMENT '机构类型',
  `leader` varchar(100) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(100) DEFAULT NULL COMMENT '办公电话',
  `address` varchar(255) DEFAULT NULL COMMENT '联系地址',
  `zip_code` varchar(100) DEFAULT NULL COMMENT '邮政编码',
  `email` varchar(300) DEFAULT NULL COMMENT '电子邮箱',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`office_code`),
  KEY `idx_sys_office_cc` (`corp_code`),
  KEY `idx_sys_office_pc` (`parent_code`),
  KEY `idx_sys_office_pcs` (`parent_codes`(255)),
  KEY `idx_sys_office_status` (`status`),
  KEY `idx_sys_office_ot` (`office_type`),
  KEY `idx_sys_office_vc` (`view_code`),
  KEY `idx_sys_office_ts` (`tree_sort`),
  KEY `idx_sys_office_tss` (`tree_sorts`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='组织机构表';

-- ----------------------------
-- Records of js_sys_office
-- ----------------------------
INSERT INTO `js_sys_office` VALUES ('SD', '0', '0,', '40', '0000000040,', '0', '0', '山东公司', 'SD', '山东公司', '山东公司', '1', null, null, null, null, null, '0', 'system', '2018-12-03 11:21:27', 'system', '2018-12-03 11:21:27', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_office` VALUES ('SDJN', 'SD', '0,SD,', '30', '0000000040,0000000030,', '0', '1', '山东公司/济南公司', 'SDJN', '济南公司', '山东济南公司', '2', null, null, null, null, null, '0', 'system', '2018-12-03 11:21:27', 'system', '2018-12-03 11:21:27', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_office` VALUES ('SDJN01', 'SDJN', '0,SD,SDJN,', '30', '0000000040,0000000030,0000000030,', '1', '2', '山东公司/济南公司/企管部', 'SDJN01', '企管部', '山东济南企管部', '3', null, null, null, null, null, '0', 'system', '2018-12-03 11:21:27', 'system', '2018-12-03 11:21:27', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_office` VALUES ('SDJN02', 'SDJN', '0,SD,SDJN,', '40', '0000000040,0000000030,0000000040,', '1', '2', '山东公司/济南公司/财务部', 'SDJN02', '财务部', '山东济南财务部', '3', null, null, null, null, null, '0', 'system', '2018-12-03 11:21:27', 'system', '2018-12-03 11:21:27', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_office` VALUES ('SDJN03', 'SDJN', '0,SD,SDJN,', '50', '0000000040,0000000030,0000000050,', '1', '2', '山东公司/济南公司/研发部', 'SDJN03', '研发部', '山东济南研发部', '3', null, null, null, null, null, '0', 'system', '2018-12-03 11:21:27', 'system', '2018-12-03 11:21:27', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_office` VALUES ('SDQD', 'SD', '0,SD,', '40', '0000000040,0000000040,', '0', '1', '山东公司/青岛公司', 'SDQD', '青岛公司', '山东青岛公司', '2', null, null, null, null, null, '0', 'system', '2018-12-03 11:21:27', 'system', '2018-12-03 11:21:27', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_office` VALUES ('SDQD01', 'SDQD', '0,SD,SDQD,', '30', '0000000040,0000000040,0000000030,', '1', '2', '山东公司/青岛公司/企管部', 'SDQD01', '企管部', '山东青岛企管部', '3', null, null, null, null, null, '0', 'system', '2018-12-03 11:21:27', 'system', '2018-12-03 11:21:27', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_office` VALUES ('SDQD02', 'SDQD', '0,SD,SDQD,', '40', '0000000040,0000000040,0000000040,', '1', '2', '山东公司/青岛公司/财务部', 'SDQD02', '财务部', '山东青岛财务部', '3', null, null, null, null, null, '0', 'system', '2018-12-03 11:21:27', 'system', '2018-12-03 11:21:27', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_office` VALUES ('SDQD03', 'SDQD', '0,SD,SDQD,', '50', '0000000040,0000000040,0000000050,', '1', '2', '山东公司/青岛公司/研发部', 'SDQD03', '研发部', '山东青岛研发部', '3', null, null, null, null, null, '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for js_sys_post
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_post`;
CREATE TABLE `js_sys_post` (
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(100) NOT NULL COMMENT '岗位名称',
  `post_type` varchar(100) DEFAULT NULL COMMENT '岗位分类（高管、中层、基层）',
  `post_sort` decimal(10,0) DEFAULT NULL COMMENT '岗位排序（升序）',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`post_code`),
  KEY `idx_sys_post_cc` (`corp_code`),
  KEY `idx_sys_post_status` (`status`),
  KEY `idx_sys_post_ps` (`post_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工岗位表';

-- ----------------------------
-- Records of js_sys_post
-- ----------------------------
INSERT INTO `js_sys_post` VALUES ('ceo', '总经理', null, '1', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');
INSERT INTO `js_sys_post` VALUES ('cfo', '财务经理', null, '2', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');
INSERT INTO `js_sys_post` VALUES ('dept', '部门经理', null, '2', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');
INSERT INTO `js_sys_post` VALUES ('hrm', '人力经理', null, '2', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');
INSERT INTO `js_sys_post` VALUES ('user', '普通员工', null, '3', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite');

-- ----------------------------
-- Table structure for js_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_role`;
CREATE TABLE `js_sys_role` (
  `role_code` varchar(64) NOT NULL COMMENT '角色编码',
  `role_name` varchar(100) NOT NULL COMMENT '角色名称',
  `role_type` varchar(100) DEFAULT NULL COMMENT '角色分类（高管、中层、基层、其它）',
  `role_sort` decimal(10,0) DEFAULT NULL COMMENT '角色排序（升序）',
  `is_sys` char(1) DEFAULT NULL COMMENT '系统内置（1是 0否）',
  `user_type` varchar(16) DEFAULT NULL COMMENT '用户类型（employee员工 member会员）',
  `data_scope` char(1) DEFAULT NULL COMMENT '数据范围设置（0未设置  1全部数据 2自定义数据）',
  `biz_scope` varchar(255) DEFAULT NULL COMMENT '适应业务范围（不同的功能，不同的数据权限支持）',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`role_code`),
  KEY `idx_sys_role_cc` (`corp_code`),
  KEY `idx_sys_role_is` (`is_sys`),
  KEY `idx_sys_role_status` (`status`),
  KEY `idx_sys_role_rs` (`role_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of js_sys_role
-- ----------------------------
INSERT INTO `js_sys_role` VALUES ('corpAdmin', '系统管理员', null, '200', '1', 'none', '0', null, '0', 'system', '2018-12-03 11:21:04', 'system', '2018-12-03 11:21:04', '客户方使用的管理员角色，客户方管理员，集团管理员', '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_role` VALUES ('default', '默认角色', null, '100', '1', 'none', '0', null, '0', 'system', '2018-12-03 11:21:04', 'system', '2018-12-03 11:21:04', '非管理员用户，共有的默认角色，在参数配置里指定', '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_role` VALUES ('dept', '部门经理', null, '40', '0', 'employee', '0', null, '0', 'system', '2018-12-03 11:21:04', 'system', '2018-12-03 11:21:04', '部门经理', '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_role` VALUES ('user', '普通员工', null, '50', '0', 'employee', '0', null, '0', 'system', '2018-12-03 11:21:04', 'system', '2018-12-03 11:21:04', '普通员工', '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for js_sys_role_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_role_data_scope`;
CREATE TABLE `js_sys_role_data_scope` (
  `role_code` varchar(64) NOT NULL COMMENT '控制角色编码',
  `ctrl_type` varchar(20) NOT NULL COMMENT '控制类型',
  `ctrl_data` varchar(64) NOT NULL COMMENT '控制数据',
  `ctrl_permi` varchar(64) NOT NULL COMMENT '控制权限',
  PRIMARY KEY (`role_code`,`ctrl_type`,`ctrl_data`,`ctrl_permi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色数据权限表';

-- ----------------------------
-- Records of js_sys_role_data_scope
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_role_menu`;
CREATE TABLE `js_sys_role_menu` (
  `role_code` varchar(64) NOT NULL COMMENT '角色编码',
  `menu_code` varchar(64) NOT NULL COMMENT '菜单编码',
  PRIMARY KEY (`role_code`,`menu_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与菜单关联表';

-- ----------------------------
-- Records of js_sys_role_menu
-- ----------------------------
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431293998346240');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431294329696256');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431296649146368');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431298603692032');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431298796630016');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431300721815552');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431302433091584');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431302592475136');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431304442163200');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431306438651904');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431306631589888');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431308800045056');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431310695870464');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431310913974272');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431313849987072');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431315615789056');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431315817115648');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431318912512000');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431320846086144');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431321043218432');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431324021174272');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431325766004736');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431325963137024');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431328970452992');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431330744643584');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431330924998656');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431332514639872');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431334368522240');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431334557265920');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431336167878656');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431338181144576');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431340198604800');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431340357988352');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431342304145408');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431343885398016');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431344317411328');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431345902858240');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431347442167808');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431347626717184');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431349526736896');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431351468699648');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431351686803456');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431353876230144');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431355646226432');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431355839164416');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431358032785408');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431359710507008');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431359953776640');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431362164174848');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431363963531264');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431364265521152');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431367176368128');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431368921198592');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431369105747968');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431372050149376');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431373895643136');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431374126329856');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431376357699584');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431377938952192');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431378144473088');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431381374087168');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431383135694848');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431383324438528');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431384863748096');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1069431386726019072');

-- ----------------------------
-- Table structure for js_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_user`;
CREATE TABLE `js_sys_user` (
  `user_code` varchar(100) NOT NULL COMMENT '用户编码',
  `login_code` varchar(100) NOT NULL COMMENT '登录账号',
  `user_name` varchar(100) NOT NULL COMMENT '用户昵称',
  `password` varchar(100) NOT NULL COMMENT '登录密码',
  `email` varchar(300) DEFAULT NULL COMMENT '电子邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号码',
  `phone` varchar(100) DEFAULT NULL COMMENT '办公电话',
  `sex` char(1) DEFAULT NULL COMMENT '用户性别',
  `avatar` varchar(1000) DEFAULT NULL COMMENT '头像路径',
  `sign` varchar(200) DEFAULT NULL COMMENT '个性签名',
  `wx_openid` varchar(100) DEFAULT NULL COMMENT '绑定的微信号',
  `mobile_imei` varchar(100) DEFAULT NULL COMMENT '绑定的手机串号',
  `user_type` varchar(16) NOT NULL COMMENT '用户类型',
  `ref_code` varchar(64) DEFAULT NULL COMMENT '用户类型引用编号',
  `ref_name` varchar(100) DEFAULT NULL COMMENT '用户类型引用姓名',
  `mgr_type` char(1) NOT NULL COMMENT '管理员类型（0非管理员 1系统管理员  2二级管理员）',
  `pwd_security_level` decimal(1,0) DEFAULT NULL COMMENT '密码安全级别（0初始 1很弱 2弱 3安全 4很安全）',
  `pwd_update_date` datetime DEFAULT NULL COMMENT '密码最后更新时间',
  `pwd_update_record` varchar(1000) DEFAULT NULL COMMENT '密码修改记录',
  `pwd_question` varchar(200) DEFAULT NULL COMMENT '密保问题',
  `pwd_question_answer` varchar(200) DEFAULT NULL COMMENT '密保问题答案',
  `pwd_question_2` varchar(200) DEFAULT NULL COMMENT '密保问题2',
  `pwd_question_answer_2` varchar(200) DEFAULT NULL COMMENT '密保问题答案2',
  `pwd_question_3` varchar(200) DEFAULT NULL COMMENT '密保问题3',
  `pwd_question_answer_3` varchar(200) DEFAULT NULL COMMENT '密保问题答案3',
  `pwd_quest_update_date` datetime DEFAULT NULL COMMENT '密码问题修改时间',
  `last_login_ip` varchar(100) DEFAULT NULL COMMENT '最后登陆IP',
  `last_login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `freeze_date` datetime DEFAULT NULL COMMENT '冻结时间',
  `freeze_cause` varchar(200) DEFAULT NULL COMMENT '冻结原因',
  `user_weight` decimal(8,0) DEFAULT '0' COMMENT '用户权重（降序）',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1删除 2停用 3冻结）',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19,0) DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19,4) DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`user_code`),
  KEY `idx_sys_user_lc` (`login_code`),
  KEY `idx_sys_user_email` (`email`(255)),
  KEY `idx_sys_user_mobile` (`mobile`),
  KEY `idx_sys_user_wo` (`wx_openid`),
  KEY `idx_sys_user_imei` (`mobile_imei`),
  KEY `idx_sys_user_rt` (`user_type`),
  KEY `idx_sys_user_rc` (`ref_code`),
  KEY `idx_sys_user_mt` (`mgr_type`),
  KEY `idx_sys_user_us` (`user_weight`),
  KEY `idx_sys_user_ud` (`update_date`),
  KEY `idx_sys_user_status` (`status`),
  KEY `idx_sys_user_cc` (`corp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of js_sys_user
-- ----------------------------
INSERT INTO `js_sys_user` VALUES ('admin', 'admin', '系统管理员', '199726ba67bd001fbeda63388198be639e3777ed221468a9893ee0ca', null, null, null, null, null, null, null, null, 'none', null, null, '1', '1', '2018-12-03 11:21:27', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:27', 'system', '2018-12-03 11:21:27', '客户方使用的系统管理员，用于一些常用的基础数据配置。', '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('system', 'system', '超级管理员', 'e104cb736c710d308c02d06c0289a974eee5abea70971d46bc137ead', null, null, null, null, null, null, null, null, 'none', null, null, '0', '1', '2018-12-03 11:21:27', null, null, null, null, null, null, null, null, '127.0.0.1', '2020-05-06 16:16:57', null, null, '0', '0', 'system', '2018-12-03 11:21:27', 'system', '2018-12-03 11:21:27', '开发者使用的最高级别管理员，主要用于开发和调试。', '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user10_nw09', 'user10', '用户10', '7e6f3b72c1c4fb9cc529a0dc75c7d284a4bbe2e8a70b5bc9d5d800cd', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user10_nw09', '用户10', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user11_ibcm', 'user11', '用户11', 'dc4b688fe329a8c377dddf0d94308a6f3432d31e0837145cbbd8f619', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user11_ibcm', '用户11', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user12_8hyj', 'user12', '用户12', 'a7316a47621e5986efbc278d3a099e5249a94b23737f43e3d0bc53fa', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user12_8hyj', '用户12', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user13_h3sk', 'user13', '用户13', 'ab7decb2136136dc787bf4e1bb0f55e68dc3db132765822615dd7034', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user13_h3sk', '用户13', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user14_deqy', 'user14', '用户14', 'ba6f7aa9e277f0b67b574dc0a1b3203f5e358ae2f7b30d04ffc9c7b3', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user14_deqy', '用户14', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user15_c5sr', 'user15', '用户15', '04fdc6a73ac1d89f2ecf75fe5ea1f1e993d3a010cf0b6c2e25a53ffa', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user15_c5sr', '用户15', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user16_pkfs', 'user16', '用户16', 'c98db574fd3b0b6c2a5b06804226b96031a639911fa3d1b176fc31dd', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user16_pkfs', '用户16', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user17_ogfo', 'user17', '用户17', 'c6df40d3966126eb67323f7ef39063169e568869c53205d6d7e6624a', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user17_ogfo', '用户17', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user18_ycie', 'user18', '用户18', 'e69e9be629856467583fc8b2ef3a4df23fd612da0fa30dc5b4e3c3ba', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user18_ycie', '用户18', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user19_urge', 'user19', '用户19', 'ecd6003cb598bbe5bc40dcb5e0732a2120e19d1651c35db0682de3fc', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user19_urge', '用户19', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user1_cwme', 'user1', '用户01', 'e2657b2e8fad4a74f0f24aa66e5ab77cd44cc8bc08baf59076207a21', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user1_cwme', '用户01', '0', '0', '2018-12-03 11:21:28', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user20_e7n4', 'user20', '用户20', '915c05116802a01f5cf21c7925b011affa66253206029cf49deb26e6', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user20_e7n4', '用户20', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user21_oxfg', 'user21', '用户21', '60be9ee1245015a6aee8ce4e6a61a216ae1247ba24aae9fc3b48c693', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user21_oxfg', '用户21', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user22_o5j5', 'user22', '用户22', 'c3009de72d75429740b665b3c036878a08495ba8dc8b422e48eb2e15', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user22_o5j5', '用户22', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user23_ymbm', 'user23', '用户23', '09eae1abcfdd138e5fa52033645bf01b6e87a8c5e3f99504a9dd59e5', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user23_ymbm', '用户23', '0', '0', '2018-12-03 11:21:29', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:29', 'system', '2018-12-03 11:21:29', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user2_fuda', 'user2', '用户02', 'bdb480ae623d2124bbbb77b8ec21221e2b8af63710cc31cebc715e63', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user2_fuda', '用户02', '0', '0', '2018-12-03 11:21:28', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user3_ll1n', 'user3', '用户03', '506f84703ca2b6ac183749e3dd2b179ab6b6e882d6bc4050a1feba69', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user3_ll1n', '用户03', '0', '0', '2018-12-03 11:21:28', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user4_m1vk', 'user4', '用户04', 'b7e8047082d12683a893e49b8b1c1f2e1d408893884c2338dd52748a', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user4_m1vk', '用户04', '0', '0', '2018-12-03 11:21:28', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user5_pwtv', 'user5', '用户05', '950e0c5e6f2cdc2914a445d9316daaee1bae1aa29b168b36a173b7f0', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user5_pwtv', '用户05', '0', '0', '2018-12-03 11:21:28', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user6_hpw5', 'user6', '用户06', '1101b9b5f65f95eced30785575387562f69f82006ca88b460ead7b56', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user6_hpw5', '用户06', '0', '0', '2018-12-03 11:21:28', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user7_lxut', 'user7', '用户07', 'bafa863d95576d8d7577ff84b1525a71307627e30e3d050ed9e99935', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user7_lxut', '用户07', '0', '0', '2018-12-03 11:21:28', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user8_wp9j', 'user8', '用户08', '51bc525aa96c1cea707e4e137afd80e370ce94401117da4d3933c301', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user8_wp9j', '用户08', '0', '0', '2018-12-03 11:21:28', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `js_sys_user` VALUES ('user9_twd4', 'user9', '用户09', '1c9614b3cc4ed886ad242721cbc764da85452270aa597e5eedf1d2bd', 'user@test.com', '18555555555', '053188888888', null, null, null, null, null, 'employee', 'user9_twd4', '用户09', '0', '0', '2018-12-03 11:21:28', null, null, null, null, null, null, null, null, null, null, null, null, '0', '0', 'system', '2018-12-03 11:21:28', 'system', '2018-12-03 11:21:28', null, '0', 'JeeSite', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for js_sys_user_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_user_data_scope`;
CREATE TABLE `js_sys_user_data_scope` (
  `user_code` varchar(100) NOT NULL COMMENT '控制用户编码',
  `ctrl_type` varchar(20) NOT NULL COMMENT '控制类型',
  `ctrl_data` varchar(64) NOT NULL COMMENT '控制数据',
  `ctrl_permi` varchar(64) NOT NULL COMMENT '控制权限',
  PRIMARY KEY (`user_code`,`ctrl_type`,`ctrl_data`,`ctrl_permi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户数据权限表';

-- ----------------------------
-- Records of js_sys_user_data_scope
-- ----------------------------

-- ----------------------------
-- Table structure for js_sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_user_role`;
CREATE TABLE `js_sys_user_role` (
  `user_code` varchar(100) NOT NULL COMMENT '用户编码',
  `role_code` varchar(64) NOT NULL COMMENT '角色编码',
  PRIMARY KEY (`user_code`,`role_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与角色关联表';

-- ----------------------------
-- Records of js_sys_user_role
-- ----------------------------
INSERT INTO `js_sys_user_role` VALUES ('user10_nw09', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user11_ibcm', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user12_8hyj', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user13_h3sk', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user14_deqy', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user15_c5sr', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user16_pkfs', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user17_ogfo', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user18_ycie', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user19_urge', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user1_cwme', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user20_e7n4', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user21_oxfg', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user22_o5j5', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user23_ymbm', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user2_fuda', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user3_ll1n', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user4_m1vk', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user5_pwtv', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user6_hpw5', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user7_lxut', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user8_wp9j', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user9_twd4', 'user');

-- ----------------------------
-- Table structure for port_deptempstation_bak
-- ----------------------------
DROP TABLE IF EXISTS `port_deptempstation_bak`;
CREATE TABLE `port_deptempstation_bak` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Dept` varchar(50) DEFAULT NULL COMMENT '部门',
  `FK_Station` varchar(50) DEFAULT NULL COMMENT '岗位',
  `FK_Emp` varchar(50) DEFAULT NULL COMMENT '操作员',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of port_deptempstation_bak
-- ----------------------------
INSERT INTO `port_deptempstation_bak` VALUES ('1001_zhanghaicheng_02', '1001', '02', 'zhanghaicheng');
INSERT INTO `port_deptempstation_bak` VALUES ('1001_zhangyifan_07', '1001', '07', 'zhangyifan');
INSERT INTO `port_deptempstation_bak` VALUES ('1001_zhoushengyu_07', '1001', '07', 'zhoushengyu');
INSERT INTO `port_deptempstation_bak` VALUES ('1002_qifenglin_03', '1002', '03', 'qifenglin');
INSERT INTO `port_deptempstation_bak` VALUES ('1002_zhoutianjiao_08', '1002', '08', 'zhoutianjiao');
INSERT INTO `port_deptempstation_bak` VALUES ('1003_fuhui_09', '1003', '09', 'fuhui');
INSERT INTO `port_deptempstation_bak` VALUES ('1003_guoxiangbin_04', '1003', '04', 'guoxiangbin');
INSERT INTO `port_deptempstation_bak` VALUES ('1004_guobaogeng_10', '1004', '10', 'guobaogeng');
INSERT INTO `port_deptempstation_bak` VALUES ('1004_yangyilei_05', '1004', '05', 'yangyilei');
INSERT INTO `port_deptempstation_bak` VALUES ('1005_liping_06', '1005', '06', 'liping');
INSERT INTO `port_deptempstation_bak` VALUES ('1005_liyan_11', '1005', '11', 'liyan');
INSERT INTO `port_deptempstation_bak` VALUES ('100_zhoupeng_01', '100', '01', 'zhoupeng');
INSERT INTO `port_deptempstation_bak` VALUES ('1099_Guest_12', '1005', '12', 'Guest');

-- ----------------------------
-- Table structure for port_deptemp_bak
-- ----------------------------
DROP TABLE IF EXISTS `port_deptemp_bak`;
CREATE TABLE `port_deptemp_bak` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Dept` varchar(50) DEFAULT NULL COMMENT '部门',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '操作员',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of port_deptemp_bak
-- ----------------------------
INSERT INTO `port_deptemp_bak` VALUES ('1001_zhanghaicheng', '1001', 'zhanghaicheng');
INSERT INTO `port_deptemp_bak` VALUES ('1001_zhangyifan', '1001', 'zhangyifan');
INSERT INTO `port_deptemp_bak` VALUES ('1001_zhoushengyu', '1001', 'zhoushengyu');
INSERT INTO `port_deptemp_bak` VALUES ('1002_qifenglin', '1002', 'qifenglin');
INSERT INTO `port_deptemp_bak` VALUES ('1002_zhoutianjiao', '1002', 'zhoutianjiao');
INSERT INTO `port_deptemp_bak` VALUES ('1003_fuhui', '1003', 'fuhui');
INSERT INTO `port_deptemp_bak` VALUES ('1003_guoxiangbin', '1003', 'guoxiangbin');
INSERT INTO `port_deptemp_bak` VALUES ('1004_guobaogeng', '1004', 'guobaogeng');
INSERT INTO `port_deptemp_bak` VALUES ('1004_yangyilei', '1004', 'yangyilei');
INSERT INTO `port_deptemp_bak` VALUES ('1005_liping', '1005', 'liping');
INSERT INTO `port_deptemp_bak` VALUES ('1005_liyan', '1005', 'liyan');
INSERT INTO `port_deptemp_bak` VALUES ('100_zhoupeng', '100', 'zhoupeng');

-- ----------------------------
-- Table structure for port_deptsearchscorp
-- ----------------------------
DROP TABLE IF EXISTS `port_deptsearchscorp`;
CREATE TABLE `port_deptsearchscorp` (
  `FK_Emp` varchar(50) NOT NULL COMMENT '操作员 - 主键',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Emp`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门查询权限';

-- ----------------------------
-- Records of port_deptsearchscorp
-- ----------------------------

-- ----------------------------
-- Table structure for port_deptstation
-- ----------------------------
DROP TABLE IF EXISTS `port_deptstation`;
CREATE TABLE `port_deptstation` (
  `FK_Dept` varchar(15) NOT NULL COMMENT '部门',
  `FK_Station` varchar(100) NOT NULL COMMENT '岗位',
  PRIMARY KEY (`FK_Dept`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of port_deptstation
-- ----------------------------
INSERT INTO `port_deptstation` VALUES ('100', '01');
INSERT INTO `port_deptstation` VALUES ('1001', '07');
INSERT INTO `port_deptstation` VALUES ('1002', '08');
INSERT INTO `port_deptstation` VALUES ('1003', '09');
INSERT INTO `port_deptstation` VALUES ('1004', '10');
INSERT INTO `port_deptstation` VALUES ('1005', '11');

-- ----------------------------
-- Table structure for port_depttype
-- ----------------------------
DROP TABLE IF EXISTS `port_depttype`;
CREATE TABLE `port_depttype` (
  `No` varchar(2) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门类型';

-- ----------------------------
-- Records of port_depttype
-- ----------------------------

-- ----------------------------
-- Table structure for port_dept_bak
-- ----------------------------
DROP TABLE IF EXISTS `port_dept_bak`;
CREATE TABLE `port_dept_bak` (
  `No` varchar(50) NOT NULL COMMENT '编号',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点编号',
  `NameOfPath` varchar(300) DEFAULT '' COMMENT '部门路径',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序号',
  `OrgNo` varchar(50) DEFAULT '' COMMENT '隶属组织',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of port_dept_bak
-- ----------------------------
INSERT INTO `port_dept_bak` VALUES ('100', '集团总部', '0', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1001', '集团市场部', '100', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1002', '集团研发部', '100', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1003', '集团服务部', '100', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1004', '集团财务部', '100', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1005', '集团人力资源部', '100', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1060', '南方分公司', '100', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1061', '市场部', '1060', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1062', '财务部', '1060', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1063', '销售部', '1060', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1070', '北方分公司', '100', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1071', '市场部', '1070', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1072', '财务部', '1070', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1073', '销售部', '1070', '', '0', '');
INSERT INTO `port_dept_bak` VALUES ('1099', '外来单位', '100', '', '0', '');

-- ----------------------------
-- Table structure for port_emp_bak
-- ----------------------------
DROP TABLE IF EXISTS `port_emp_bak`;
CREATE TABLE `port_emp_bak` (
  `No` varchar(50) NOT NULL DEFAULT '',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `Pass` varchar(100) DEFAULT NULL,
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门',
  `SID` varchar(36) DEFAULT NULL COMMENT '安全校验码',
  `Tel` varchar(20) DEFAULT '' COMMENT '电话',
  `Email` varchar(100) DEFAULT '' COMMENT '邮箱',
  `PinYin` varchar(500) DEFAULT '' COMMENT '拼音',
  `SignType` int(11) DEFAULT '0' COMMENT '签字类型',
  `Idx` int(11) DEFAULT '0' COMMENT '序号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of port_emp_bak
-- ----------------------------
INSERT INTO `port_emp_bak` VALUES ('admin', 'admin', '123', '100', null, '0531-82374939', 'zhoupeng@ccflow.org', '', '0', '0');
INSERT INTO `port_emp_bak` VALUES ('fuhui', '福惠', '123', '1003', null, '0531-82374939', 'fuhui@ccflow.org', '', '0', '0');
INSERT INTO `port_emp_bak` VALUES ('guobaogeng', '郭宝庚', '123', '1004', null, '0531-82374939', 'guobaogeng@ccflow.org', '', '0', '0');
INSERT INTO `port_emp_bak` VALUES ('guoxiangbin', '郭祥斌', '123', '1003', null, '0531-82374939', 'guoxiangbin@ccflow.org', '', '0', '0');
INSERT INTO `port_emp_bak` VALUES ('liping', '李萍', '123', '1005', null, '0531-82374939', 'liping@ccflow.org', '', '0', '0');
INSERT INTO `port_emp_bak` VALUES ('liyan', '李言', '123', '1005', null, '0531-82374939', 'liyan@ccflow.org', '', '0', '0');
INSERT INTO `port_emp_bak` VALUES ('qifenglin', '祁凤林', '123', '1002', null, '0531-82374939', 'qifenglin@ccflow.org', '', '0', '0');
INSERT INTO `port_emp_bak` VALUES ('yangyilei', '杨依雷', '123', '1004', null, '0531-82374939', 'yangyilei@ccflow.org', '', '0', '0');
INSERT INTO `port_emp_bak` VALUES ('zhanghaicheng', '张海成', '123', '1001', null, '0531-82374939', 'zhanghaicheng@ccflow.org', '', '0', '0');
INSERT INTO `port_emp_bak` VALUES ('zhangyifan', '张一帆', '123', '1001', null, '0531-82374939', 'zhangyifan@ccflow.org', '', '0', '0');
INSERT INTO `port_emp_bak` VALUES ('zhoupeng', '周朋', '123', '100', null, '0531-82374939', 'zhoupeng@ccflow.org', '', '0', '0');
INSERT INTO `port_emp_bak` VALUES ('zhoushengyu', '周升雨', '123', '1001', null, '0531-82374939', 'zhoushengyu@ccflow.org', '', '0', '0');
INSERT INTO `port_emp_bak` VALUES ('zhoutianjiao', '周天娇', '123', '1002', null, '0531-82374939', 'zhoutianjiao@ccflow.org', '', '0', '0');

-- ----------------------------
-- Table structure for port_helper
-- ----------------------------
DROP TABLE IF EXISTS `port_helper`;
CREATE TABLE `port_helper` (
  `No` varchar(3) NOT NULL COMMENT '编号',
  `Name` text COMMENT '描述',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '附件或图片',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of port_helper
-- ----------------------------

-- ----------------------------
-- Table structure for port_inc
-- ----------------------------
DROP TABLE IF EXISTS `port_inc`;
CREATE TABLE `port_inc` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(30) DEFAULT NULL COMMENT '父节点编号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='独立组织';

-- ----------------------------
-- Records of port_inc
-- ----------------------------

-- ----------------------------
-- Table structure for port_org
-- ----------------------------
DROP TABLE IF EXISTS `port_org`;
CREATE TABLE `port_org` (
  `No` varchar(30) NOT NULL,
  `Name` varchar(60) DEFAULT NULL,
  `ParentNo` varchar(60) DEFAULT NULL,
  `ParentName` varchar(60) DEFAULT NULL,
  `Adminer` varchar(60) DEFAULT NULL,
  `AdminerName` varchar(60) DEFAULT NULL,
  `Idx` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of port_org
-- ----------------------------

-- ----------------------------
-- Table structure for port_stationtype_bak
-- ----------------------------
DROP TABLE IF EXISTS `port_stationtype_bak`;
CREATE TABLE `port_stationtype_bak` (
  `No` varchar(2) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序',
  `OrgNo` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of port_stationtype_bak
-- ----------------------------
INSERT INTO `port_stationtype_bak` VALUES ('1', '高层', '0', null);
INSERT INTO `port_stationtype_bak` VALUES ('2', '中层', '0', null);
INSERT INTO `port_stationtype_bak` VALUES ('3', '基层', '0', null);

-- ----------------------------
-- Table structure for port_station_bak
-- ----------------------------
DROP TABLE IF EXISTS `port_station_bak`;
CREATE TABLE `port_station_bak` (
  `No` varchar(4) NOT NULL COMMENT '编号',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `FK_StationType` varchar(100) DEFAULT NULL COMMENT '类型',
  `OrgNo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of port_station_bak
-- ----------------------------
INSERT INTO `port_station_bak` VALUES ('01', '总经理', '1', '0');
INSERT INTO `port_station_bak` VALUES ('02', '市场部经理', '2', '0');
INSERT INTO `port_station_bak` VALUES ('03', '研发部经理', '2', '0');
INSERT INTO `port_station_bak` VALUES ('04', '客服部经理', '2', '0');
INSERT INTO `port_station_bak` VALUES ('05', '财务部经理', '2', '0');
INSERT INTO `port_station_bak` VALUES ('06', '人力资源部经理', '2', '0');
INSERT INTO `port_station_bak` VALUES ('07', '销售人员岗', '3', '0');
INSERT INTO `port_station_bak` VALUES ('08', '程序员岗', '3', '0');
INSERT INTO `port_station_bak` VALUES ('09', '技术服务岗', '3', '0');
INSERT INTO `port_station_bak` VALUES ('10', '出纳岗', '3', '0');
INSERT INTO `port_station_bak` VALUES ('11', '人力资源助理岗', '3', '0');
INSERT INTO `port_station_bak` VALUES ('12', '外来人员岗', '3', '0');

-- ----------------------------
-- Table structure for pub_day
-- ----------------------------
DROP TABLE IF EXISTS `pub_day`;
CREATE TABLE `pub_day` (
  `No` varchar(30) NOT NULL COMMENT '编号',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pub_day
-- ----------------------------

-- ----------------------------
-- Table structure for pub_nd
-- ----------------------------
DROP TABLE IF EXISTS `pub_nd`;
CREATE TABLE `pub_nd` (
  `No` varchar(30) NOT NULL COMMENT '编号',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pub_nd
-- ----------------------------

-- ----------------------------
-- Table structure for pub_ny
-- ----------------------------
DROP TABLE IF EXISTS `pub_ny`;
CREATE TABLE `pub_ny` (
  `No` varchar(30) NOT NULL COMMENT '编号',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pub_ny
-- ----------------------------
INSERT INTO `pub_ny` VALUES ('2019-09', '2019-09');

-- ----------------------------
-- Table structure for pub_yf
-- ----------------------------
DROP TABLE IF EXISTS `pub_yf`;
CREATE TABLE `pub_yf` (
  `No` varchar(30) NOT NULL COMMENT '编号',
  `Name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pub_yf
-- ----------------------------

-- ----------------------------
-- Table structure for spgw
-- ----------------------------
DROP TABLE IF EXISTS `spgw`;
CREATE TABLE `spgw` (
  `TXR` varchar(300) DEFAULT NULL COMMENT 'TXR',
  `BGSR` varchar(300) DEFAULT NULL COMMENT '审批人',
  `CWCR` varchar(300) DEFAULT NULL COMMENT '审核人',
  `GBCR` varchar(300) DEFAULT NULL COMMENT '审核人',
  `GHCR` varchar(300) DEFAULT NULL COMMENT '审核人',
  `PFRQ` varchar(50) DEFAULT NULL COMMENT '批复日期',
  `SQDW` varchar(300) DEFAULT NULL COMMENT '申请单位',
  `TZRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `TZYJ` varchar(300) DEFAULT NULL COMMENT '厅长审批',
  `BGSRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `BGSYJ` varchar(300) DEFAULT NULL COMMENT '办公室审核',
  `CWCRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `CWCYJ` varchar(300) DEFAULT NULL COMMENT '财务处审核',
  `GBCRQ` varchar(50) DEFAULT NULL COMMENT '时间',
  `GHCRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `GHCYJ` varchar(300) DEFAULT NULL COMMENT '规划处意见',
  `DJGLCR` varchar(300) DEFAULT NULL COMMENT '审核人',
  `Context` varchar(300) DEFAULT NULL COMMENT '批复内容',
  `DJGLCRQ` varchar(50) DEFAULT NULL COMMENT '日期',
  `DJGLCYJ` varchar(300) DEFAULT NULL COMMENT '地籍管理处意见',
  `STGBCYJ` varchar(300) DEFAULT NULL COMMENT '省厅耕保处意见',
  `JBXX_XMMC` varchar(300) DEFAULT NULL COMMENT 'JBXX_XMMC',
  `PiFuWenHao` varchar(300) DEFAULT NULL COMMENT '批复文号',
  `OID` int(11) NOT NULL COMMENT 'OID',
  `RDT` varchar(50) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of spgw
-- ----------------------------

-- ----------------------------
-- Table structure for sys_cfield
-- ----------------------------
DROP TABLE IF EXISTS `sys_cfield`;
CREATE TABLE `sys_cfield` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `EnsName` varchar(100) DEFAULT NULL COMMENT '实体类名称',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '工作人员',
  `Attrs` text COMMENT '属性s',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='列选择';

-- ----------------------------
-- Records of sys_cfield
-- ----------------------------

-- ----------------------------
-- Table structure for sys_contrast
-- ----------------------------
DROP TABLE IF EXISTS `sys_contrast`;
CREATE TABLE `sys_contrast` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `ContrastKey` varchar(20) DEFAULT NULL COMMENT '对比项目',
  `KeyVal1` varchar(20) DEFAULT NULL COMMENT 'KeyVal1',
  `KeyVal2` varchar(20) DEFAULT NULL COMMENT 'KeyVal2',
  `SortBy` varchar(20) DEFAULT NULL COMMENT 'SortBy',
  `KeyOfNum` varchar(20) DEFAULT NULL COMMENT 'KeyOfNum',
  `GroupWay` int(11) DEFAULT NULL COMMENT '求什么?SumAvg',
  `OrderWay` varchar(10) DEFAULT NULL COMMENT 'OrderWay',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='对比状态存储';

-- ----------------------------
-- Records of sys_contrast
-- ----------------------------

-- ----------------------------
-- Table structure for sys_datarpt
-- ----------------------------
DROP TABLE IF EXISTS `sys_datarpt`;
CREATE TABLE `sys_datarpt` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `ColCount` varchar(50) DEFAULT NULL COMMENT '列',
  `RowCount` varchar(50) DEFAULT NULL COMMENT '行',
  `Val` float DEFAULT NULL COMMENT '值',
  `RefOID` float DEFAULT NULL COMMENT '关联的值',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报表数据存储模版';

-- ----------------------------
-- Records of sys_datarpt
-- ----------------------------

-- ----------------------------
-- Table structure for sys_defval
-- ----------------------------
DROP TABLE IF EXISTS `sys_defval`;
CREATE TABLE `sys_defval` (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '实体',
  `AttrKey` varchar(50) DEFAULT NULL COMMENT '节点对应字段',
  `LB` int(11) DEFAULT NULL COMMENT '类别',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '人员',
  `CurValue` text COMMENT '文本',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='选择词汇';

-- ----------------------------
-- Records of sys_defval
-- ----------------------------

-- ----------------------------
-- Table structure for sys_docfile
-- ----------------------------
DROP TABLE IF EXISTS `sys_docfile`;
CREATE TABLE `sys_docfile` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FileName` varchar(200) DEFAULT NULL COMMENT '名称',
  `FileSize` int(11) DEFAULT '0' COMMENT '大小',
  `FileType` varchar(50) DEFAULT NULL COMMENT '文件类型',
  `D1` text COMMENT 'D1',
  `D2` text COMMENT 'D2',
  `D3` text COMMENT 'D3',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_docfile
-- ----------------------------

-- ----------------------------
-- Table structure for sys_domain
-- ----------------------------
DROP TABLE IF EXISTS `sys_domain`;
CREATE TABLE `sys_domain` (
  `No` varchar(30) NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(30) DEFAULT NULL COMMENT 'Name',
  `DBLink` varchar(130) DEFAULT NULL COMMENT 'DBLink',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='域';

-- ----------------------------
-- Records of sys_domain
-- ----------------------------

-- ----------------------------
-- Table structure for sys_encfg
-- ----------------------------
DROP TABLE IF EXISTS `sys_encfg`;
CREATE TABLE `sys_encfg` (
  `No` varchar(100) NOT NULL COMMENT '实体名称',
  `GroupTitle` varchar(2000) DEFAULT NULL COMMENT '分组标签',
  `Url` varchar(500) DEFAULT NULL COMMENT '要打开的Url',
  `FJSavePath` varchar(100) DEFAULT NULL COMMENT '保存路径',
  `FJWebPath` varchar(100) DEFAULT NULL COMMENT '附件Web路径',
  `Datan` varchar(200) DEFAULT NULL COMMENT '字段数据分析方式',
  `UI` varchar(2000) DEFAULT NULL COMMENT 'UI设置',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  `UrlExt` varchar(500) DEFAULT '' COMMENT '要打开的Url',
  `ColorSet` varchar(500) DEFAULT '' COMMENT '颜色设置',
  `FieldSet` varchar(500) DEFAULT '' COMMENT '字段设置',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_encfg
-- ----------------------------
INSERT INTO `sys_encfg` VALUES ('BP.Frm.FrmBill', '@No=基础信息,单据基础配置信息.@BtnNewLable=单据按钮权限,用于控制每个功能按钮启用规则.@BtnImpExcel=列表按钮,列表按钮控制@Designer=设计者,流程开发设计者信息', null, null, null, null, null, null, '', '', '');
INSERT INTO `sys_encfg` VALUES ('BP.Frm.FrmDict', '@No=基础信息,单据基础配置信息.@BtnNewLable=单据按钮权限,用于控制每个功能按钮启用规则.@BtnImpExcel=列表按钮,列表按钮控制@Designer=设计者,流程开发设计者信息', null, null, null, null, null, null, '', '', '');
INSERT INTO `sys_encfg` VALUES ('BP.Sys.FrmUI.FrmAttachmentExt', '@MyPK=基础信息,附件的基本配置.\r\n@DeleteWay=权限控制,控制附件的下载与上传权限.@IsRowLock=WebOffice属性,设置与公文有关系的属性配置.\r\n@IsToHeLiuHZ=流程相关,控制节点附件的分合流.', null, null, null, null, null, null, '', '', '');
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.FlowExt', '@No=基础信息,基础信息权限信息.@IsBatchStart=数据&表单,数据导入导出.@IsFrmEnable=轨迹@DesignerNo=设计者,流程开发设计者信息', null, null, null, null, null, null, '', '', '');
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.FlowSheet', '@No=基本配置@FlowRunWay=启动方式,配置工作流程如何自动发起，该选项要与流程服务一起工作才有效.@StartLimitRole=启动限制规则@StartGuideWay=发起前置导航@CFlowWay=延续流程@DTSWay=流程数据与业务数据同步@PStarter=轨迹查看权限', null, null, null, null, null, null, '', '', '');
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.FrmNodeComponent', '@NodeID=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SFLab=父子流程组件,在该节点上配置与显示父子流程.@FrmThreadLab=子线程组件,对合流节点有效，用于配置与现实子线程运行的情况。@FrmTrackLab=轨迹组件,用于显示流程运行的轨迹图.@FTCLab=流转自定义,在每个节点上自己控制节点的处理人.', null, null, null, null, null, null, '', '', '');
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.MapDataExt', '@No=基本属性@Designer=设计者信息', null, null, null, null, null, null, '', '', '');
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.MapDtlExt', '@No=基础信息,基础信息权限信息.@IsExp=数据导入导出,数据导入导出.@IsEnableLink=超链接,显示在从表的右边.@IsCopyNDData=流程相关,与流程相关的配置非流程可以忽略.', null, null, null, null, null, null, '', '', '');
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.MapFrmFool', '@No=基础属性,基础属性.@Designer=设计者信息,设计者的单位信息，人员信息，可以上传到表单云.', null, null, null, null, null, null, '', '', '');
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.NodeExt', '@NodeID=基本配置@SendLab=按钮权限,控制工作节点可操作按钮.@RunModel=运行模式,分合流,父子流程@AutoJumpRole0=跳转,自动跳转规则当遇到该节点时如何让其自动的执行下一步.', null, null, null, null, null, null, '', '', '');
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.NodeSheet', '@NodeID=基本配置@FormType=表单@FWCSta=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SFSta=父子流程,对启动，查看父子流程的控件设置.@SendLab=按钮权限,控制工作节点可操作按钮.@RunModel=运行模式,分合流,父子流程@AutoJumpRole0=跳转,自动跳转规则当遇到该节点时如何让其自动的执行下一步.@MPhone_WorkModel=移动,与手机平板电脑相关的应用设置.@OfficeOpen=公文按钮,只有当该节点是公文流程时候有效', null, null, null, null, null, null, '', '', '');

-- ----------------------------
-- Table structure for sys_enum
-- ----------------------------
DROP TABLE IF EXISTS `sys_enum`;
CREATE TABLE `sys_enum` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `Lab` varchar(300) DEFAULT NULL COMMENT 'Lab',
  `EnumKey` varchar(100) DEFAULT NULL COMMENT 'EnumKey',
  `IntKey` int(11) DEFAULT '0' COMMENT 'Val',
  `Lang` varchar(10) DEFAULT NULL COMMENT '语言',
  `OrgNo` varchar(50) DEFAULT '' COMMENT 'OrgNo',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_enum
-- ----------------------------
INSERT INTO `sys_enum` VALUES ('ActionType_CH_0', 'GET', 'ActionType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ActionType_CH_1', 'POST', 'ActionType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_0', '短信', 'AlertType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_1', '邮件', 'AlertType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_2', '邮件与短信', 'AlertType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_3', '系统(内部)消息', 'AlertType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_0', '不接收', 'AlertWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_1', '短信', 'AlertWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_2', '邮件', 'AlertWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_3', '内部消息', 'AlertWay', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_4', 'QQ消息', 'AlertWay', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_5', 'RTX消息', 'AlertWay', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_6', 'MSN消息', 'AlertWay', '6', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AppModel_CH_0', 'BS系统', 'AppModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AppModel_CH_1', 'CS系统', 'AppModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AppType_CH_0', '外部Url连接', 'AppType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AppType_CH_1', '本地可执行文件', 'AppType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AthCtrlWay_CH_0', 'PK-主键', 'AthCtrlWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AthCtrlWay_CH_1', 'FID-流程ID', 'AthCtrlWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AthCtrlWay_CH_2', 'ParentID-父流程ID', 'AthCtrlWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AthCtrlWay_CH_3', '仅能查看自己上传的附件', 'AthCtrlWay', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AthCtrlWay_CH_4', '按照WorkID计算(对流程节点表单有效)', 'AthCtrlWay', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AthSaveWay_CH_0', '保存到IIS服务器', 'AthSaveWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AthSaveWay_CH_1', '保存到数据库', 'AthSaveWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AthSaveWay_CH_2', 'ftp服务器', 'AthSaveWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AthUploadWay_CH_0', '继承模式', 'AthUploadWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AthUploadWay_CH_1', '协作模式', 'AthUploadWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AuthorWay_CH_0', '不授权', 'AuthorWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AuthorWay_CH_1', '全部流程授权', 'AuthorWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('AuthorWay_CH_2', '指定流程授权', 'AuthorWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('BillOpenModel_CH_0', '下载本地', 'BillOpenModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('BillOpenModel_CH_1', '在线WebOffice打开', 'BillOpenModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_0', '上一步可以撤销', 'CancelRole', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_1', '不能撤销', 'CancelRole', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_2', '上一步与开始节点可以撤销', 'CancelRole', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_3', '指定的节点可以撤销', 'CancelRole', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_0', '不能抄送', 'CCRole', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_1', '手工抄送', 'CCRole', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_2', '自动抄送', 'CCRole', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_3', '手工与自动', 'CCRole', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_4', '按表单SysCCEmps字段计算', 'CCRole', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_5', '在发送前打开抄送窗口', 'CCRole', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCStaWay_CH_0', '仅按岗位计算', 'CCStaWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCStaWay_CH_1', '按岗位智能计算(当前节点)', 'CCStaWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCStaWay_CH_2', '按岗位智能计算(发送到节点)', 'CCStaWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCStaWay_CH_3', '按岗位与部门的交集', 'CCStaWay', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCStaWay_CH_4', '按直线上级部门找岗位下的人员(当前节点)', 'CCStaWay', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCStaWay_CH_5', '按直线上级部门找岗位下的人员(接受节点)', 'CCStaWay', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCWriteTo_CH_0', '写入抄送列表', 'CCWriteTo', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCWriteTo_CH_1', '写入待办', 'CCWriteTo', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CCWriteTo_CH_2', '写入待办与抄送列表', 'CCWriteTo', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CeShiMoShi_CH_0', '强办模式', 'CeShiMoShi', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CeShiMoShi_CH_1', '协作模式', 'CeShiMoShi', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CeShiMoShi_CH_2', '队列模式', 'CeShiMoShi', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CeShiMoShi_CH_3', '共享模式', 'CeShiMoShi', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CGDD_CH_0', '本市', 'CGDD', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CGDD_CH_1', '外地', 'CGDD', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CHAlertRole_CH_0', '不提示', 'CHAlertRole', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CHAlertRole_CH_1', '每天1次', 'CHAlertRole', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CHAlertRole_CH_2', '每天2次', 'CHAlertRole', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CHAlertWay_CH_0', '邮件', 'CHAlertWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CHAlertWay_CH_1', '短信', 'CHAlertWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CHAlertWay_CH_2', 'CCIM即时通讯', 'CHAlertWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ChartType_CH_0', '几何图形', 'ChartType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ChartType_CH_1', '肖像图片', 'ChartType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_0', '及时完成', 'CHSta', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_1', '按期完成', 'CHSta', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_2', '逾期完成', 'CHSta', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_3', '超期完成', 'CHSta', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrDT_CH_0', '跨0个单元格', 'ColSpanAttrDT', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrDT_CH_1', '跨1个单元格', 'ColSpanAttrDT', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrDT_CH_2', '跨2个单元格', 'ColSpanAttrDT', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrDT_CH_3', '跨3个单元格', 'ColSpanAttrDT', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrDT_CH_4', '跨4个单元格', 'ColSpanAttrDT', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrString_CH_1', '跨1个单元格', 'ColSpanAttrString', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrString_CH_2', '跨2个单元格', 'ColSpanAttrString', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrString_CH_3', '跨3个单元格', 'ColSpanAttrString', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrString_CH_4', '跨4个单元格', 'ColSpanAttrString', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrString_CH_5', '跨4个单元格', 'ColSpanAttrString', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ColSpanAttrString_CH_6', '跨4个单元格', 'ColSpanAttrString', '6', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CondModel_CH_0', '由连接线条件控制', 'CondModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CondModel_CH_1', '按照用户选择计算', 'CondModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CondModel_CH_2', '发送按钮旁下拉框选择', 'CondModel', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_0', '当前单元格', 'ConfirmKind', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_1', '左方单元格', 'ConfirmKind', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_2', '上方单元格', 'ConfirmKind', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_3', '右方单元格', 'ConfirmKind', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_4', '下方单元格', 'ConfirmKind', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ConnJudgeWay_CH_0', 'or', 'ConnJudgeWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ConnJudgeWay_CH_1', 'and', 'ConnJudgeWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_0', '单个', 'CtrlWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_1', '多个', 'CtrlWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_2', '指定', 'CtrlWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DataStoreModel_CH_0', '数据轨迹模式', 'DataStoreModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DataStoreModel_CH_1', '数据合并模式', 'DataStoreModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DataType_CH_0', '字符串', 'DataType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DataType_CH_1', '整数', 'DataType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DataType_CH_2', '浮点数', 'DataType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DataType_CH_3', '日期', 'DataType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DataType_CH_4', '日期时间', 'DataType', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DataType_CH_5', '外键', 'DataType', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DataType_CH_6', '枚举', 'DataType', '6', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_0', '应用系统主数据库(默认)', 'DBSrcType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_1', 'SQLServer数据库', 'DBSrcType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_100', 'WebService数据源', 'DBSrcType', '100', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_2', 'Oracle数据库', 'DBSrcType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_3', 'MySQL数据库', 'DBSrcType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_4', 'Informix数据库', 'DBSrcType', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_50', 'Dubbo服务', 'DBSrcType', '50', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_0', '不能删除', 'DelEnable', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_1', '逻辑删除', 'DelEnable', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_2', '记录日志方式删除', 'DelEnable', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_3', '彻底删除', 'DelEnable', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_4', '让用户决定删除方式', 'DelEnable', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DeleteWay_CH_0', '不能删除', 'DeleteWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DeleteWay_CH_1', '删除所有', 'DeleteWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DeleteWay_CH_2', '只能删除自己上传的', 'DeleteWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DocType_CH_0', '正式公文', 'DocType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DocType_CH_1', '便函', 'DocType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('Draft_CH_0', '无(不设草稿)', 'Draft', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('Draft_CH_1', '保存到待办', 'Draft', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('Draft_CH_2', '保存到草稿箱', 'Draft', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DtlAddRecModel_CH_0', '按设置的数量初始化空白行', 'DtlAddRecModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DtlAddRecModel_CH_1', '用按钮增加空白行', 'DtlAddRecModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DtlOpenType_CH_0', '操作员', 'DtlOpenType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DtlOpenType_CH_1', '工作ID', 'DtlOpenType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DtlOpenType_CH_2', '流程ID', 'DtlOpenType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DtlSaveModel_CH_0', '自动存盘(失去焦点自动存盘)', 'DtlSaveModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DtlSaveModel_CH_1', '手动存盘(保存按钮触发存盘)', 'DtlSaveModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DtlVer_CH_0', '2017传统版', 'DtlVer', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DTSearchWay_CH_0', '不启用', 'DTSearchWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DTSearchWay_CH_1', '按日期', 'DTSearchWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DTSearchWay_CH_2', '按日期时间', 'DTSearchWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DTSWay_CH_0', '不考核', 'DTSWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DTSWay_CH_1', '按照时效考核', 'DTSWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('DTSWay_CH_2', '按照工作量考核', 'DTSWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_0', '无编辑器', 'EditerType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_1', 'Sina编辑器0', 'EditerType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_2', 'FKEditer', 'EditerType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_3', 'KindEditor', 'EditerType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_4', '百度的UEditor', 'EditerType', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EditModel_CH_0', '表格模式', 'EditModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EditModel_CH_1', '傻瓜表单', 'EditModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EditModel_CH_2', '自由表单', 'EditModel', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EntityType_CH_0', '独立表单', 'EntityType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EntityType_CH_1', '单据', 'EntityType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EntityType_CH_2', '编号名称实体', 'EntityType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EntityType_CH_3', '树结构实体', 'EntityType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EnumUIContralType_CH_1', '下拉框', 'EnumUIContralType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EnumUIContralType_CH_3', '单选按钮', 'EnumUIContralType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EventType_CH_0', '禁用', 'EventType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EventType_CH_1', '执行URL', 'EventType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('EventType_CH_2', '执行CCFromRef.js', 'EventType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ExcelType_CH_0', '普通文件数据提取', 'ExcelType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ExcelType_CH_1', '流程附件数据提取', 'ExcelType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ExpType_CH_3', '按照SQL计算', 'ExpType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ExpType_CH_4', '按照参数计算', 'ExpType', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FeiYongYuSuan_CH_0', '1万元以下', 'FeiYongYuSuan', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FeiYongYuSuan_CH_1', '1-2万元', 'FeiYongYuSuan', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FeiYongYuSuan_CH_2', '2-5万元', 'FeiYongYuSuan', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FeiYongYuSuan_CH_3', '5-10万元', 'FeiYongYuSuan', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FeiYongYuSuan_CH_4', '10-20万元', 'FeiYongYuSuan', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FeiYongYuSuan_CH_5', '20-30万元', 'FeiYongYuSuan', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FeiYongYuSuan_CH_6', '30-50万元', 'FeiYongYuSuan', '6', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FeiYongYuSuan_CH_7', '50-70万元', 'FeiYongYuSuan', '7', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FeiYongYuSuan_CH_8', '70-100万元', 'FeiYongYuSuan', '8', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FeiYongYuSuan_CH_9', '100万元', 'FeiYongYuSuan', '9', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FindLeader_CH_0', '直接领导', 'FindLeader', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FindLeader_CH_1', '指定职务级别的领导', 'FindLeader', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FindLeader_CH_2', '指定职务的领导', 'FindLeader', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FindLeader_CH_3', '指定岗位的领导', 'FindLeader', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_0', '关闭附件', 'FJOpen', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_1', '操作员', 'FJOpen', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_2', '工作ID', 'FJOpen', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_3', '流程ID', 'FJOpen', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_0', '超级管理员可以删除', 'FlowDeleteRole', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_1', '分级管理员可以删除', 'FlowDeleteRole', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_2', '发起人可以删除', 'FlowDeleteRole', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_3', '节点启动删除按钮的操作员', 'FlowDeleteRole', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_0', '手工启动', 'FlowRunWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_1', '指定人员按时启动', 'FlowRunWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_2', '数据集按时启动', 'FlowRunWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_3', '触发式启动', 'FlowRunWay', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FLRole_CH_0', '按接受人', 'FLRole', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FLRole_CH_1', '按部门', 'FLRole', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FLRole_CH_2', '按岗位', 'FLRole', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmSln_CH_0', '默认方案', 'FrmSln', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmSln_CH_1', '只读方案', 'FrmSln', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmSln_CH_2', '自定义方案', 'FrmSln', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmThreadSta_CH_0', '禁用', 'FrmThreadSta', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmThreadSta_CH_1', '启用', 'FrmThreadSta', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_0', '傻瓜表单', 'FrmType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_1', '自由表单', 'FrmType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_11', '累加表单', 'FrmType', '11', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_3', '嵌入式表单', 'FrmType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_4', 'Word表单', 'FrmType', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_5', '在线编辑模式Excel表单', 'FrmType', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_6', 'VSTO模式Excel表单', 'FrmType', '6', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_7', '实体类组件', 'FrmType', '7', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmType_CH_8', '开发者表单', 'FrmType', '8', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_0', '不显示', 'FrmUrlShowWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_1', '自动大小', 'FrmUrlShowWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_2', '指定大小', 'FrmUrlShowWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_3', '新窗口', 'FrmUrlShowWay', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FTCWorkModel_CH_0', '简洁模式', 'FTCWorkModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FTCWorkModel_CH_1', '高级模式', 'FTCWorkModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_0', '不启用', 'FWCAth', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_1', '多附件', 'FWCAth', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_2', '单附件(暂不支持)', 'FWCAth', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_3', '图片附件(暂不支持)', 'FWCAth', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCMsgShow_CH_0', '都显示', 'FWCMsgShow', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCMsgShow_CH_1', '仅显示自己的意见', 'FWCMsgShow', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCOrderModel_CH_0', '按审批时间先后排序', 'FWCOrderModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCOrderModel_CH_1', '按照接受人员列表先后顺序(官职大小)', 'FWCOrderModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCShowModel_CH_0', '表格方式', 'FWCShowModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCShowModel_CH_1', '自由模式', 'FWCShowModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCSta_CH_0', '禁用', 'FWCSta', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCSta_CH_1', '启用', 'FWCSta', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCSta_CH_2', '只读', 'FWCSta', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCType_CH_0', '审核组件', 'FWCType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCType_CH_1', '日志组件', 'FWCType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCType_CH_2', '周报组件', 'FWCType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCType_CH_3', '月报组件', 'FWCType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCVer_CH_0', '2018', 'FWCVer', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FWCVer_CH_1', '2019', 'FWCVer', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FYLX_CH_0', '汽车票', 'FYLX', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FYLX_CH_1', '打的票', 'FYLX', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('FYLX_CH_2', '火车票', 'FYLX', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('GengDiJianSheJiBenTiaoJian_CH_0', '条件1', 'GengDiJianSheJiBenTiaoJian', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('GengDiJianSheJiBenTiaoJian_CH_1', '条件2', 'GengDiJianSheJiBenTiaoJian', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('GengDiJianSheJiBenTiaoJian_CH_2', '条件3', 'GengDiJianSheJiBenTiaoJian', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('HuiQianRole_CH_0', '不启用', 'HuiQianRole', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('HuiQianRole_CH_1', '协作(同事)模式', 'HuiQianRole', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('HuiQianRole_CH_4', '组长(领导)模式', 'HuiQianRole', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('HungUpWay_CH_0', '无限挂起', 'HungUpWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('HungUpWay_CH_1', '按指定的时间解除挂起并通知我自己', 'HungUpWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('HungUpWay_CH_2', '按指定的时间解除挂起并通知所有人', 'HungUpWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ImgSrcType_CH_0', '本地', 'ImgSrcType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ImgSrcType_CH_1', 'URL', 'ImgSrcType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ImpModel_CH_0', '不导入', 'ImpModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ImpModel_CH_1', '按配置模式导入', 'ImpModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ImpModel_CH_2', '按照xls文件模版导入', 'ImpModel', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSubFlowOver_CH_0', '不处理', 'IsAutoSendSubFlowOver', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSubFlowOver_CH_1', '让父流程自动运行下一步', 'IsAutoSendSubFlowOver', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSubFlowOver_CH_2', '结束父流程', 'IsAutoSendSubFlowOver', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_0', '无', 'IsSigan', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_1', '图片签名', 'IsSigan', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_2', '山东CA', 'IsSigan', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_3', '广东CA', 'IsSigan', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_4', '图片盖章', 'IsSigan', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_0', 'yyyy-MM-dd', 'IsSupperText', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_1', 'yyyy-MM-dd HH:mm', 'IsSupperText', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_2', 'yyyy-MM-dd HH:mm:ss', 'IsSupperText', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_3', 'yyyy-MM', 'IsSupperText', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_4', 'HH:mm', 'IsSupperText', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_5', 'HH:mm:ss', 'IsSupperText', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('jd_CH_0', '未定义', 'jd', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JiMiChengDu_CH_0', '公开', 'JiMiChengDu', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JiMiChengDu_CH_1', '保密', 'JiMiChengDu', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JiMiChengDu_CH_2', '秘密', 'JiMiChengDu', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JiMiChengDu_CH_3', '机密', 'JiMiChengDu', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JingJiLeiXing_CH_0', '内资', 'JingJiLeiXing', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JingJiLeiXing_CH_1', '外资', 'JingJiLeiXing', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JinJiChengDu_CH_0', '平件', 'JinJiChengDu', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JinJiChengDu_CH_1', '紧急', 'JinJiChengDu', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('jjlx1_CH_0', '内资', 'jjlx1', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_0', '一般', 'JMCD', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_1', '保密', 'JMCD', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_2', '秘密', 'JMCD', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_3', '机密', 'JMCD', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_0', '不能跳转', 'JumpWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_1', '只能向后跳转', 'JumpWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_2', '只能向前跳转', 'JumpWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_3', '任意节点跳转', 'JumpWay', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_4', '按指定规则跳转', 'JumpWay', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('LGType_CH_0', '普通', 'LGType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('LGType_CH_1', '枚举', 'LGType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('LGType_CH_2', '外键', 'LGType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('LGType_CH_3', '打开系统页面', 'LGType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ListShowModel_CH_0', '表格', 'ListShowModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ListShowModel_CH_1', '卡片', 'ListShowModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MenuCtrlWay_CH_0', '按照设置的控制', 'MenuCtrlWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MenuCtrlWay_CH_1', '任何人都可以使用', 'MenuCtrlWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MenuCtrlWay_CH_2', 'Admin用户可以使用', 'MenuCtrlWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_0', '系统根目录', 'MenuType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_1', '系统类别', 'MenuType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_2', '系统', 'MenuType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_3', '目录', 'MenuType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_4', '功能', 'MenuType', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_5', '功能控制点', 'MenuType', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MiMiDengJi_CH_0', '无', 'MiMiDengJi', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MiMiDengJi_CH_1', '普通', 'MiMiDengJi', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MiMiDengJi_CH_2', '秘密', 'MiMiDengJi', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MiMiDengJi_CH_3', '机密', 'MiMiDengJi', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MiMiDengJi_CH_4', '绝密', 'MiMiDengJi', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MobileShowModel_CH_0', '新页面显示模式', 'MobileShowModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MobileShowModel_CH_1', '列表模式', 'MobileShowModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('Model_CH_0', '普通', 'Model', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('Model_CH_1', '固定行', 'Model', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MoveToShowWay_CH_0', '不显示', 'MoveToShowWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MoveToShowWay_CH_1', '下拉列表0', 'MoveToShowWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MoveToShowWay_CH_2', '平铺', 'MoveToShowWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_0', '不发送', 'MsgCtrl', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_1', '按设置的下一步接受人自动发送（默认）', 'MsgCtrl', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_2', '由本节点表单系统字段(IsSendEmail,IsSendSMS)来决定', 'MsgCtrl', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_3', '由SDK开发者参数(IsSendEmail,IsSendSMS)来决定', 'MsgCtrl', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_1', '字符串String', 'MyDataType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_2', '整数类型Int', 'MyDataType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_3', '浮点类型AppFloat', 'MyDataType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_4', '判断类型Boolean', 'MyDataType', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_5', '双精度类型Double', 'MyDataType', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_6', '日期型Date', 'MyDataType', '6', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_7', '时间类型Datetime', 'MyDataType', '7', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_8', '金额类型AppMoney', 'MyDataType', '8', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MyDeptRole_CH_0', '仅部门领导可以查看', 'MyDeptRole', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MyDeptRole_CH_1', '部门下所有的人都可以查看', 'MyDeptRole', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('MyDeptRole_CH_2', '本部门里指定岗位的人可以查看', 'MyDeptRole', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('OpenWay_CH_0', '新窗口', 'OpenWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('OpenWay_CH_1', '本窗口', 'OpenWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('OpenWay_CH_2', '覆盖新窗口', 'OpenWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PopValFormat_CH_0', 'No(仅编号)', 'PopValFormat', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PopValFormat_CH_1', 'Name(仅名称)', 'PopValFormat', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PopValFormat_CH_2', 'No,Name(编号与名称,比如zhangsan,张三;lisi,李四;)', 'PopValFormat', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PrintDocEnable_CH_0', '不打印', 'PrintDocEnable', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PrintDocEnable_CH_1', '打印网页', 'PrintDocEnable', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PrintDocEnable_CH_2', '打印RTF模板', 'PrintDocEnable', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PrintDocEnable_CH_3', '打印Word模版', 'PrintDocEnable', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PrintPDFModle_CH_0', '全部打印', 'PrintPDFModle', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PrintPDFModle_CH_1', '单个表单打印(针对树形表单)', 'PrintPDFModle', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PRI_CH_0', '低', 'PRI', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PRI_CH_1', '中', 'PRI', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PRI_CH_2', '高', 'PRI', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_0', '按照指定节点的工作人员', 'PushWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_1', '按照指定的工作人员', 'PushWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_2', '按照指定的工作岗位', 'PushWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_3', '按照指定的部门', 'PushWay', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_4', '按照指定的SQL', 'PushWay', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_5', '按照系统指定的字段', 'PushWay', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('QingJiaLeiXing_CH_0', '事假', 'QingJiaLeiXing', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('QingJiaLeiXing_CH_1', '病假', 'QingJiaLeiXing', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('QingJiaLeiXing_CH_2', '婚假', 'QingJiaLeiXing', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('QRModel_CH_0', '不生成', 'QRModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('QRModel_CH_1', '生成二维码', 'QRModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('RBShowModel_CH_0', '竖向', 'RBShowModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('RBShowModel_CH_3', '横向', 'RBShowModel', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_0', '不回执', 'ReadReceipts', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_1', '自动回执', 'ReadReceipts', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_2', '由上一节点表单字段决定', 'ReadReceipts', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_3', '由SDK开发者参数决定', 'ReadReceipts', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReadRole_CH_0', '不控制', 'ReadRole', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReadRole_CH_1', '未阅读阻止发送', 'ReadRole', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReadRole_CH_2', '未阅读做记录', 'ReadRole', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReturnOneNodeRole_CH_0', '不启用', 'ReturnOneNodeRole', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReturnOneNodeRole_CH_1', '按照[退回信息填写字段]作为退回意见直接退回', 'ReturnOneNodeRole', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReturnOneNodeRole_CH_2', '按照[审核组件]填写的信息作为退回意见直接退回', 'ReturnOneNodeRole', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_0', '不能退回', 'ReturnRole', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_1', '只能退回上一个节点', 'ReturnRole', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_2', '可退回以前任意节点', 'ReturnRole', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_3', '可退回指定的节点', 'ReturnRole', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_4', '由流程图设计的退回路线决定', 'ReturnRole', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReturnSendModel_CH_0', '从退回节点正常执行', 'ReturnSendModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReturnSendModel_CH_1', '直接发送到当前节点', 'ReturnSendModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ReturnSendModel_CH_2', '直接发送到当前节点的下一个节点', 'ReturnSendModel', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('RowSpanAttrString_CH_1', '跨1行', 'RowSpanAttrString', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('RowSpanAttrString_CH_2', '跨2行', 'RowSpanAttrString', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('RowSpanAttrString_CH_3', '跨3行', 'RowSpanAttrString', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SaveModel_CH_0', '仅节点表', 'SaveModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SaveModel_CH_1', '节点表与Rpt表', 'SaveModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('sbsy_CH_0', '自有', 'sbsy', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SearchUrlOpenType_CH_0', 'En.htm', 'SearchUrlOpenType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SearchUrlOpenType_CH_1', 'EnOnly.htm', 'SearchUrlOpenType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SearchUrlOpenType_CH_2', '自定义url', 'SearchUrlOpenType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('sex__CH_0', '男', 'sex_', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('sex__CH_1', '女', 'sex_', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SFOpenType_CH_0', '工作查看器', 'SFOpenType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SFOpenType_CH_1', '傻瓜表单轨迹查看器', 'SFOpenType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SFShowCtrl_CH_0', '可以看所有的子流程', 'SFShowCtrl', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SFShowCtrl_CH_1', '仅仅可以看自己发起的子流程', 'SFShowCtrl', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SFShowModel_CH_0', '表格方式', 'SFShowModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SFShowModel_CH_1', '自由模式', 'SFShowModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SFSta_CH_0', '禁用', 'SFSta', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SFSta_CH_1', '启用', 'SFSta', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SFSta_CH_2', '只读', 'SFSta', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SharingType_CH_0', '共享', 'SharingType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SharingType_CH_1', '私有', 'SharingType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ShenFen_CH_0', '工人', 'ShenFen', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ShenFen_CH_1', '农民', 'ShenFen', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ShenFen_CH_2', '干部', 'ShenFen', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ShiFouYunXuZuoYe_CH_0', '符合安全标准允许作业', 'ShiFouYunXuZuoYe', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ShiFouYunXuZuoYe_CH_1', '不符合安全标准需要整改', 'ShiFouYunXuZuoYe', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ShowWhere_CH_0', '树形表单', 'ShowWhere', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ShowWhere_CH_1', '工具栏', 'ShowWhere', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ShowWhere_CH_2', '抄送工具栏', 'ShowWhere', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SignType_CH_0', '不签名', 'SignType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SignType_CH_1', '图片签名', 'SignType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SignType_CH_2', '电子签名', 'SignType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_0', '方向条件', 'SQLType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_1', '接受人规则', 'SQLType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_2', '下拉框数据过滤', 'SQLType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_3', '级联下拉框', 'SQLType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_4', 'PopVal开窗返回值', 'SQLType', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_5', '人员选择器人员选择范围', 'SQLType', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_0', '本地的类', 'SrcType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_1', '创建表', 'SrcType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_2', '表或视图', 'SrcType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_3', 'SQL查询表', 'SrcType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_4', 'WebServices', 'SrcType', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_5', '微服务Handler外部数据源', 'SrcType', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_6', 'JavaScript外部数据源', 'SrcType', '6', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_7', '系统字典表', 'SrcType', '7', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SSOType_CH_0', 'SID验证', 'SSOType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SSOType_CH_1', '连接', 'SSOType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SSOType_CH_2', '表单提交', 'SSOType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SSOType_CH_3', '不传值', 'SSOType', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SubFlowStartWay_CH_0', '不启动', 'SubFlowStartWay', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SubFlowStartWay_CH_1', '指定的字段启动', 'SubFlowStartWay', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SubFlowStartWay_CH_2', '按明细表启动', 'SubFlowStartWay', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SubFlowType_CH_0', '手动启动子流程', 'SubFlowType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SubFlowType_CH_1', '触发启动子流程', 'SubFlowType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SubFlowType_CH_2', '延续子流程', 'SubFlowType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SubThreadType_CH_0', '同表单', 'SubThreadType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('SubThreadType_CH_1', '异表单', 'SubThreadType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TableCol_CH_0', '4列', 'TableCol', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TableCol_CH_1', '6列', 'TableCol', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TableCol_CH_2', '3列', 'TableCol', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TabType_CH_0', '本地表或视图', 'TabType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TabType_CH_1', '通过一个SQL确定的一个外部数据源', 'TabType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TabType_CH_2', '通过WebServices获得的一个数据源', 'TabType', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('Target_CH_0', '新窗口', 'Target', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('Target_CH_1', '本窗口', 'Target', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('Target_CH_2', '父窗口', 'Target', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_0', '未开始', 'TaskSta', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_1', '进行中', 'TaskSta', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_2', '完成', 'TaskSta', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_3', '推迟', 'TaskSta', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TemplateFileModel_CH_0', 'rtf模版', 'TemplateFileModel', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TemplateFileModel_CH_1', 'vsto模式的word模版', 'TemplateFileModel', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TemplateFileModel_CH_2', 'vsto模式的excel模版', 'TemplateFileModel', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ThreadKillRole_CH_0', '不能删除', 'ThreadKillRole', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ThreadKillRole_CH_1', '手工删除', 'ThreadKillRole', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ThreadKillRole_CH_2', '自动删除', 'ThreadKillRole', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TimelineRole_CH_0', '按节点(由节点属性来定义)', 'TimelineRole', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TimelineRole_CH_1', '按发起人(开始节点SysSDTOfFlow字段计算)', 'TimelineRole', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ToobarExcType_CH_0', '超链接', 'ToobarExcType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ToobarExcType_CH_1', '函数', 'ToobarExcType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_0', '本周', 'TSpan', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_1', '上周', 'TSpan', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_2', '两周以前', 'TSpan', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_3', '三周以前', 'TSpan', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_4', '更早', 'TSpan', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UIRowStyleGlo_CH_0', '无风格', 'UIRowStyleGlo', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UIRowStyleGlo_CH_1', '交替风格', 'UIRowStyleGlo', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UIRowStyleGlo_CH_2', '鼠标移动', 'UIRowStyleGlo', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UIRowStyleGlo_CH_3', '交替并鼠标移动', 'UIRowStyleGlo', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UploadFileCheck_CH_0', '不控制', 'UploadFileCheck', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UploadFileCheck_CH_1', '上传附件个数不能为0', 'UploadFileCheck', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UploadFileCheck_CH_2', '每个类别下面的个数不能为0', 'UploadFileCheck', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UploadFileNumCheck_CH_0', '不用校验', 'UploadFileNumCheck', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UploadFileNumCheck_CH_1', '不能为空', 'UploadFileNumCheck', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UploadFileNumCheck_CH_2', '每个类别下不能为空', 'UploadFileNumCheck', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UrlSrcType_CH_0', '自定义', 'UrlSrcType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UrlSrcType_CH_1', '表单库', 'UrlSrcType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UserType_CH_0', '普通用户', 'UserType', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UserType_CH_1', '管理员用户', 'UserType', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UseSta_CH_0', '禁用', 'UseSta', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('UseSta_CH_1', '启用', 'UseSta', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_10', '批处理', 'WFStateApp', '10', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_2', '运行中', 'WFStateApp', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_3', '已完成', 'WFStateApp', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_4', '挂起', 'WFStateApp', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_5', '退回', 'WFStateApp', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_6', '转发', 'WFStateApp', '6', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_7', '删除', 'WFStateApp', '7', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_8', '加签', 'WFStateApp', '8', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_9', '冻结', 'WFStateApp', '9', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFState_CH_0', '空白', 'WFState', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFState_CH_1', '草稿', 'WFState', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFState_CH_10', '批处理', 'WFState', '10', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFState_CH_11', '加签回复状态', 'WFState', '11', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFState_CH_2', '运行中', 'WFState', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFState_CH_3', '已完成', 'WFState', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFState_CH_4', '挂起', 'WFState', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFState_CH_5', '退回', 'WFState', '5', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFState_CH_6', '转发', 'WFState', '6', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFState_CH_7', '删除', 'WFState', '7', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFState_CH_8', '加签', 'WFState', '8', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFState_CH_9', '冻结', 'WFState', '9', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFSta_CH_0', '运行中', 'WFSta', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFSta_CH_1', '已完成', 'WFSta', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WFSta_CH_2', '其他', 'WFSta', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WhenOverSize_CH_0', '不处理', 'WhenOverSize', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WhenOverSize_CH_1', '向下顺增行', 'WhenOverSize', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WhenOverSize_CH_2', '次页显示', 'WhenOverSize', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WhoExeIt_CH_0', '操作员执行', 'WhoExeIt', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WhoExeIt_CH_1', '机器执行', 'WhoExeIt', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WhoExeIt_CH_2', '混合执行', 'WhoExeIt', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_0', 'WorkID是主键', 'WhoIsPK', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_1', 'FID是主键(干流程的WorkID)', 'WhoIsPK', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_2', '父流程ID是主键', 'WhoIsPK', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_3', '延续流程ID是主键', 'WhoIsPK', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('XB_CH_0', '女', 'XB', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('XB_CH_1', '男', 'XB', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('XingBie_CH_0', '男', 'XingBie', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('XingBie_CH_1', '女', 'XingBie', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('xqsj_CH_0', '长期有效', 'xqsj', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('xqsj_CH_1', '截止于', 'xqsj', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('xsq_CH_0', '未绑定', 'xsq', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_0', '不能退回', 'YBFlowReturnRole', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_1', '退回到父流程的开始节点', 'YBFlowReturnRole', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_2', '退回到父流程的任何节点', 'YBFlowReturnRole', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_3', '退回父流程的启动节点', 'YBFlowReturnRole', '3', 'CH', '');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_4', '可退回到指定的节点', 'YBFlowReturnRole', '4', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ZhuYaoShiChang_CH_0', '国外', 'ZhuYaoShiChang', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ZhuYaoShiChang_CH_1', '国内', 'ZhuYaoShiChang', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ZhuYaoShiChang_CH_2', '内外并重', 'ZhuYaoShiChang', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ZZMM_CH_0', '少先队员', 'ZZMM', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ZZMM_CH_1', '团员', 'ZZMM', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('ZZMM_CH_2', '党员', 'ZZMM', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('显示方式_CH_0', '4列', '显示方式', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('显示方式_CH_1', '6列', '显示方式', '1', 'CH', '');
INSERT INTO `sys_enum` VALUES ('显示方式_CH_2', '上下模式3列', '显示方式', '2', 'CH', '');
INSERT INTO `sys_enum` VALUES ('表单展示方式_CH_0', '普通方式', '表单展示方式', '0', 'CH', '');
INSERT INTO `sys_enum` VALUES ('表单展示方式_CH_1', '页签方式', '表单展示方式', '1', 'CH', '');

-- ----------------------------
-- Table structure for sys_enummain
-- ----------------------------
DROP TABLE IF EXISTS `sys_enummain`;
CREATE TABLE `sys_enummain` (
  `No` varchar(40) NOT NULL COMMENT '编号',
  `Name` varchar(40) DEFAULT NULL COMMENT '名称',
  `CfgVal` varchar(1500) DEFAULT NULL COMMENT '配置信息',
  `Lang` varchar(10) DEFAULT NULL COMMENT '语言',
  `EnumKey` varchar(40) DEFAULT '' COMMENT 'EnumKey',
  `OrgNo` varchar(50) DEFAULT '' COMMENT 'OrgNo',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_enummain
-- ----------------------------
INSERT INTO `sys_enummain` VALUES ('CeShiMoShi', '测试模式', '@0=强办模式@1=协作模式@2=队列模式@3=共享模式', 'CH', '', '');
INSERT INTO `sys_enummain` VALUES ('CGDD', '采购地点', '@0=本市@1=外地', 'CH', '', '');
INSERT INTO `sys_enummain` VALUES ('FYLX', '费用类型', '@0=汽车票@1=打的票@2=火车票', 'CH', '', '');
INSERT INTO `sys_enummain` VALUES ('QingJiaLeiXing', '请假类型', '@0=事假@1=病假@2=婚假', 'CH', '', '');
INSERT INTO `sys_enummain` VALUES ('ShiFouYunXuZuoYe', '是否允许作业', '@0=符合安全标准允许作业@1=不符合安全标准需要整改', 'CH', '', '');

-- ----------------------------
-- Table structure for sys_enver
-- ----------------------------
DROP TABLE IF EXISTS `sys_enver`;
CREATE TABLE `sys_enver` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `No` varchar(50) DEFAULT NULL COMMENT '实体类',
  `Name` varchar(100) DEFAULT NULL COMMENT '实体名',
  `PKValue` varchar(300) DEFAULT NULL COMMENT '主键值',
  `EVer` int(11) DEFAULT '1' COMMENT '版本号',
  `Rec` varchar(100) DEFAULT NULL COMMENT '修改人',
  `RDT` varchar(50) DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_enver
-- ----------------------------

-- ----------------------------
-- Table structure for sys_enverdtl
-- ----------------------------
DROP TABLE IF EXISTS `sys_enverdtl`;
CREATE TABLE `sys_enverdtl` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `EnName` varchar(200) DEFAULT NULL COMMENT '实体名',
  `EnVerPK` varchar(100) DEFAULT NULL COMMENT '版本主表PK',
  `AttrKey` varchar(100) DEFAULT NULL COMMENT '字段',
  `AttrName` varchar(200) DEFAULT NULL COMMENT '字段名',
  `OldVal` varchar(100) DEFAULT NULL COMMENT '旧值',
  `NewVal` varchar(100) DEFAULT NULL COMMENT '新值',
  `EnVer` int(11) DEFAULT '1' COMMENT '版本号(日期)',
  `RDT` varchar(50) DEFAULT NULL COMMENT '日期',
  `Rec` varchar(100) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_enverdtl
-- ----------------------------

-- ----------------------------
-- Table structure for sys_excelfield
-- ----------------------------
DROP TABLE IF EXISTS `sys_excelfield`;
CREATE TABLE `sys_excelfield` (
  `No` varchar(36) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `CellName` varchar(50) DEFAULT NULL COMMENT '单元格名称',
  `CellRow` int(11) DEFAULT '0' COMMENT '行号',
  `CellColumn` int(11) DEFAULT '0' COMMENT '列号',
  `FK_ExcelSheet` varchar(100) DEFAULT NULL COMMENT '所属ExcelSheet表',
  `Field` varchar(50) DEFAULT NULL COMMENT '存储字段名',
  `FK_ExcelTable` varchar(100) DEFAULT NULL COMMENT '存储数据表',
  `DataType` int(11) DEFAULT '0' COMMENT '值类型',
  `UIBindKey` varchar(100) DEFAULT NULL COMMENT '外键表/枚举',
  `UIRefKey` varchar(30) DEFAULT NULL COMMENT '外键表No',
  `UIRefKeyText` varchar(30) DEFAULT NULL COMMENT '外键表Name',
  `Validators` text COMMENT '校验器',
  `FK_ExcelFile` varchar(100) DEFAULT NULL COMMENT '所属Excel模板',
  `AtPara` text COMMENT '参数',
  `ConfirmKind` int(11) DEFAULT '0' COMMENT '单元格确认方式',
  `ConfirmCellCount` int(11) DEFAULT '1' COMMENT '单元格确认方向移动量',
  `ConfirmCellValue` varchar(200) DEFAULT NULL COMMENT '对应单元格值',
  `ConfirmRepeatIndex` int(11) DEFAULT '0' COMMENT '对应单元格值重复选定次序',
  `SkipIsNull` int(11) DEFAULT '0' COMMENT '不计非空',
  `SyncToField` varchar(100) DEFAULT NULL COMMENT '同步到字段',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_excelfield
-- ----------------------------

-- ----------------------------
-- Table structure for sys_excelfile
-- ----------------------------
DROP TABLE IF EXISTS `sys_excelfile`;
CREATE TABLE `sys_excelfile` (
  `No` varchar(36) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Mark` varchar(50) DEFAULT NULL COMMENT '标识',
  `ExcelType` int(11) DEFAULT '0' COMMENT '类型',
  `Note` text COMMENT '上传说明',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '模板文件',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_excelfile
-- ----------------------------

-- ----------------------------
-- Table structure for sys_excelsheet
-- ----------------------------
DROP TABLE IF EXISTS `sys_excelsheet`;
CREATE TABLE `sys_excelsheet` (
  `No` varchar(36) NOT NULL COMMENT 'No',
  `Name` varchar(50) DEFAULT NULL COMMENT 'Sheet名称',
  `FK_ExcelFile` varchar(100) DEFAULT NULL COMMENT 'Excel模板',
  `SheetIndex` int(11) DEFAULT '0' COMMENT 'Sheet索引',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_excelsheet
-- ----------------------------

-- ----------------------------
-- Table structure for sys_exceltable
-- ----------------------------
DROP TABLE IF EXISTS `sys_exceltable`;
CREATE TABLE `sys_exceltable` (
  `No` varchar(36) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '数据表名',
  `FK_ExcelFile` varchar(100) DEFAULT NULL COMMENT 'Excel模板',
  `IsDtl` int(11) DEFAULT '0' COMMENT '是否明细表',
  `Note` text COMMENT '数据表说明',
  `SyncToTable` varchar(100) DEFAULT NULL COMMENT '同步到表',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_exceltable
-- ----------------------------

-- ----------------------------
-- Table structure for sys_filemanager
-- ----------------------------
DROP TABLE IF EXISTS `sys_filemanager`;
CREATE TABLE `sys_filemanager` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `AttrFileName` varchar(50) DEFAULT NULL COMMENT '指定名称',
  `AttrFileNo` varchar(50) DEFAULT NULL COMMENT '指定编号',
  `EnName` varchar(50) DEFAULT NULL COMMENT '关联的表',
  `RefVal` varchar(50) DEFAULT NULL COMMENT '主键值',
  `WebPath` varchar(100) DEFAULT NULL COMMENT 'Web路径',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '文件名称',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  `RDT` varchar(50) DEFAULT NULL COMMENT '上传时间',
  `Rec` varchar(50) DEFAULT NULL COMMENT '上传人',
  `Doc` text COMMENT '内容',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_filemanager
-- ----------------------------

-- ----------------------------
-- Table structure for sys_formtree
-- ----------------------------
DROP TABLE IF EXISTS `sys_formtree`;
CREATE TABLE `sys_formtree` (
  `No` varchar(10) NOT NULL COMMENT '编号',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点No',
  `OrgNo` varchar(100) DEFAULT NULL COMMENT '组织编号',
  `Idx` int(11) DEFAULT '0' COMMENT 'Idx',
  `IsDir` int(11) DEFAULT '0',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_formtree
-- ----------------------------
INSERT INTO `sys_formtree` VALUES ('01', '表单元素', '1', null, '0', '0');
INSERT INTO `sys_formtree` VALUES ('1', '表单库', '0', null, '0', '0');

-- ----------------------------
-- Table structure for sys_frmattachment
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmattachment`;
CREATE TABLE `sys_frmattachment` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `NoOfObj` varchar(50) DEFAULT NULL COMMENT '附件标识',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点控制(对sln有效)',
  `AthRunModel` int(11) DEFAULT '0' COMMENT '运行模式',
  `Name` varchar(50) DEFAULT NULL COMMENT '附件名称',
  `Exts` varchar(200) DEFAULT NULL,
  `NumOfUpload` int(11) DEFAULT '0' COMMENT '最低上传数量',
  `AthSaveWay` int(11) DEFAULT '0' COMMENT '保存方式',
  `SaveTo` varchar(150) DEFAULT NULL COMMENT '保存到',
  `Sort` varchar(500) DEFAULT NULL COMMENT '类别',
  `IsTurn2Html` int(11) DEFAULT '0' COMMENT '是否转换成html(方便手机浏览)',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `W` float DEFAULT NULL COMMENT '宽度',
  `H` float DEFAULT NULL COMMENT '高度',
  `DeleteWay` int(11) DEFAULT '0' COMMENT '附件删除规则',
  `IsUpload` int(11) DEFAULT '1' COMMENT '是否可以上传',
  `IsDownload` int(11) DEFAULT '1' COMMENT '是否可以下载',
  `IsOrder` int(11) DEFAULT '0' COMMENT '是否可以排序',
  `IsAutoSize` int(11) DEFAULT '1' COMMENT '自动控制大小',
  `IsNote` int(11) DEFAULT '1' COMMENT '是否增加备注',
  `IsExpCol` int(11) DEFAULT '1' COMMENT '是否启用扩展列',
  `IsShowTitle` int(11) DEFAULT '1' COMMENT '是否显示标题列',
  `UploadType` int(11) DEFAULT '0' COMMENT '上传类型',
  `AthUploadWay` int(11) DEFAULT '0' COMMENT '控制上传控制方式',
  `CtrlWay` int(11) DEFAULT '0' COMMENT '控制呈现控制方式',
  `IsRowLock` int(11) DEFAULT '1' COMMENT '是否启用锁定行',
  `IsWoEnableWF` int(11) DEFAULT '1' COMMENT '是否启用weboffice',
  `IsWoEnableSave` int(11) DEFAULT '1' COMMENT '是否启用保存',
  `IsWoEnableReadonly` int(11) DEFAULT '1' COMMENT '是否只读',
  `IsWoEnableRevise` int(11) DEFAULT '1' COMMENT '是否启用修订',
  `IsWoEnableViewKeepMark` int(11) DEFAULT '1' COMMENT '是否查看用户留痕',
  `IsWoEnablePrint` int(11) DEFAULT '1' COMMENT '是否打印',
  `IsWoEnableSeal` int(11) DEFAULT '1' COMMENT '是否启用签章',
  `IsWoEnableOver` int(11) DEFAULT '1' COMMENT '是否启用套红',
  `IsWoEnableTemplete` int(11) DEFAULT '1' COMMENT '是否启用公文模板',
  `IsWoEnableCheck` int(11) DEFAULT '1' COMMENT '是否自动写入审核信息',
  `IsWoEnableInsertFlow` int(11) DEFAULT '1' COMMENT '是否插入流程',
  `IsWoEnableInsertFengXian` int(11) DEFAULT '1' COMMENT '是否插入风险点',
  `IsWoEnableMarks` int(11) DEFAULT '1' COMMENT '是否启用留痕模式',
  `IsWoEnableDown` int(11) DEFAULT '1' COMMENT '是否启用下载',
  `IsToHeLiuHZ` int(11) DEFAULT '1' COMMENT '该附件是否要汇总到合流节点上去？(对子线程节点有效)',
  `IsHeLiuHuiZong` int(11) DEFAULT '1' COMMENT '是否是合流节点的汇总附件组件？(对合流节点有效)',
  `DataRefNoOfObj` varchar(150) DEFAULT NULL COMMENT '对应附件标识',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  `GroupID` int(11) DEFAULT '0' COMMENT 'GroupID',
  `GUID` varchar(128) DEFAULT '' COMMENT 'GUID',
  `IsDelete` int(11) DEFAULT '1',
  `ReadRole` int(11) DEFAULT '0',
  `UploadFileNumCheck` int(11) DEFAULT '0',
  `TopNumOfUpload` int(11) DEFAULT '99',
  `FileMaxSize` int(11) DEFAULT '10240',
  `IsVisable` int(11) DEFAULT '1',
  `FileType` int(11) DEFAULT '0' COMMENT '附件类型',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmattachment
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmattachmentdb
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmattachmentdb`;
CREATE TABLE `sys_frmattachmentdb` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `FK_FrmAttachment` varchar(500) DEFAULT NULL COMMENT '附件主键',
  `NoOfObj` varchar(50) DEFAULT NULL COMMENT '附件标识',
  `RefPKVal` varchar(50) DEFAULT NULL COMMENT '实体主键',
  `FID` int(11) DEFAULT '0' COMMENT 'FID',
  `NodeID` varchar(50) DEFAULT NULL COMMENT '节点ID',
  `Sort` varchar(200) DEFAULT NULL COMMENT '类别',
  `FileFullName` varchar(700) DEFAULT NULL COMMENT '文件路径',
  `FileName` varchar(500) DEFAULT NULL COMMENT '名称',
  `FileExts` varchar(50) DEFAULT NULL COMMENT '扩展',
  `FileSize` float DEFAULT NULL COMMENT '文件大小',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `Rec` varchar(50) DEFAULT NULL COMMENT '记录人',
  `RecName` varchar(50) DEFAULT NULL COMMENT '记录人名字',
  `MyNote` text COMMENT '备注',
  `IsRowLock` int(11) DEFAULT '0' COMMENT '是否锁定行',
  `Idx` int(11) DEFAULT '0' COMMENT '排序',
  `UploadGUID` varchar(500) DEFAULT NULL COMMENT '上传GUID',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmattachmentdb
-- ----------------------------
INSERT INTO `sys_frmattachmentdb` VALUES ('23f37bb3608b4f39992cd3181da541e3', 'ND20801', 'ND20801_Ath1', 'Ath1', '128', '0', '20801', '', 'D:\\jeesite4-jflow\\web\\src\\main\\webapp\\/DataUser/UploadFile/ND20801/\\128\\23f37bb3608b4f39992cd3181da541e3.1cae3637a1d4f5dd8ec63fdf0bc687c3.png', '1cae3637a1d4f5dd8ec63fdf0bc687c3.png', 'png', '68.8477', '2019-03-11', 'admin', '系统管理员', '', '0', '0', '23f37bb3608b4f39992cd3181da541e3', '');
INSERT INTO `sys_frmattachmentdb` VALUES ('78ac849cf3154f778c25591d190c37b8', 'ND20701', 'ND20701_Ath1', 'Ath1', '127', '0', '20701', '', 'D:\\jeesite4-jflow\\web\\src\\main\\webapp\\/DataUser/UploadFile/ND20701/\\127\\78ac849cf3154f778c25591d190c37b8.1cae3637a1d4f5dd8ec63fdf0bc687c3.png', '1cae3637a1d4f5dd8ec63fdf0bc687c3.png', 'png', '68.8477', '2019-03-09', 'admin', '系统管理员', '', '0', '0', '78ac849cf3154f778c25591d190c37b8', '');
INSERT INTO `sys_frmattachmentdb` VALUES ('795fd02e67144baeb2cf544198037067', 'ND20701', 'ND20701_Ath1', 'Ath1', '126', '0', '20701', '', 'D:\\jeesite4-jflow\\web\\src\\main\\webapp\\/DataUser/UploadFile/ND20701/\\126\\795fd02e67144baeb2cf544198037067.1cae3637a1d4f5dd8ec63fdf0bc687c3.png', '1cae3637a1d4f5dd8ec63fdf0bc687c3.png', 'png', '68.8477', '2019-03-09', 'admin', '系统管理员', '', '0', '0', '795fd02e67144baeb2cf544198037067', '');
INSERT INTO `sys_frmattachmentdb` VALUES ('a5d64372f8944f03b49f907e7cc1f88e', 'ND20701', 'ND20701_Ath1', 'Ath1', '126', '0', '20701', '', 'D:\\jeesite4-jflow3\\web\\src\\main\\webapp\\/DataUser/UploadFile/ND20701/\\126\\a5d64372f8944f03b49f907e7cc1f88e.1cae3637a1d4f5dd8ec63fdf0bc687c3.png', '1cae3637a1d4f5dd8ec63fdf0bc687c3.png', 'png', '68.8477', '2019-03-09', 'admin', '系统管理员', '', '0', '0', 'a5d64372f8944f03b49f907e7cc1f88e', '');
INSERT INTO `sys_frmattachmentdb` VALUES ('c7f888b5c5a6425381558c2c29e1b3f9', 'ND20701', 'ND20701_Ath1', 'Ath1', '127', '0', '20701', '', 'D:\\jeesite4-jflow\\web\\src\\main\\webapp\\/DataUser/UploadFile/ND20701/\\127\\c7f888b5c5a6425381558c2c29e1b3f9.1fab5316dfd667e31e937259d484c992.png', '1fab5316dfd667e31e937259d484c992.png', 'png', '25.3721', '2019-03-09', 'admin', '系统管理员', '', '0', '0', 'c7f888b5c5a6425381558c2c29e1b3f9', '');
INSERT INTO `sys_frmattachmentdb` VALUES ('e28108de2cb04282bd55fdf9eb64249d', 'ND20701', 'ND20701_Ath1', 'Ath1', '126', '0', '20701', '', 'D:\\jeesite4-jflow\\web\\src\\main\\webapp\\/DataUser/UploadFile/ND20701/\\126\\e28108de2cb04282bd55fdf9eb64249d.5cebdbbdc3088ba927a5745ef0feb55d.jpg', '5cebdbbdc3088ba927a5745ef0feb55d.jpg', 'jpg', '79.4951', '2019-03-09', 'admin', '系统管理员', '', '0', '0', 'e28108de2cb04282bd55fdf9eb64249d', '');

-- ----------------------------
-- Table structure for sys_frmbtn
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmbtn`;
CREATE TABLE `sys_frmbtn` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `Text` text COMMENT '标签',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `IsView` int(11) DEFAULT '0' COMMENT '是否可见',
  `IsEnable` int(11) DEFAULT '0' COMMENT '是否起用',
  `UAC` int(11) DEFAULT '0' COMMENT '控制类型',
  `UACContext` text COMMENT '控制内容',
  `EventType` int(11) DEFAULT '0' COMMENT '事件类型',
  `EventContext` text COMMENT '事件内容',
  `MsgOK` varchar(500) DEFAULT NULL COMMENT '运行成功提示',
  `MsgErr` varchar(500) DEFAULT NULL COMMENT '运行失败提示',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `GroupID` int(11) DEFAULT '0' COMMENT '所在分组',
  `GroupIDText` varchar(50) DEFAULT '0' COMMENT '所在分组',
  `BtnType` int(11) DEFAULT '0',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmbtn
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmele
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmele`;
CREATE TABLE `sys_frmele` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `EleType` varchar(50) DEFAULT NULL COMMENT '类型',
  `EleID` varchar(50) DEFAULT NULL COMMENT '控件ID值(对部分控件有效)',
  `EleName` varchar(200) DEFAULT NULL COMMENT '控件名称(对部分控件有效)',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `Tag1` varchar(50) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(50) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(50) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(50) DEFAULT NULL COMMENT 'Tag4',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `IsEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `AtPara` text COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmele
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmeledb
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmeledb`;
CREATE TABLE `sys_frmeledb` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `EleID` varchar(50) DEFAULT NULL COMMENT 'EleID',
  `RefPKVal` varchar(50) DEFAULT NULL COMMENT 'RefPKVal',
  `FID` int(11) DEFAULT '0' COMMENT 'FID',
  `Tag1` varchar(1000) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(1000) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(1000) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(1000) DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(1000) DEFAULT NULL COMMENT 'Tag5',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmeledb
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmevent
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmevent`;
CREATE TABLE `sys_frmevent` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Event` varchar(400) DEFAULT NULL COMMENT '事件名称',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程编号',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点ID',
  `EventDoType` int(11) DEFAULT '0' COMMENT '事件类型',
  `DoDoc` varchar(400) DEFAULT NULL COMMENT '执行内容',
  `MsgOK` varchar(400) DEFAULT NULL COMMENT '成功执行提示',
  `MsgError` varchar(400) DEFAULT NULL COMMENT '异常信息提示',
  `MsgCtrl` int(11) DEFAULT '0' COMMENT '消息发送控制',
  `MailEnable` int(11) DEFAULT '1' COMMENT '是否启用邮件发送？(如果启用就要设置邮件模版，支持ccflow表达式。)',
  `MailTitle` varchar(200) DEFAULT NULL COMMENT '邮件标题模版',
  `MailDoc` text COMMENT '邮件内容模版',
  `SMSEnable` int(11) DEFAULT '0' COMMENT '是否启用短信发送？(如果启用就要设置短信模版，支持ccflow表达式。)',
  `SMSDoc` text COMMENT '短信内容模版',
  `MobilePushEnable` int(11) DEFAULT '1' COMMENT '是否推送到手机、pad端。',
  `AtPara` text COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmevent
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmimg
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmimg`;
CREATE TABLE `sys_frmimg` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(200) DEFAULT NULL,
  `ImgAppType` int(11) DEFAULT '0' COMMENT '应用类型',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `ImgURL` varchar(200) DEFAULT NULL COMMENT 'ImgURL',
  `ImgPath` varchar(200) DEFAULT NULL COMMENT 'ImgPath',
  `LinkURL` varchar(200) DEFAULT NULL COMMENT 'LinkURL',
  `LinkTarget` varchar(200) DEFAULT NULL COMMENT 'LinkTarget',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `Tag0` varchar(500) DEFAULT NULL COMMENT '参数',
  `ImgSrcType` int(11) DEFAULT '0' COMMENT '图片来源0=本地,1=URL',
  `IsEdit` int(11) DEFAULT '0' COMMENT '是否可以编辑',
  `Name` varchar(500) DEFAULT NULL COMMENT '中文名称',
  `EnPK` varchar(500) DEFAULT NULL COMMENT '英文名称',
  `GroupID` varchar(50) DEFAULT '0' COMMENT '所在分组',
  `GroupIDText` varchar(500) DEFAULT NULL,
  `KeyOfEn` varchar(200) DEFAULT NULL,
  `ColSpan` int(11) DEFAULT '0',
  `TextColSpan` int(11) DEFAULT '1',
  `RowSpan` int(11) DEFAULT '1',
  `UIWidth` int(11) DEFAULT '0' COMMENT '宽度',
  `UIHeight` int(11) DEFAULT '0' COMMENT '高度',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmimg
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmimgath
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmimgath`;
CREATE TABLE `sys_frmimgath` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `CtrlID` varchar(200) DEFAULT NULL COMMENT '控件ID',
  `Name` varchar(200) DEFAULT NULL COMMENT '中文名称',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `IsEdit` int(11) DEFAULT '1' COMMENT '是否可编辑',
  `IsRequired` int(11) DEFAULT '0' COMMENT '是否必填项',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmimgath
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmimgathdb
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmimgathdb`;
CREATE TABLE `sys_frmimgathdb` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '附件ID',
  `FK_FrmImgAth` varchar(50) DEFAULT NULL COMMENT '图片附件编号',
  `RefPKVal` varchar(50) DEFAULT NULL COMMENT '实体主键',
  `FileFullName` varchar(700) DEFAULT NULL COMMENT '文件全路径',
  `FileName` varchar(500) DEFAULT NULL COMMENT '名称',
  `FileExts` varchar(50) DEFAULT NULL COMMENT '扩展名',
  `FileSize` float DEFAULT NULL COMMENT '文件大小',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `Rec` varchar(50) DEFAULT NULL COMMENT '记录人',
  `RecName` varchar(50) DEFAULT NULL COMMENT '记录人名字',
  `MyNote` text COMMENT '备注',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmimgathdb
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmlab
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmlab`;
CREATE TABLE `sys_frmlab` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `Text` text COMMENT 'Label',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `FontSize` int(11) DEFAULT '12' COMMENT '字体大小',
  `FontColor` varchar(50) DEFAULT NULL COMMENT '颜色',
  `FontName` varchar(50) DEFAULT NULL COMMENT '字体名称',
  `FontStyle` varchar(200) DEFAULT NULL COMMENT '字体风格',
  `FontWeight` varchar(50) DEFAULT NULL COMMENT '字体宽度',
  `IsBold` int(11) DEFAULT '0' COMMENT '是否粗体',
  `IsItalic` int(11) DEFAULT '0' COMMENT '是否斜体',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmlab
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmline
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmline`;
CREATE TABLE `sys_frmline` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `X1` float DEFAULT NULL COMMENT 'X1',
  `Y1` float DEFAULT NULL COMMENT 'Y1',
  `X2` float DEFAULT NULL COMMENT 'X2',
  `Y2` float DEFAULT NULL COMMENT 'Y2',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `BorderWidth` float DEFAULT NULL COMMENT '宽度',
  `BorderColor` varchar(30) DEFAULT NULL COMMENT '颜色',
  `GUID` varchar(128) DEFAULT NULL COMMENT '初始的GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmline
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmlink
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmlink`;
CREATE TABLE `sys_frmlink` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `Text` varchar(500) DEFAULT NULL COMMENT 'Label',
  `URL` varchar(500) DEFAULT NULL COMMENT 'URL',
  `Target` varchar(20) DEFAULT NULL COMMENT 'Target',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `FontSize` int(11) DEFAULT '12' COMMENT 'FontSize',
  `FontColor` varchar(50) DEFAULT NULL COMMENT 'FontColor',
  `FontName` varchar(50) DEFAULT NULL COMMENT 'FontName',
  `FontStyle` varchar(50) DEFAULT NULL COMMENT 'FontStyle',
  `IsBold` int(11) DEFAULT '0' COMMENT 'IsBold',
  `IsItalic` int(11) DEFAULT '0' COMMENT 'IsItalic',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `GroupID` varchar(50) DEFAULT '0' COMMENT '显示的分组',
  `GroupIDText` varchar(50) DEFAULT '0' COMMENT '显示的分组',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmlink
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmrb
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmrb`;
CREATE TABLE `sys_frmrb` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(300) DEFAULT NULL,
  `KeyOfEn` varchar(300) DEFAULT NULL,
  `EnumKey` varchar(30) DEFAULT NULL COMMENT '枚举值',
  `Lab` varchar(500) DEFAULT NULL,
  `IntKey` int(11) DEFAULT '0' COMMENT 'IntKey',
  `UIIsEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `Script` text COMMENT '要执行的脚本',
  `FieldsCfg` text COMMENT '配置信息@FieldName=Sta',
  `SetVal` varchar(200) DEFAULT NULL COMMENT '设置的值',
  `Tip` varchar(1000) DEFAULT NULL COMMENT '选择后提示的信息',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(1000) DEFAULT '',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmrb
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmreportfield
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmreportfield`;
CREATE TABLE `sys_frmreportfield` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单编号',
  `KeyOfEn` varchar(100) DEFAULT NULL COMMENT '字段名',
  `Name` varchar(200) DEFAULT NULL COMMENT '显示中文名',
  `UIWidth` varchar(100) DEFAULT NULL COMMENT '宽度',
  `UIVisible` int(11) DEFAULT '1' COMMENT '是否显示',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmreportfield
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmrpt
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmrpt`;
CREATE TABLE `sys_frmrpt` (
  `No` varchar(20) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '描述',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `PTable` varchar(30) DEFAULT NULL COMMENT '物理表',
  `SQLOfColumn` varchar(300) DEFAULT NULL COMMENT '列的数据源',
  `SQLOfRow` varchar(300) DEFAULT NULL COMMENT '行数据源',
  `RowIdx` int(11) DEFAULT '99' COMMENT '位置',
  `GroupID` int(11) DEFAULT '0' COMMENT 'GroupID',
  `IsShowSum` int(11) DEFAULT '1' COMMENT 'IsShowSum',
  `IsShowIdx` int(11) DEFAULT '1' COMMENT 'IsShowIdx',
  `IsCopyNDData` int(11) DEFAULT '1' COMMENT 'IsCopyNDData',
  `IsHLDtl` int(11) DEFAULT '0' COMMENT '是否是合流汇总',
  `IsReadonly` int(11) DEFAULT '0' COMMENT 'IsReadonly',
  `IsShowTitle` int(11) DEFAULT '1' COMMENT 'IsShowTitle',
  `IsView` int(11) DEFAULT '1' COMMENT '是否可见',
  `IsExp` int(11) DEFAULT '1' COMMENT 'IsExp',
  `IsImp` int(11) DEFAULT '1' COMMENT 'IsImp',
  `IsInsert` int(11) DEFAULT '1' COMMENT 'IsInsert',
  `IsDelete` int(11) DEFAULT '1' COMMENT 'IsDelete',
  `IsUpdate` int(11) DEFAULT '1' COMMENT 'IsUpdate',
  `IsEnablePass` int(11) DEFAULT '0' COMMENT '是否启用通过审核功能?',
  `IsEnableAthM` int(11) DEFAULT '0' COMMENT '是否启用多附件',
  `IsEnableM2M` int(11) DEFAULT '0' COMMENT '是否启用M2M',
  `IsEnableM2MM` int(11) DEFAULT '0' COMMENT '是否启用M2M',
  `WhenOverSize` int(11) DEFAULT '0' COMMENT 'WhenOverSize',
  `DtlOpenType` int(11) DEFAULT '1' COMMENT '数据开放类型',
  `EditModel` int(11) DEFAULT '0' COMMENT '显示格式',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `FrmW` float DEFAULT NULL COMMENT 'FrmW',
  `FrmH` float DEFAULT NULL COMMENT 'FrmH',
  `MTR` varchar(3000) DEFAULT NULL COMMENT '多表头列',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmrpt
-- ----------------------------

-- ----------------------------
-- Table structure for sys_frmsln
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmsln`;
CREATE TABLE `sys_frmsln` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Flow` varchar(4) DEFAULT NULL COMMENT '流程编号',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `KeyOfEn` varchar(200) DEFAULT NULL COMMENT '字段',
  `Name` varchar(500) DEFAULT NULL COMMENT '字段名',
  `EleType` varchar(20) DEFAULT NULL COMMENT '类型',
  `UIIsEnable` int(11) DEFAULT '1' COMMENT '是否可用',
  `UIVisible` int(11) DEFAULT '1' COMMENT '是否可见',
  `IsSigan` int(11) DEFAULT '0' COMMENT '是否签名',
  `IsNotNull` int(11) DEFAULT '0' COMMENT '是否为空',
  `RegularExp` varchar(500) DEFAULT NULL COMMENT '正则表达式',
  `IsWriteToFlowTable` int(11) DEFAULT '0' COMMENT '是否写入流程表',
  `IsWriteToGenerWorkFlow` int(11) DEFAULT '0' COMMENT '是否写入流程注册表',
  `DefVal` varchar(200) DEFAULT NULL COMMENT '默认值',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_frmsln
-- ----------------------------

-- ----------------------------
-- Table structure for sys_glovar
-- ----------------------------
DROP TABLE IF EXISTS `sys_glovar`;
CREATE TABLE `sys_glovar` (
  `No` varchar(50) NOT NULL COMMENT '键',
  `Name` varchar(120) DEFAULT NULL COMMENT '名称',
  `Val` text COMMENT '值',
  `GroupKey` varchar(120) DEFAULT NULL COMMENT '分组值',
  `Note` text COMMENT '说明',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_glovar
-- ----------------------------
INSERT INTO `sys_glovar` VALUES ('0', '选择系统约定默认值', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('@FK_ND', '当前年度', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('@FK_YF', '当前月份', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('@OrgName', '登录人员组织名称', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('@OrgNo', '登录人员组织', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('@WebUser.FK_Dept', '登陆人员部门编号', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('@WebUser.FK_DeptFullName', '登陆人员部门全称', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('@WebUser.FK_DeptName', '登陆人员部门名称', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('@WebUser.Name', '登陆人员名称', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('@WebUser.No', '登陆人员账号', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('@yyyy年MM月dd日', '当前日期(yyyy年MM月dd日)', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('@yyyy年MM月dd日HH时mm分', '当前日期(yyyy年MM月dd日HH时mm分)', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('@yy年MM月dd日', '当前日期(yy年MM月dd日)', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('@yy年MM月dd日HH时mm分', '当前日期(yy年MM月dd日HH时mm分)', null, 'DefVal', null, '0');
INSERT INTO `sys_glovar` VALUES ('DTSTodoStaPM2018-12-03', '时效调度 WFTodoSta PM 调度', 'DTSTodoStaPM2018-12-03', 'WF', '时效调度PMDTSTodoStaPM2018-12-03', '0');
INSERT INTO `sys_glovar` VALUES ('DTSTodoStaPM2019-08-31', '时效调度 WFTodoSta PM 调度', 'DTSTodoStaPM2019-08-31', 'WF', '时效调度PMDTSTodoStaPM2019-08-31', '0');

-- ----------------------------
-- Table structure for sys_groupenstemplate
-- ----------------------------
DROP TABLE IF EXISTS `sys_groupenstemplate`;
CREATE TABLE `sys_groupenstemplate` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `EnName` varchar(500) DEFAULT NULL COMMENT '表称',
  `Name` varchar(500) DEFAULT NULL COMMENT '报表名',
  `EnsName` varchar(90) DEFAULT NULL COMMENT '报表类名',
  `OperateCol` varchar(90) DEFAULT NULL COMMENT '操作属性',
  `Attrs` varchar(90) DEFAULT NULL COMMENT '运算属性',
  `Rec` varchar(90) DEFAULT NULL COMMENT '记录人',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_groupenstemplate
-- ----------------------------

-- ----------------------------
-- Table structure for sys_groupfield
-- ----------------------------
DROP TABLE IF EXISTS `sys_groupfield`;
CREATE TABLE `sys_groupfield` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `Lab` varchar(500) DEFAULT NULL COMMENT '标签',
  `FrmID` varchar(200) DEFAULT NULL COMMENT '表单ID',
  `CtrlType` varchar(50) DEFAULT NULL COMMENT '控件类型',
  `CtrlID` varchar(500) DEFAULT NULL COMMENT '控件ID',
  `Idx` int(11) DEFAULT '99' COMMENT '顺序号',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(3000) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_groupfield
-- ----------------------------
INSERT INTO `sys_groupfield` VALUES ('1228', '请假流程-经典模式报表', 'ND1MyRpt', '', '', '1', '', '');
INSERT INTO `sys_groupfield` VALUES ('1258', '请假流程报表', 'ND2MyRpt', '', '', '1', '', '');
INSERT INTO `sys_groupfield` VALUES ('1287', '请假流程报表', 'ND4MyRpt', '', '', '1', '', '');

-- ----------------------------
-- Table structure for sys_m2m
-- ----------------------------
DROP TABLE IF EXISTS `sys_m2m`;
CREATE TABLE `sys_m2m` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `M2MNo` varchar(20) DEFAULT NULL COMMENT 'M2MNo',
  `EnOID` int(11) DEFAULT NULL COMMENT '实体OID',
  `DtlObj` varchar(20) DEFAULT NULL COMMENT 'DtlObj(对于m2mm有效)',
  `Doc` text COMMENT '内容',
  `ValsName` text COMMENT 'ValsName',
  `ValsSQL` text COMMENT 'ValsSQL',
  `NumSelected` int(11) DEFAULT NULL COMMENT '选择数',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='M2M数据存储';

-- ----------------------------
-- Records of sys_m2m
-- ----------------------------

-- ----------------------------
-- Table structure for sys_mapattr
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapattr`;
CREATE TABLE `sys_mapattr` (
  `MyPK` varchar(200) NOT NULL DEFAULT '',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '实体标识',
  `KeyOfEn` varchar(200) DEFAULT NULL COMMENT '属性',
  `Name` varchar(200) DEFAULT NULL COMMENT '描述',
  `DefVal` varchar(4000) DEFAULT NULL,
  `UIContralType` int(11) DEFAULT '0' COMMENT '控件',
  `MyDataType` int(11) DEFAULT '1' COMMENT '数据类型',
  `LGType` int(11) DEFAULT '0' COMMENT '逻辑类型',
  `UIWidth` float DEFAULT NULL COMMENT '宽度',
  `UIHeight` float DEFAULT NULL COMMENT '高度',
  `MinLen` int(11) DEFAULT '0' COMMENT '最小长度',
  `MaxLen` int(11) DEFAULT '300' COMMENT '最大长度',
  `UIBindKey` varchar(100) DEFAULT NULL COMMENT '绑定的信息',
  `UIRefKey` varchar(30) DEFAULT NULL COMMENT '绑定的Key',
  `UIRefKeyText` varchar(30) DEFAULT NULL COMMENT '绑定的Text',
  `UIVisible` int(11) DEFAULT '1' COMMENT '是否可见',
  `UIIsEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `UIIsLine` int(11) DEFAULT '0' COMMENT '是否单独栏显示',
  `UIIsInput` int(11) DEFAULT '0' COMMENT '是否必填字段',
  `IsRichText` int(11) DEFAULT '0' COMMENT '富文本',
  `IsSupperText` int(11) DEFAULT '0' COMMENT '富文本',
  `FontSize` int(11) DEFAULT '0' COMMENT '富文本',
  `IsSigan` int(11) DEFAULT '0' COMMENT '签字？',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `Tag` varchar(100) DEFAULT NULL COMMENT '标识（存放临时数据）',
  `EditType` int(11) DEFAULT '0' COMMENT '编辑类型',
  `Tip` varchar(800) DEFAULT NULL,
  `ColSpan` int(11) DEFAULT '1' COMMENT '单元格数量',
  `GroupID` varchar(100) DEFAULT NULL COMMENT '显示的分组',
  `IsEnableInAPP` int(11) DEFAULT '1' COMMENT '是否在移动端中显示',
  `Idx` int(11) DEFAULT '0' COMMENT '序号',
  `AtPara` text COMMENT 'AtPara',
  `DefValText` varchar(50) DEFAULT '0' COMMENT '默认值（选中）',
  `RBShowModel` int(11) DEFAULT '0' COMMENT '单选按钮的展现方式',
  `IsEnableJS` int(11) DEFAULT '0' COMMENT '是否启用JS高级设置？',
  `GroupIDText` varchar(500) DEFAULT NULL,
  `ExtDefVal` varchar(50) DEFAULT '0' COMMENT '系统默认值',
  `ExtDefValText` varchar(500) DEFAULT NULL,
  `ExtIsSum` int(11) DEFAULT '0' COMMENT '是否显示合计(对从表有效)',
  `Tag1` varchar(100) DEFAULT '',
  `Tag2` varchar(100) DEFAULT '',
  `Tag3` varchar(100) DEFAULT '',
  `TextColSpan` int(11) DEFAULT '1',
  `RowSpan` int(11) DEFAULT '1',
  `ExtRows` float(50,2) DEFAULT '1.00',
  `DefValType` int(11) DEFAULT '1' COMMENT '默认值类型',
  `IsSecret` int(11) DEFAULT '0' COMMENT '是否保密',
  `CSS` varchar(100) DEFAULT '0' COMMENT '自定义样式',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_mapattr
-- ----------------------------
INSERT INTO `sys_mapattr` VALUES ('MyDeptRole_OID', 'MyDeptRole', 'OID', 'OID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '1234', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('MyDeptRole_RDT', 'MyDeptRole', 'RDT', '更新时间', '@RDT', '0', '7', '0', '145', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '1234', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND105_CDT', 'ND105', 'CDT', '完成时间', '@RDT', '0', '7', '0', '145', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND105_Emps', 'ND105', 'Emps', 'Emps', '', '0', '1', '0', '100', '23', '0', '400', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND105_FID', 'ND105', 'FID', 'FID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND105_FK_Dept', 'ND105', 'FK_Dept', '隶属部门', '', '0', '1', '0', '100', '23', '0', '100', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND105_OID', 'ND105', 'OID', 'WorkID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND105_RDT', 'ND105', 'RDT', '接受时间', '', '0', '7', '0', '145', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND105_Rec', 'ND105', 'Rec', '记录人', '@WebUser.No', '0', '1', '0', '100', '23', '0', '20', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND12204_CDT', 'ND12204', 'CDT', '完成时间', '@RDT', '0', '7', '0', '145', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND12204_Emps', 'ND12204', 'Emps', 'Emps', '', '0', '1', '0', '100', '23', '0', '400', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND12204_FID', 'ND12204', 'FID', 'FID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND12204_FK_Dept', 'ND12204', 'FK_Dept', '操作员部门', '', '0', '1', '0', '100', '23', '0', '100', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND12204_OID', 'ND12204', 'OID', 'WorkID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND12204_RDT', 'ND12204', 'RDT', '接受时间', '', '0', '7', '0', '145', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND12204_Rec', 'ND12204', 'Rec', '记录人', '@WebUser.No', '0', '1', '0', '100', '23', '0', '20', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND13099_CDT', 'ND13099', 'CDT', '完成时间', '@RDT', '0', '7', '0', '145', '23', '0', '500', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND13099_Emps', 'ND13099', 'Emps', 'Emps', '', '0', '1', '0', '100', '23', '0', '400', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND13099_FID', 'ND13099', 'FID', 'FID', '0', '0', '2', '0', '100', '23', '0', '500', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND13099_FK_Dept', 'ND13099', 'FK_Dept', '隶属部门', '', '0', '1', '0', '100', '23', '0', '100', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND13099_OID', 'ND13099', 'OID', 'WorkID', '0', '0', '2', '0', '100', '23', '0', '500', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND13099_RDT', 'ND13099', 'RDT', '接受时间', '', '0', '7', '0', '145', '23', '0', '500', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND13099_Rec', 'ND13099', 'Rec', '记录人', '@WebUser.No', '0', '1', '0', '100', '23', '0', '20', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1MyRpt_FK_Dept', 'ND1MyRpt', 'FK_Dept', '操作员部门', '', '1', '1', '2', '100', '23', '0', '100', 'BP.Port.Depts', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1228', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1MyRpt_FK_NY', 'ND1MyRpt', 'FK_NY', '年月', '', '1', '1', '2', '100', '23', '0', '7', 'BP.Pub.NYs', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1228', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1MyRpt_FlowDaySpan', 'ND1MyRpt', 'FlowDaySpan', '跨度(天)', '', '0', '8', '0', '100', '23', '0', '300', '', '', '', '1', '1', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1226', '1', '-101', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1MyRpt_FlowEmps', 'ND1MyRpt', 'FlowEmps', '参与人', '', '0', '1', '0', '100', '23', '0', '1000', '', '', '', '1', '0', '1', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1226', '1', '-100', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1MyRpt_FlowEnder', 'ND1MyRpt', 'FlowEnder', '结束人', '', '1', '1', '2', '100', '23', '0', '20', 'BP.Port.Emps', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1226', '1', '-1', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1MyRpt_FlowEnderRDT', 'ND1MyRpt', 'FlowEnderRDT', '结束时间', '', '0', '7', '0', '145', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '', '1', '', '1', '1226', '1', '-101', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1MyRpt_FlowEndNode', 'ND1MyRpt', 'FlowEndNode', '结束节点', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1226', '1', '-101', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1MyRpt_FlowStarter', 'ND1MyRpt', 'FlowStarter', '发起人', '', '1', '1', '2', '100', '23', '0', '20', 'BP.Port.Emps', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1226', '1', '-1', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1MyRpt_FlowStartRDT', 'ND1MyRpt', 'FlowStartRDT', '发起时间', '', '0', '7', '0', '145', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '', '1', '', '1', '1226', '1', '-101', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1MyRpt_OID', 'ND1MyRpt', 'OID', 'WorkID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '1224', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1MyRpt_Title', 'ND1MyRpt', 'Title', '标题', '', '0', '1', '0', '251', '23', '0', '200', '', '', '', '0', '0', '1', '0', '0', '0', '0', '0', '174.83', '54.4', '', '', '0', '', '1', '1220', '1', '-100', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1MyRpt_WFSta', 'ND1MyRpt', 'WFSta', '状态', '', '1', '2', '1', '100', '23', '0', '1000', 'WFSta', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1224', '1', '-1', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1MyRpt_WFState', 'ND1MyRpt', 'WFState', '流程状态', '', '1', '2', '1', '100', '23', '0', '1000', 'WFState', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1226', '1', '-1', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND20505_CDT', 'ND20505', 'CDT', '完成时间', '@RDT', '0', '7', '0', '145', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND20505_Emps', 'ND20505', 'Emps', 'Emps', '', '0', '1', '0', '100', '23', '0', '400', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND20505_FID', 'ND20505', 'FID', 'FID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND20505_FK_Dept', 'ND20505', 'FK_Dept', '隶属部门', '', '0', '1', '0', '100', '23', '0', '100', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND20505_OID', 'ND20505', 'OID', 'WorkID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND20505_RDT', 'ND20505', 'RDT', '接受时间', '', '0', '7', '0', '145', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND20505_Rec', 'ND20505', 'Rec', '记录人', '@WebUser.No', '0', '1', '0', '100', '23', '0', '20', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND2MyRpt_FK_Dept', 'ND2MyRpt', 'FK_Dept', '操作员部门', '', '1', '1', '2', '100', '23', '0', '100', 'BP.Port.Depts', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1258', '1', '999', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND2MyRpt_FK_NY', 'ND2MyRpt', 'FK_NY', '年月', '', '1', '1', '2', '100', '23', '0', '7', 'BP.Pub.NYs', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1258', '1', '999', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND2MyRpt_FlowDaySpan', 'ND2MyRpt', 'FlowDaySpan', '跨度(天)', '', '0', '8', '0', '100', '23', '0', '300', '', '', '', '1', '1', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1256', '1', '-101', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND2MyRpt_FlowEmps', 'ND2MyRpt', 'FlowEmps', '参与人', '', '0', '1', '0', '100', '23', '0', '1000', '', '', '', '1', '0', '1', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1256', '1', '-100', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND2MyRpt_FlowEnder', 'ND2MyRpt', 'FlowEnder', '结束人', '', '1', '1', '2', '100', '23', '0', '20', 'BP.Port.Emps', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1256', '1', '-1', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND2MyRpt_FlowEnderRDT', 'ND2MyRpt', 'FlowEnderRDT', '结束时间', '', '0', '7', '0', '145', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '', '1', '', '1', '1256', '1', '-101', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND2MyRpt_FlowEndNode', 'ND2MyRpt', 'FlowEndNode', '结束节点', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1256', '1', '-101', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND2MyRpt_FlowStarter', 'ND2MyRpt', 'FlowStarter', '发起人', '', '1', '1', '2', '100', '23', '0', '20', 'BP.Port.Emps', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1256', '1', '-1', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND2MyRpt_FlowStartRDT', 'ND2MyRpt', 'FlowStartRDT', '发起时间', '', '0', '7', '0', '145', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '', '1', '', '1', '1256', '1', '-101', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND2MyRpt_OID', 'ND2MyRpt', 'OID', 'OID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '1254', '1', '999', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND2MyRpt_Title', 'ND2MyRpt', 'Title', '标题', '', '0', '1', '0', '251', '23', '0', '200', '', '', '', '0', '0', '1', '0', '0', '0', '0', '0', '174.83', '54.4', '', '', '0', '', '1', '1246', '1', '-100', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND2MyRpt_WFSta', 'ND2MyRpt', 'WFSta', '状态', '', '1', '2', '1', '100', '23', '0', '1000', 'WFSta', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1254', '1', '-1', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND2MyRpt_WFState', 'ND2MyRpt', 'WFState', '流程状态', '', '1', '2', '1', '100', '23', '0', '1000', 'WFState', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1256', '1', '-1', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4502Dtl1_ChanPinMingCheng', 'ND4502Dtl1', 'ChanPinMingCheng', '产品名称', '', '0', '1', '0', '100', '23', '0', '300', '0', '', '', '1', '1', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '6', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4502Dtl1_FID', 'ND4502Dtl1', 'FID', 'FID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '1', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4502Dtl1_OID', 'ND4502Dtl1', 'OID', '主键', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '2', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4502Dtl1_RDT', 'ND4502Dtl1', 'RDT', '记录时间', '', '0', '7', '0', '145', '23', '0', '20', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '0', '1', '3', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4502Dtl1_Rec', 'ND4502Dtl1', 'Rec', '记录人', '@WebUser.No', '0', '1', '0', '100', '23', '0', '20', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '@WebUser.No', '2', '', '1', '0', '1', '4', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4502Dtl1_RefPK', 'ND4502Dtl1', 'RefPK', '关联ID', '0', '0', '1', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '5', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4502Dtl1_TianXieRen', 'ND4502Dtl1', 'TianXieRen', '填写人', '@WebUser.Name', '0', '1', '0', '100', '23', '0', '300', '0', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4502Dtl1_XiaoJi', 'ND4502Dtl1', 'XiaoJi', '小计', '0.00', '0', '8', '0', '100', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '9', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4502Dtl1_XiaoShouJiaGe', 'ND4502Dtl1', 'XiaoShouJiaGe', '销售价格', '0.00', '0', '8', '0', '100', '23', '0', '300', '', '', '', '1', '1', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '7', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4502Dtl1_XiaoShouShuLiang', 'ND4502Dtl1', 'XiaoShouShuLiang', '销售数量', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '1', '1', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '8', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4503Dtl1_ChanPinMingCheng', 'ND4503Dtl1', 'ChanPinMingCheng', '产品名称', '', '0', '1', '0', '100', '23', '0', '300', '0', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '6', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4503Dtl1_FID', 'ND4503Dtl1', 'FID', 'FID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '1', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4503Dtl1_OID', 'ND4503Dtl1', 'OID', '主键', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '2', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4503Dtl1_RDT', 'ND4503Dtl1', 'RDT', '记录时间', '', '0', '7', '0', '145', '23', '0', '20', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '0', '1', '3', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4503Dtl1_Rec', 'ND4503Dtl1', 'Rec', '记录人', '', '0', '1', '0', '100', '23', '0', '20', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '@WebUser.No', '2', '', '1', '0', '1', '4', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4503Dtl1_RefPK', 'ND4503Dtl1', 'RefPK', '关联ID', '0', '0', '1', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '5', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4503Dtl1_TianXieRen', 'ND4503Dtl1', 'TianXieRen', '填写人', '', '0', '1', '0', '80', '23', '0', '300', '0', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '224', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4503Dtl1_XiaoJi', 'ND4503Dtl1', 'XiaoJi', '小计', '0.00', '0', '8', '0', '100', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '9', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4503Dtl1_XiaoShouJiaGe', 'ND4503Dtl1', 'XiaoShouJiaGe', '销售价格', '0.00', '0', '8', '0', '100', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '7', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4503Dtl1_XiaoShouJingLiBeiZhu', 'ND4503Dtl1', 'XiaoShouJingLiBeiZhu', '销售经理备注', '', '0', '1', '0', '100', '23', '0', '300', '0', '', '', '1', '1', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '224', '1', '10', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4503Dtl1_XiaoShouShuLiang', 'ND4503Dtl1', 'XiaoShouShuLiang', '销售数量', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '8', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4504Dtl1_ChanPinMingCheng', 'ND4504Dtl1', 'ChanPinMingCheng', '产品名称', '', '0', '1', '0', '100', '23', '0', '300', '0', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '6', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4504Dtl1_FID', 'ND4504Dtl1', 'FID', 'FID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '1', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4504Dtl1_OID', 'ND4504Dtl1', 'OID', '主键', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '2', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4504Dtl1_RDT', 'ND4504Dtl1', 'RDT', '记录时间', '', '0', '7', '0', '145', '23', '0', '20', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '0', '1', '3', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4504Dtl1_Rec', 'ND4504Dtl1', 'Rec', '记录人', '', '0', '1', '0', '100', '23', '0', '20', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '@WebUser.No', '2', '', '1', '0', '1', '4', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4504Dtl1_RefPK', 'ND4504Dtl1', 'RefPK', '关联ID', '0', '0', '1', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '5', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4504Dtl1_TianXieRen', 'ND4504Dtl1', 'TianXieRen', '填写人', '', '0', '1', '0', '80', '23', '0', '300', '0', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '227', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4504Dtl1_XiaoJi', 'ND4504Dtl1', 'XiaoJi', '小计', '0.00', '0', '8', '0', '100', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '9', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4504Dtl1_XiaoShouJiaGe', 'ND4504Dtl1', 'XiaoShouJiaGe', '销售价格', '0.00', '0', '8', '0', '100', '23', '0', '300', '', '', '', '1', '1', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '227', '1', '7', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4504Dtl1_XiaoShouJingLiBeiZhu', 'ND4504Dtl1', 'XiaoShouJingLiBeiZhu', '销售经理备注', '', '0', '1', '0', '100', '23', '0', '300', '0', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '224', '1', '10', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4504Dtl1_XiaoShouShuLiang', 'ND4504Dtl1', 'XiaoShouShuLiang', '销售数量', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '1', '1', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '227', '1', '8', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4599Dtl1_ChanPinMingCheng', 'ND4599Dtl1', 'ChanPinMingCheng', '产品名称', '', '0', '1', '0', '100', '23', '0', '300', '0', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '6', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4599Dtl1_FID', 'ND4599Dtl1', 'FID', 'FID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '1', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4599Dtl1_OID', 'ND4599Dtl1', 'OID', '主键', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '2', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4599Dtl1_RDT', 'ND4599Dtl1', 'RDT', '记录时间', '', '0', '7', '0', '145', '23', '0', '20', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '0', '1', '3', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4599Dtl1_Rec', 'ND4599Dtl1', 'Rec', '记录人', '', '0', '1', '0', '100', '23', '0', '20', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '@WebUser.No', '2', '', '1', '0', '1', '4', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4599Dtl1_RefPK', 'ND4599Dtl1', 'RefPK', '关联ID', '0', '0', '1', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '0', '1', '5', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4599Dtl1_TianXieRen', 'ND4599Dtl1', 'TianXieRen', '填写人', '', '0', '1', '0', '100', '23', '0', '300', '0', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '230', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4599Dtl1_XiaoJi', 'ND4599Dtl1', 'XiaoJi', '小计', '0.00', '0', '8', '0', '100', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '9', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4599Dtl1_XiaoShouJiaGe', 'ND4599Dtl1', 'XiaoShouJiaGe', '销售价格', '0.00', '0', '8', '0', '100', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '7', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4599Dtl1_XiaoShouJingLiBeiZhu', 'ND4599Dtl1', 'XiaoShouJingLiBeiZhu', '销售经理备注', '', '0', '1', '0', '100', '23', '0', '300', '0', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '224', '1', '10', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4599Dtl1_XiaoShouShuLiang', 'ND4599Dtl1', 'XiaoShouShuLiang', '销售数量', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '0', '', '1', '221', '1', '8', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4MyRpt_FK_Dept', 'ND4MyRpt', 'FK_Dept', '操作员部门', '', '1', '1', '2', '100', '23', '0', '100', 'BP.Port.Depts', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1287', '1', '999', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4MyRpt_FK_NY', 'ND4MyRpt', 'FK_NY', '年月', '', '1', '1', '2', '100', '23', '0', '7', 'BP.Pub.NYs', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '0', '1', '999', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4MyRpt_FlowDaySpan', 'ND4MyRpt', 'FlowDaySpan', '跨度(天)', '', '0', '8', '0', '100', '23', '0', '300', '', '', '', '1', '1', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1282', '1', '-101', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4MyRpt_FlowEmps', 'ND4MyRpt', 'FlowEmps', '参与人', '', '0', '1', '0', '100', '23', '0', '1000', '', '', '', '1', '0', '1', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1282', '1', '-100', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4MyRpt_FlowEnder', 'ND4MyRpt', 'FlowEnder', '结束人', '', '1', '1', '2', '100', '23', '0', '20', 'BP.Port.Emps', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1282', '1', '-1', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4MyRpt_FlowEnderRDT', 'ND4MyRpt', 'FlowEnderRDT', '结束时间', '', '0', '7', '0', '145', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '', '1', '', '1', '1282', '1', '-101', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4MyRpt_FlowEndNode', 'ND4MyRpt', 'FlowEndNode', '结束节点', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1282', '1', '-101', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4MyRpt_FlowStarter', 'ND4MyRpt', 'FlowStarter', '发起人', '', '1', '1', '2', '100', '23', '0', '20', 'BP.Port.Emps', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1282', '1', '-1', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4MyRpt_FlowStartRDT', 'ND4MyRpt', 'FlowStartRDT', '发起时间', '', '0', '7', '0', '145', '23', '0', '300', '', '', '', '1', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '', '1', '', '1', '1282', '1', '-101', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4MyRpt_OID', 'ND4MyRpt', 'OID', 'OID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '1281', '1', '999', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4MyRpt_Title', 'ND4MyRpt', 'Title', '标题', '', '0', '1', '0', '251', '23', '0', '200', '', '', '', '0', '0', '1', '0', '0', '0', '0', '0', '174.83', '54.4', '', '', '0', '', '1', '1274', '1', '-100', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4MyRpt_WFSta', 'ND4MyRpt', 'WFSta', '状态', '', '1', '2', '1', '100', '23', '0', '1000', 'WFSta', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1281', '1', '-1', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND4MyRpt_WFState', 'ND4MyRpt', 'WFState', '流程状态', '', '1', '2', '1', '100', '23', '0', '1000', 'WFState', '', '', '1', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '1', '', '1', '1282', '1', '-1', '', '0', '0', '0', null, '0', null, '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('_OID', '', 'OID', 'OID', '0', '0', '2', '0', '100', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '5', '5', '', '', '2', '', '1', '1218', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');
INSERT INTO `sys_mapattr` VALUES ('_RDT', '', 'RDT', '更新时间', '@RDT', '0', '7', '0', '145', '23', '0', '300', '', '', '', '0', '0', '0', '0', '0', '1', '0', '0', '5', '5', '', '1', '1', '', '1', '1218', '1', '999', '', '0', '0', '0', '0', '0', '0', '0', '', '', '', '1', '1', '1.00', '1', '0', '0');

-- ----------------------------
-- Table structure for sys_mapdata
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapdata`;
CREATE TABLE `sys_mapdata` (
  `No` varchar(200) NOT NULL COMMENT '表单编号',
  `Name` varchar(500) DEFAULT NULL COMMENT '表单名称',
  `OfficeOpenLab` varchar(50) DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOverLab` varchar(50) DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeOverEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeMarksEnable` int(11) DEFAULT '1' COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) DEFAULT NULL COMMENT '打印按钮标签',
  `OfficePrintEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSealLab` varchar(50) DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeSealEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeNodeInfo` int(11) DEFAULT '0' COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) DEFAULT '0' COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeIsMarks` int(11) DEFAULT '1' COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) DEFAULT '1' COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) DEFAULT '0' COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) DEFAULT NULL COMMENT '自动套红模板',
  `PTable` varchar(500) DEFAULT NULL,
  `IsTemplate` int(11) DEFAULT '0' COMMENT '是否是表单模版',
  `AtPara` text COMMENT 'AtPara',
  `FormEventEntity` varchar(100) DEFAULT '' COMMENT '事件实体',
  `EnPK` varchar(200) DEFAULT '' COMMENT '实体主键',
  `PTableModel` int(11) DEFAULT '0' COMMENT '表存储模式',
  `Url` varchar(500) DEFAULT '' COMMENT '连接(对嵌入式表单有效)',
  `Dtls` varchar(500) DEFAULT '' COMMENT '从表',
  `FrmW` int(11) DEFAULT '900' COMMENT 'FrmW',
  `FrmH` int(11) DEFAULT '1200' COMMENT 'FrmH',
  `TableCol` int(11) DEFAULT '4' COMMENT '傻瓜表单显示的列',
  `Tag` varchar(500) DEFAULT '' COMMENT 'Tag',
  `FK_FrmSort` varchar(500) DEFAULT '' COMMENT '表单类别',
  `FK_FormTree` varchar(500) DEFAULT '' COMMENT '表单树类别',
  `FrmType` int(11) DEFAULT '1' COMMENT '表单类型',
  `AppType` int(11) DEFAULT '0' COMMENT '应用类型',
  `DBSrc` varchar(100) DEFAULT 'local' COMMENT '数据源',
  `BodyAttr` varchar(100) DEFAULT '' COMMENT '表单Body属性',
  `Note` varchar(4000) DEFAULT NULL,
  `Designer` varchar(500) DEFAULT '' COMMENT '设计者',
  `DesignerUnit` varchar(500) DEFAULT '' COMMENT '单位',
  `DesignerContact` varchar(500) DEFAULT '' COMMENT '联系方式',
  `Idx` int(11) DEFAULT '100' COMMENT '顺序号',
  `GUID` varchar(128) DEFAULT '' COMMENT 'GUID',
  `Ver` varchar(30) DEFAULT '' COMMENT '版本号',
  `FlowCtrls` varchar(200) DEFAULT '' COMMENT '流程控件',
  `FK_Flow` varchar(50) DEFAULT '' COMMENT '独立表单属性:FK_Flow',
  `DBURL` int(11) DEFAULT '0' COMMENT 'DBURL',
  `MyFileName` varchar(300) DEFAULT '' COMMENT '表单模版',
  `MyFilePath` varchar(300) DEFAULT '' COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT '' COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT '' COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float(11,2) DEFAULT '0.00' COMMENT 'MyFileSize',
  `RightViewWay` int(11) DEFAULT '0' COMMENT '报表查看权限控制方式',
  `RightViewTag` text COMMENT '报表查看权限控制Tag',
  `RightDeptWay` int(11) DEFAULT '0' COMMENT '部门数据查看控制方式',
  `RightDeptTag` text COMMENT '部门数据查看控制Tag',
  `TemplaterVer` varchar(30) DEFAULT '' COMMENT '模版编号',
  `DBSave` varchar(50) DEFAULT '' COMMENT 'Excel数据文件存储',
  `TableWidth` int(11) DEFAULT '800',
  `TableHeight` int(11) DEFAULT '900',
  `FrmShowType` int(11) DEFAULT '0' COMMENT '表单展示方式',
  `EntityType` int(11) DEFAULT '0' COMMENT '业务类型',
  `OrgNo` varchar(50) DEFAULT '' COMMENT 'OrgNo',
  `IsEnableJS` int(11) DEFAULT '0',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_mapdata
-- ----------------------------
INSERT INTO `sys_mapdata` VALUES ('ND1MyRpt', '请假流程-经典模式报表', null, '0', null, '0', null, '1', null, '0', null, '0', null, '0', '1', null, '0', null, '0', null, '0', '0', '0', null, '0', '1', null, '1', '0', null, 'ND1Rpt', '0', '@IsHaveCA=0', '', '', '0', '', '', '900', '1200', '0', '', '', '', '0', '0', 'local', '', '默认.', '', '', '', '100', '', '2019-08-31 11:50:11', '', '001', '0', '', '', '', '', '0', '0', '0.00', '0', '', '0', '', '', '', '800', '900', '0', '0', '', '0');
INSERT INTO `sys_mapdata` VALUES ('ND2MyRpt', '请假流程报表', null, '0', null, '0', null, '1', null, '0', null, '0', null, '0', '1', null, '0', null, '0', null, '0', '0', '0', null, '0', '1', null, '1', '0', null, 'ND2Rpt', '0', '@IsHaveCA=0', '', '', '0', '', '', '900', '1200', '0', '', '', '', '0', '0', 'local', '', '默认.', '', '', '', '100', '', '2019-08-31 19:00:55', '', '002', '0', '', '', '', '', '0', '0', '0.00', '0', '', '0', '', '', '', '800', '900', '0', '0', '', '0');
INSERT INTO `sys_mapdata` VALUES ('ND4MyRpt', '请假流程报表', null, '0', null, '0', null, '1', null, '0', null, '0', null, '0', '1', null, '0', null, '0', null, '0', '0', '0', null, '0', '1', null, '1', '0', null, 'ND4Rpt', '0', '@IsHaveCA=0', '', '', '0', '', '', '900', '1200', '0', '', '', '', '0', '0', 'local', '', '默认.', '', '', '', '100', '', '2019-09-02 16:21:41', '', '004', '0', '', '', '', '', '0', '0', '0.00', '0', '', '0', '', '', '', '800', '900', '0', '0', '', '0');

-- ----------------------------
-- Table structure for sys_mapdtl
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapdtl`;
CREATE TABLE `sys_mapdtl` (
  `No` varchar(100) NOT NULL COMMENT '编号',
  `Name` varchar(200) DEFAULT NULL COMMENT '描述',
  `Alias` varchar(200) DEFAULT NULL COMMENT '别名',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `PTable` varchar(200) DEFAULT NULL COMMENT '物理表',
  `GroupField` varchar(300) DEFAULT NULL COMMENT '分组字段',
  `RefPK` varchar(100) DEFAULT NULL COMMENT '关联的主键',
  `FEBD` varchar(100) DEFAULT NULL COMMENT '映射的事件实体类',
  `Model` int(11) DEFAULT '0' COMMENT '工作模式',
  `RowsOfList` int(11) DEFAULT '6' COMMENT '初始化行数',
  `IsEnableGroupField` int(11) DEFAULT '0' COMMENT '是否启用分组字段',
  `IsShowSum` int(11) DEFAULT '1' COMMENT '是否显示合计？',
  `IsShowIdx` int(11) DEFAULT '1' COMMENT '是否显示序号？',
  `IsCopyNDData` int(11) DEFAULT '1' COMMENT '是否允许Copy数据',
  `IsHLDtl` int(11) DEFAULT '0' COMMENT '是否是合流汇总',
  `IsReadonly` int(11) DEFAULT '0' COMMENT '是否只读？',
  `IsShowTitle` int(11) DEFAULT '1' COMMENT '是否显示标题？',
  `IsView` int(11) DEFAULT '1' COMMENT '是否可见',
  `IsInsert` int(11) DEFAULT '1' COMMENT '是否可以插入行？',
  `IsDelete` int(11) DEFAULT '1' COMMENT '是否可以删除行',
  `IsUpdate` int(11) DEFAULT '1' COMMENT '是否可以更新？',
  `IsEnablePass` int(11) DEFAULT '0' COMMENT '是否启用通过审核功能?',
  `IsEnableAthM` int(11) DEFAULT '0' COMMENT '是否启用多附件',
  `IsEnableM2M` int(11) DEFAULT '0' COMMENT '是否启用M2M',
  `IsEnableM2MM` int(11) DEFAULT '0' COMMENT '是否启用M2M',
  `WhenOverSize` int(11) DEFAULT '0' COMMENT '列表数据显示格式',
  `DtlOpenType` int(11) DEFAULT '1' COMMENT '数据开放类型',
  `ListShowModel` int(11) DEFAULT '0' COMMENT '列表数据显示格式',
  `EditModel` int(11) DEFAULT '0' COMMENT '行数据显示格式',
  `X` float DEFAULT NULL COMMENT '距左',
  `Y` float DEFAULT NULL COMMENT '距上',
  `H` float DEFAULT NULL COMMENT '高度',
  `W` float DEFAULT NULL COMMENT '宽度',
  `FrmW` float DEFAULT NULL COMMENT '表单宽度',
  `FrmH` float DEFAULT NULL COMMENT '表单高度',
  `MTR` varchar(4000) DEFAULT NULL,
  `FilterSQLExp` varchar(200) DEFAULT NULL COMMENT '过滤SQL表达式',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点(用户独立表单权限控制)',
  `ShowCols` varchar(500) DEFAULT NULL COMMENT '显示的列',
  `IsExp` int(11) DEFAULT '1' COMMENT 'IsExp',
  `ImpModel` int(11) DEFAULT '0' COMMENT '导入规则',
  `ImpSQLSearch` varchar(4000) DEFAULT NULL,
  `ImpSQLInit` varchar(4000) DEFAULT NULL,
  `ImpSQLFullOneRow` varchar(4000) DEFAULT NULL,
  `ImpSQLNames` varchar(900) DEFAULT NULL COMMENT '字段中文名',
  `ColAutoExp` varchar(200) DEFAULT NULL COMMENT '列自动计算表达式',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(300) DEFAULT NULL COMMENT 'AtPara',
  `IsEnableLink` int(11) DEFAULT '0' COMMENT '是否启用超链接',
  `LinkLabel` varchar(50) DEFAULT '' COMMENT '超连接标签',
  `LinkTarget` varchar(10) DEFAULT '' COMMENT '连接目标',
  `LinkUrl` varchar(200) DEFAULT '' COMMENT '连接URL',
  `SubThreadWorker` varchar(50) DEFAULT '' COMMENT '子线程处理人字段',
  `SubThreadWorkerText` varchar(200) DEFAULT NULL,
  `ImpSQLFull` varchar(500) DEFAULT '',
  `PTableModel` int(11) DEFAULT '0',
  `DtlVer` int(11) DEFAULT '0' COMMENT '使用版本',
  `MobileShowModel` int(11) DEFAULT '0' COMMENT '移动端数据显示方式',
  `MobileShowField` varchar(100) DEFAULT '' COMMENT '移动端列表显示字段',
  `IsImp` int(11) DEFAULT '0' COMMENT '是否可以导出？',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_mapdtl
-- ----------------------------

-- ----------------------------
-- Table structure for sys_mapext
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapext`;
CREATE TABLE `sys_mapext` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `ExtType` varchar(30) DEFAULT NULL COMMENT '类型',
  `DoWay` int(11) DEFAULT '0' COMMENT '执行方式',
  `AttrOfOper` varchar(30) DEFAULT NULL COMMENT '操作的Attr',
  `AttrsOfActive` varchar(900) DEFAULT NULL COMMENT '激活的字段',
  `Doc` text COMMENT '内容',
  `Tag` varchar(2000) DEFAULT NULL COMMENT 'Tag',
  `Tag1` varchar(2000) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(2000) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(2000) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(2000) DEFAULT NULL COMMENT 'Tag4',
  `H` int(11) DEFAULT '500' COMMENT '高度',
  `W` int(11) DEFAULT '400' COMMENT '宽度',
  `DBType` int(11) DEFAULT '0' COMMENT '数据类型',
  `FK_DBSrc` varchar(100) DEFAULT NULL COMMENT '数据源',
  `PRI` int(11) DEFAULT '0' COMMENT 'PRI',
  `AtPara` text COMMENT '参数',
  `DBSrc` varchar(20) DEFAULT '',
  `Tag5` varchar(2000) DEFAULT '',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_mapext
-- ----------------------------
INSERT INTO `sys_mapext` VALUES ('AutoFull_ND4502Dtl1_XiaoJi', 'ND4502Dtl1', 'AutoFull', '0', 'XiaoJi', '', '@销售价格*@销售数量', '1', '', '', '', '', '500', '400', '0', '', '0', '', '', '');
INSERT INTO `sys_mapext` VALUES ('AutoFull_ND4503Dtl1_XiaoJi', 'ND4503Dtl1', 'AutoFull', '0', 'XiaoJi', '', '@销售价格*@销售数量', '1', '', '', '', '', '500', '400', '0', '', '0', '', '', '');
INSERT INTO `sys_mapext` VALUES ('AutoFull_ND4504Dtl1_XiaoJi', 'ND4504Dtl1', 'AutoFull', '0', 'XiaoJi', '', '@销售价格*@销售数量', '1', '', '', '', '', '500', '400', '0', '', '0', '', '', '');
INSERT INTO `sys_mapext` VALUES ('AutoFull_ND4599Dtl1_XiaoJi', 'ND4599Dtl1', 'AutoFull', '0', 'XiaoJi', '', '@销售价格*@销售数量', '1', '', '', '', '', '500', '400', '0', '', '0', '', '', '');

-- ----------------------------
-- Table structure for sys_mapframe
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapframe`;
CREATE TABLE `sys_mapframe` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '表单ID',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `URL` varchar(3000) DEFAULT NULL COMMENT 'URL',
  `UrlSrcType` int(11) DEFAULT '0' COMMENT 'URL来源',
  `FrmID` varchar(50) DEFAULT NULL COMMENT '表单表单',
  `FrmIDText` varchar(50) DEFAULT NULL COMMENT '表单表单',
  `Y` varchar(20) DEFAULT NULL COMMENT 'Y',
  `X` varchar(20) DEFAULT NULL COMMENT 'x',
  `W` varchar(20) DEFAULT NULL COMMENT '宽度',
  `H` varchar(20) DEFAULT NULL COMMENT '高度',
  `IsAutoSize` int(11) DEFAULT '1' COMMENT '是否自动设置大小',
  `EleType` varchar(50) DEFAULT NULL COMMENT '类型',
  `GUID` varchar(128) DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_mapframe
-- ----------------------------

-- ----------------------------
-- Table structure for sys_mapm2m
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapm2m`;
CREATE TABLE `sys_mapm2m` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT '主表',
  `NoOfObj` varchar(20) DEFAULT NULL COMMENT '编号',
  `Name` varchar(200) DEFAULT NULL COMMENT '名称',
  `DBOfLists` text COMMENT '列表数据源(对一对多对多模式有效）',
  `DBOfObjs` text COMMENT 'DBOfObjs',
  `DBOfGroups` text COMMENT 'DBOfGroups',
  `H` float DEFAULT NULL COMMENT 'H',
  `W` float DEFAULT NULL COMMENT 'W',
  `X` float DEFAULT NULL COMMENT 'X',
  `Y` float DEFAULT NULL COMMENT 'Y',
  `ShowWay` int(11) DEFAULT NULL COMMENT '显示方式',
  `M2MType` int(11) DEFAULT NULL COMMENT '类型',
  `RowIdx` int(11) DEFAULT NULL COMMENT '位置',
  `GroupID` int(11) DEFAULT NULL COMMENT '分组ID',
  `Cols` int(11) DEFAULT NULL COMMENT '记录呈现列数',
  `IsDelete` int(11) DEFAULT NULL COMMENT '可删除否',
  `IsInsert` int(11) DEFAULT NULL COMMENT '可插入否',
  `IsCheckAll` int(11) DEFAULT NULL COMMENT '是否显示选择全部',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='多选';

-- ----------------------------
-- Records of sys_mapm2m
-- ----------------------------

-- ----------------------------
-- Table structure for sys_rptdept
-- ----------------------------
DROP TABLE IF EXISTS `sys_rptdept`;
CREATE TABLE `sys_rptdept` (
  `FK_Rpt` varchar(15) NOT NULL COMMENT '报表',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门',
  PRIMARY KEY (`FK_Rpt`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_rptdept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_rptemp
-- ----------------------------
DROP TABLE IF EXISTS `sys_rptemp`;
CREATE TABLE `sys_rptemp` (
  `FK_Rpt` varchar(15) NOT NULL COMMENT '报表',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员',
  PRIMARY KEY (`FK_Rpt`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_rptemp
-- ----------------------------

-- ----------------------------
-- Table structure for sys_rptstation
-- ----------------------------
DROP TABLE IF EXISTS `sys_rptstation`;
CREATE TABLE `sys_rptstation` (
  `FK_Rpt` varchar(15) NOT NULL COMMENT '报表',
  `FK_Station` varchar(100) NOT NULL COMMENT '岗位',
  PRIMARY KEY (`FK_Rpt`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_rptstation
-- ----------------------------

-- ----------------------------
-- Table structure for sys_rpttemplate
-- ----------------------------
DROP TABLE IF EXISTS `sys_rpttemplate`;
CREATE TABLE `sys_rpttemplate` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `EnsName` varchar(500) DEFAULT NULL COMMENT '类名',
  `FK_Emp` varchar(20) DEFAULT NULL COMMENT '操作员',
  `D1` varchar(90) DEFAULT NULL COMMENT 'D1',
  `D2` varchar(90) DEFAULT NULL COMMENT 'D2',
  `D3` varchar(90) DEFAULT NULL COMMENT 'D3',
  `AlObjs` varchar(90) DEFAULT NULL COMMENT '要分析的对象',
  `Height` int(11) DEFAULT '600' COMMENT 'Height',
  `Width` int(11) DEFAULT '800' COMMENT 'Width',
  `IsSumBig` int(11) DEFAULT '0' COMMENT '是否显示大合计',
  `IsSumLittle` int(11) DEFAULT '0' COMMENT '是否显示小合计',
  `IsSumRight` int(11) DEFAULT '0' COMMENT '是否显示右合计',
  `PercentModel` int(11) DEFAULT '0' COMMENT '比率显示方式',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_rpttemplate
-- ----------------------------

-- ----------------------------
-- Table structure for sys_serial
-- ----------------------------
DROP TABLE IF EXISTS `sys_serial`;
CREATE TABLE `sys_serial` (
  `CfgKey` varchar(100) NOT NULL COMMENT 'CfgKey',
  `IntVal` int(11) DEFAULT '0' COMMENT '属性',
  PRIMARY KEY (`CfgKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_serial
-- ----------------------------
INSERT INTO `sys_serial` VALUES ('BP.WF.Template.FlowSort', '105');
INSERT INTO `sys_serial` VALUES ('Demo_Resume', '279');
INSERT INTO `sys_serial` VALUES ('OID', '1287');
INSERT INTO `sys_serial` VALUES ('Template', '100');
INSERT INTO `sys_serial` VALUES ('UpdataCCFlowVer', '2020042118');
INSERT INTO `sys_serial` VALUES ('Ver', '20200411');
INSERT INTO `sys_serial` VALUES ('WorkID', '148');

-- ----------------------------
-- Table structure for sys_sfdbsrc
-- ----------------------------
DROP TABLE IF EXISTS `sys_sfdbsrc`;
CREATE TABLE `sys_sfdbsrc` (
  `No` varchar(20) NOT NULL COMMENT '数据源编号(必须是英文)',
  `Name` varchar(30) DEFAULT NULL COMMENT '数据源名称',
  `DBSrcType` int(11) DEFAULT '0' COMMENT '数据源类型',
  `UserID` varchar(30) DEFAULT NULL COMMENT '数据库登录用户ID',
  `Password` varchar(30) DEFAULT NULL COMMENT '数据库登录用户密码',
  `IP` varchar(500) DEFAULT NULL COMMENT 'IP地址/数据库实例名',
  `DBName` varchar(30) DEFAULT NULL COMMENT '数据库名称/Oracle保持为空',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sfdbsrc
-- ----------------------------
INSERT INTO `sys_sfdbsrc` VALUES ('local', '本机数据源(默认)', '0', '', '', '', '');

-- ----------------------------
-- Table structure for sys_sftable
-- ----------------------------
DROP TABLE IF EXISTS `sys_sftable`;
CREATE TABLE `sys_sftable` (
  `No` varchar(200) NOT NULL COMMENT '表英文名称',
  `Name` varchar(200) DEFAULT NULL COMMENT '表中文名称',
  `SrcType` int(11) DEFAULT '0' COMMENT '数据表类型',
  `CodeStruct` int(11) DEFAULT '0' COMMENT '字典表类型',
  `RootVal` varchar(200) DEFAULT NULL COMMENT '根节点值',
  `FK_Val` varchar(200) DEFAULT NULL COMMENT '默认创建的字段名',
  `TableDesc` varchar(200) DEFAULT NULL COMMENT '表描述',
  `DefVal` varchar(200) DEFAULT NULL COMMENT '默认值',
  `FK_SFDBSrc` varchar(100) DEFAULT NULL COMMENT '数据源',
  `SrcTable` varchar(200) DEFAULT NULL COMMENT '数据源表',
  `ColumnValue` varchar(200) DEFAULT NULL COMMENT '显示的值(编号列)',
  `ColumnText` varchar(200) DEFAULT NULL COMMENT '显示的文字(名称列)',
  `ParentValue` varchar(200) DEFAULT NULL COMMENT '父级值(父级列)',
  `SelectStatement` varchar(1000) DEFAULT NULL COMMENT '查询语句',
  `RDT` varchar(50) DEFAULT NULL COMMENT '加入日期',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sftable
-- ----------------------------
INSERT INTO `sys_sftable` VALUES ('BP.Port.Depts', '部门', '0', '0', null, 'FK_Dept', '部门', '', 'local', null, null, null, null, null, null);
INSERT INTO `sys_sftable` VALUES ('BP.Port.Emps', '人员', '0', '0', null, 'FK_Emp', '系统中的操作员', '', 'local', null, null, null, null, null, null);
INSERT INTO `sys_sftable` VALUES ('BP.Port.Stations', '岗位', '0', '0', null, 'FK_Station', '工作岗位', '', 'local', null, null, null, null, null, null);
INSERT INTO `sys_sftable` VALUES ('BP.Pub.Days', '日', '0', '0', null, 'FK_Day', '1-31日', '', 'local', null, null, null, null, null, null);
INSERT INTO `sys_sftable` VALUES ('BP.Pub.NYs', '年月', '0', '0', null, 'FK_NY', '年度与月份', '', 'local', null, null, null, null, null, null);
INSERT INTO `sys_sftable` VALUES ('BP.Pub.YFs', '月', '0', '0', null, 'FK_YF', '1-12月', '', 'local', null, null, null, null, null, null);
INSERT INTO `sys_sftable` VALUES ('CN_City', '城市', '1', '0', null, 'FK_City', '中国的市级城市', '', 'local', null, null, null, null, null, null);
INSERT INTO `sys_sftable` VALUES ('CN_PQ', '地区', '1', '0', null, 'FK_DQ', '华北、西北、西南。。。', '', 'local', null, null, null, null, null, null);
INSERT INTO `sys_sftable` VALUES ('CN_SF', '省份', '1', '0', null, 'FK_SF', '中国的省份。', '', 'local', null, null, null, null, null, null);
INSERT INTO `sys_sftable` VALUES ('Demo_BanJi', '班级', '1', '0', null, 'FK_BJ', '班级', '', 'local', null, null, null, null, null, null);
INSERT INTO `sys_sftable` VALUES ('Demo_Student', '学生', '1', '0', null, 'FK_Student', '学生', '', 'local', null, null, null, null, null, null);
INSERT INTO `sys_sftable` VALUES ('Port_Dept', '部门', '1', '0', null, 'FK_Dept', '部门', '', 'local', null, null, null, null, null, null);
INSERT INTO `sys_sftable` VALUES ('Port_Emp', '操作员', '1', '0', null, 'FK_Emp', '操作员', '', 'local', null, null, null, null, null, null);
INSERT INTO `sys_sftable` VALUES ('Port_Station', '岗位', '1', '0', null, 'FK_Station', '岗位', '', 'local', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_sms
-- ----------------------------
DROP TABLE IF EXISTS `sys_sms`;
CREATE TABLE `sys_sms` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人(可以为空)',
  `SendTo` varchar(200) DEFAULT NULL COMMENT '发送给(可以为空)',
  `RDT` varchar(50) DEFAULT NULL COMMENT '写入时间',
  `Mobile` varchar(30) DEFAULT NULL COMMENT '手机号(可以为空)',
  `MobileSta` int(11) DEFAULT '0' COMMENT '消息状态',
  `MobileInfo` varchar(1000) DEFAULT NULL COMMENT '短信信息',
  `Email` varchar(200) DEFAULT NULL COMMENT 'Email(可以为空)',
  `EmailSta` int(11) DEFAULT '0' COMMENT 'EmaiSta消息状态',
  `EmailTitle` varchar(3000) DEFAULT NULL COMMENT '标题',
  `EmailDoc` text COMMENT '内容',
  `SendDT` varchar(50) DEFAULT NULL COMMENT '发送时间',
  `IsRead` int(11) DEFAULT '0' COMMENT '是否读取?',
  `IsAlert` int(11) DEFAULT '0' COMMENT '是否提示?',
  `MsgFlag` varchar(200) DEFAULT NULL COMMENT '消息标记(用于防止发送重复)',
  `MsgType` varchar(200) DEFAULT NULL COMMENT '消息类型(CC抄送,Todolist待办,Return退回,Etc其他消息...)',
  `AtPara` varchar(500) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sms
-- ----------------------------
INSERT INTO `sys_sms` VALUES ('789ee172c3674a60b9072f5e29ca696f', 'admin', 'system', '2019-09-02 17:08', '', '0', '有新工作{山东公司-admin,系统管理员在2019-09-02 17:08发起.}需要您处理, 发送人:admin, 系统管理员,打开http:/localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=system_148_201_2019-09-02 .', '', '3', '', '', '', '0', '0', 'WKAlt202_148', 'SendSuccess', '@FK_Flow=002@WorkID=148@FK_Node=201@OpenUrl=http:/localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=system_148_201_2019-09-02');
INSERT INTO `sys_sms` VALUES ('ab17a36421c241a983d63624af381d1e', 'admin', 'admin', '2019-09-02 16:22', '', '0', '有新工作{山东公司-admin,系统管理员在2019-09-02 16:21发起.}需要您处理, 发送人:admin, 系统管理员,打开http:/localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=admin_147_401_2019-09-02 .', '', '3', '', '', '', '0', '0', 'WKAlt402_147', 'SendSuccess', '@FK_Flow=004@WorkID=147@FK_Node=401@OpenUrl=http:/localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=admin_147_401_2019-09-02');
INSERT INTO `sys_sms` VALUES ('e569a5036a234a36ad1f29bbff9f3236', 'admin', 'admin', '2019-09-02 16:20', '', '0', '有新工作{山东公司-admin,系统管理员在2019-09-02 16:20发起.}需要您处理, 发送人:admin, 系统管理员,打开http:/localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=admin_145_201_2019-09-02 .', '', '3', '', '', '', '0', '0', 'WKAlt202_145', 'SendSuccess', '@FK_Flow=002@WorkID=145@FK_Node=201@OpenUrl=http:/localhost:8080/jflow-web/WF/Do.htm?DoType=OF&SID=admin_145_201_2019-09-02');

-- ----------------------------
-- Table structure for sys_userlogt
-- ----------------------------
DROP TABLE IF EXISTS `sys_userlogt`;
CREATE TABLE `sys_userlogt` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Emp` varchar(30) DEFAULT NULL COMMENT '用户',
  `IP` varchar(200) DEFAULT NULL COMMENT 'IP',
  `LogFlag` varchar(300) DEFAULT NULL COMMENT '标识',
  `Docs` varchar(300) DEFAULT NULL COMMENT '说明',
  `RDT` varchar(20) DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_userlogt
-- ----------------------------
INSERT INTO `sys_userlogt` VALUES ('01f0903801e544fc8650f8a646bffe65', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:46:10');
INSERT INTO `sys_userlogt` VALUES ('03c3eb4d6b6141d4bd59f35928a66721', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 15:06:40');
INSERT INTO `sys_userlogt` VALUES ('04524ba37d2f4473baf7a7bf9f2051c8', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 15:01:41');
INSERT INTO `sys_userlogt` VALUES ('04c1a561faa54b388123f35f300ce978', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:53:11');
INSERT INTO `sys_userlogt` VALUES ('06f592c31fdb48df8179b9f10c4f88d2', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 11:41:19');
INSERT INTO `sys_userlogt` VALUES ('0c26445f12674d24951417a33efd4c07', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:28:34');
INSERT INTO `sys_userlogt` VALUES ('11b0b37a903042f19e94acba1086797f', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 11:34:21');
INSERT INTO `sys_userlogt` VALUES ('150c7d779ebb4e398efa006d192f2b67', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 14:58:50');
INSERT INTO `sys_userlogt` VALUES ('17dd3802a13e4bed97615ff0f842a8db', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 10:04:45');
INSERT INTO `sys_userlogt` VALUES ('1d64d05ba3e64b8b8460f407d978df4c', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 14:58:45');
INSERT INTO `sys_userlogt` VALUES ('1ee926bf669d48ab95cba5c6f6882122', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-08 17:12:56');
INSERT INTO `sys_userlogt` VALUES ('23758487e19c4a51ad3252cde46eff4e', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:58:54');
INSERT INTO `sys_userlogt` VALUES ('36f275d1b6714a028af3b8aaca1d5577', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 14:59:20');
INSERT INTO `sys_userlogt` VALUES ('3882d8f7d30a49debc2a170468724e5a', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:58:50');
INSERT INTO `sys_userlogt` VALUES ('3b2cdb725e3e4f00ab3a71528686e97a', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:14:45');
INSERT INTO `sys_userlogt` VALUES ('444abe42db0a439d90dd06d2d614e2bc', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:45:58');
INSERT INTO `sys_userlogt` VALUES ('44e3a62dc11348f29aed3fdd95776efa', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 14:24:23');
INSERT INTO `sys_userlogt` VALUES ('4b8195327ff14f14bc511b8676a9e5e5', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 15:07:10');
INSERT INTO `sys_userlogt` VALUES ('4c7a342188b9489c8f877c9b8d86ec25', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:37:48');
INSERT INTO `sys_userlogt` VALUES ('54314c0454da404f9a6bdac360a10c70', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 14:03:05');
INSERT INTO `sys_userlogt` VALUES ('595698e9f6a549c9b44815ab5b01532d', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:37:40');
INSERT INTO `sys_userlogt` VALUES ('5c0654d913884927869015d6c11c500e', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 14:18:07');
INSERT INTO `sys_userlogt` VALUES ('5c46a54655384464a6a8ca9a3e576d1b', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 11:35:30');
INSERT INTO `sys_userlogt` VALUES ('674d908f356744aaa6cd82ccb9fda053', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 14:59:09');
INSERT INTO `sys_userlogt` VALUES ('6b1afbd17d474f93a2d94d95a0a5d02a', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 10:25:46');
INSERT INTO `sys_userlogt` VALUES ('6eada14d4e1f4b93a9772f02bc1fe61e', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:53:04');
INSERT INTO `sys_userlogt` VALUES ('7256c7cff28e412ab053fd0a671cbfb7', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 11:40:38');
INSERT INTO `sys_userlogt` VALUES ('72d1aa282f0d4f00aa8a2578e5df0ab5', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 14:24:19');
INSERT INTO `sys_userlogt` VALUES ('7ae0bd7c23f94fdba9830e272921bb9e', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 10:02:17');
INSERT INTO `sys_userlogt` VALUES ('7cd9c503e8384166b21d01733dea34cf', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 12:08:01');
INSERT INTO `sys_userlogt` VALUES ('7d7a35c8a0ef4c3baa27a5a710c93dc9', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:52:59');
INSERT INTO `sys_userlogt` VALUES ('805db20566344b4fafef0e460686d00d', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 15:05:28');
INSERT INTO `sys_userlogt` VALUES ('80757d84036c4d7892bb34d4aadb7890', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:28:27');
INSERT INTO `sys_userlogt` VALUES ('81608043e48144819227e9681dd9fa3b', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:19:20');
INSERT INTO `sys_userlogt` VALUES ('83fc9ad75e144f648fdc4df314ff499a', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 11:34:11');
INSERT INTO `sys_userlogt` VALUES ('85014537ddba4945b1d79d9945924ee8', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:19:25');
INSERT INTO `sys_userlogt` VALUES ('87ee680279d34d1b8c29ce3e66922cc7', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 10:01:18');
INSERT INTO `sys_userlogt` VALUES ('8aa14819fb1845adb13a4be648a82366', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 10:03:30');
INSERT INTO `sys_userlogt` VALUES ('8dd332978e564f4591e96d14c511232e', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 14:18:48');
INSERT INTO `sys_userlogt` VALUES ('8f2f8f7ae3664d508b5174f378d59cf3', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:46:04');
INSERT INTO `sys_userlogt` VALUES ('913352d11a5f43cca6aa972b76b3e2c8', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:19:51');
INSERT INTO `sys_userlogt` VALUES ('9405d6771e434476b408802682359267', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:37:36');
INSERT INTO `sys_userlogt` VALUES ('9ed527fecb9647b0bad2c4e9911e19b8', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:28:55');
INSERT INTO `sys_userlogt` VALUES ('a06f7c5550d844c0a31f90f997557a76', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-08 17:12:46');
INSERT INTO `sys_userlogt` VALUES ('a151631be2e945a089917b9267b2219d', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 10:01:30');
INSERT INTO `sys_userlogt` VALUES ('a40ac683ce394e358bdd5cac077dfa60', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 15:05:24');
INSERT INTO `sys_userlogt` VALUES ('a80e37a9252a4ff4b6db183345ceae10', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 12:07:52');
INSERT INTO `sys_userlogt` VALUES ('b403a0504e124394a693ce421e9713a9', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 14:02:48');
INSERT INTO `sys_userlogt` VALUES ('b7c05d605c4b4bda89b099eaa91acaac', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 15:01:44');
INSERT INTO `sys_userlogt` VALUES ('b817ed1c2cec4450843bb4483e78e5ff', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 15:07:01');
INSERT INTO `sys_userlogt` VALUES ('c4ffed0c83fd49e59fabb3fbb96a3f5f', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:55:46');
INSERT INTO `sys_userlogt` VALUES ('c87f04cb1160486c85038151d279d68a', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-09 11:40:31');
INSERT INTO `sys_userlogt` VALUES ('cb2f6b6223de424fb494c3629f56ced9', 'admin', '127.0.0.1', 'SignIn', '登录', '2019-03-08 17:15:55');
INSERT INTO `sys_userlogt` VALUES ('eae6d5023b5147dfb3580f5609935403', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:33:23');
INSERT INTO `sys_userlogt` VALUES ('edcdad9112064125a0141cb9b7b16429', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:56:04');
INSERT INTO `sys_userlogt` VALUES ('ef4d9bd3c957485b8ec7a73632829e01', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 14:18:11');
INSERT INTO `sys_userlogt` VALUES ('f1778414c419482682a5a6581f713acc', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:33:19');
INSERT INTO `sys_userlogt` VALUES ('f57c3cf698f74d01a06b1e8c9fd76987', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:59:00');
INSERT INTO `sys_userlogt` VALUES ('fabcd1863e2044dfba8eceb16649386e', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 14:02:43');
INSERT INTO `sys_userlogt` VALUES ('faf06dad3dff4166a3ee8763892d7279', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:55:42');
INSERT INTO `sys_userlogt` VALUES ('fb04a19e6c9149a29cbdaa490f1f9c05', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 14:03:36');
INSERT INTO `sys_userlogt` VALUES ('fedf6a50b1674495bf130bf1fb118d59', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:33:29');
INSERT INTO `sys_userlogt` VALUES ('ff31d84b51874480b51540ba9f70940e', 'admin', '0:0:0:0:0:0:0:1', 'SignIn', '登录', '2019-03-09 13:14:58');

-- ----------------------------
-- Table structure for sys_userregedit
-- ----------------------------
DROP TABLE IF EXISTS `sys_userregedit`;
CREATE TABLE `sys_userregedit` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `EnsName` varchar(100) DEFAULT NULL COMMENT '实体类名称',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '工作人员',
  `Attrs` text COMMENT '属性s',
  `CfgKey` varchar(200) DEFAULT '' COMMENT '键',
  `Vals` varchar(2000) DEFAULT '' COMMENT '值',
  `GenerSQL` varchar(2000) DEFAULT '' COMMENT 'GenerSQL',
  `Paras` varchar(2000) DEFAULT '' COMMENT 'Paras',
  `NumKey` varchar(300) DEFAULT '' COMMENT '分析的Key',
  `OrderBy` varchar(300) DEFAULT '' COMMENT 'OrderBy',
  `OrderWay` varchar(300) DEFAULT '' COMMENT 'OrderWay',
  `SearchKey` varchar(300) DEFAULT '' COMMENT 'SearchKey',
  `MVals` varchar(2000) DEFAULT '' COMMENT 'MVals',
  `IsPic` int(11) DEFAULT '0' COMMENT '是否图片',
  `DTFrom` varchar(20) DEFAULT '' COMMENT '查询时间从',
  `DTTo` varchar(20) DEFAULT '' COMMENT '到',
  `AtPara` text COMMENT 'AtPara',
  `FK_MapData` varchar(100) DEFAULT '' COMMENT '实体',
  `AttrKey` varchar(50) DEFAULT '' COMMENT '节点对应字段',
  `LB` int(11) DEFAULT '0' COMMENT '类别',
  `CurValue` text COMMENT '文本',
  `ContrastKey` varchar(20) DEFAULT '' COMMENT '对比项目',
  `KeyVal1` varchar(20) DEFAULT '' COMMENT 'KeyVal1',
  `KeyVal2` varchar(20) DEFAULT '' COMMENT 'KeyVal2',
  `SortBy` varchar(20) DEFAULT '' COMMENT 'SortBy',
  `KeyOfNum` varchar(20) DEFAULT '' COMMENT 'KeyOfNum',
  `GroupWay` int(11) DEFAULT '1' COMMENT '求什么?SumAvg',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_userregedit
-- ----------------------------
INSERT INTO `sys_userregedit` VALUES ('adminND1RptAdminer_SearchAttrs', null, 'admin', null, 'ND1RptAdminer_SearchAttrs', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '0', null, '', '', '', '', '', '1');
INSERT INTO `sys_userregedit` VALUES ('adminND1RptMyDept_SearchAttrs', null, 'admin', null, 'ND1RptMyDept_SearchAttrs', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '0', null, '', '', '', '', '', '1');
INSERT INTO `sys_userregedit` VALUES ('adminND1RptMyJoin_SearchAttrs', null, 'admin', null, 'ND1RptMyJoin_SearchAttrs', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '0', null, '', '', '', '', '', '1');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.GPM.Depts_SearchAttrs', null, '', null, '', '', '', '', '', '', '', '', '', '0', '', '', '@RecCount=9', '', '', '0', null, '', '', '', '', '', '1');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.GPM.Emps_SearchAttrs', null, '', null, '', '', '', '', '', '', '', '', '', '0', '', '', '@RecCount=25', '', '', '0', null, '', '', '', '', '', '1');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.GPM.StationTypes_SearchAttrs', null, '', null, '', '', '', '', '', '', '', '', '', '0', '', '', '@RecCount=4', '', '', '0', null, '', '', '', '', '', '1');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.Port.Stations_SearchAttrs', null, '', null, '', '', '', '', '', '', '', '', '', '0', '', '', '@RecCount=4', '', '', '0', null, '', '', '', '', '', '1');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.Port.StationTypes_SearchAttrs', null, '', null, '', '', '', '', '', '', '', '', '', '0', '', '', '@RecCount=4', '', '', '0', null, '', '', '', '', '', '1');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.WF.Data.Delays_SearchAttrs', null, '', null, '', '', '', '', '', '', '', '', '', '0', '', '', '@RecCount=0', '', '', '0', null, '', '', '', '', '', '1');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.WF.Data.MyJoinFlows_SearchAttrs', null, '', null, '', '', '', '', '', '', '', '', '', '0', '', '', '@RecCount=0', '', '', '0', null, '', '', '', '', '', '1');
INSERT INTO `sys_userregedit` VALUES ('admin_BP.WF.Data.MyStartFlows_SearchAttrs', null, '', null, '', '', '', '', '', '', '', '', '', '0', '', '', '@RecCount=0', '', '', '0', null, '', '', '', '', '', '1');

-- ----------------------------
-- Table structure for sys_wfsealdata
-- ----------------------------
DROP TABLE IF EXISTS `sys_wfsealdata`;
CREATE TABLE `sys_wfsealdata` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `OID` varchar(200) DEFAULT NULL COMMENT 'OID',
  `FK_Node` varchar(200) DEFAULT NULL COMMENT 'FK_Node',
  `FK_MapData` varchar(100) DEFAULT NULL COMMENT 'FK_MapData',
  `SealData` text COMMENT 'SealData',
  `RDT` varchar(20) DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_wfsealdata
-- ----------------------------

-- ----------------------------
-- Table structure for wf_accepterrole
-- ----------------------------
DROP TABLE IF EXISTS `wf_accepterrole`;
CREATE TABLE `wf_accepterrole` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `Name` varchar(200) DEFAULT NULL,
  `FK_Node` varchar(100) DEFAULT NULL COMMENT '节点',
  `FK_Mode` int(11) DEFAULT '0' COMMENT '模式类型',
  `Tag0` varchar(999) DEFAULT NULL COMMENT 'Tag0',
  `Tag1` varchar(999) DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(999) DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(999) DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(999) DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(999) DEFAULT NULL COMMENT 'Tag5',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_accepterrole
-- ----------------------------

-- ----------------------------
-- Table structure for wf_auth
-- ----------------------------
DROP TABLE IF EXISTS `wf_auth`;
CREATE TABLE `wf_auth` (
  `MyPK` varchar(100) NOT NULL,
  `Auther` varchar(100) DEFAULT NULL,
  `AuthType` int(11) DEFAULT '0' COMMENT '类型(0=全部流程1=指定流程)',
  `EmpNo` varchar(100) DEFAULT NULL,
  `EmpName` varchar(100) DEFAULT NULL,
  `FlowNo` varchar(100) DEFAULT NULL,
  `FlowName` varchar(100) DEFAULT NULL,
  `TakeBackDT` varchar(50) DEFAULT NULL,
  `RDT` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_auth
-- ----------------------------

-- ----------------------------
-- Table structure for wf_bill
-- ----------------------------
DROP TABLE IF EXISTS `wf_bill`;
CREATE TABLE `wf_bill` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `WorkID` int(11) DEFAULT '0' COMMENT '工作ID',
  `FID` int(11) DEFAULT '0' COMMENT 'FID',
  `FK_Flow` varchar(4) DEFAULT NULL COMMENT '流程',
  `FK_BillType` varchar(300) DEFAULT NULL COMMENT '单据类型',
  `Title` varchar(900) DEFAULT NULL COMMENT '标题',
  `FK_Starter` varchar(50) DEFAULT NULL COMMENT '发起人',
  `StartDT` varchar(50) DEFAULT NULL COMMENT '发起时间',
  `Url` varchar(2000) DEFAULT NULL COMMENT 'Url',
  `FullPath` varchar(2000) DEFAULT NULL COMMENT 'FullPath',
  `FK_Emp` varchar(100) DEFAULT NULL COMMENT '打印人',
  `RDT` varchar(50) DEFAULT NULL COMMENT '打印时间',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '隶属部门',
  `FK_NY` varchar(100) DEFAULT NULL COMMENT '隶属年月',
  `Emps` text COMMENT 'Emps',
  `FK_Node` varchar(30) DEFAULT NULL COMMENT '节点',
  `FK_Bill` varchar(500) DEFAULT NULL COMMENT 'FK_Bill',
  `MyNum` int(11) DEFAULT '1' COMMENT '个数',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_bill
-- ----------------------------

-- ----------------------------
-- Table structure for wf_billtemplate
-- ----------------------------
DROP TABLE IF EXISTS `wf_billtemplate`;
CREATE TABLE `wf_billtemplate` (
  `No` varchar(190) NOT NULL COMMENT 'No',
  `Name` varchar(200) DEFAULT NULL COMMENT 'Name',
  `TempFilePath` varchar(200) DEFAULT NULL COMMENT '模板路径',
  `NodeID` int(11) DEFAULT '0' COMMENT 'NodeID',
  `BillFileType` int(11) DEFAULT '0' COMMENT '生成的文件类型',
  `BillOpenModel` int(11) DEFAULT '0' COMMENT '生成的文件打开方式',
  `QRModel` int(11) DEFAULT '0' COMMENT '二维码生成方式',
  `FK_BillType` varchar(4) DEFAULT NULL COMMENT '单据类型',
  `IDX` varchar(200) DEFAULT NULL COMMENT 'IDX',
  `ExpField` varchar(800) DEFAULT NULL COMMENT '要排除的字段',
  `ReplaceVal` varchar(3000) DEFAULT NULL COMMENT '要替换的值',
  `FK_MapData` varchar(300) DEFAULT '',
  `TemplateFileModel` int(11) DEFAULT '0',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_billtemplate
-- ----------------------------
INSERT INTO `wf_billtemplate` VALUES ('100', 'Jflow (5)', 'Jflow (5).doc', '301', '0', '0', '0', '', '', '', '', 'ND301', '1');

-- ----------------------------
-- Table structure for wf_billtype
-- ----------------------------
DROP TABLE IF EXISTS `wf_billtype`;
CREATE TABLE `wf_billtype` (
  `No` varchar(2) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `FK_Flow` varchar(50) DEFAULT NULL COMMENT '流程',
  `IDX` int(11) DEFAULT '0' COMMENT 'IDX',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_billtype
-- ----------------------------

-- ----------------------------
-- Table structure for wf_ccdept
-- ----------------------------
DROP TABLE IF EXISTS `wf_ccdept`;
CREATE TABLE `wf_ccdept` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门',
  PRIMARY KEY (`FK_Node`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_ccdept
-- ----------------------------

-- ----------------------------
-- Table structure for wf_ccemp
-- ----------------------------
DROP TABLE IF EXISTS `wf_ccemp`;
CREATE TABLE `wf_ccemp` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员',
  PRIMARY KEY (`FK_Node`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_ccemp
-- ----------------------------

-- ----------------------------
-- Table structure for wf_cclist
-- ----------------------------
DROP TABLE IF EXISTS `wf_cclist`;
CREATE TABLE `wf_cclist` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `Title` varchar(500) DEFAULT NULL COMMENT '标题',
  `Sta` int(11) DEFAULT '0' COMMENT '状态',
  `FK_Flow` varchar(3) DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(200) DEFAULT NULL COMMENT '流程名称',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `NodeName` varchar(500) DEFAULT NULL COMMENT '节点名称',
  `WorkID` int(11) DEFAULT '0' COMMENT '工作ID',
  `FID` int(11) DEFAULT '0' COMMENT 'FID',
  `Doc` text COMMENT '内容',
  `Rec` varchar(50) DEFAULT NULL COMMENT '抄送人员',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `CCTo` varchar(50) DEFAULT NULL COMMENT '抄送给',
  `CCToName` varchar(50) DEFAULT NULL COMMENT '抄送给(人员名称)',
  `CCToDept` varchar(50) DEFAULT NULL COMMENT '抄送到部门',
  `CCToDeptName` varchar(600) DEFAULT NULL COMMENT '抄送给部门名称',
  `CDT` varchar(50) DEFAULT NULL COMMENT '打开时间',
  `PFlowNo` varchar(100) DEFAULT NULL COMMENT '父流程编号',
  `PWorkID` int(11) DEFAULT '0' COMMENT '父流程WorkID',
  `InEmpWorks` int(11) DEFAULT '0' COMMENT '是否加入待办列表',
  `CCToOrgNo` varchar(50) DEFAULT '' COMMENT '抄送到组织',
  `CCToOrgName` varchar(600) DEFAULT '' COMMENT '抄送给组织名称',
  `Domain` varchar(50) DEFAULT '' COMMENT 'Domain',
  `OrgNo` varchar(50) DEFAULT '' COMMENT 'OrgNo',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_cclist
-- ----------------------------

-- ----------------------------
-- Table structure for wf_ccstation
-- ----------------------------
DROP TABLE IF EXISTS `wf_ccstation`;
CREATE TABLE `wf_ccstation` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `FK_Station` varchar(100) NOT NULL COMMENT '工作岗位',
  PRIMARY KEY (`FK_Node`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_ccstation
-- ----------------------------

-- ----------------------------
-- Table structure for wf_ch
-- ----------------------------
DROP TABLE IF EXISTS `wf_ch`;
CREATE TABLE `wf_ch` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `WorkID` int(11) DEFAULT '0' COMMENT '工作ID',
  `FID` int(11) DEFAULT '0' COMMENT 'FID',
  `Title` varchar(900) DEFAULT NULL COMMENT '标题',
  `FK_Flow` varchar(100) DEFAULT NULL,
  `FK_FlowT` varchar(200) DEFAULT NULL COMMENT '流程名称',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `FK_NodeT` varchar(200) DEFAULT NULL COMMENT '节点名称',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人',
  `SenderT` varchar(200) DEFAULT NULL COMMENT '发送人名称',
  `FK_Emp` varchar(100) DEFAULT NULL,
  `FK_EmpT` varchar(200) DEFAULT NULL COMMENT '当事人名称',
  `GroupEmps` varchar(400) DEFAULT NULL COMMENT '相关当事人',
  `GroupEmpsNames` varchar(900) DEFAULT NULL COMMENT '相关当事人名称',
  `GroupEmpsNum` int(11) DEFAULT '1' COMMENT '相关当事人数量',
  `DTFrom` varchar(50) DEFAULT NULL COMMENT '任务下达时间',
  `DTTo` varchar(50) DEFAULT NULL COMMENT '任务处理时间',
  `SDT` varchar(50) DEFAULT NULL COMMENT '应完成日期',
  `FK_Dept` varchar(100) DEFAULT NULL,
  `FK_DeptT` varchar(500) DEFAULT NULL COMMENT '部门名称',
  `FK_NY` varchar(100) DEFAULT NULL,
  `DTSWay` int(11) DEFAULT '0' COMMENT '考核方式',
  `TimeLimit` varchar(50) DEFAULT NULL COMMENT '规定限期',
  `OverMinutes` float DEFAULT NULL COMMENT '逾期分钟',
  `UseDays` float DEFAULT NULL COMMENT '实际使用天',
  `OverDays` float DEFAULT NULL COMMENT '逾期天',
  `CHSta` int(11) DEFAULT '0' COMMENT '状态',
  `WeekNum` int(11) DEFAULT '0' COMMENT '第几周',
  `Points` float DEFAULT NULL COMMENT '总扣分',
  `MyNum` int(11) DEFAULT '1' COMMENT '个数',
  `UseMinutes` float(50,2) DEFAULT '0.00',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_ch
-- ----------------------------

-- ----------------------------
-- Table structure for wf_cheval
-- ----------------------------
DROP TABLE IF EXISTS `wf_cheval`;
CREATE TABLE `wf_cheval` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `Title` varchar(500) DEFAULT NULL COMMENT '标题',
  `FK_Flow` varchar(7) DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `WorkID` int(11) DEFAULT '0' COMMENT '工作ID',
  `FK_Node` int(11) DEFAULT '0' COMMENT '评价节点',
  `NodeName` varchar(100) DEFAULT NULL COMMENT '节点名称',
  `Rec` varchar(50) DEFAULT NULL COMMENT '评价人',
  `RecName` varchar(50) DEFAULT NULL COMMENT '评价人名称',
  `RDT` varchar(50) DEFAULT NULL COMMENT '评价日期',
  `EvalEmpNo` varchar(50) DEFAULT NULL COMMENT '被考核的人员编号',
  `EvalEmpName` varchar(50) DEFAULT NULL COMMENT '被考核的人员名称',
  `EvalCent` varchar(20) DEFAULT NULL COMMENT '评价分值',
  `EvalNote` varchar(20) DEFAULT NULL COMMENT '评价内容',
  `FK_Dept` varchar(50) DEFAULT NULL COMMENT '部门',
  `DeptName` varchar(100) DEFAULT NULL COMMENT '部门名称',
  `FK_NY` varchar(7) DEFAULT NULL COMMENT '年月',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_cheval
-- ----------------------------

-- ----------------------------
-- Table structure for wf_cond
-- ----------------------------
DROP TABLE IF EXISTS `wf_cond`;
CREATE TABLE `wf_cond` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `CondType` int(11) DEFAULT '0' COMMENT '条件类型',
  `DataFrom` int(11) DEFAULT '0' COMMENT '条件数据来源0表单,1岗位(对方向条件有效)',
  `FK_Flow` varchar(60) DEFAULT NULL COMMENT '流程',
  `NodeID` int(11) DEFAULT '0' COMMENT '发生的事件MainNode',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点ID',
  `FK_Attr` varchar(80) DEFAULT NULL COMMENT '属性',
  `AttrKey` varchar(60) DEFAULT NULL COMMENT '属性键',
  `AttrName` varchar(500) DEFAULT NULL COMMENT '中文名称',
  `FK_Operator` varchar(60) DEFAULT NULL COMMENT '运算符号',
  `OperatorValue` text COMMENT '要运算的值',
  `OperatorValueT` text COMMENT '要运算的值T',
  `ToNodeID` int(11) DEFAULT '0' COMMENT 'ToNodeID（对方向条件有效）',
  `ConnJudgeWay` int(11) DEFAULT '0' COMMENT '条件关系',
  `MyPOID` int(11) DEFAULT '0' COMMENT 'MyPOID',
  `PRI` int(11) DEFAULT '0' COMMENT '计算优先级',
  `CondOrAnd` int(11) DEFAULT '0' COMMENT '方向条件类型',
  `Note` varchar(500) DEFAULT NULL COMMENT '备注',
  `AtPara` varchar(2000) DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_cond
-- ----------------------------

-- ----------------------------
-- Table structure for wf_dataapply
-- ----------------------------
DROP TABLE IF EXISTS `wf_dataapply`;
CREATE TABLE `wf_dataapply` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `NodeId` int(11) DEFAULT NULL COMMENT 'NodeId',
  `RunState` int(11) DEFAULT NULL COMMENT '运行状态0,没有提交，1，提交申请执行审批中，2，审核完毕。',
  `ApplyDays` int(11) DEFAULT NULL COMMENT '申请天数',
  `ApplyData` varchar(50) DEFAULT NULL COMMENT '申请日期',
  `Applyer` varchar(100) DEFAULT NULL COMMENT '申请人,外键:对应物理表:Port_Emp,表描述:用户',
  `ApplyNote1` text COMMENT '申请原因',
  `ApplyNote2` text COMMENT '申请备注',
  `Checker` varchar(100) DEFAULT NULL COMMENT '审批人,外键:对应物理表:Port_Emp,表描述:用户',
  `CheckerData` varchar(50) DEFAULT NULL COMMENT '审批日期',
  `CheckerDays` int(11) DEFAULT NULL COMMENT '批准天数',
  `CheckerNote1` text COMMENT '审批意见',
  `CheckerNote2` text COMMENT '审批备注',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='追加时间申请';

-- ----------------------------
-- Records of wf_dataapply
-- ----------------------------

-- ----------------------------
-- Table structure for wf_deptflowsearch
-- ----------------------------
DROP TABLE IF EXISTS `wf_deptflowsearch`;
CREATE TABLE `wf_deptflowsearch` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Emp` varchar(50) DEFAULT NULL COMMENT '操作员',
  `FK_Flow` varchar(50) DEFAULT NULL COMMENT '流程编号',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门编号',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_deptflowsearch
-- ----------------------------

-- ----------------------------
-- Table structure for wf_direction
-- ----------------------------
DROP TABLE IF EXISTS `wf_direction`;
CREATE TABLE `wf_direction` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Flow` varchar(10) DEFAULT NULL COMMENT '流程',
  `Node` int(11) DEFAULT '0' COMMENT '从节点',
  `ToNode` int(11) DEFAULT '0' COMMENT '到节点',
  `IsCanBack` int(11) DEFAULT '0' COMMENT '是否可以原路返回(对后退线有效)',
  `Dots` varchar(300) DEFAULT NULL COMMENT '轨迹信息',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_direction
-- ----------------------------

-- ----------------------------
-- Table structure for wf_directionstation
-- ----------------------------
DROP TABLE IF EXISTS `wf_directionstation`;
CREATE TABLE `wf_directionstation` (
  `FK_Direction` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `FK_Station` varchar(100) NOT NULL COMMENT '工作岗位',
  PRIMARY KEY (`FK_Direction`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_directionstation
-- ----------------------------

-- ----------------------------
-- Table structure for wf_emp
-- ----------------------------
DROP TABLE IF EXISTS `wf_emp`;
CREATE TABLE `wf_emp` (
  `No` varchar(50) NOT NULL COMMENT '帐号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '主部门',
  `OrgNo` varchar(100) DEFAULT NULL COMMENT '组织',
  `UseSta` int(11) DEFAULT '3' COMMENT '用户状态',
  `MyNum` int(11) DEFAULT '1' COMMENT '个数',
  `UserType` int(11) DEFAULT '3' COMMENT '用户状态',
  `RootOfFlow` varchar(100) DEFAULT '' COMMENT '流程权限节点',
  `RootOfForm` varchar(100) DEFAULT '' COMMENT '表单权限节点',
  `RootOfDept` varchar(100) DEFAULT '' COMMENT '组织结构权限节点',
  `Tel` varchar(50) DEFAULT '' COMMENT 'Tel',
  `Email` varchar(50) DEFAULT '' COMMENT 'Email',
  `TM` varchar(50) DEFAULT '' COMMENT '即时通讯号',
  `AlertWay` int(11) DEFAULT '3' COMMENT '收听方式',
  `Author` varchar(50) DEFAULT '' COMMENT '授权人',
  `AuthorDate` varchar(50) DEFAULT '' COMMENT '授权日期',
  `AuthorWay` int(11) DEFAULT '0' COMMENT '授权方式',
  `AuthorToDate` varchar(50) DEFAULT NULL,
  `AuthorFlows` text COMMENT '可以执行的授权流程',
  `Stas` varchar(3000) DEFAULT '' COMMENT '岗位s',
  `Depts` varchar(100) DEFAULT '' COMMENT 'Deptss',
  `FtpUrl` varchar(50) DEFAULT '' COMMENT 'FtpUrl',
  `Msg` text COMMENT 'Msg',
  `Style` text COMMENT 'Style',
  `StartFlows` longtext,
  `SPass` varchar(200) DEFAULT '' COMMENT '图片签名密码',
  `Idx` int(11) DEFAULT '0' COMMENT 'Idx',
  `AtPara` text,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_emp
-- ----------------------------
INSERT INTO `wf_emp` VALUES ('admin', '系统管理员', 'SD', null, '1', '1', '3', '', '', '', '82374939-601', 'admin@ccflow.org', '', '3', '', '', '0', '', '', '', '', '', '', '', '', '', '0', null);

-- ----------------------------
-- Table structure for wf_findworkerrole
-- ----------------------------
DROP TABLE IF EXISTS `wf_findworkerrole`;
CREATE TABLE `wf_findworkerrole` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `Name` varchar(200) DEFAULT NULL COMMENT 'Name',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点ID',
  `SortVal0` varchar(200) DEFAULT NULL COMMENT 'SortVal0',
  `SortText0` varchar(200) DEFAULT NULL COMMENT 'SortText0',
  `SortVal1` varchar(200) DEFAULT NULL COMMENT 'SortVal1',
  `SortText1` varchar(200) DEFAULT NULL COMMENT 'SortText1',
  `SortVal2` varchar(200) DEFAULT NULL COMMENT 'SortText2',
  `SortText2` varchar(200) DEFAULT NULL COMMENT 'SortText2',
  `SortVal3` varchar(200) DEFAULT NULL COMMENT 'SortVal3',
  `SortText3` varchar(200) DEFAULT NULL COMMENT 'SortText3',
  `TagVal0` varchar(1000) DEFAULT NULL COMMENT 'TagVal0',
  `TagVal1` varchar(1000) DEFAULT NULL COMMENT 'TagVal1',
  `TagVal2` varchar(1000) DEFAULT NULL COMMENT 'TagVal2',
  `TagVal3` varchar(1000) DEFAULT NULL COMMENT 'TagVal3',
  `TagText0` varchar(1000) DEFAULT NULL COMMENT 'TagText0',
  `TagText1` varchar(1000) DEFAULT NULL COMMENT 'TagText1',
  `TagText2` varchar(1000) DEFAULT NULL COMMENT 'TagText2',
  `TagText3` varchar(1000) DEFAULT NULL COMMENT 'TagText3',
  `IsEnable` int(11) DEFAULT '1' COMMENT '是否可用',
  `Idx` int(11) DEFAULT '0' COMMENT 'IDX',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_findworkerrole
-- ----------------------------

-- ----------------------------
-- Table structure for wf_flow
-- ----------------------------
DROP TABLE IF EXISTS `wf_flow`;
CREATE TABLE `wf_flow` (
  `No` varchar(200) NOT NULL DEFAULT '',
  `FK_FlowSort` varchar(100) DEFAULT NULL COMMENT '流程类别',
  `Name` varchar(500) DEFAULT NULL,
  `FlowMark` varchar(150) DEFAULT NULL COMMENT '流程标记',
  `FlowEventEntity` varchar(150) DEFAULT NULL COMMENT '流程事件实体',
  `TitleRole` varchar(150) DEFAULT NULL COMMENT '标题生成规则',
  `IsCanStart` int(11) DEFAULT '1' COMMENT '可以独立启动否？(独立启动的流程可以显示在发起流程列表里)',
  `IsFullSA` int(11) DEFAULT '0' COMMENT '是否自动计算未来的处理人？',
  `IsAutoSendSubFlowOver` int(11) DEFAULT '0' COMMENT '为子流程时结束规则',
  `IsGuestFlow` int(11) DEFAULT '0' COMMENT '是否外部用户参与流程(非组织结构人员参与的流程)',
  `FlowAppType` int(11) DEFAULT '0' COMMENT '流程应用类型',
  `TimelineRole` int(11) DEFAULT '0' COMMENT '时效性规则',
  `Draft` int(11) DEFAULT '0' COMMENT '草稿规则',
  `FlowDeleteRole` int(11) DEFAULT '0' COMMENT '流程实例删除规则',
  `HelpUrl` varchar(300) DEFAULT NULL COMMENT '帮助文档',
  `SysType` varchar(100) DEFAULT NULL COMMENT '系统类型',
  `Tester` varchar(300) DEFAULT NULL COMMENT '发起测试人',
  `NodeAppType` varchar(50) DEFAULT NULL COMMENT '业务类型枚举(可为Null)',
  `NodeAppTypeText` varchar(50) DEFAULT NULL COMMENT '业务类型枚举(可为Null)',
  `ChartType` int(11) DEFAULT '1' COMMENT '节点图形类型',
  `IsBatchStart` int(11) DEFAULT '0' COMMENT '是否可以批量发起流程？(如果是就要设置发起的需要填写的字段,多个用逗号分开)',
  `BatchStartFields` varchar(500) DEFAULT NULL COMMENT '发起字段s',
  `HistoryFields` varchar(500) DEFAULT NULL COMMENT '历史查看字段',
  `IsResetData` int(11) DEFAULT '0' COMMENT '是否启用开始节点数据重置按钮？',
  `IsLoadPriData` int(11) DEFAULT '0' COMMENT '是否自动装载上一笔数据？',
  `IsDBTemplate` int(11) DEFAULT '1' COMMENT '是否启用数据模版？',
  `IsStartInMobile` int(11) DEFAULT '1' COMMENT '是否可以在手机里启用？(如果发起表单特别复杂就不要在手机里启用了)',
  `IsMD5` int(11) DEFAULT '0' COMMENT '是否是数据加密流程(MD5数据加密防篡改)',
  `DataStoreModel` int(11) DEFAULT '0' COMMENT '数据存储',
  `PTable` varchar(30) DEFAULT NULL COMMENT '流程数据存储表',
  `FlowNoteExp` varchar(500) DEFAULT NULL COMMENT '备注的表达式',
  `BillNoFormat` varchar(50) DEFAULT NULL COMMENT '单据编号格式',
  `DesignerNo` varchar(50) DEFAULT NULL COMMENT '设计者编号',
  `DesignerName` varchar(50) DEFAULT NULL COMMENT '设计者名称',
  `Note` text COMMENT '流程描述',
  `FlowRunWay` int(11) DEFAULT '0' COMMENT '运行方式',
  `RunObj` varchar(3000) DEFAULT NULL,
  `RunSQL` varchar(2000) DEFAULT '' COMMENT '流程结束执行后执行的SQL',
  `NumOfBill` int(11) DEFAULT '0' COMMENT '是否有单据',
  `NumOfDtl` int(11) DEFAULT '0' COMMENT 'NumOfDtl',
  `AvgDay` float(11,2) DEFAULT '0.00' COMMENT '平均运行用天',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序号(在发起列表中)',
  `Paras` varchar(2000) DEFAULT '' COMMENT '参数',
  `DRCtrlType` int(11) DEFAULT '0' COMMENT '部门查询权限控制方式',
  `StartLimitRole` int(11) DEFAULT '0' COMMENT '启动限制规则',
  `StartLimitPara` varchar(500) DEFAULT '' COMMENT '规则内容',
  `StartLimitAlert` varchar(500) DEFAULT '' COMMENT '限制提示',
  `StartLimitWhen` int(11) DEFAULT '0' COMMENT '提示时间',
  `StartGuideWay` int(11) DEFAULT '0' COMMENT '前置导航方式',
  `StartGuideLink` varchar(200) DEFAULT '' COMMENT '右侧的连接',
  `StartGuideLab` varchar(200) DEFAULT '' COMMENT '连接标签',
  `StartGuidePara1` varchar(500) DEFAULT '' COMMENT '参数1',
  `StartGuidePara2` varchar(500) DEFAULT '' COMMENT '参数2',
  `StartGuidePara3` varchar(500) DEFAULT '' COMMENT '参数3',
  `Ver` varchar(20) DEFAULT '' COMMENT '版本号',
  `DType` int(11) DEFAULT '0' COMMENT '设计类型0=ccbpm,1=bpmn',
  `AtPara` varchar(1000) DEFAULT '' COMMENT 'AtPara',
  `DTSWay` int(11) DEFAULT '0' COMMENT '同步方式',
  `DTSDBSrc` varchar(200) DEFAULT '' COMMENT '数据源',
  `DTSBTable` varchar(200) DEFAULT '' COMMENT '业务表名',
  `DTSBTablePK` varchar(32) DEFAULT '' COMMENT '业务表主键',
  `DTSTime` int(11) DEFAULT '0' COMMENT '执行同步时间点',
  `DTSSpecNodes` varchar(200) DEFAULT '' COMMENT '指定的节点ID',
  `DTSField` int(11) DEFAULT '0' COMMENT '要同步的字段计算方式',
  `DTSFields` varchar(2000) DEFAULT '' COMMENT '要同步的字段s,中间用逗号分开.',
  `MyDeptRole` int(11) DEFAULT '0' COMMENT '本部门发起的流程',
  `PStarter` int(11) DEFAULT '1' COMMENT '发起人可看(必选)',
  `PWorker` int(11) DEFAULT '1' COMMENT '参与人可看(必选)',
  `PCCer` int(11) DEFAULT '1' COMMENT '被抄送人可看(必选)',
  `PMyDept` int(11) DEFAULT '1' COMMENT '本部门人可看',
  `PPMyDept` int(11) DEFAULT '1' COMMENT '直属上级部门可看(比如:我是)',
  `PPDept` int(11) DEFAULT '1' COMMENT '上级部门可看',
  `PSameDept` int(11) DEFAULT '1' COMMENT '平级部门可看',
  `PSpecDept` int(11) DEFAULT '1' COMMENT '指定部门可看',
  `PSpecDeptExt` varchar(200) DEFAULT '' COMMENT '部门编号',
  `PSpecSta` int(11) DEFAULT '1' COMMENT '指定的岗位可看',
  `PSpecStaExt` varchar(200) DEFAULT '' COMMENT '岗位编号',
  `PSpecGroup` int(11) DEFAULT '1' COMMENT '指定的权限组可看',
  `PSpecGroupExt` varchar(200) DEFAULT '' COMMENT '权限组',
  `PSpecEmp` int(11) DEFAULT '1' COMMENT '指定的人员可看',
  `PSpecEmpExt` varchar(200) DEFAULT '' COMMENT '指定的人员编号',
  `HostRun` varchar(40) DEFAULT '',
  `IsFrmEnable` int(11) DEFAULT '1' COMMENT '是否显示表单',
  `IsTruckEnable` int(11) DEFAULT '1' COMMENT '是否显示轨迹图',
  `IsTimeBaseEnable` int(11) DEFAULT '1' COMMENT '是否显示时间轴',
  `IsTableEnable` int(11) DEFAULT '1' COMMENT '是否显示时间表',
  `IsOPEnable` int(11) DEFAULT '1' COMMENT '是否显示操作',
  `SDTOfFlowRole` int(11) DEFAULT '0' COMMENT '流程计划完成日期计算规则',
  `SDTOfFlowRoleSQL` varchar(200) DEFAULT '' COMMENT '流程计划完成日期计算规则SQL',
  `FlowFrmType` int(11) DEFAULT '0' COMMENT '流程表单类型',
  `FrmUrl` varchar(150) DEFAULT '' COMMENT '表单Url',
  `IsToParentNextNode` int(11) DEFAULT '0' COMMENT '子流程运行到该节点时，让父流程自动运行到下一步',
  `OrgNo` varchar(50) DEFAULT '' COMMENT 'OrgNo',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_flow
-- ----------------------------

-- ----------------------------
-- Table structure for wf_flowemp
-- ----------------------------
DROP TABLE IF EXISTS `wf_flowemp`;
CREATE TABLE `wf_flowemp` (
  `FK_Flow` varchar(100) NOT NULL COMMENT 'FK_Flow,主外键:对应物理表:WF_Flow,表描述:流程',
  `FK_Emp` varchar(100) NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Flow`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程岗位属性信息';

-- ----------------------------
-- Records of wf_flowemp
-- ----------------------------

-- ----------------------------
-- Table structure for wf_flowformtree
-- ----------------------------
DROP TABLE IF EXISTS `wf_flowformtree`;
CREATE TABLE `wf_flowformtree` (
  `No` varchar(10) NOT NULL COMMENT '编号',
  `Name` varchar(100) DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点No',
  `Idx` int(11) DEFAULT '0' COMMENT 'Idx',
  `FK_Flow` varchar(20) DEFAULT NULL COMMENT '流程编号',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_flowformtree
-- ----------------------------

-- ----------------------------
-- Table structure for wf_flownode
-- ----------------------------
DROP TABLE IF EXISTS `wf_flownode`;
CREATE TABLE `wf_flownode` (
  `FK_Flow` varchar(20) NOT NULL COMMENT '流程编号 - 主键',
  `FK_Node` varchar(20) NOT NULL COMMENT '节点 - 主键',
  PRIMARY KEY (`FK_Flow`,`FK_Node`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程抄送节点';

-- ----------------------------
-- Records of wf_flownode
-- ----------------------------

-- ----------------------------
-- Table structure for wf_floworg
-- ----------------------------
DROP TABLE IF EXISTS `wf_floworg`;
CREATE TABLE `wf_floworg` (
  `FlowNo` varchar(100) NOT NULL,
  `OrgNo` varchar(100) NOT NULL,
  PRIMARY KEY (`FlowNo`,`OrgNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_floworg
-- ----------------------------

-- ----------------------------
-- Table structure for wf_flowsort
-- ----------------------------
DROP TABLE IF EXISTS `wf_flowsort`;
CREATE TABLE `wf_flowsort` (
  `No` varchar(100) NOT NULL COMMENT '编号',
  `Name` varchar(200) DEFAULT NULL,
  `ParentNo` varchar(100) DEFAULT NULL COMMENT '父节点No',
  `OrgNo` varchar(150) DEFAULT NULL,
  `Idx` int(11) DEFAULT '0' COMMENT 'Idx',
  `Domain` varchar(100) DEFAULT '',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_flowsort
-- ----------------------------
INSERT INTO `wf_flowsort` VALUES ('01.', '线性流程', '99', '0', '0', '');
INSERT INTO `wf_flowsort` VALUES ('02.', '同表单分合流', '99', '0', '0', '');
INSERT INTO `wf_flowsort` VALUES ('03.', '异表单分合流', '99', '0', '0', '');
INSERT INTO `wf_flowsort` VALUES ('04.', '父子流程', '99', '0', '0', '');
INSERT INTO `wf_flowsort` VALUES ('99', '流程树', '0', '0', '0', '');

-- ----------------------------
-- Table structure for wf_frmnode
-- ----------------------------
DROP TABLE IF EXISTS `wf_frmnode`;
CREATE TABLE `wf_frmnode` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Frm` varchar(200) DEFAULT NULL,
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点编号',
  `FK_Flow` varchar(20) DEFAULT NULL COMMENT '流程编号',
  `OfficeOpenLab` varchar(50) DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOverLab` varchar(50) DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeOverEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeMarksEnable` int(11) DEFAULT '1' COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) DEFAULT NULL COMMENT '打印按钮标签',
  `OfficePrintEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSealLab` varchar(50) DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeSealEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeNodeInfo` int(11) DEFAULT '0' COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) DEFAULT '0' COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeIsMarks` int(11) DEFAULT '1' COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) DEFAULT '1' COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) DEFAULT '0' COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) DEFAULT NULL COMMENT '自动套红模板',
  `FrmSln` int(11) DEFAULT '0' COMMENT '表单控制方案',
  `FrmType` varchar(20) DEFAULT '0' COMMENT '表单类型',
  `IsPrint` int(11) DEFAULT '0' COMMENT '是否可以打印',
  `IsEnableLoadData` int(11) DEFAULT '0' COMMENT '是否启用装载填充事件',
  `IsDefaultOpen` int(11) DEFAULT '0' COMMENT '是否默认打开',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序号',
  `WhoIsPK` int(11) DEFAULT '0' COMMENT '谁是主键？',
  `Is1ToN` int(11) DEFAULT '0' COMMENT '是否1变N？',
  `HuiZong` varchar(300) DEFAULT '' COMMENT '子线程要汇总的数据表',
  `FrmEnableRole` int(11) DEFAULT '0' COMMENT '表单启用规则',
  `FrmEnableExp` varchar(4000) DEFAULT NULL,
  `TempleteFile` varchar(500) DEFAULT '' COMMENT '模版文件',
  `IsEnable` int(11) DEFAULT '1' COMMENT '是否显示',
  `GuanJianZiDuan` varchar(20) DEFAULT '' COMMENT '关键字段',
  `IsEnableFWC` int(11) DEFAULT '0',
  `IsCloseEtcFrm` int(11) DEFAULT '0' COMMENT '打开时是否关闭其它的页面？',
  `FrmNameShow` varchar(100) DEFAULT '' COMMENT '表单显示名字',
  `CheckField` varchar(50) DEFAULT '' COMMENT '签批字段',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_frmnode
-- ----------------------------

-- ----------------------------
-- Table structure for wf_generworkerlist
-- ----------------------------
DROP TABLE IF EXISTS `wf_generworkerlist`;
CREATE TABLE `wf_generworkerlist` (
  `WorkID` int(11) NOT NULL DEFAULT '0' COMMENT '工作ID',
  `FK_Emp` varchar(20) NOT NULL COMMENT '人员',
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点ID',
  `FID` int(11) DEFAULT '0' COMMENT '流程ID',
  `FK_EmpText` varchar(30) DEFAULT NULL COMMENT '人员名称',
  `FK_NodeText` varchar(100) DEFAULT NULL COMMENT '节点名称',
  `FK_Flow` varchar(3) DEFAULT NULL COMMENT '流程',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '使用部门',
  `SDT` varchar(50) DEFAULT NULL COMMENT '应完成日期',
  `DTOfWarning` varchar(50) DEFAULT NULL COMMENT '警告日期',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录时间',
  `CDT` varchar(50) DEFAULT NULL COMMENT '完成时间',
  `IsEnable` int(11) DEFAULT '1' COMMENT '是否可用',
  `IsRead` int(11) DEFAULT '0' COMMENT '是否读取',
  `IsPass` int(11) DEFAULT '0' COMMENT '是否通过(对合流节点有效)',
  `WhoExeIt` int(11) DEFAULT '0' COMMENT '谁执行它',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人',
  `PRI` int(11) DEFAULT '1' COMMENT '优先级',
  `PressTimes` int(11) DEFAULT '0' COMMENT '催办次数',
  `DTOfHungUp` varchar(50) DEFAULT NULL COMMENT '挂起时间',
  `DTOfUnHungUp` varchar(50) DEFAULT NULL COMMENT '预计解除挂起时间',
  `HungUpTimes` int(11) DEFAULT '0' COMMENT '挂起次数',
  `GuestNo` varchar(30) DEFAULT NULL COMMENT '外部用户编号',
  `GuestName` varchar(100) DEFAULT NULL COMMENT '外部用户名称',
  `AtPara` text COMMENT 'AtPara',
  PRIMARY KEY (`WorkID`,`FK_Emp`,`FK_Node`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_generworkerlist
-- ----------------------------

-- ----------------------------
-- Table structure for wf_generworkflow
-- ----------------------------
DROP TABLE IF EXISTS `wf_generworkflow`;
CREATE TABLE `wf_generworkflow` (
  `WorkID` int(11) NOT NULL DEFAULT '0' COMMENT 'WorkID',
  `FID` int(11) DEFAULT '0' COMMENT '流程ID',
  `FK_FlowSort` varchar(100) DEFAULT NULL,
  `SysType` varchar(10) DEFAULT NULL COMMENT '系统类别',
  `FK_Flow` varchar(100) DEFAULT NULL,
  `FlowName` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `Title` varchar(1000) DEFAULT NULL COMMENT '标题',
  `WFSta` int(11) DEFAULT '0' COMMENT '状态',
  `WFState` int(11) DEFAULT '0' COMMENT '流程状态',
  `Starter` varchar(200) DEFAULT NULL COMMENT '发起人',
  `StarterName` varchar(200) DEFAULT NULL COMMENT '发起人名称',
  `Sender` varchar(200) DEFAULT NULL COMMENT '发送人',
  `RDT` varchar(50) DEFAULT NULL COMMENT '记录日期',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `NodeName` varchar(100) DEFAULT NULL COMMENT '节点名称',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门',
  `DeptName` varchar(100) DEFAULT NULL COMMENT '部门名称',
  `PRI` int(11) DEFAULT '1' COMMENT '优先级',
  `SDTOfNode` varchar(50) DEFAULT NULL COMMENT '节点应完成时间',
  `SDTOfFlow` varchar(50) DEFAULT NULL COMMENT '流程应完成时间',
  `PFlowNo` varchar(3) DEFAULT NULL COMMENT '父流程编号',
  `PWorkID` int(11) DEFAULT '0' COMMENT '父流程ID',
  `PNodeID` int(11) DEFAULT '0' COMMENT '父流程调用节点',
  `PFID` int(11) DEFAULT '0' COMMENT '父流程调用的PFID',
  `PEmp` varchar(32) DEFAULT NULL COMMENT '子流程的调用人',
  `GuestNo` varchar(100) DEFAULT NULL COMMENT '客户编号',
  `GuestName` varchar(100) DEFAULT NULL COMMENT '客户名称',
  `BillNo` varchar(100) DEFAULT NULL COMMENT '单据编号',
  `FlowNote` text COMMENT '备注',
  `TodoEmps` text COMMENT '待办人员',
  `TodoEmpsNum` int(11) DEFAULT '0' COMMENT '待办人员数量',
  `TaskSta` int(11) DEFAULT '0' COMMENT '共享状态',
  `AtPara` varchar(2000) DEFAULT NULL COMMENT '参数(流程运行设置临时存储的参数)',
  `Emps` text COMMENT '参与人',
  `GUID` varchar(36) DEFAULT NULL COMMENT 'GUID',
  `FK_NY` varchar(100) DEFAULT NULL,
  `WeekNum` int(11) DEFAULT '0' COMMENT '周次',
  `TSpan` int(11) DEFAULT '0' COMMENT '时间间隔',
  `TodoSta` int(11) DEFAULT '0' COMMENT '待办状态',
  `MyNum` int(11) DEFAULT '1' COMMENT '个数',
  `SendDT` varchar(50) DEFAULT NULL,
  `Domain` varchar(100) DEFAULT '',
  `PrjNo` varchar(100) DEFAULT '',
  `PrjName` varchar(100) DEFAULT '',
  `SDTOfFlowWarning` varchar(50) DEFAULT NULL,
  `OrgNo` varchar(50) DEFAULT '' COMMENT 'OrgNo',
  PRIMARY KEY (`WorkID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_generworkflow
-- ----------------------------

-- ----------------------------
-- Table structure for wf_hungup
-- ----------------------------
DROP TABLE IF EXISTS `wf_hungup`;
CREATE TABLE `wf_hungup` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点ID',
  `WorkID` int(11) DEFAULT '0' COMMENT 'WorkID',
  `HungUpWay` int(11) DEFAULT '0' COMMENT '挂起方式',
  `Note` text COMMENT '挂起原因(标题与内容支持变量)',
  `Rec` varchar(50) DEFAULT NULL COMMENT '挂起人',
  `DTOfHungUp` varchar(50) DEFAULT NULL COMMENT '挂起时间',
  `DTOfUnHungUp` varchar(50) DEFAULT NULL COMMENT '实际解除挂起时间',
  `DTOfUnHungUpPlan` varchar(50) DEFAULT NULL COMMENT '预计解除挂起时间',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_hungup
-- ----------------------------

-- ----------------------------
-- Table structure for wf_labnote
-- ----------------------------
DROP TABLE IF EXISTS `wf_labnote`;
CREATE TABLE `wf_labnote` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `Name` varchar(3000) DEFAULT NULL,
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程',
  `X` int(11) DEFAULT '0' COMMENT 'X坐标',
  `Y` int(11) DEFAULT '0' COMMENT 'Y坐标',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_labnote
-- ----------------------------

-- ----------------------------
-- Table structure for wf_node
-- ----------------------------
DROP TABLE IF EXISTS `wf_node`;
CREATE TABLE `wf_node` (
  `NodeID` int(11) NOT NULL DEFAULT '0' COMMENT '节点ID',
  `Step` int(11) DEFAULT '0' COMMENT '步骤(无计算意义)',
  `FK_Flow` varchar(150) DEFAULT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `Tip` varchar(100) DEFAULT NULL COMMENT '操作提示',
  `WhoExeIt` int(11) DEFAULT '0' COMMENT '谁执行它',
  `ReadReceipts` int(11) DEFAULT '0' COMMENT '已读回执',
  `CondModel` int(11) DEFAULT '0' COMMENT '方向条件控制规则',
  `CancelRole` int(11) DEFAULT '0' COMMENT '撤销规则',
  `CancelDisWhenRead` int(11) DEFAULT '0' COMMENT '对方已经打开就不能撤销',
  `IsTask` int(11) DEFAULT '1' COMMENT '允许分配工作否?',
  `IsRM` int(11) DEFAULT '1' COMMENT '是否启用投递路径自动记忆功能?',
  `DTFrom` varchar(50) DEFAULT NULL COMMENT '生命周期从',
  `DTTo` varchar(50) DEFAULT NULL COMMENT '生命周期到',
  `IsBUnit` int(11) DEFAULT '0' COMMENT '是否是节点模版（业务单元）?',
  `FocusField` varchar(50) DEFAULT NULL COMMENT '焦点字段',
  `SaveModel` int(11) DEFAULT '0' COMMENT '保存方式',
  `IsGuestNode` int(11) DEFAULT '0' COMMENT '是否是外部用户执行的节点(非组织结构人员参与处理工作的节点)?',
  `NodeAppType` int(11) DEFAULT '0' COMMENT '节点业务类型',
  `FWCSta` int(11) DEFAULT '0' COMMENT '节点状态',
  `SelfParas` varchar(1000) DEFAULT NULL,
  `RunModel` int(11) DEFAULT '0' COMMENT '节点类型',
  `SubThreadType` int(11) DEFAULT '0' COMMENT '子线程类型',
  `PassRate` float DEFAULT NULL COMMENT '完成通过率',
  `SubFlowStartWay` int(11) DEFAULT '0' COMMENT '子线程启动方式',
  `SubFlowStartParas` varchar(100) DEFAULT NULL COMMENT '启动参数',
  `ThreadIsCanDel` int(11) DEFAULT '0' COMMENT '是否可以删除子线程(当前节点已经发送出去的线程，并且当前节点是分流，或者分合流有效，在子线程退回后的操作)？',
  `ThreadIsCanShift` int(11) DEFAULT '0' COMMENT '是否可以移交子线程(当前节点已经发送出去的线程，并且当前节点是分流，或者分合流有效，在子线程退回后的操作)？',
  `IsAllowRepeatEmps` int(11) DEFAULT '0' COMMENT '是否允许子线程接受人员重复(仅当分流点向子线程发送时有效)?',
  `AutoRunEnable` int(11) DEFAULT '0' COMMENT '是否启用自动运行？(仅当分流点向子线程发送时有效)',
  `AutoRunParas` varchar(100) DEFAULT NULL COMMENT '自动运行SQL',
  `AutoJumpRole0` int(11) DEFAULT '0' COMMENT '处理人就是发起人',
  `AutoJumpRole1` int(11) DEFAULT '0' COMMENT '处理人已经出现过',
  `AutoJumpRole2` int(11) DEFAULT '0' COMMENT '处理人与上一步相同',
  `WhenNoWorker` int(11) DEFAULT '0' COMMENT '(是)找不到人就跳转,(否)提示错误.',
  `SendLab` varchar(50) DEFAULT NULL COMMENT '发送按钮标签',
  `SendJS` varchar(999) DEFAULT NULL COMMENT '按钮JS函数',
  `SaveLab` varchar(50) DEFAULT NULL COMMENT '保存按钮标签',
  `SaveEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `ThreadLab` varchar(50) DEFAULT NULL COMMENT '子线程按钮标签',
  `ThreadEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `ThreadKillRole` int(11) DEFAULT '0' COMMENT '子线程删除方式',
  `JumpWayLab` varchar(50) DEFAULT NULL COMMENT '跳转按钮标签',
  `JumpWay` int(11) DEFAULT '0' COMMENT '跳转规则',
  `JumpToNodes` varchar(200) DEFAULT NULL COMMENT '可跳转的节点',
  `ReturnLab` varchar(50) DEFAULT NULL COMMENT '退回按钮标签',
  `ReturnRole` int(11) DEFAULT '0' COMMENT '退回规则',
  `ReturnAlert` varchar(999) DEFAULT NULL COMMENT '被退回后信息提示',
  `IsBackTracking` int(11) DEFAULT '1' COMMENT '是否可以原路返回(启用退回功能才有效)',
  `ReturnField` varchar(50) DEFAULT NULL COMMENT '退回信息填写字段',
  `ReturnReasonsItems` varchar(999) DEFAULT NULL COMMENT '退回原因',
  `CCLab` varchar(50) DEFAULT NULL COMMENT '抄送按钮标签',
  `CCRole` int(11) DEFAULT '0' COMMENT '抄送规则',
  `CCWriteTo` int(11) DEFAULT '0' COMMENT '抄送写入规则',
  `ShiftLab` varchar(50) DEFAULT NULL COMMENT '移交按钮标签',
  `ShiftEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `DelLab` varchar(50) DEFAULT NULL COMMENT '删除按钮标签',
  `DelEnable` int(11) DEFAULT '0' COMMENT '删除规则',
  `EndFlowLab` varchar(50) DEFAULT NULL COMMENT '结束流程按钮标签',
  `EndFlowEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `PrintHtmlLab` varchar(50) DEFAULT NULL COMMENT '打印Html标签',
  `PrintHtmlEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `PrintPDFLab` varchar(50) DEFAULT NULL COMMENT '打印pdf标签',
  `PrintPDFEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `PrintZipLab` varchar(50) DEFAULT NULL COMMENT '打包下载zip按钮标签',
  `PrintZipEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `PrintDocLab` varchar(50) DEFAULT NULL COMMENT '打印单据按钮标签',
  `PrintDocEnable` int(11) DEFAULT '0' COMMENT '打印方式',
  `TrackLab` varchar(50) DEFAULT NULL COMMENT '轨迹按钮标签',
  `TrackEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `HungLab` varchar(50) DEFAULT NULL COMMENT '挂起按钮标签',
  `HungEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `SearchLab` varchar(50) DEFAULT NULL COMMENT '查询按钮标签',
  `SearchEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `WorkCheckLab` varchar(50) DEFAULT NULL COMMENT '审核按钮标签',
  `WorkCheckEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `AskforLab` varchar(50) DEFAULT NULL COMMENT '加签按钮标签',
  `AskforEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `HuiQianLab` varchar(50) DEFAULT NULL COMMENT '会签标签',
  `HuiQianRole` int(11) DEFAULT '0' COMMENT '会签模式',
  `TCLab` varchar(50) DEFAULT NULL COMMENT '流转自定义',
  `TCEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `WebOffice` varchar(50) DEFAULT NULL COMMENT '文档按钮标签',
  `WebOfficeEnable` int(11) DEFAULT '0' COMMENT '文档启用方式',
  `PRILab` varchar(50) DEFAULT NULL COMMENT '重要性',
  `PRIEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `CHLab` varchar(50) DEFAULT NULL COMMENT '节点时限',
  `CHEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `AllotLab` varchar(50) DEFAULT NULL COMMENT '分配按钮标签',
  `AllotEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `FocusLab` varchar(50) DEFAULT NULL COMMENT '关注',
  `FocusEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `ConfirmLab` varchar(50) DEFAULT NULL COMMENT '确认按钮标签',
  `ConfirmEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `CCIsAttr` int(11) DEFAULT '0' COMMENT '按表单字段抄送',
  `CCFormAttr` varchar(100) DEFAULT '' COMMENT '抄送人员字段',
  `CCIsStations` int(11) DEFAULT '0' COMMENT '按照岗位抄送',
  `CCStaWay` int(11) DEFAULT '0' COMMENT '抄送岗位计算方式',
  `CCIsDepts` int(11) DEFAULT '0' COMMENT '按照部门抄送',
  `CCIsEmps` int(11) DEFAULT '0' COMMENT '按照人员抄送',
  `CCIsSQLs` int(11) DEFAULT '0' COMMENT '按照SQL抄送',
  `CCSQL` varchar(500) DEFAULT NULL,
  `CCTitle` varchar(100) DEFAULT '' COMMENT '抄送标题',
  `CCDoc` text COMMENT '抄送内容(标题与内容支持变量)',
  `FWCLab` varchar(200) DEFAULT NULL,
  `FWCShowModel` int(11) DEFAULT '1' COMMENT '显示方式',
  `FWCType` int(11) DEFAULT '0' COMMENT '审核组件',
  `FWCNodeName` varchar(100) DEFAULT '' COMMENT '节点意见名称',
  `FWCAth` int(11) DEFAULT '0' COMMENT '附件上传',
  `FWCTrackEnable` int(11) DEFAULT '1' COMMENT '轨迹图是否显示？',
  `FWCListEnable` int(11) DEFAULT '1' COMMENT '历史审核信息是否显示？(否,仅出现意见框)',
  `FWCIsShowAllStep` int(11) DEFAULT '0' COMMENT '在轨迹表里是否显示所有的步骤？',
  `FWCOpLabel` varchar(50) DEFAULT '审核' COMMENT '操作名词(审核/审阅/批示)',
  `FWCDefInfo` varchar(50) DEFAULT '同意' COMMENT '默认审核信息',
  `SigantureEnabel` int(11) DEFAULT '0' COMMENT '操作人是否显示为图片签名？',
  `FWCIsFullInfo` int(11) DEFAULT '1' COMMENT '如果用户未审核是否按照默认意见填充？',
  `FWC_X` float(11,2) DEFAULT '300.00' COMMENT '位置X',
  `FWC_Y` float(11,2) DEFAULT '500.00' COMMENT '位置Y',
  `FWC_H` float(11,2) DEFAULT '300.00' COMMENT '高度(0=100%)',
  `FWC_W` float(11,2) DEFAULT '400.00' COMMENT '宽度(0=100%)',
  `FWCFields` varchar(50) DEFAULT '' COMMENT '审批格式字段',
  `FWCIsShowTruck` int(11) DEFAULT '0' COMMENT '是否显示未审核的轨迹？',
  `FWCIsShowReturnMsg` int(11) DEFAULT '0' COMMENT '是否显示退回信息？',
  `FWCOrderModel` int(11) DEFAULT '0' COMMENT '协作模式下操作员显示顺序',
  `FWCMsgShow` int(11) DEFAULT '0' COMMENT '审核意见显示方式',
  `SFLab` varchar(200) DEFAULT '子流程' COMMENT '显示标签',
  `SFSta` int(11) DEFAULT '0' COMMENT '父子流程状态',
  `SFShowModel` int(11) DEFAULT '1' COMMENT '显示方式',
  `SFCaption` varchar(100) DEFAULT '启动子流程' COMMENT '连接标题',
  `SFDefInfo` varchar(50) DEFAULT '' COMMENT '可启动的子流程编号(多个用逗号分开)',
  `SFActiveFlows` varchar(100) DEFAULT NULL,
  `SF_X` float(11,2) DEFAULT '5.00' COMMENT '位置X',
  `SF_Y` float(11,2) DEFAULT '5.00' COMMENT '位置Y',
  `SF_H` float(11,2) DEFAULT '300.00' COMMENT '高度',
  `SF_W` float(11,2) DEFAULT '400.00' COMMENT '宽度',
  `SFFields` varchar(50) DEFAULT '' COMMENT '审批格式字段',
  `SFShowCtrl` int(11) DEFAULT '0' COMMENT '显示控制方式',
  `SFOpenType` int(11) DEFAULT '0' COMMENT '打开子流程显示',
  `FrmThreadLab` varchar(200) DEFAULT '子线程' COMMENT '显示标签',
  `FrmThreadSta` int(11) DEFAULT '0' COMMENT '组件状态',
  `FrmThread_X` float(11,2) DEFAULT '5.00' COMMENT '位置X',
  `FrmThread_Y` float(11,2) DEFAULT '5.00' COMMENT '位置Y',
  `FrmThread_H` float(11,2) DEFAULT '300.00' COMMENT '高度',
  `FrmThread_W` float(11,2) DEFAULT '400.00' COMMENT '宽度',
  `FrmTrackLab` varchar(200) DEFAULT '轨迹' COMMENT '显示标签',
  `FrmTrackSta` int(11) DEFAULT '0' COMMENT '组件状态',
  `FrmTrack_X` float(11,2) DEFAULT '5.00' COMMENT '位置X',
  `FrmTrack_Y` float(11,2) DEFAULT '5.00' COMMENT '位置Y',
  `FrmTrack_H` float(11,2) DEFAULT '300.00' COMMENT '高度',
  `FrmTrack_W` float(11,2) DEFAULT '400.00' COMMENT '宽度',
  `CheckNodes` varchar(50) DEFAULT '' COMMENT '工作节点s',
  `DeliveryWay` int(11) DEFAULT '0' COMMENT '访问规则',
  `FTCLab` varchar(50) DEFAULT '流转自定义' COMMENT '显示标签',
  `FTCSta` int(11) DEFAULT '0' COMMENT '组件状态',
  `FTCWorkModel` int(11) DEFAULT '0' COMMENT '工作模式',
  `FTC_X` float(11,2) DEFAULT '5.00' COMMENT '位置X',
  `FTC_Y` float(11,2) DEFAULT '5.00' COMMENT '位置Y',
  `FTC_H` float(11,2) DEFAULT '300.00' COMMENT '高度',
  `FTC_W` float(11,2) DEFAULT '400.00' COMMENT '宽度',
  `SelectAccepterLab` varchar(50) DEFAULT '接受人' COMMENT '接受人按钮标签',
  `SelectAccepterEnable` int(11) DEFAULT '0' COMMENT '方式',
  `BatchLab` varchar(50) DEFAULT '批量审核' COMMENT '批量审核标签',
  `BatchEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOpenLab` varchar(50) DEFAULT '打开本地' COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) DEFAULT '打开模板' COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) DEFAULT '保存' COMMENT '保存标签',
  `OfficeSaveEnable` int(11) DEFAULT '1' COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) DEFAULT '接受修订' COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) DEFAULT '拒绝修订' COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeOverLab` varchar(50) DEFAULT '套红' COMMENT '套红标签',
  `OfficeOverEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeMarksEnable` int(11) DEFAULT '1' COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) DEFAULT '打印' COMMENT '打印标签',
  `OfficePrintEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeSealLab` varchar(50) DEFAULT '签章' COMMENT '签章标签',
  `OfficeSealEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) DEFAULT '插入流程' COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeNodeInfo` int(11) DEFAULT '0' COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) DEFAULT '0' COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) DEFAULT '下载' COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) DEFAULT '0' COMMENT '是否启用',
  `OfficeIsMarks` int(11) DEFAULT '1' COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) DEFAULT '' COMMENT '指定文档模板',
  `OfficeIsParent` int(11) DEFAULT '1' COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) DEFAULT '0' COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) DEFAULT '' COMMENT '自动套红模板',
  `SelectorModel` int(11) DEFAULT '5' COMMENT '显示方式',
  `FK_SQLTemplate` varchar(50) DEFAULT '' COMMENT 'SQL模版',
  `FK_SQLTemplateText` varchar(50) DEFAULT '' COMMENT 'SQL模版',
  `IsAutoLoadEmps` int(11) DEFAULT '1' COMMENT '是否自动加载上一次选择的人员？',
  `IsSimpleSelector` int(11) DEFAULT '0' COMMENT '是否单项选择(只能选择一个人)？',
  `IsEnableDeptRange` int(11) DEFAULT '0' COMMENT '是否启用部门搜索范围限定(对使用通用人员选择器有效)？',
  `IsEnableStaRange` int(11) DEFAULT '0' COMMENT '是否启用岗位搜索范围限定(对使用通用人员选择器有效)？',
  `SelectorP1` text COMMENT '分组参数:可以为空,比如:SELECT No,Name,ParentNo FROM  Port_Dept',
  `SelectorP2` text COMMENT '操作员数据源:比如:SELECT No,Name,FK_Dept FROM  Port_Emp',
  `SelectorP3` text COMMENT '默认选择的数据源:比如:SELECT FK_Emp FROM  WF_GenerWorkerList WHERE FK_Node=102 AND WorkID=@WorkID',
  `SelectorP4` text COMMENT '强制选择的数据源:比如:SELECT FK_Emp FROM  WF_GenerWorkerList WHERE FK_Node=102 AND WorkID=@WorkID',
  `X` int(11) DEFAULT '0' COMMENT 'X坐标',
  `Y` int(11) DEFAULT '0' COMMENT 'Y坐标',
  `OfficeOpen` varchar(50) DEFAULT '打开本地' COMMENT '打开本地标签',
  `OfficeOpenTemplate` varchar(50) DEFAULT '打开模板' COMMENT '打开模板标签',
  `OfficeSave` varchar(50) DEFAULT '保存' COMMENT '保存标签',
  `OfficeAccept` varchar(50) DEFAULT '接受修订' COMMENT '接受修订标签',
  `OfficeRefuse` varchar(50) DEFAULT '拒绝修订' COMMENT '拒绝修订标签',
  `OfficeOver` varchar(50) DEFAULT '套红按钮' COMMENT '套红按钮标签',
  `OfficeMarks` int(11) DEFAULT '1' COMMENT '是否查看用户留痕',
  `OfficeReadOnly` int(11) DEFAULT '0' COMMENT '是否只读',
  `OfficePrint` varchar(50) DEFAULT '打印按钮' COMMENT '打印按钮标签',
  `OfficeSeal` varchar(50) DEFAULT '签章按钮' COMMENT '签章按钮标签',
  `OfficeInsertFlow` varchar(50) DEFAULT '插入流程' COMMENT '插入流程标签',
  `OfficeIsTrueTH` int(11) DEFAULT '0' COMMENT '是否自动套红',
  `WebOfficeFrmModel` int(11) DEFAULT '0' COMMENT '表单工作方式',
  `ICON` varchar(70) DEFAULT '' COMMENT '节点ICON图片路径',
  `NodeWorkType` int(11) DEFAULT '0' COMMENT '节点类型',
  `FlowName` varchar(200) DEFAULT '' COMMENT '流程名',
  `FrmAttr` varchar(300) DEFAULT '' COMMENT 'FrmAttr',
  `TimeLimit` float(11,2) DEFAULT '2.00' COMMENT '限期(天)',
  `TWay` int(11) DEFAULT '0' COMMENT '时间计算方式',
  `TAlertRole` int(11) DEFAULT '0' COMMENT '逾期提醒规则',
  `TAlertWay` int(11) DEFAULT '0' COMMENT '逾期提醒方式',
  `WarningDay` float(11,2) DEFAULT '1.00' COMMENT '工作预警(天)',
  `WAlertRole` int(11) DEFAULT '0' COMMENT '预警提醒规则',
  `WAlertWay` int(11) DEFAULT '0' COMMENT '预警提醒方式',
  `TCent` float(11,2) DEFAULT '2.00' COMMENT '扣分(每延期1小时)',
  `CHWay` int(11) DEFAULT '0' COMMENT '考核方式',
  `IsEval` int(11) DEFAULT '0' COMMENT '是否工作质量考核',
  `OutTimeDeal` int(11) DEFAULT '0' COMMENT '超时处理方式',
  `DoOutTime` varchar(300) DEFAULT '' COMMENT '超时处理内容',
  `Doc` varchar(100) DEFAULT '' COMMENT '描述',
  `IsExpSender` int(11) DEFAULT '1' COMMENT '本节点接收人不允许包含上一步发送人',
  `DeliveryParas` varchar(600) DEFAULT NULL,
  `NodeFrmID` varchar(50) DEFAULT '' COMMENT '节点表单ID',
  `IsCanDelFlow` int(11) DEFAULT '0' COMMENT '是否可以删除流程',
  `TodolistModel` int(11) DEFAULT '0' COMMENT '多人处理规则',
  `TeamLeaderConfirmRole` int(11) DEFAULT '0' COMMENT '组长确认规则',
  `TeamLeaderConfirmDoc` varchar(100) DEFAULT '' COMMENT '组长确认设置内容',
  `IsHandOver` int(11) DEFAULT '0' COMMENT '是否可以移交',
  `BlockModel` int(11) DEFAULT '0' COMMENT '阻塞模式',
  `BlockExp` varchar(200) DEFAULT '' COMMENT '阻塞表达式',
  `BlockAlert` varchar(100) DEFAULT '' COMMENT '被阻塞提示信息',
  `BatchRole` int(11) DEFAULT '0' COMMENT '批处理',
  `BatchListCount` int(11) DEFAULT '12' COMMENT '批处理数量',
  `BatchParas` varchar(500) DEFAULT '' COMMENT '参数',
  `FormType` int(11) DEFAULT '1' COMMENT '表单类型',
  `FormUrl` varchar(300) DEFAULT 'http://' COMMENT '表单URL',
  `TurnToDeal` int(11) DEFAULT '0' COMMENT '转向处理',
  `TurnToDealDoc` varchar(200) DEFAULT '' COMMENT '发送后提示信息',
  `NodePosType` int(11) DEFAULT '0' COMMENT '位置',
  `IsCCFlow` int(11) DEFAULT '0' COMMENT '是否有流程完成条件',
  `HisStas` varchar(3000) DEFAULT '' COMMENT '岗位',
  `HisDeptStrs` varchar(3000) DEFAULT '' COMMENT '部门',
  `HisToNDs` varchar(50) DEFAULT '' COMMENT '转到的节点',
  `HisBillIDs` varchar(50) DEFAULT '' COMMENT '单据IDs',
  `HisSubFlows` varchar(30) DEFAULT '' COMMENT 'HisSubFlows',
  `PTable` varchar(100) DEFAULT '' COMMENT '物理表',
  `ShowSheets` varchar(100) DEFAULT '' COMMENT '显示的表单',
  `GroupStaNDs` varchar(500) DEFAULT NULL,
  `RefOneFrmTreeType` varchar(100) DEFAULT '' COMMENT '独立表单类型',
  `AtPara` varchar(500) DEFAULT '' COMMENT 'AtPara',
  `TSpanHour` float(50,2) DEFAULT '0.00',
  `SubFlowLab` varchar(50) DEFAULT '子流程',
  `SubFlowEnable` int(11) DEFAULT '0',
  `FWCVer` int(11) DEFAULT '0',
  `ReturnOneNodeRole` int(11) DEFAULT '0',
  `PrintPDFModle` int(11) DEFAULT '0',
  `ShuiYinModle` varchar(100) DEFAULT '',
  `NoteLab` varchar(50) DEFAULT '备注',
  `NoteEnable` int(11) DEFAULT '0',
  `IsYouLiTai` int(11) DEFAULT '0',
  `OfficeBtnLab` varchar(50) DEFAULT '打开公文',
  `OfficeBtnEnable` int(11) DEFAULT '0',
  `ListLab` varchar(50) DEFAULT '列表',
  `ListEnable` int(11) DEFAULT '1',
  `IsToParentNextNode` int(11) DEFAULT '0' COMMENT '子流程运行到该节点时，让父流程自动运行到下一步',
  `CHRole` int(11) DEFAULT '0' COMMENT '时限规则',
  `HelpLab` varchar(50) DEFAULT '帮助提示' COMMENT '帮助',
  `HelpRole` int(11) DEFAULT '0' COMMENT '帮助提示规则',
  `ReturnCHEnable` int(11) DEFAULT '0' COMMENT '是否启用退回考核规则',
  `HuiQianLeaderRole` int(11) DEFAULT '0' COMMENT '组长会签规则',
  `IsOpenOver` int(11) DEFAULT '0' COMMENT '是否打开即审批?',
  `DoOutTimeCond` varchar(200) DEFAULT '' COMMENT '执行超时的条件',
  `CheckField` varchar(50) DEFAULT '' COMMENT '签批字段',
  `CheckFieldText` varchar(200) DEFAULT '' COMMENT '签批字段',
  `FWCView` varchar(200) DEFAULT '' COMMENT '审核意见立场',
  `FWCNewDuanYu` varchar(100) DEFAULT '',
  PRIMARY KEY (`NodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_node
-- ----------------------------

-- ----------------------------
-- Table structure for wf_nodecancel
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodecancel`;
CREATE TABLE `wf_nodecancel` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `CancelTo` int(11) NOT NULL DEFAULT '0' COMMENT '撤销到',
  PRIMARY KEY (`FK_Node`,`CancelTo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_nodecancel
-- ----------------------------

-- ----------------------------
-- Table structure for wf_nodedept
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodedept`;
CREATE TABLE `wf_nodedept` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `FK_Dept` varchar(100) NOT NULL COMMENT '部门',
  PRIMARY KEY (`FK_Node`,`FK_Dept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_nodedept
-- ----------------------------

-- ----------------------------
-- Table structure for wf_nodeemp
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodeemp`;
CREATE TABLE `wf_nodeemp` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT 'Node',
  `FK_Emp` varchar(100) NOT NULL COMMENT '到人员',
  PRIMARY KEY (`FK_Node`,`FK_Emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_nodeemp
-- ----------------------------

-- ----------------------------
-- Table structure for wf_nodeflow
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodeflow`;
CREATE TABLE `wf_nodeflow` (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Flow` varchar(100) NOT NULL COMMENT '子流程,主外键:对应物理表:WF_Flow,表描述:流程',
  PRIMARY KEY (`FK_Node`,`FK_Flow`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点调用子流程';

-- ----------------------------
-- Records of wf_nodeflow
-- ----------------------------

-- ----------------------------
-- Table structure for wf_nodereturn
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodereturn`;
CREATE TABLE `wf_nodereturn` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `ReturnTo` int(11) NOT NULL DEFAULT '0' COMMENT '退回到',
  `Dots` varchar(300) DEFAULT NULL COMMENT '轨迹信息',
  PRIMARY KEY (`FK_Node`,`ReturnTo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_nodereturn
-- ----------------------------

-- ----------------------------
-- Table structure for wf_nodestation
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodestation`;
CREATE TABLE `wf_nodestation` (
  `FK_Node` int(11) NOT NULL DEFAULT '0' COMMENT '节点',
  `FK_Station` varchar(100) NOT NULL COMMENT '工作岗位',
  PRIMARY KEY (`FK_Node`,`FK_Station`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_nodestation
-- ----------------------------

-- ----------------------------
-- Table structure for wf_nodesubflow
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodesubflow`;
CREATE TABLE `wf_nodesubflow` (
  `MyPK` varchar(100) CHARACTER SET utf8 NOT NULL,
  `FK_Node` int(11) DEFAULT NULL,
  `SubFlowType` int(11) DEFAULT NULL,
  `FK_Flow` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `FlowName` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ExpType` int(11) DEFAULT NULL,
  `CondExp` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `YBFlowReturnRole` int(11) DEFAULT NULL,
  `ReturnToNode` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `ReturnToNodeText` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `Idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of wf_nodesubflow
-- ----------------------------

-- ----------------------------
-- Table structure for wf_nodetoolbar
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodetoolbar`;
CREATE TABLE `wf_nodetoolbar` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `Title` varchar(100) DEFAULT NULL COMMENT '标题',
  `Target` varchar(100) DEFAULT NULL COMMENT '目标',
  `Url` varchar(500) DEFAULT NULL COMMENT '连接',
  `ShowWhere` int(11) DEFAULT '1' COMMENT '显示位置',
  `Idx` int(11) DEFAULT '0' COMMENT '显示顺序',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `MyFileName` varchar(300) DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) DEFAULT '0' COMMENT 'MyFileH',
  `MyFileW` int(11) DEFAULT '0' COMMENT 'MyFileW',
  `MyFileSize` float DEFAULT NULL COMMENT 'MyFileSize',
  `ExcType` int(11) DEFAULT '0',
  `UrlExt` varchar(500) DEFAULT '' COMMENT '连接/函数',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_nodetoolbar
-- ----------------------------

-- ----------------------------
-- Table structure for wf_powermodel
-- ----------------------------
DROP TABLE IF EXISTS `wf_powermodel`;
CREATE TABLE `wf_powermodel` (
  `MyPK` varchar(100) NOT NULL,
  `Model` varchar(100) DEFAULT NULL,
  `PowerFlag` varchar(100) DEFAULT NULL,
  `PowerFlagName` varchar(100) DEFAULT NULL,
  `PowerCtrlType` int(11) DEFAULT '0' COMMENT '控制类型',
  `EmpNo` varchar(100) DEFAULT NULL,
  `EmpName` varchar(100) DEFAULT NULL,
  `StaNo` varchar(100) DEFAULT NULL,
  `StaName` varchar(100) DEFAULT NULL,
  `FlowNo` varchar(100) DEFAULT NULL,
  `FrmID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_powermodel
-- ----------------------------

-- ----------------------------
-- Table structure for wf_pushmsg
-- ----------------------------
DROP TABLE IF EXISTS `wf_pushmsg`;
CREATE TABLE `wf_pushmsg` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Flow` varchar(3) DEFAULT NULL COMMENT '流程',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `FK_Event` varchar(20) DEFAULT NULL,
  `PushWay` int(11) DEFAULT '0' COMMENT '推送方式',
  `PushDoc` text COMMENT '推送保存内容',
  `Tag` varchar(500) DEFAULT NULL COMMENT 'Tag',
  `SMSPushWay` int(11) DEFAULT '0' COMMENT '短信发送方式',
  `SMSField` varchar(100) DEFAULT NULL COMMENT '短信字段',
  `SMSDoc` text COMMENT '短信内容模版',
  `SMSNodes` varchar(100) DEFAULT NULL COMMENT 'SMS节点s',
  `MailPushWay` int(11) DEFAULT '0' COMMENT '邮件发送方式',
  `MailAddress` varchar(100) DEFAULT NULL COMMENT '邮件字段',
  `MailTitle` varchar(200) DEFAULT NULL COMMENT '邮件标题模版',
  `MailDoc` text COMMENT '邮件内容模版',
  `MailNodes` varchar(100) DEFAULT NULL COMMENT 'Mail节点s',
  `SMSPushModel` varchar(50) DEFAULT 'Email' COMMENT '短消息发送设置',
  `BySQL` varchar(500) DEFAULT '' COMMENT '按照SQL计算',
  `ByEmps` varchar(100) DEFAULT '' COMMENT '发送给指定的人员',
  `AtPara` varchar(500) DEFAULT '' COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_pushmsg
-- ----------------------------

-- ----------------------------
-- Table structure for wf_rememberme
-- ----------------------------
DROP TABLE IF EXISTS `wf_rememberme`;
CREATE TABLE `wf_rememberme` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点',
  `FK_Emp` varchar(30) DEFAULT NULL COMMENT '当前操作人员',
  `Objs` text COMMENT '分配人员',
  `ObjsExt` text COMMENT '分配人员Ext',
  `Emps` text COMMENT '所有的工作人员',
  `EmpsExt` text COMMENT '工作人员Ext',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_rememberme
-- ----------------------------

-- ----------------------------
-- Table structure for wf_returnwork
-- ----------------------------
DROP TABLE IF EXISTS `wf_returnwork`;
CREATE TABLE `wf_returnwork` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `WorkID` int(11) DEFAULT '0' COMMENT 'WorkID',
  `ReturnNode` int(11) DEFAULT '0' COMMENT '退回节点',
  `ReturnNodeName` varchar(100) DEFAULT NULL COMMENT '退回节点名称',
  `Returner` varchar(20) DEFAULT NULL COMMENT '退回人',
  `ReturnerName` varchar(100) DEFAULT NULL COMMENT '退回人名称',
  `ReturnToNode` int(11) DEFAULT '0' COMMENT 'ReturnToNode',
  `ReturnToEmp` text COMMENT '退回给',
  `BeiZhu` text COMMENT '退回原因',
  `RDT` varchar(50) DEFAULT NULL COMMENT '退回日期',
  `IsBackTracking` int(11) DEFAULT '0' COMMENT '是否要原路返回?',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_returnwork
-- ----------------------------

-- ----------------------------
-- Table structure for wf_selectaccper
-- ----------------------------
DROP TABLE IF EXISTS `wf_selectaccper`;
CREATE TABLE `wf_selectaccper` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Node` int(11) DEFAULT '0' COMMENT '接受人节点',
  `WorkID` int(11) DEFAULT '0' COMMENT 'WorkID',
  `FK_Emp` varchar(20) DEFAULT NULL COMMENT 'FK_Emp',
  `EmpName` varchar(20) DEFAULT NULL COMMENT 'EmpName',
  `DeptName` varchar(400) DEFAULT NULL COMMENT '部门名称',
  `AccType` int(11) DEFAULT '0' COMMENT '类型(@0=接受人@1=抄送人)',
  `Rec` varchar(20) DEFAULT NULL COMMENT '记录人',
  `Info` varchar(200) DEFAULT NULL COMMENT '办理意见信息',
  `IsRemember` int(11) DEFAULT '0' COMMENT '以后发送是否按本次计算',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序号(可以用于流程队列审核模式)',
  `Tag` varchar(200) DEFAULT NULL COMMENT '维度信息Tag',
  `TimeLimit` int(11) DEFAULT '0' COMMENT '时限-天',
  `TSpanHour` float DEFAULT NULL COMMENT '时限-小时',
  `ADT` varchar(50) DEFAULT NULL COMMENT '到达日期(计划)',
  `SDT` varchar(50) DEFAULT NULL COMMENT '应完成日期(计划)',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_selectaccper
-- ----------------------------

-- ----------------------------
-- Table structure for wf_selectinfo
-- ----------------------------
DROP TABLE IF EXISTS `wf_selectinfo`;
CREATE TABLE `wf_selectinfo` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `AcceptNodeID` int(11) DEFAULT NULL COMMENT '接受节点',
  `WorkID` int(11) DEFAULT NULL COMMENT '工作ID',
  `InfoLeft` varchar(200) DEFAULT NULL COMMENT 'InfoLeft',
  `InfoCenter` varchar(200) DEFAULT NULL COMMENT 'InfoCenter',
  `InfoRight` varchar(200) DEFAULT NULL COMMENT 'InfoLeft',
  `AccType` int(11) DEFAULT NULL COMMENT '类型(@0=接受人@1=抄送人)',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='选择接受/抄送人节点信息';

-- ----------------------------
-- Records of wf_selectinfo
-- ----------------------------

-- ----------------------------
-- Table structure for wf_shiftwork
-- ----------------------------
DROP TABLE IF EXISTS `wf_shiftwork`;
CREATE TABLE `wf_shiftwork` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `WorkID` int(11) DEFAULT '0' COMMENT '工作ID',
  `FK_Node` int(11) DEFAULT '0' COMMENT 'FK_Node',
  `FK_Emp` varchar(40) DEFAULT NULL COMMENT '移交人',
  `FK_EmpName` varchar(40) DEFAULT NULL COMMENT '移交人名称',
  `ToEmp` varchar(40) DEFAULT NULL COMMENT '移交给',
  `ToEmpName` varchar(40) DEFAULT NULL COMMENT '移交给名称',
  `RDT` varchar(50) DEFAULT NULL COMMENT '移交时间',
  `Note` varchar(2000) DEFAULT NULL COMMENT '移交原因',
  `IsRead` int(11) DEFAULT '0' COMMENT '是否读取？',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_shiftwork
-- ----------------------------

-- ----------------------------
-- Table structure for wf_sqltemplate
-- ----------------------------
DROP TABLE IF EXISTS `wf_sqltemplate`;
CREATE TABLE `wf_sqltemplate` (
  `No` varchar(3) NOT NULL COMMENT '编号',
  `SQLType` int(11) DEFAULT '0' COMMENT '模版SQL类型',
  `Name` varchar(200) DEFAULT NULL COMMENT 'SQL说明',
  `Docs` text COMMENT 'SQL模版',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_sqltemplate
-- ----------------------------

-- ----------------------------
-- Table structure for wf_task
-- ----------------------------
DROP TABLE IF EXISTS `wf_task`;
CREATE TABLE `wf_task` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Flow` varchar(200) DEFAULT NULL COMMENT '流程编号',
  `Starter` varchar(200) DEFAULT NULL COMMENT '发起人',
  `Paras` text COMMENT '参数',
  `TaskSta` int(11) DEFAULT '0' COMMENT '任务状态',
  `Msg` text COMMENT '消息',
  `StartDT` varchar(20) DEFAULT NULL COMMENT '发起时间',
  `RDT` varchar(20) DEFAULT NULL COMMENT '插入数据时间',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_task
-- ----------------------------

-- ----------------------------
-- Table structure for wf_testapi
-- ----------------------------
DROP TABLE IF EXISTS `wf_testapi`;
CREATE TABLE `wf_testapi` (
  `No` varchar(92) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_testapi
-- ----------------------------

-- ----------------------------
-- Table structure for wf_testcase
-- ----------------------------
DROP TABLE IF EXISTS `wf_testcase`;
CREATE TABLE `wf_testcase` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程编号',
  `ParaType` varchar(100) DEFAULT NULL COMMENT '参数类型',
  `Vals` varchar(500) DEFAULT NULL COMMENT '值s',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_testcase
-- ----------------------------

-- ----------------------------
-- Table structure for wf_testsample
-- ----------------------------
DROP TABLE IF EXISTS `wf_testsample`;
CREATE TABLE `wf_testsample` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `Name` varchar(50) DEFAULT NULL COMMENT '测试名称',
  `FK_API` varchar(100) DEFAULT NULL COMMENT '测试的API',
  `FK_Ver` varchar(100) DEFAULT NULL COMMENT '测试的版本',
  `DTFrom` varchar(50) DEFAULT NULL COMMENT '从',
  `DTTo` varchar(50) DEFAULT NULL COMMENT '到',
  `TimeUse` float DEFAULT NULL COMMENT '用时(毫秒)',
  `TimesPerSecond` float DEFAULT NULL COMMENT '每秒跑多少个?',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_testsample
-- ----------------------------

-- ----------------------------
-- Table structure for wf_testver
-- ----------------------------
DROP TABLE IF EXISTS `wf_testver`;
CREATE TABLE `wf_testver` (
  `No` varchar(92) NOT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_testver
-- ----------------------------

-- ----------------------------
-- Table structure for wf_transfercustom
-- ----------------------------
DROP TABLE IF EXISTS `wf_transfercustom`;
CREATE TABLE `wf_transfercustom` (
  `MyPK` varchar(100) NOT NULL COMMENT '主键MyPK',
  `WorkID` int(11) DEFAULT '0' COMMENT 'WorkID',
  `FK_Node` int(11) DEFAULT '0' COMMENT '节点ID',
  `Worker` varchar(200) DEFAULT NULL COMMENT '处理人(多个人用逗号分开)',
  `WorkerName` varchar(200) DEFAULT NULL COMMENT '处理人(多个人用逗号分开)',
  `SubFlowNo` varchar(3) DEFAULT NULL COMMENT '要经过的子流程编号',
  `PlanDT` varchar(50) DEFAULT NULL COMMENT '计划完成日期',
  `TodolistModel` int(11) DEFAULT '0' COMMENT '多人工作处理模式',
  `Idx` int(11) DEFAULT '0' COMMENT '顺序号',
  `NodeName` varchar(200) DEFAULT '',
  `IsEnable` int(11) DEFAULT '0',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_transfercustom
-- ----------------------------

-- ----------------------------
-- Table structure for wf_turnto
-- ----------------------------
DROP TABLE IF EXISTS `wf_turnto`;
CREATE TABLE `wf_turnto` (
  `MyPK` varchar(100) NOT NULL COMMENT 'MyPK - 主键',
  `TurnToType` int(11) DEFAULT NULL COMMENT '条件类型',
  `FK_Flow` varchar(60) DEFAULT NULL COMMENT '流程',
  `FK_Node` int(11) DEFAULT NULL COMMENT '节点ID',
  `FK_Attr` varchar(80) DEFAULT NULL COMMENT '属性外键Sys_MapAttr',
  `AttrKey` varchar(80) DEFAULT NULL COMMENT '键值',
  `AttrT` varchar(80) DEFAULT NULL COMMENT '属性名称',
  `FK_Operator` varchar(60) DEFAULT NULL COMMENT '运算符号',
  `OperatorValue` varchar(60) DEFAULT NULL COMMENT '要运算的值',
  `OperatorValueT` varchar(60) DEFAULT NULL COMMENT '要运算的值T',
  `TurnToURL` varchar(700) DEFAULT NULL COMMENT '要转向的URL',
  PRIMARY KEY (`MyPK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='转向条件';

-- ----------------------------
-- Records of wf_turnto
-- ----------------------------

-- ----------------------------
-- Table structure for wf_workflowdeletelog
-- ----------------------------
DROP TABLE IF EXISTS `wf_workflowdeletelog`;
CREATE TABLE `wf_workflowdeletelog` (
  `OID` int(11) NOT NULL COMMENT 'OID',
  `FID` int(11) DEFAULT '0' COMMENT 'FID',
  `FK_Dept` varchar(100) DEFAULT NULL COMMENT '部门',
  `Title` varchar(100) DEFAULT NULL COMMENT '标题',
  `FlowStarter` varchar(100) DEFAULT NULL COMMENT '发起人',
  `FlowStartRDT` varchar(50) DEFAULT NULL COMMENT '发起时间',
  `FK_NY` varchar(100) DEFAULT NULL COMMENT '年月',
  `FK_Flow` varchar(100) DEFAULT NULL COMMENT '流程',
  `FlowEnderRDT` varchar(50) DEFAULT NULL COMMENT '最后处理时间',
  `FlowEndNode` int(11) DEFAULT '0' COMMENT '停留节点',
  `FlowDaySpan` float DEFAULT NULL COMMENT '跨度(天)',
  `FlowEmps` varchar(100) DEFAULT NULL COMMENT '参与人',
  `Oper` varchar(20) DEFAULT NULL COMMENT '删除人员',
  `OperDept` varchar(20) DEFAULT NULL COMMENT '删除人员部门',
  `OperDeptName` varchar(200) DEFAULT NULL COMMENT '删除人员名称',
  `DeleteNote` text COMMENT '删除原因',
  `DeleteDT` varchar(50) DEFAULT NULL COMMENT '删除日期',
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_workflowdeletelog
-- ----------------------------

-- ----------------------------
-- View structure for port_emp
-- ----------------------------
DROP VIEW IF EXISTS `port_emp`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `port_emp` AS select `u`.`user_code` AS `No`,`e`.`emp_code` AS `EmpNo`,`u`.`user_name` AS `Name`,'123' AS `Pass`,(case when (`e`.`office_code` <> '') then `e`.`office_code` else (select `oo`.`office_code` from `js_sys_office` `oo` where (`oo`.`parent_code` = '0') limit 1) end) AS `FK_Dept`,'' AS `FK_Duty`,'admin' AS `Leader`,`u`.`extend_s1` AS `SID`,`u`.`mobile` AS `Tel`,`u`.`email` AS `Email`,'' AS `PinYin`,'' AS `SignType`,1 AS `NumOfDept`,0 AS `Idx`,NULL AS `OrgNo` from (`js_sys_user` `u` left join `js_sys_employee` `e` on((`e`.`emp_code` = `u`.`ref_code`))) where (((`u`.`status` = '0') and (`u`.`user_type` = 'employee') and (`e`.`status` = '0')) or (`u`.`login_code` = 'system') or (`u`.`login_code` = 'admin')) ;

-- ----------------------------
-- View structure for port_dept
-- ----------------------------
DROP VIEW IF EXISTS `port_dept`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `port_dept` AS select `o`.`office_code` AS `No`,`o`.`office_name` AS `Name`,'' AS `NameOfPath`,(case when (`o`.`parent_code` = '0') then '0' else `o`.`parent_code` end) AS `ParentNo`,'' AS `TreeNo`,'admin' AS `Leader`,'' AS `Tel`,`o`.`tree_sort` AS `Idx`,(case when (`o`.`tree_leaf` = '0') then 1 else 0 end) AS `IsDir`,'' AS `OrgNo` from `js_sys_office` `o` where (`o`.`status` = '0') ;

-- ----------------------------
-- View structure for port_deptemp
-- ----------------------------
DROP VIEW IF EXISTS `port_deptemp`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `port_deptemp` AS select concat(`e`.`FK_Dept`,'_',`e`.`No`) AS `MyPk`,`e`.`No` AS `FK_Emp`,`e`.`FK_Dept` AS `FK_Dept`,'' AS `FK_Duty`,0 AS `DutyLevel`,'admin' AS `Leader`,NULL AS `OrgNo` from `port_emp` `e` where (`e`.`FK_Dept` is not null) ;

-- ----------------------------
-- View structure for port_empstation
-- ----------------------------
DROP VIEW IF EXISTS `port_empstation`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `port_empstation` AS select `ur`.`user_code` AS `FK_Emp`,`ur`.`role_code` AS `FK_Station` from `js_sys_user_role` `ur` ;

-- ----------------------------
-- View structure for port_deptempstation
-- ----------------------------
DROP VIEW IF EXISTS `port_deptempstation`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `port_deptempstation` AS select concat(`e`.`FK_Dept`,'_',`e`.`No`,'_',`s`.`FK_Station`) AS `MyPk`,`e`.`FK_Dept` AS `FK_Dept`,`s`.`FK_Station` AS `FK_Station`,`e`.`No` AS `FK_Emp`,NULL AS `OrgNo` from (`port_emp` `e` join `port_empstation` `s`) where ((`e`.`No` = `s`.`FK_Emp`) and (`e`.`FK_Dept` is not null)) ;

-- ----------------------------
-- View structure for port_station
-- ----------------------------
DROP VIEW IF EXISTS `port_station`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `port_station` AS select `r`.`role_code` AS `No`,`r`.`role_name` AS `Name`,`r`.`role_type` AS `FK_StationType`,'' AS `DutyReq`,'' AS `Makings`,'' AS `OrgNo` from `js_sys_role` `r` ;

-- ----------------------------
-- View structure for port_stationtype
-- ----------------------------
DROP VIEW IF EXISTS `port_stationtype`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `port_stationtype` AS select `d`.`dict_value` AS `No`,`d`.`dict_label` AS `Name`,'' AS `Idx`,NULL AS `OrgNo` from `js_sys_dict_data` `d` where (`d`.`dict_type` = 'sys_role_type') ;

-- ----------------------------
-- View structure for v_flowstarterbpm
-- ----------------------------
DROP VIEW IF EXISTS `v_flowstarterbpm`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_flowstarterbpm` AS select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp`,`c`.`OrgNo` AS `OrgNo` from ((`wf_node` `a` join `wf_nodestation` `b`) join `port_deptempstation` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Station` = `c`.`FK_Station`) and ((`a`.`DeliveryWay` = 0) or (`a`.`DeliveryWay` = 14))) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`FK_Emp` AS `FK_Emp`,`c`.`OrgNo` AS `OrgNo` from ((`wf_node` `a` join `wf_nodedept` `b`) join `port_deptemp` `c`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`b`.`FK_Dept` = `c`.`FK_Dept`) and (`a`.`DeliveryWay` = 1)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`FK_Emp` AS `FK_Emp`,'' AS `OrgNo` from (`wf_node` `a` join `wf_nodeemp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`DeliveryWay` = 3)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`b`.`No` AS `FK_Emp`,`b`.`OrgNo` AS `OrgNo` from (`wf_node` `a` join `port_emp` `b`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`DeliveryWay` = 4)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`e`.`FK_Emp` AS `FK_Emp`,`e`.`OrgNo` AS `OrgNo` from (((`wf_node` `a` join `wf_nodedept` `b`) join `wf_nodestation` `c`) join `port_deptempstation` `e`) where ((`a`.`NodePosType` = 0) and ((`a`.`WhoExeIt` = 0) or (`a`.`WhoExeIt` = 2)) and (`a`.`NodeID` = `b`.`FK_Node`) and (`a`.`NodeID` = `c`.`FK_Node`) and (`b`.`FK_Dept` = `e`.`FK_Dept`) and (`c`.`FK_Station` = `e`.`FK_Station`) and (`a`.`DeliveryWay` = 9)) union select `a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`c`.`No` AS `FK_Emp`,`b`.`OrgNo` AS `OrgNo` from ((`wf_node` `a` join `wf_floworg` `b`) join `port_emp` `c`) where ((`a`.`FK_Flow` = `b`.`FlowNo`) and (`b`.`OrgNo` = `c`.`OrgNo`) and (`a`.`DeliveryWay` = 22)) ;

-- ----------------------------
-- View structure for v_myflowdata
-- ----------------------------
DROP VIEW IF EXISTS `v_myflowdata`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_myflowdata` AS select `a`.`WorkID` AS `WorkID`,`a`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`Title` AS `Title`,`a`.`WFSta` AS `WFSta`,`a`.`WFState` AS `WFState`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,`a`.`Sender` AS `Sender`,`a`.`RDT` AS `RDT`,`a`.`FK_Node` AS `FK_Node`,`a`.`NodeName` AS `NodeName`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`PRI` AS `PRI`,`a`.`SDTOfNode` AS `SDTOfNode`,`a`.`SDTOfFlow` AS `SDTOfFlow`,`a`.`PFlowNo` AS `PFlowNo`,`a`.`PWorkID` AS `PWorkID`,`a`.`PNodeID` AS `PNodeID`,`a`.`PFID` AS `PFID`,`a`.`PEmp` AS `PEmp`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,`a`.`TaskSta` AS `TaskSta`,`a`.`AtPara` AS `AtPara`,`a`.`Emps` AS `Emps`,`a`.`GUID` AS `GUID`,`a`.`FK_NY` AS `FK_NY`,`a`.`WeekNum` AS `WeekNum`,`a`.`TSpan` AS `TSpan`,`a`.`TodoSta` AS `TodoSta`,`a`.`MyNum` AS `MyNum`,`a`.`SendDT` AS `SendDT`,`a`.`Domain` AS `Domain`,`a`.`PrjNo` AS `PrjNo`,`a`.`PrjName` AS `PrjName`,`b`.`EmpNo` AS `MyEmpNo` from (`wf_generworkflow` `a` join `wf_powermodel` `b`) where ((`a`.`FK_Flow` = `b`.`FlowNo`) and (`b`.`PowerCtrlType` = 1) and (`a`.`WFState` >= 2)) union select `a`.`WorkID` AS `WorkID`,`a`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`Title` AS `Title`,`a`.`WFSta` AS `WFSta`,`a`.`WFState` AS `WFState`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,`a`.`Sender` AS `Sender`,`a`.`RDT` AS `RDT`,`a`.`FK_Node` AS `FK_Node`,`a`.`NodeName` AS `NodeName`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`PRI` AS `PRI`,`a`.`SDTOfNode` AS `SDTOfNode`,`a`.`SDTOfFlow` AS `SDTOfFlow`,`a`.`PFlowNo` AS `PFlowNo`,`a`.`PWorkID` AS `PWorkID`,`a`.`PNodeID` AS `PNodeID`,`a`.`PFID` AS `PFID`,`a`.`PEmp` AS `PEmp`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,`a`.`TaskSta` AS `TaskSta`,`a`.`AtPara` AS `AtPara`,`a`.`Emps` AS `Emps`,`a`.`GUID` AS `GUID`,`a`.`FK_NY` AS `FK_NY`,`a`.`WeekNum` AS `WeekNum`,`a`.`TSpan` AS `TSpan`,`a`.`TodoSta` AS `TodoSta`,`a`.`MyNum` AS `MyNum`,`a`.`SendDT` AS `SendDT`,`a`.`Domain` AS `Domain`,`a`.`PrjNo` AS `PrjNo`,`a`.`PrjName` AS `PrjName`,`c`.`No` AS `MyEmpNo` from (((`wf_generworkflow` `a` join `wf_powermodel` `b`) join `port_emp` `c`) join `port_deptempstation` `d`) where ((`a`.`FK_Flow` = `b`.`FlowNo`) and (`b`.`PowerCtrlType` = 0) and (`c`.`No` = `d`.`FK_Emp`) and (`b`.`StaNo` = `d`.`FK_Station`) and (`a`.`WFState` >= 2)) ;

-- ----------------------------
-- View structure for v_totalch
-- ----------------------------
DROP VIEW IF EXISTS `v_totalch`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_totalch` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`)) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ANQI`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp` ;

-- ----------------------------
-- View structure for v_totalchweek
-- ----------------------------
DROP VIEW IF EXISTS `v_totalchweek`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_totalchweek` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,`wf_ch`.`WeekNum` AS `WeekNum`,`wf_ch`.`FK_NY` AS `FK_NY`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `AnQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`WeekNum` = `wf_ch`.`WeekNum`)))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp`,`wf_ch`.`WeekNum`,`wf_ch`.`FK_NY` ;

-- ----------------------------
-- View structure for v_totalchyf
-- ----------------------------
DROP VIEW IF EXISTS `v_totalchyf`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_totalchyf` AS select `wf_ch`.`FK_Emp` AS `FK_Emp`,`wf_ch`.`FK_NY` AS `FK_NY`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `AllNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `ASNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` >= 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `CSNum`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 0) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `JiShi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `AnQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 2) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `YuQi`,(select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` = 3) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) AS `ChaoQi`,round((((select cast(count(`a`.`MyPK`) as decimal(10,0)) AS `Num` from `wf_ch` `a` where ((`a`.`CHSta` <= 1) and (`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`))) / (select count(`a`.`MyPK`) AS `Num` from `wf_ch` `a` where ((`a`.`FK_Emp` = `wf_ch`.`FK_Emp`) and (`a`.`FK_NY` = `wf_ch`.`FK_NY`)))) * 100),2) AS `WCRate` from `wf_ch` group by `wf_ch`.`FK_Emp`,`wf_ch`.`FK_NY` ;

-- ----------------------------
-- View structure for v_wf_authtodolist
-- ----------------------------
DROP VIEW IF EXISTS `v_wf_authtodolist`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_wf_authtodolist` AS select `b`.`FK_Emp` AS `Auther`,`b`.`FK_EmpText` AS `AuthName`,`a`.`PWorkID` AS `PWorkID`,`a`.`FK_Node` AS `FK_Node`,`a`.`FID` AS `FID`,`a`.`WorkID` AS `WorkID`,`c`.`EmpNo` AS `EmpNo`,`c`.`TakeBackDT` AS `TakeBackDT`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`Title` AS `Title` from ((`wf_generworkflow` `a` join `wf_generworkerlist` `b`) join `wf_auth` `c`) where ((`a`.`WorkID` = `b`.`WorkID`) and (`c`.`AuthType` = 1) and (`b`.`FK_Emp` = `c`.`Auther`) and (`b`.`IsPass` = 0) and (`b`.`IsEnable` = 1) and (`a`.`FK_Node` = `b`.`FK_Node`) and (`a`.`WFState` >= 2)) union select `b`.`FK_Emp` AS `Auther`,`b`.`FK_EmpText` AS `AuthName`,`a`.`PWorkID` AS `PWorkID`,`a`.`FK_Node` AS `FK_Node`,`a`.`FID` AS `FID`,`a`.`WorkID` AS `WorkID`,`c`.`EmpNo` AS `EmpNo`,`c`.`TakeBackDT` AS `TakeBackDT`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`Title` AS `Title` from ((`wf_generworkflow` `a` join `wf_generworkerlist` `b`) join `wf_auth` `c`) where ((`a`.`WorkID` = `b`.`WorkID`) and (`c`.`AuthType` = 2) and (`b`.`FK_Emp` = `c`.`Auther`) and (`b`.`IsPass` = 0) and (`b`.`IsEnable` = 1) and (`a`.`FK_Node` = `b`.`FK_Node`) and (`a`.`WFState` >= 2) and (`a`.`FK_Flow` = `c`.`FlowNo`)) ;

-- ----------------------------
-- View structure for wf_empworks
-- ----------------------------
DROP VIEW IF EXISTS `wf_empworks`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `wf_empworks` AS select `a`.`PRI` AS `PRI`,`a`.`WorkID` AS `WorkID`,`b`.`IsRead` AS `IsRead`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,`a`.`WFState` AS `WFState`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`PWorkID` AS `PWorkID`,`a`.`PFlowNo` AS `PFlowNo`,`b`.`FK_Node` AS `FK_Node`,`b`.`FK_NodeText` AS `NodeName`,`b`.`FK_Dept` AS `WorkerDept`,`a`.`Title` AS `Title`,`a`.`RDT` AS `RDT`,`b`.`RDT` AS `ADT`,`b`.`SDT` AS `SDT`,`b`.`FK_Emp` AS `FK_Emp`,`b`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`SDTOfNode` AS `SDTOfNode`,`b`.`PressTimes` AS `PressTimes`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,`a`.`TodoSta` AS `TodoSta`,`a`.`TaskSta` AS `TaskSta`,0 AS `ListType`,`a`.`Sender` AS `Sender`,`a`.`AtPara` AS `AtPara`,1 AS `MyNum` from (`wf_generworkflow` `a` join `wf_generworkerlist` `b`) where ((`b`.`IsEnable` = 1) and (`b`.`IsPass` = 0) and (`a`.`WorkID` = `b`.`WorkID`) and (`a`.`FK_Node` = `b`.`FK_Node`) and (`a`.`WFState` <> 0) and (`b`.`WhoExeIt` <> 1)) union select `a`.`PRI` AS `PRI`,`a`.`WorkID` AS `WorkID`,`b`.`Sta` AS `IsRead`,`a`.`Starter` AS `Starter`,`a`.`StarterName` AS `StarterName`,2 AS `WFState`,`a`.`FK_Dept` AS `FK_Dept`,`a`.`DeptName` AS `DeptName`,`a`.`FK_Flow` AS `FK_Flow`,`a`.`FlowName` AS `FlowName`,`a`.`PWorkID` AS `PWorkID`,`a`.`PFlowNo` AS `PFlowNo`,`b`.`FK_Node` AS `FK_Node`,`b`.`NodeName` AS `NodeName`,`b`.`CCToDept` AS `WorkerDept`,`a`.`Title` AS `Title`,`a`.`RDT` AS `RDT`,`b`.`RDT` AS `ADT`,`b`.`RDT` AS `SDT`,`b`.`CCTo` AS `FK_Emp`,`b`.`FID` AS `FID`,`a`.`FK_FlowSort` AS `FK_FlowSort`,`a`.`SysType` AS `SysType`,`a`.`SDTOfNode` AS `SDTOfNode`,0 AS `PressTimes`,`a`.`GuestNo` AS `GuestNo`,`a`.`GuestName` AS `GuestName`,`a`.`BillNo` AS `BillNo`,`a`.`FlowNote` AS `FlowNote`,`a`.`TodoEmps` AS `TodoEmps`,`a`.`TodoEmpsNum` AS `TodoEmpsNum`,0 AS `TodoSta`,0 AS `TaskSta`,1 AS `ListType`,`b`.`Rec` AS `Sender`,('@IsCC=1' or `a`.`AtPara`) AS `AtPara`,1 AS `MyNum` from (`wf_generworkflow` `a` join `wf_cclist` `b`) where ((`a`.`WorkID` = `b`.`WorkID`) and (`b`.`Sta` <= 1) and (`b`.`InEmpWorks` = 1) and (`a`.`WFState` <> 0)) ;

-- ----------------------------
-- View structure for v_wf_delay
-- ----------------------------
DROP VIEW IF EXISTS `v_wf_delay`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_wf_delay` AS select concat(`wf_empworks`.`WorkID`,'_',`wf_empworks`.`FK_Emp`,'_',`wf_empworks`.`FK_Node`) AS `MyPK`,`wf_empworks`.`PRI` AS `PRI`,`wf_empworks`.`WorkID` AS `WorkID`,`wf_empworks`.`IsRead` AS `IsRead`,`wf_empworks`.`Starter` AS `Starter`,`wf_empworks`.`StarterName` AS `StarterName`,`wf_empworks`.`WFState` AS `WFState`,`wf_empworks`.`FK_Dept` AS `FK_Dept`,`wf_empworks`.`DeptName` AS `DeptName`,`wf_empworks`.`FK_Flow` AS `FK_Flow`,`wf_empworks`.`FlowName` AS `FlowName`,`wf_empworks`.`PWorkID` AS `PWorkID`,`wf_empworks`.`PFlowNo` AS `PFlowNo`,`wf_empworks`.`FK_Node` AS `FK_Node`,`wf_empworks`.`NodeName` AS `NodeName`,`wf_empworks`.`WorkerDept` AS `WorkerDept`,`wf_empworks`.`Title` AS `Title`,`wf_empworks`.`RDT` AS `RDT`,`wf_empworks`.`ADT` AS `ADT`,`wf_empworks`.`SDT` AS `SDT`,`wf_empworks`.`FK_Emp` AS `FK_Emp`,`wf_empworks`.`FID` AS `FID`,`wf_empworks`.`FK_FlowSort` AS `FK_FlowSort`,`wf_empworks`.`SysType` AS `SysType`,`wf_empworks`.`SDTOfNode` AS `SDTOfNode`,`wf_empworks`.`PressTimes` AS `PressTimes`,`wf_empworks`.`GuestNo` AS `GuestNo`,`wf_empworks`.`GuestName` AS `GuestName`,`wf_empworks`.`BillNo` AS `BillNo`,`wf_empworks`.`FlowNote` AS `FlowNote`,`wf_empworks`.`TodoEmps` AS `TodoEmps`,`wf_empworks`.`TodoEmpsNum` AS `TodoEmpsNum`,`wf_empworks`.`TodoSta` AS `TodoSta`,`wf_empworks`.`TaskSta` AS `TaskSta`,`wf_empworks`.`ListType` AS `ListType`,`wf_empworks`.`Sender` AS `Sender`,`wf_empworks`.`AtPara` AS `AtPara`,`wf_empworks`.`MyNum` AS `MyNum` from `wf_empworks` where (`wf_empworks`.`SDT` > now()) ;
