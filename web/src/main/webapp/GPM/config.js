﻿/**
 * 说明：
 * 1. 该配置文件用于配置 组织组织结构维护.
 * 2. 使用实体类进行管理维护.
 * */

/* 新建部门部门编号模式. 0 = 手工输入自己编号.  1=自动生成.  由实体类的beforeInser 生成.  */
var NewDeptNoModel = 0;

//部门类名.
var deptEnsName = "BP.GPM.Depts";
var deptEnName = "BP.GPM.Dept";

/*  新建人员编号模式. 0 = 手工输入自己编号.  1 = 自动生成，由实体类的beforeInser 生成. */
var NewEmpNoModel = 0;

//操作员类名.
var empEnsName = "BP.GPM.Emps";
var empEnName = "BP.GPM.Emp";




