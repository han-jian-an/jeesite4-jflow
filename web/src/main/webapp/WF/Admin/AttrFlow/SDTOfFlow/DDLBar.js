﻿
function InitBar(optionKey) {

    var html = "<b>流程计划时间计算</b>:";

    html += "<select id='changBar' onchange='changeOption()'>";

    html += "<option value=" + SDTOfFlow.None + ">&nbsp;&nbsp;&nbsp;&nbsp;不使用</option>";
    html += "<option value=" + SDTOfFlow.NodeFrmDT + ">&nbsp;&nbsp;&nbsp;&nbsp;按照节点表单的日期计算（NodeFrmDT）</option>";
    html += "<option value=" + SDTOfFlow.SQLDT + ">&nbsp;&nbsp;&nbsp;&nbsp;按照sql计算</option>";
    html += "<option value=" + SDTOfFlow.NodeSumDT + ">&nbsp;&nbsp;&nbsp;&nbsp;按照所有节点的时间之和计算NodeSumDT</option>";
    html += "<option value=" + SDTOfFlow.DaysDT + ">&nbsp;&nbsp;&nbsp;&nbsp;按照规定的天数计算</option>";
    html += "<option value=" + SDTOfFlow.TimeDT + ">&nbsp;&nbsp;&nbsp;&nbsp;按照时间规则计算</option>";
    html += "<option value=" + SDTOfFlow.ChildFlowDT + ">&nbsp;&nbsp;&nbsp;&nbsp;为子流程时的规则</option>";
    html += "<option value=" + SDTOfFlow.AttrNonredundant + ">&nbsp;&nbsp;&nbsp;&nbsp;按照发起字段不能重复规则</option>";

    html += "</select >";

    html += "<input  id='Btn_Save' type=button onclick='Save()' value='保存' />";
    //html += "<input  id='Btn_Help' type=button onclick='Adv()' value='高级设置' />";
    html += "<input  id='Btn_Help' type=button onclick='HelpOnline()' value='在线帮助' />";


    document.getElementById("bar").innerHTML = html;
    $("#changBar option[value='" + optionKey + "']").attr("selected", "selected");
}

function Adv()
{
    var url = "Adv.htm?FK_Flow=" + GetQueryString("FK_Flow");
    OpenEasyUiDialogExt(url, '高级设置', 600, 400, false);
}

function HelpOnline() {
    var url = "http://ccbpm.mydoc.io";
    window.open(url);
}

function changeOption() {

    var flowNo = GetQueryString("FK_Flow");
    if (flowNo == null)
        flowNo = '001';

    var obj = document.getElementById("changBar");
    var sele = obj.options;
    var index = obj.selectedIndex;
    var optionKey = optionKey = sele[index].value;
    var url = GetUrl(optionKey);

    window.location.href = url + "?FK_Flow=" + flowNo;
}

function GetUrl(optionKey) {

    switch (parseInt(optionKey)) {
        case SDTOfFlow.None:
            url = "0.None.htm";
            break;
        case SDTOfFlow.NodeFrmDT:
            url = "1.NodeFrmDT.htm";
            break;
        case SDTOfFlow.SQLDT:
            url = "2.SQLDT.htm";
            break;
        case SDTOfFlow.NodeSumDT:
            url = "3.NodeSumDT.htm";
            break;
        case SDTOfFlow.DaysDT:
            url = "4.DaysDT.htm";
            break;
        case SDTOfFlow.TimeDT:
            url = "5.TimeDT.htm";
            break;
        case SDTOfFlow.ChildFlowDT:
            url = "6.ChildFlowDT.htm";
            break;
        case SDTOfFlow.AttrNonredundant:
            url = "7.AttrNonredundant.htm";
            break;
        default:
            url = "0.None.htm";
            break;
    }

    return url;
}

function CheckFlow(flowNo) {
    var flow = new Entity('BP.WF.Flow', flowNo);
    flow.DoMethodReturnString("DoCheck"); //重置密码:不带参数的方法. 
}

function SaveAndClose() {

    Save();
    window.close();
}

//打开窗体.
function OpenEasyUiDialogExt(url, title, w, h, isReload) {
    OpenEasyUiDialog(url, "eudlgframe", title, w, h, "icon-property", true, null, null, null, function () {
        if (isReload == true) {
            window.location.href = window.location.href;
        }
    });
}

$(function () {

    jQuery.getScript(basePath + "/WF/Admin/Admin.js")
        .done(function () {
            /* 耶，没有问题，这里可以干点什么 */
            // alert('ok');
        })
        .fail(function () {
            /* 靠，马上执行挽救操作 */
            //alert('err');
        });
});