﻿$(function () {

    jQuery.getScript(basePath + "/WF/Admin/Admin.js")
        .done(function () {
            /* 耶，没有问题，这里可以干点什么 */
            //alert('ok');
        })
        .fail(function () {
            /* 靠，马上执行挽救操作 */
            //alert('err');
        });
});


var optionKey = 0;
function InitBar(key) {

    optionKey = key;

    var webUser = new WebUser();
    var nodeID = GetQueryString("FK_Node");
    var str = nodeID.substr(nodeID.length - 2);
    var isSatrtNode = false;
    if (str == "01")
        isSatrtNode = true;

    // var html = "<div style='background-color:Silver' > 请选择访问规则: ";
    var html = "<div style='padding:5px' >考核规则: ";

    html += "<select id='changBar' onchange='changeOption()'>";

    html += "<option value=" + CHWay.None + ">&nbsp;&nbsp;&nbsp;&nbsp;不考核</option>";
    html += "<option value=" + CHWay.ByTime + " >&nbsp;&nbsp;&nbsp;&nbsp;按照时效考核</option>";
    html += "<option value=" + CHWay.WorkLoad + " >&nbsp;&nbsp;&nbsp;&nbsp;按工作量考核</option>";
    html += "<option value=" + CHWay.IsQuality + " >&nbsp;&nbsp;&nbsp;&nbsp;是否是考核质量点</option>";
    html += "</select >";

    html += "<input  id='Btn_Save' type=button onclick='Save()' value='保存' />";
    //html += "<input  id='Btn_Back' type=button onclick='Back()' value='返回' />";
    //html += "<input type=button onclick='AdvSetting()' value='高级设置' />";
    //   html += "<input type=button onclick='Help()' value='帮助' />";
    html += "</div>";

    document.getElementById("bar").innerHTML = html;

    $("#changBar option[value='" + optionKey + "']").attr("selected", "selected");


}
function Back() {
    url = "../AccepterRole/Default.htm?FK_Node=" + GetQueryString("FK_Node") + "&FK_Flow=" + GetQueryString("FK_Flow");
    window.location.href = url;
}

function Help() {

    var url = "";
    switch (optionKey) {
        case SelectorModel.Station:
            url = 'http://bbs.ccflow.org/showtopic-131376.aspx';
            break;
        case SelectorModel.Dept:
            url = 'http://bbs.ccflow.org/showtopic-131376.aspx';
            break;
        default:
            url = "http://ccbpm.mydoc.io/?v=5404&t=17906";
            break;
    }

    window.open(url);
}

function GenerUrlByOptionKey(optionKey) {
    var roleName = "";
    switch (parseInt(optionKey)) {
        case CHWay.None:
            roleName = "0.None.htm";
            break;
        case CHWay.ByTime:
            roleName = "1.ByTime.htm";
            break;
        case CHWay.ByWorkNum:
            roleName = "2.ByWorkNum.htm";
            break;
        case CHWay.IsQuality:
            roleName = "3.IsQuality.htm";
            break;
        default:
            roleName = "0.None.htm";
            break;
    }
    return roleName;
}


function changeOption() {
    var nodeID = GetQueryString("FK_Node");
    var obj = document.getElementById("changBar");
    var sele = obj.options;
    var index = obj.selectedIndex;
    var optionKey = 0;
    if (index > 0) {
        optionKey = sele[index].value
    }
    var roleName = GenerUrlByOptionKey(optionKey);
    window.location.href = roleName + "?FK_Node=" + nodeID + "&FK_Flow=" + GetQueryString("FK_Flow");
}
function SaveAndClose() {
    Save();
    window.close();
}

//打开窗体.
function OpenEasyUiDialogExt(url, title, w, h, isReload) {

    OpenEasyUiDialog(url, "eudlgframe", title, w, h, "icon-property", true, null, null, null, function () {
        if (isReload == true) {
            window.location.href = window.location.href;
        }
    });
}

//高级设置.
function AdvSetting() {

    var nodeID = GetQueryString("FK_Node");
    var url = "AdvSetting.htm?FK_Node=" + nodeID + "&M=" + Math.random();
    OpenEasyUiDialogExt(url, "高级设置", 600, 500, false);
}