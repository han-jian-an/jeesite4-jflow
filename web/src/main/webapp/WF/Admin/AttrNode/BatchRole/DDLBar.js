﻿
function InitBar(optionKey) {

    var html = "批处理模式:";
    html += "<select id='changBar' onchange='changeOption()'>";

    //html += "<option value=null  disabled='disabled'>+批处理规则</option>";
    html += "<option value=" + BatchRole.None + " >&nbsp;&nbsp;&nbsp;&nbsp;不处理</option>";
    html += "<option value=" + BatchRole.WorkCheckModel + " >&nbsp;&nbsp;&nbsp;&nbsp;审核组件模式 </option>";
    html += "<option value=" + BatchRole.Group + " >&nbsp;&nbsp;&nbsp;&nbsp;审核字段分组模式</option>";
    html += "</select >";

    html += "<input  id='Btn_Save' type=button onclick='Save()' value='保存' />";

    //   html += "<input  id='Btn_SaveAndClose' type=button onclick='SaveAndClose()' value='保存并关闭' />";
    //  html += "<input type=button onclick='OldVer()' value='使用旧版本' />";
    //  html += "<input  id='Btn_Help' type=button onclick='Help()' value='视频帮助' />";
    // html += "<input id='Btn_Advanced' type=button onclick='AdvSetting()' value='高级设置' />";

    document.getElementById("bar").innerHTML = html;
    $("#changBar option[value='" + optionKey + "']").attr("selected", "selected");
}


function HelpOnline() {
    var url = "http://ccbpm.mydoc.io";
    window.open(url);
}
function changeOption() {
    var nodeID = GetQueryString("FK_Node");
    var obj = document.getElementById("changBar");
    var sele = obj.options;
    var index = obj.selectedIndex;
    var optionKey = optionKey = sele[index].value;

    var url = GetUrl(optionKey);
    window.location.href = url + "?FK_Node=" + nodeID +"&FK_Flow=" + GetQueryString("FK_Flow");;
}

function GetUrl(optionKey) {
    var url = "";
    switch (parseInt(optionKey)) {
        case BatchRole.None:
            url = "0.None.htm";
            break;
        case BatchRole.WorkCheckModel:
            url = "1.WorkCheck.htm";
            break;
        case BatchRole.Group:
            url = "2.GroupFieldCheck.htm";

    }
    return url;
}